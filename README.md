# Sympozer Backend

## Installation guide

### For Windows

* Clone code to your  "project-folder"

* Download and install Nodejs at: https://nodejs.org/en/download/

* Install nodemon globally (to run server in realtime)
```
npm install -g nodemon
```

* Install dependencies of project
```
cd project-folder
npm install
```

+ Download and install MongoDB at: https://www.mongodb.org/downloads#production

	* Add **MongoDB/bin** to your environment variable PATH

	* Create database storing location for Mongodb
	```
	cd E:\MongoDBStore
        ```
        ```
	mkdir data
	```
        ```
        cd data
	```
        ```
        mkdir db
	```

+ Run application

	* Run Mongod for database server: 
	```
	mongod --dbpath "E:\MongoDBStore"
	```
	(leave this console open)

	* Open the second console to create database:
	```
	mongo
	```
	, then:
	```
	use Sympozer
	```
	(this console can now be closed)

	* Open another console and run Nodemon for application server
        ```
	cd project-folder
        ```
        ``` 	
        nodemon app.js localhost 27017 http://localhost:3000
        ```

	* Application should be now availble at http://127.0.0.1:3000

### For Linux

Not that different...

### Utils

* [Robomongo](https://robomongo.org/): a user interface application to access to database Mongodb.

## Server management

The server is built using Docker containers : 

- One reverse nginx proxy
Back : 
- One MongoDB database (sympozer-back-mongo)
- One application (sympozer-back-node)
External : 
- Two applications (login, external => sympozer-ext-node)
- One MongoDB database  (sympozer-ext-mongo)

Configuration files are located here : `/usr/local/etc/`

All code repositories are here : `/usr/local/src/`

All the commands to build up the whole infrastructure are located in : `/usr/local/bin/`

There is no dockerfile.

## Renewal of https certs

We installed cert-bot which enabled us to create ssl certificates.

Install procedure : https://certbot.eff.org/

Then you have to run this command every 3 months : 

`certbot renew --renew-hook /path/to/renew-hook-script`

Why do you need to do it every 3 months : 

We still don't have any Dockerfile to build all the containers from the ground up, they are still managed by bash files in `/usr/local/bin/`.

An improvement would be creating dockerfiles for each container, installing the cert-bot and then running the renewal inside using cron.


## About Sympozer-express

Application backend - express version of Sympozer project - TER M1 2015-1016 Université Lyon 1

[Development version](http://dev.sympozer.com/)