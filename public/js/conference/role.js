$(document).ready(function() {

    var listUserHolder = $('#list-user-holder')

    // load list people that don't belong to conference
    $('#collapse').on('show.bs.collapse', function(){
        var self = $(this)
        var confId = self.data('conf-id')

        $.ajax({
                url: "roles/get-list-avaiable-person",
                method: "POST",
                data: { 'confId' : confId },
                dataType: "html"
        }) // end ajax
        .done(function(response){
            listUserHolder.html(response)
        }) // end done
        .fail(function(response){
            listUserHolder.html(response)
            console.log(response)
        }) // end fail


    }) // end .on show.bs.collapse


    /*****************/
    $('#close-button').on('click', function(event){
        event.preventDefault()

        $('#collapse').collapse('hide')

    }) // end close-button .onclick


    /*****************/
    $('#add-button').on('click', function(event){
        event.preventDefault()

        var listAddedUser = []
        listUserHolder.find('input[type=checkbox]').each(function(){
            var self = $(this)
            if(self.is(':checked')){
                listAddedUser.push({
                                    '_id'    : self.data('person-id'),
                                    'acronym': self.data('person-acronym'),
                                    'sha'    : self.data('sha') 
                                    })
            } // end if

        })  // end .each


        if(listAddedUser.length <= 0) return

        $.ajax({
                url: "roles/add-person-to-conf",
                method: "POST",
                data: { 'listPerson' : listAddedUser },
                dataType: "JSON"
        }) // end ajax
        .done(function(response){
            if(response.state == 'success'){
                location.reload()
            }
            else{
                showErrorNoty(response)
            }
        }) // end done

    }) // end add-button .onclick


    $('.list-role-anchor').each(function() {
        var self = $(this)
        var readonly = !self.data('is-owner')

        self.dynamicInput({
            'class'    : 'form-control',
            'placeholder': 'Chair/ author/ ...',
            'readonly'  : readonly
        }) // end dynmaicInput
    }) // end list-role-anchor

    // $('.list-publication-anchor').each(function() {
    //     var self = $(this)
    //     var readonly = !self.data('is-owner')

    //     self.dynamicInput({
    //         'class'    : 'form-control',
    //         'placeholder': 'Publication uri in dataset',
    //         'readonly'  : readonly
    //     }) // end dynmaicInput
    // }) // end list-publication-anchor



    $('.save-person-button').on('click', function(event){

        event.preventDefault()
        var self = $(this)
        var roleAnchor = $('#list-role-anchor-'+self.data('index'))
        // var publicationAnchor = $('#list-publication-anchor-'+self.data('index'))

        var listRole = []
        roleAnchor.find('input').each(function(index, el){

            var text = $(el).val()
            if( typeof(text) !== 'undefined' && text !== null && text !== '')
                listRole.push( text )

        }) // end each

        // var listPublication = []
        // publicationAnchor.find('input').each(function(index, el){

        //     var text = $(el).val()
        //     if( typeof(text) !== 'undefined' && text !== null && text !== '')
        //         listPublication.push( text )

        // }) // end each
        
        $.ajax({
            url: "roles/edit-person-in-conference",
            method: "POST",
            data: {
                    'listRole': listRole,
                    // 'listPublication': listPublication,
                    'person': {
                        'id': self.data('person-id'),
                        'acronym': self.data('person-acronym'),
                        'lastname': self.data('person-lastname'),
                        'firstname': self.data('person-firstname')
                    }
                  },
            dataType: "JSON"
        }) // end ajax
        .done(function(response){
            showResponseNoty(response)
        }) // end done        


    }) // end save person button click


    var sendRemovePersonSignal = function(params){

        $.ajax({
            url: "roles/remove-person-from-conf",
            method: "POST",
            data: params,
            dataType: "JSON"
        }) // end ajax
        .done(function(response){
            showResponseNoty(response)
        }) // end done


    } // end sendRemovePersonSignal



}) // end document ready