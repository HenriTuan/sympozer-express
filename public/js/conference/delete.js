$(document).ready(function() {

	$('.delete-conf-btn').click(function(event) {

		// get confAcronym
		var self = $(this);
		var confId = self.data('conf-id').replace(/"/g, '');
		console.log(confId);
		
		$.allowPersist = true
    	//show "do you want to undo" box
    	$.undoBox = noty({
    		layout: 'topRight',
    		type: 'success',
    		text: 'Your conference is being deleted ...',
    		timeout: '1200',
    		theme: 'relax',

    		animation: {
		        open: 'animated slideInUp', // Animate.css class names
		        close: 'animated slideOutUp', // Animate.css class names
			}, // end animation

    		callback:{

    			onClose: function(){
    				sendDeleteSignalToServer(confId)
    			}, // end onClose

    		}, // end callback
    		
    		buttons:[
    			{ // UNDO button
					addClass: 'btn btn-warning', 
					text: 'Undo', 
					onClick: undoButtonClick
				}
			], // end buttons array

    	}) // end undoBox

	}) // end #delete-button click	
	
	
}) // end document ready

function undoButtonClick(){
	$.allowPersist = false
	// hide undoBox, show removedBox
	$.undoBox.close()
	$.removedBox = noty({
		text: 'Your conference has been restored !', 
		layout: 'topRight',
		type: 'success',
		theme: 'relax',
		timeout: '1200',

		animation: {
	        open: 'animated slideInUp', // Animate.css class names
	        close: 'animated slideOutUp', // Animate.css class names
		}, // end animation
	}) // end removedBox


} // end undoButtonClick


function sendDeleteSignalToServer(confId){
	/* allowPersist == true: delete conference
   			 		== false: don't delete conference
	*/

	if($.allowPersist){
		$.post('/conference/delete/persist', {'id': confId}, function(response) {
		  	showResponseNoty(response)
	   }) // end get('persist')

	}else{
		window.stop()
	} // end if $.allowPersist	

}// end sendDeleteSignalToServer