/**
 * Created by Mahmoud on 05/04/2017.
 */
$(document).ready(function() {

    var eventForm = $("#eventCategory-form")



    eventForm.submit(function(event){

        event.preventDefault()
        var formData =
            eventForm.serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {})

        eventForm.validate();
        if (eventForm.valid()) {

            showUndoBox(
                'Your event category is being created ...',
                sendPersistSignalToServer,
                formData,
                'Your event category has not been created.'
            )

        } // end formValidator.validate

    }) // end #create-button click


    $('#addEventCategory').click(function(event){
        event.preventDefault()

    })

    $('#eventCatPersistBtn').click(function (event){
        event.preventDefault()
        eventForm.submit()
    })

    function sendPersistSignalToServer(formData){



        $.ajax({
            url: "/events/category/persist",
            type: "POST",
            data: {
                formData :{
                    name : $('#nameEventCat').val()
                }
            },
            dataType: 'json'
        })
            .done(function( data ) {
                showResponseNoty(data)
            }) // end .done



    }// end sendCreateSignalToServer

    function saveEventCat() {

        console.log($('#nameEventCat').val())
        $.ajax({
            url: '/events/category/persist',
            method: 'POST',
            data: {
                formData :{
                    name : $('#nameEventCat').val()
                }
            },
            success: function () {
                console.log('Event category had been created !')
                $('#category').append("<option value="+$('#nameEventCat').val()+">"+$('#nameEventCat').val()+"</option>")
            },
            error: function () {
                console.log('error !')
            }
        });
    }





})