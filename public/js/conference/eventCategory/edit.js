$(document).ready(function() {

    var eventForm = $("#event-form")

    eventForm.submit(function(event){
        event.preventDefault()

        var formData =
            eventForm.serializeArray().reduce(function(obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {})

        eventForm.validate();
        if (eventForm.valid()) {
            showUndoBox(
                'Your event category is being saved ...',
                sendSaveSignalToServer,
                formData,
                'Your event category has not been changed.'
            )

        } // end formValidator.validate

    }) // end form submit



}) // end document ready



function sendSaveSignalToServer(formData){
    var confAcronym = $('#conf-info').data('acronym')

    $.ajax({
        url: "save",
        type: "POST",
        data: {'formData': formData, 'confAcronym': confAcronym},
        dataType: 'json'
    })
        .done(function( data ) {
            showResponseNoty(data)
        }) // end .done


}// end sendSaveSignalToServer