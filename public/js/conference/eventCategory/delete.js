$(document).ready(function() {

    $('.delete-eventCat-btn').click(function(event) {
        event.preventDefault()

        // get eventSlug
        var self = $(this)
        var eventName = self.data('eventcat-name')
        console.log(eventName)
        showUndoBox(
            'Your event category is being deleted ...',
            sendDeleteSignalToServer,
            eventName,
            'Your event category has been deleted.'
        )


    }) // end .delete-button click


}) // end document ready



function sendDeleteSignalToServer(eventCatName){

    $.post('/events/category/delete/persist',
        {'eventCatName': eventCatName}, function(response) {
            showResponseNoty(response)
        })



}// end sendDeleteSignalToServer