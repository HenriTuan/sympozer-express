$(document).ready(function () {

  $('.delete-orga-btn').click(function (event) {
    event.preventDefault()

    // get orgaSlug
    var self = $(this)
    var orgaId = self.data('orga-id').replace(/"/g, '');
    const confId = self.data('conf-id').replace(/"/g, '');

    showUndoBox(
      'Your organization is being deleted ...',
      sendDeleteSignalToServer,
      {
        orgaId: orgaId,
        confId: confId,
      },
      'Your organization has been restored.'
    )


  }) // end .delete-button click


}) // end document ready


function sendDeleteSignalToServer(params) {

  $.post('/conference/' + params.confId + '/organization/delete/' + params.orgaId,
    null, function (response) {
      showResponseNoty(response)
    })


}// end sendDeleteSignalToServer