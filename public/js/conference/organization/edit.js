$(document).ready(function() {

	var organizationForm = $("#organization-form")
    var logoUploadForm = $("#logo-upload")
    var logo 	  = $("#logo")

    var logoUrlUploadForm = $("#logo-url-form")
    var logoUrlInput  = $("#logo-url")
    var logoUrlUpload = $("#logo-url-upload")
    var logoUrlRemove = $("#logo-url-remove")
    var logoUrlLoad   = $("#logo-url-load")

	organizationForm.submit(function(event){
		event.preventDefault()

		var formData = 
			organizationForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {})

		organizationForm.validate();
		if (organizationForm.valid()) {
			showUndoBox(
	                        'Your organization is being saved ...', 
	                        sendSaveSignalToServer, 
	                        formData,
	                        'Your organization has not been changed.'
                   		)

      	} // end formValidator.validate

	}) // end form submit

	
	/** logo upload handle **/

	$("#logo").fileinput({

	    overwriteInitial: true,
	    maxFileSize: 5000,
	    showClose: false,
	    showCaption: false,
   	    showUpload: false,
	    browseLabel: '',
	    removeLabel: 'Cancel',
	    removeClass: 'btn btn-warning',
	    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
	    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
	    removeTitle: 'Cancel or reset changes',
	    elErrorContainer: '#kv-avatar-errors',
	    msgErrorClass: 'alert alert-block alert-danger',
	    allowedFileExtensions: ["jpg", "png", "jpeg"]

	}); // end .fileinput


	logoUrlInput.on('input', function(event) {

  		if($.trim(logoUrlInput.val()) == ''){
  			$("#logo").fileinput("enable");
  		}else{
  			$("#logo").fileinput("disable");
  		}

	}) // end in input


	logoUrlLoad.on('click', function(event){
		event.preventDefault()
		var logoUrl = $.trim(logoUrlInput.val())
  		if( logoUrl !== ''){
  			var logoImg = $("#logo-upload img")
  			logoImg.attr("src", logoUrl)
  		}

	}) // end logoUrlLoad onclick


	logoUrlRemove.on('click', function(event){
		event.preventDefault()

		logoUrlInput.val('')
		$("#logo").fileinput("enable");
		var defaultLogUrl = logo.data("default-logo-url")
		 $("#logo-upload img").attr("src", defaultLogUrl)
	}) // end logoUrlRemove onclick


	/*** end logo url input handler **/

	
}) // end document ready



function sendSaveSignalToServer(formData){
	var confId = $('#conf-info').data('id');

	if( $('#logo-upload img').attr('src') == $('#logo').data('default-img-src') ){ // if logo src not changed
		formData.logo_path = $('#logo').data('default-img-src')
		$.ajax({
      		url: "save",
      		type: "POST",
      		data: {'formData': formData, 'confAcronym': confId},
      		dataType: 'json'
    	})
    	.done(function( data ) {
        	showResponseNoty(data)
 		}) // end .done
	}else{

		if($.trim($("#logo-url").val()) !== ''){
			// if there is a logoUrl, don't upload image in logoForm, pass logoUrl in submit confForm
			formData.logo_path = $.trim($("#logo-url").val())
			$.ajax({
          		url: "save",
          		type: "POST",
          		data: {'formData': formData, 'confAcronym': confId},
          		dataType: 'json'
        	})
        	.done(function( data ) {
            	showResponseNoty(data)
     		}) // end .done

		}else{

			// if logoUrl is empty, upload image in logoForm, then submit confForm
			var logoFormData = new FormData(document.getElementById("logo-upload"))

			$.ajax({
		          url: "/file-handle/image/upload-image",
		          type: "POST",
		          data: logoFormData,
		          processData: false,  // tell jQuery not to process the data
		          contentType: false   // tell jQuery not to set contentType
		        })
				.done(function( data ) {
		            formData.logo_path = data.imgUrl
					$.ajax({
	              		url: "save",
	              		type: "POST",
	              		data: {'formData': formData, 'confAcronym': confId},
	              		dataType: 'json'
	            	})
	            	.done(function( data ) {
	                	showResponseNoty(data)
	         		}) // end .done
				}) // end .done		
		} // end if $.trim(logoUrlInput.val()) !== ''
	} // end if logo not changed


}// end sendSaveSignalToServer