$(document).ready(function() {

	var orgaForm = $("#organization-form")

    var logoUploadForm = $("#logo-upload")
    var logo 	  = $("#logo")

    var logoUrlUploadForm = $("#logo-url-form")
    var logoUrlInput  = $("#logo-url")
    var logoUrlRemove = $("#logo-url-remove")
    var logoUrlLoad   = $("#logo-url-load")


	orgaForm.submit(function(event){
		event.preventDefault()
		var formData = 
			orgaForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {})

        orgaForm.validate();
		if (orgaForm.valid()) {

			showUndoBox(
	                        'Your organization is being created ...', 
	                        sendPersistSignalToServer, 
	                        formData,
	                        'Your organization has not been created.'
                   		)

      	} // end formValidator.validate

	}) // end #create-button click


	/** file input config **/
	$("#logo").fileinput({

	    overwriteInitial: true,
	    maxFileSize: 5000,
	    showClose: false,
	    showCaption: false,
	    browseLabel: '',
	    removeLabel: 'Cancel',
	    removeClass: 'btn btn-warning',
	    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
	    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
	   	showUpload: false,
	    elErrorContainer: '#kv-logotar-errors',
	    msgErrorClass: 'alert alert-block alert-danger',
	    allowedFileExtensions: ["jpg", "png", "jpeg"]

	})
	/* end .fileinput */


	/** upload logo url handler **/
	logoUrlInput.on('input', function(event) {

  		if($.trim(logoUrlInput.val()) == ''){
  			$("#logo").fileinput("enable");
  		}else{
  			$("#logo").fileinput("disable");
  		}

	}) // end in input


	logoUrlLoad.on('click', function(event){
		event.preventDefault()
		var logoUrl = $.trim(logoUrlInput.val())
  		if( logoUrl !== ''){
  			var logoImg = $("#logo-upload img")
  			logoImg.attr("src", logoUrl)
  		}

	}) // end logoUrlLoad onclick


	logoUrlRemove.on('click', function(event){
		event.preventDefault()

		logoUrlInput.val('')
		$("#logo").fileinput("enable")
		var defaultAvaUrl = logo.data("default-logo-url")
		$("#logo-upload img").attr({
			src: defaultAvaUrl,
			alt: 'Avatar'
		})
	}) // end logoUrlRemove onclick
	/** end upload logo url handler **/


	/*** end dynamic input init ***/



}) // end document ready




function sendPersistSignalToServer(formData){
	var confAcronym = $('#conf-info').data('acronym') 

	if($.trim($("#logo-url").val()) !== ''){
		// if there is a logoUrl, don't upload image in logoForm, pass logoUrl in submit orgaForm
		var logoUrl = $.trim($("#logo-url").val())
		formData.logo_path = logoUrl

		$.ajax({
      		url: "persist",
      		type: "POST",
      		data: {'formData': formData, 'confAcronym': confAcronym},
      		dataType: 'json'
    	})
    	.done(function( data ) {
        	showResponseNoty(data)
 		}) // end .done

	}else{

		// if logoUrl is empty, upload image in logoForm, then submit orgaForm
		var logoFormData = new FormData(document.getElementById("logo-upload"))

		$.ajax({
	          url: "/file-handle/image/upload-image",
	          type: "POST",
	          data: logoFormData,
	          processData: false,  // tell jQuery not to process the data
	          contentType: false   // tell jQuery not to set contentType
	        })
			.done(function( data ) {

	            formData.logo_path = data.imgUrl;
				$.ajax({
              		url: "persist",
              		type: "POST",
              		data: {'formData': formData, 'confAcronym': confAcronym},
              		dataType: 'json'
            	})
            	.done(function( data ) {
                	showResponseNoty(data)
         		}) // end .done
         		
			}) // end .done		
	} // end if $.trim(logoUrlInput.val()) !== ''


}// end sendCreateSignalToServer