$(document).ready(function () {

	var listUserHolder = $('#list-user-holder')

	// load list all people
	$('#collapse').on('show.bs.collapse', function () {
		var self = $(this)
		var orgaId = self.data('orga-id')

		$.ajax({
			url: "members/get-list-avaiable-member",
			method: "POST",
			data: {
				"orgaId": orgaId
			},
			dataType: "html"
		}) // end ajax
			.done(function (response) {
				listUserHolder.html(response)
				if (response == '<link rel="stylesheet" href="/css/component/list-user.css"/><div role="alert" style="margin-bottom: 5px" class="alert alert-danger"><span aria-hidden="true" class="glyphicon glyphicon-exclamation-sign"></span>  There is no person avaiable.</div>') {
					$('.add-button').remove();
				}
			}) // end done
			.fail(function (response) {
				listUserHolder.html(response)
				console.log(response)
			}) // end fail


	}) // end .on show.bs.collapse


	/*****************/
	$('.close-button').on('click', function (event) {
		event.preventDefault()
		$('#collapse').collapse('hide')
	}) // end close-button .onclick


	/*****************/
	$('.add-button').on('click', function (event) {
		event.preventDefault()

		var listAddedUser = []
		listUserHolder.find('input[type=checkbox]').each(function () {

			var self = $(this)
			if (self.is(':checked')) {
				listAddedUser.push(self.data('person-id'))
			}

		})  // end .each

		if (listAddedUser.length <= 0) return

		var self = $(this)
		var orgaId = self.data('orga-id')

		showUndoBox(
			'Your member is being added ...',
			sendAddPersonSignal,
			{
				'orgaId': orgaId,
				'listPersonsIds': listAddedUser
			},
			'Your member has not been added.'
		)

	}) // end add-button .onclick

	var sendAddPersonSignal = function (params) {

		$.ajax({
			url: "members/add-member-to-orga",
			method: "POST",
			data: params,
			dataType: "JSON"
		}) // end ajax
			.done(function (response) {

				showResponseNoty(response);

			}) // end done

	}

	$('.remove-member-button').on('click', function (event) {

		event.preventDefault()
		var self = $(this)

		var orgaId = self.data('orga-id')
		var personId = self.data('person-id')

		showUndoBox(
			'Your member is being removed ...',
			sendRemovePersonSignal,
			{
				'orgaId': orgaId,
				'personId': personId
			},
			'Your member has not been removed.'
		)

	}) // end delete member on click


	var sendRemovePersonSignal = function (params) {

		$.ajax({
			url: "members/remove-member-from-orga",
			method: "POST",
			data: params,
			dataType: "JSON"
		}) // end ajax
			.done(function (response) {
				showResponseNoty(response);
			}) // end done


	} // end sendRemovePersonSignal



}) // end document ready