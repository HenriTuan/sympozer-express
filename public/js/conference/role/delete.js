$(document).ready(function() {

	$('.delete-role-btn').on('click', function(event){

	    event.preventDefault();
	    const self = $(this);
	    const roleId = self.data('role-id').replace(/"/g, '');

	    showUndoBox(
	                    'Your role is being removed ...', 
	                    sendDeleteSignalToServer, 
	                	roleId,
	                    'Your role has not been removed.'
	                )

	}) // end delete role on click

})




function sendDeleteSignalToServer(roleId){

	const confId = $('#conf-info').data('id').replace(/"/g, '');
	$.post('/conference/'+confId+'/role/delete/' + roleId, null, function(response) {
    	showResponseNoty(response)
   }) // end get('persist')

}// end sendDeleteSignalToServer
