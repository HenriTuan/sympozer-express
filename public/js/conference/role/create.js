$(document).ready(function() {

	var roleForm = $("#role-form")


	roleForm.submit(function(event){
		event.preventDefault()

		var formData = 
			roleForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {})

		roleForm.validate();
		if (roleForm.valid()) {

			showUndoBox(
	                        'Your role is being created ...', 
	                        sendPersistSignalToServer, 
	                        formData,
	                        'Your role has not been created.'
                   		)


      	} // end formValidator.validate

	}) // end #create-button click


}) // end document ready




function sendPersistSignalToServer(formData){

	var confAcronym = $('#conf-info').data('acronym') 
	var confId = $('#conf-info').data('id')

	console.log(formData)	
	$.ajax({
  		url: "persist",
  		type: "POST",
  		data: {'formData': formData, 'confAcronym': confAcronym},
  		dataType: 'json'
	})
	.done(function( data ) {
    	showResponseNoty(data)
	}) // end .done

	

}// end sendCreateSignalToServer