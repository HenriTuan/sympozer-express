$(document).ready(function() {

	var roleForm = $("#role-form")


	roleForm.submit(function(event){
		event.preventDefault()

		var formData = 
			roleForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {})

		roleForm.validate();
  		if (roleForm.valid()) {

			showUndoBox(
	                        'Your role is being saved ...',
	                        sendSaveSignalToServer,
	                        formData,
	                        'Your role has not been changed.'
                   		)


      	} // end formValidator.validate

	}) // end form submit
	
}) // end document ready




function sendSaveSignalToServer(formData){
	$.ajax({
  		url: "save",
  		type: "POST",
  		data: {'formData': formData},
  		dataType: 'json'
	})
	.done(function( data ) {
    	showResponseNoty(data)
	}) // end .done

}// end sendSaveSignalToServer