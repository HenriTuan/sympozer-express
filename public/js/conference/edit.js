$(document).ready(function () {

    var conferenceForm = $("#conference-form");
    var logoUploadForm = $("#logo-upload");
    var confLogo = $("#conf-logo");

    var logoUrlUploadForm = $("#logo-url-form");
    var logoUrlInput = $("#logo-url");
    var logoUrlUpload = $("#logo-url-upload");
    var logoUrlRemove = $("#logo-url-remove");
    var logoUrlLoad = $("#logo-url-load");

    $(document).on("click", ".fileinput-remove", function (e) {
        e.preventDefault();
        removeLogo();
    });

    conferenceForm.submit(function (event) {
        event.preventDefault()

        var formData =
            conferenceForm.serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {})


        conferenceForm.validate({
            rules: {
                uri: 'url',
                //acronym: 'validAcronym'
            }
        });
        if (conferenceForm.valid()) {

            $.allowPersist = true
            //show "do you want to undo" box
            $.undoBox = noty({
                layout: 'topRight',
                type: 'success',
                text: 'Your conference is saving ...',
                timeout: '1200',
                theme: 'relax',

                animation: {
                    open: 'animated slideInUp', // Animate.css class names
                    close: 'animated slideOutUp', // Animate.css class names
                }, // end animation

                callback: {

                    onClose: function () {
                        sendSaveSignalToServer(formData)
                    }, // end onClose

                }, // end callback

                buttons: [
                    { // UNDO button
                        addClass: 'btn btn-warning',
                        text: 'Undo',
                        onClick: undoButtonClick
                    }
                ], // end buttons array

            }) // end undoBox

        } // end formValidator.validate

    }) // end form submit


    /** logo upload handle **/

    $("#conf-logo").fileinput({

        overwriteInitial: true,
        maxFileSize: 5000,
        showClose: false,
        showCaption: false,
        showUpload: false,
        browseLabel: '',
        removeLabel: 'Cancel',
        removeClass: 'btn btn-warning',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-avatar-errors',
        msgErrorClass: 'alert alert-block alert-danger',
        allowedFileExtensions: ["jpg", "png", "jpeg"]

    }); // end .fileinput


    logoUrlInput.on('input', function (event) {

        if ($.trim(logoUrlInput.val()) == '') {
            $("#conf-logo").fileinput("enable");
        } else {
            $("#conf-logo").fileinput("disable");
        }

    }) // end in input


    logoUrlLoad.on('click', function (event) {
        event.preventDefault()
        var logoUrl = $.trim(logoUrlInput.val())
        if (logoUrl !== '') {
            var logoImg = $("#logo-upload img")
            logoImg.attr("src", logoUrl)
        }

    }) // end logoUrlLoad onclick


    logoUrlRemove.on('click', function (event) {
        event.preventDefault();

        removeLogo();
    });// end logoUrlRemove onclick


    /*** end logo url input handler **/

    function removeLogo() {
        logoUrlInput.val('');
        $("#conf-logo").fileinput("enable");
        //const defaultLogUrl = confLogo.data("default-logo-url");
        $("#logo-upload img").attr("src", '');
        $(".file-default-preview img").attr("src", '');
    }

});// end document ready


function undoButtonClick() {
    $.allowPersist = false
    // hide undoBox, show removedBox
    $.undoBox.close()
    $.removedBox = noty({
        text: 'Your conference has not been changed.',
        layout: 'topRight',
        type: 'error',
        theme: 'relax',
        timeout: '1200',

        animation: {
            open: 'animated slideInUp', // Animate.css class names
            close: 'animated slideOutUp', // Animate.css class names
        }, // end animation
    }) // end removedBox


} // end undoButtonClick


function sendSaveSignalToServer(formData) {
    /* allowPersist == true: create new conference
                == false: don't create new conference
    */

    if ($.allowPersist) {

        if ($('#logo-upload img').attr('src') == $('#conf-logo').data('default-img-src')) { // if logo src not changed

            $.ajax({
                url: "save",
                type: "POST",
                data: {'formData': formData, 'logoUrl': null},
                dataType: 'json'
            })
                .done(function (data) {
                    showResponseNoty(data)
                }) // end .done
        } else {

            if ($.trim($("#logo-url").val()) !== '') {
                // if there is a logoUrl, don't upload image in logoForm, pass logoUrl in submit confForm
                var logoUrl = $.trim($("#logo-url").val())
                $.ajax({
                    url: "save",
                    type: "POST",
                    data: {'formData': formData, 'logoUrl': logoUrl},
                    dataType: 'json'
                })
                    .done(function (data) {
                        showResponseNoty(data)
                    }) // end .done

            } else {

                // if logoUrl is empty, upload image in logoForm, then submit confForm
                var logoFormData = new FormData(document.getElementById("logo-upload"))

                $.ajax({
                    url: "/file-handle/image/upload-image",
                    type: "POST",
                    data: logoFormData,
                    processData: false,  // tell jQuery not to process the data
                    contentType: false   // tell jQuery not to set contentType
                })
                    .done(function (data) {
                        var logoUrl = data.imgUrl
                        $.ajax({
                            url: "save",
                            type: "POST",
                            data: {'formData': formData, 'logoUrl': logoUrl},
                            dataType: 'json'
                        })
                            .done(function (data) {
                                showResponseNoty(data)
                            }) // end .done
                    }) // end .done
            } // end if $.trim(logoUrlInput.val()) !== ''
        } // end if logo not changed
    } else {
        window.stop()
    } // end if $.allowPersist

}// end sendSaveSignalToServer