$(document).ready(function() {

	var personForm = $("#person-form")

    var avaUploadForm = $("#ava-upload")
    var ava 	  	  = $("#ava")

    var avaUrlUploadForm = $("#ava-url-form")
    var avaUrlInput  = $("#ava-url")
    var avaUrlRemove = $("#ava-url-remove")
    var avaUrlLoad   = $("#ava-url-load")

    var socialAccountAnchor = $("#social-account-anchor")


	personForm.submit(function(event){
		event.preventDefault()
		var formData = 
			personForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {})

		formData.social_account = []

		socialAccountAnchor.find('input').each(function(index, el) {
			var text = $(el).val()
			if( typeof(text) !== 'undefined' && text !== null && text !== '')
				formData.social_account.push( text )
		})

        let vals = $('.role-input:checked').map(function(_, el) {
            return $(el).attr('id');
        }).get();

        formData.idRoles = vals;


        personForm.validate();
		if (personForm.valid()) {

			showUndoBox(
	                        'Your person is being created ...', 
	                        sendPersistSignalToServer, 
	                        formData,
	                        'Your person has not been created.'
                   		)


      	} // end formValidator.validate

	}) // end #create-button click


	/** file input config **/
	$("#ava").fileinput({

	    overwriteInitial: true,
	    maxFileSize: 5000,
	    showClose: false,
	    showCaption: false,
	    browseLabel: '',
	    removeLabel: 'Cancel',
	    removeClass: 'btn btn-warning',
	    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
	    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
	   	showUpload: false,
	    elErrorContainer: '#kv-avatar-errors',
	    msgErrorClass: 'alert alert-block alert-danger',
	    allowedFileExtensions: ["jpg", "png", "jpeg"]

	})
	/* end .fileinput */


	/** upload ava url handler **/
	avaUrlInput.on('input', function(event) {

  		if($.trim(avaUrlInput.val()) == ''){
  			$("#ava").fileinput("enable");
  		}else{
  			$("#ava").fileinput("disable");
  		}

	}) // end in input


	avaUrlLoad.on('click', function(event){
		event.preventDefault()
		var avaUrl = $.trim(avaUrlInput.val())
  		if( avaUrl !== ''){
  			var avaImg = $("#ava-upload img")
  			avaImg.attr("src", avaUrl)
  		}

	}) // end avaUrlLoad onclick


	avaUrlRemove.on('click', function(event){
		event.preventDefault()

		avaUrlInput.val('')
		$("#ava").fileinput("enable");
		var defaultAvaUrl = ava.data("default-ava-url")
		$("#ava-upload img").attr({
			src: defaultAvaUrl,
			alt: 'Avatar'
		});
	}) // end avaUrlRemove onclick
	/** end upload ava url handler **/


	/*** dynamic input init ****/
	socialAccountAnchor.dynamicInput({
			'class'	   : 'form-control',
			'placeholder': 'https://www.facebook.com/person',
			'label'		: 'Social accounts'
	})

	/*** end dynamic input init ***/



}) // end document ready




function sendPersistSignalToServer(formData){

	var confAcronym = $('#conf-info').data('acronym') 
	var confId = $('#conf-info').data('id')

	if($.trim($("#ava-url").val()) !== ''){
		// if there is a avaUrl, don't upload image in avaForm, pass avaUrl in submit personForm
		var avaUrl = $.trim($("#ava-url").val())
		formData.ava_path = avaUrl

		$.ajax({
      		url: "persist",
      		type: "POST",
      		data: {'formData': formData, 'confAcronym': confAcronym},
      		dataType: 'json'
    	})
    	.done(function( data ) {
        	showResponseNoty(data)
 		}) // end .done

	}else{

		// if avaUrl is empty, upload image in avaForm, then submit personForm
		var avaFormData = new FormData(document.getElementById("ava-upload"))
		
		$.ajax({
	          url: "/file-handle/image/upload-image",
	          type: "POST",
	          data: avaFormData,
	          processData: false,  // tell jQuery not to process the data
	          contentType: false   // tell jQuery not to set contentType
	        })
			.done(function( data ) {
	            formData.ava_path = data.imgUrl
				$.ajax({
              		url: "persist",
              		type: "POST",
              		data: {'formData': formData, 'confAcronym': confAcronym},
              		dataType: 'json'
            	})
            	.done(function( data ) {
                	showResponseNoty(data)
         		}) // end .done
			}) // end .done		
	} // end if $.trim(avaUrlInput.val()) !== ''

}// end sendCreateSignalToServer