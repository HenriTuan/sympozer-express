$(document).ready(function() {

	var personForm = $("#person-form")
    var avaUploadForm = $("#ava-upload")
    var ava 	  = $("#ava")

    var avaUrlUploadForm = $("#ava-url-form")
    var avaUrlInput  = $("#ava-url")
    var avaUrlUpload = $("#ava-url-upload")
    var avaUrlRemove = $("#ava-url-remove")
    var avaUrlLoad   = $("#ava-url-load")

    var socialAccountAnchor = $("#social-account-anchor")

    $(document).on("click", ".fileinput-remove", function (e) {
        e.preventDefault();
        removeLogo();
    });

	personForm.submit(function(event){
		event.preventDefault()

		var formData = 
			personForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {})

		formData.social_account = [];

		socialAccountAnchor.find('input').each(function(index, el) {
			var text = $(el).val();
			if( typeof(text) !== 'undefined' && text !== null && text !== '')
				formData.social_account.push( text )
		})

        let vals = $('.role-input:checked').map(function(_, el) {
            return $(el).attr('id');
        }).get();

		formData.idRoles = vals;

		personForm.validate();
  		if (personForm.valid()) {

			showUndoBox(
	                        'Your person is being saved ...', 
	                        sendSaveSignalToServer, 
	                        formData,
	                        'Your person has not been changed.'
                   		)


      	} // end formValidator.validate

	}) // end form submit

	
	/** ava upload handle **/

	$("#ava").fileinput({

	    overwriteInitial: true,
	    maxFileSize: 5000,
	    showClose: false,
	    showCaption: false,
   	    showUpload: false,
	    browseLabel: '',
	    removeLabel: 'Cancel',
	    removeClass: 'btn btn-warning',
	    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
	    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
	    removeTitle: 'Cancel or reset changes',
	    elErrorContainer: '#kv-avatar-errors',
	    msgErrorClass: 'alert alert-block alert-danger',
	    allowedFileExtensions: ["jpg", "png", "jpeg"]

	}); // end .fileinput


	avaUrlInput.on('input', function(event) {

  		if($.trim(avaUrlInput.val()) == ''){
  			$("#ava").fileinput("enable");
  		}else{
  			$("#ava").fileinput("disable");
  		}

	}) // end in input


	avaUrlLoad.on('click', function(event){
		event.preventDefault()
		var avaUrl = $.trim(avaUrlInput.val())
  		if( avaUrl !== ''){
  			var avaImg = $("#ava-upload img")
  			avaImg.attr("src", avaUrl)
  		}

	}) // end avaUrlLoad onclick


	avaUrlRemove.on('click', function(event){
		event.preventDefault()

        removeLogo();
	});// end avaUrlRemove onclick


	/*** end ava url input handler **/

	function removeLogo(){
        avaUrlInput.val('');
        $("#ava").fileinput("enable");
        $("#ava-upload img").attr("src", '');
        $(".file-default-preview img").attr("src", '');
	}


	/*** dynamic input init ****/
	socialAccountAnchor.dynamicInput({
			'class'	   : 'form-control',
			'placeholder': 'https://www.facebook.com/person',
			'label'		: 'Social accounts'
	})

	
}) // end document ready




function sendSaveSignalToServer(formData){
	var confAcronym = $('#conf-info').data('acronym')

		if( $('#ava-upload img').attr('src') == $('#ava').data('default-img-src') ){ // if ava src not changed
			formData.ava_path = $('#ava').data('default-img-src')
			$.ajax({
          		url: "save",
          		type: "POST",
          		data: {'formData': formData, 'confAcronym': confAcronym},
          		dataType: 'json'
        	})
        	.done(function( data ) {
            	showResponseNoty(data)
     		}) // end .done
		}else{
	
			if($.trim($("#ava-url").val()) !== ''){
				// if there is a avaUrl, don't upload image in avaForm, pass avaUrl in submit personForm
				formData.ava_path = $.trim($("#ava-url").val())
				$.ajax({
	          		url: "save",
	          		type: "POST",
	          		data: {'formData': formData, 'confAcronym': confAcronym},
	          		dataType: 'json'
	        	})
	        	.done(function( data ) {
	            	showResponseNoty(data)
	     		}) // end .done

			}else{

				// if avaUrl is empty, upload image in avaForm, then submit personForm
				var logoFormData = new FormData(document.getElementById("ava-upload"))

				$.ajax({
			          url: "/file-handle/image/upload-image",
			          type: "POST",
			          data: logoFormData,
			          processData: false,  // tell jQuery not to process the data
			          contentType: false   // tell jQuery not to set contentType
			        })
					.done(function( data ) {
			            formData.ava_path = data.imgUrl
						$.ajax({
		              		url: "save",
		              		type: "POST",
		              		data: {'formData': formData, 'confAcronym': confAcronym},
		              		dataType: 'json'
		            	})
		            	.done(function( data ) {
		                	showResponseNoty(data)
		         		}) // end .done
					}) // end .done		
			} // end if $.trim(avaUrlInput.val()) !== ''
		} // end if ava not changed


}// end sendSaveSignalToServer