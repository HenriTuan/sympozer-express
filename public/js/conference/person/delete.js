$(document).ready(function () {

  $('.delete-person-btn').on('click', function (event) {

    event.preventDefault()
    var self = $(this)
    const idPersonConference = self.data('person-id').replace(/"/g, '');
    showUndoBox(
      'Your person is being removed ...',
      sendDeleteSignalToServer,
      idPersonConference,
      'Your person has not been removed.'
    )

  }) // end delete person on click

})


function sendDeleteSignalToServer(idPersonConference) {

  var confId = $('#conf-info').data('id').replace(/"/g, '');
  $.post('/conference/' + confId + '/person/delete/' + idPersonConference, null, function (response) {
    showResponseNoty(response)
  }) // end get('persist')

}// end sendDeleteSignalToServer
