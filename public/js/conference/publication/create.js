$(document).ready(function () {

    const pubForm = $("#publication-form");
    const idConf = $('#conf-info').data('id').replace(/"/g, '');
    let events = [];

    const group = $("ol.authors-list").sortable({
        group: 'serialization',
        delay: 500,
        onDrop: function ($item, container, _super) {
            const data = group.sortable("serialize").get();
            selectedAuthors = data[0].map((el) => el.id);
            _super($item, container);
        }
    });

    let selectedAuthors = [];

    $('#authors-input').autocomplete({
        source: function (req, res) {
            const confId = $('#conf-info').data("id");
            $.get({
                url: '/conference/' + confId.replace(/^"?(.+?)"?$/, '$1') + "/person/rest",
                data: {
                    'id_min': -1,
                    'term': req.term,
                    'page_size': 20
                },
                accept: "application/json",
                success: function (data) {
                    result = [];
                    data.forEach((el) => {
                        if (!selectedAuthors.includes(el._id)) {
                            result.push({label: el.displayname, value: el._id})
                        }
                    });
                    res(result);
                },
                error: function (err) {
                    res("")
                }
            });
        },
        minLength: 0,
        delay: 500,
        select: function (event, ui) {
            event.preventDefault();
            $(event.target).val("");
            selectedAuthors.push(ui.item.value);
            $('.authors-list').append(`
				<li class="list-group-item" data-id="${ui.item.value}">
					<span>${ui.item.label}</span>
					<button class="remove-author btn"><span class="glyphicon glyphicon-remove"></span></button>
				</li>
			`)
        }
    });

    $('.authors-list').on('click', '.remove-author', function (event) {
        event.preventDefault();
        const target = $(event.target);
        const id = target.parent().data("id");
        selectedAuthors.splice(selectedAuthors.indexOf(id), 1);
        target.parent().remove();
    });

    const getEvents = function (idConf, term) {
        return $.get({
            url: '/conference/' + idConf + "/event/find",
            data: {
                'term': term,
                'page_size': 20
            },
            accept: "application/json",
        });
    };

    $('#events-input-add').autocomplete({
        source: function (req, res) {
            let promise = getEvents(idConf, req.term);
            promise.done((data) => {
                if(data && data.events){
                    let result = [];
                    data.events.forEach((el) => {
                        result.push({label: el.name, value: el._id})
                    });
                    res(result);
                }
            })
                .fail((err) => res(""))
        },
        minLength: 0,
        delay: 500,
        select: function (e, ui) {
            e.preventDefault();
            $(e.target).val("");

            const find = events.find((event) => {
                return event.id === ui.item.value;
            });

            if (!find) {
                events.push({
                    name: ui.item.label,
                    id: ui.item.value,
                });

                paintEvents($("#addTableEvents"));
            }
        }
    });

    $(document).on("click", ".btnRemoveEvent", function () {
        const id = $(this).data("id");
        if (!id) {
            return false;
        }

        events = events.filter((event) => {
            const idEvent = event.id || event._id;

            return idEvent !== id;
        });

        paintEvents($("#addTableEvents"));
    });

    function paintEvents(eventsTable) {
        //On vide la table
        eventsTable.empty();

        if (events.length > 0) {
            //On ajoute l'entête
            eventsTable.append("<thead><tr><th>Id</th><th>Name</th><th></th></tr></thead>");

            //On ajoute le contenu
            let tbody = "<tbody>";

            events.forEach((event) => {
                const name = event.name;
                const id = event.id || event._id;
                tbody += "<tr><td>" + name + "</td><td><button class='btn btn-danger btnRemoveEvent' data-id='" + id + "'>Remove</button></td></tr>"
            });

            tbody += "</tbody>";

            eventsTable.html(tbody);
        }
    }

    pubForm.submit(function (event) {

        event.preventDefault();
        const formData =
            pubForm.serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

        formData.idAuthors = selectedAuthors;
        formData.keywords = $('#keywords-input').val().split(",");
        formData.keywords = formData.keywords[0] == "" ? [] : formData.keywords;
        formData.idsEvent = events.map((event) => event.id);

        pubForm.validate();
        if (pubForm.valid()) {

            showUndoBox(
                'Your publication is being created ...',
                sendPersistSignalToServer,
                formData,
                'Your publication has not been created.'
            )

        } // end formValidator.validate

    }) // end #create-button click

});// end document ready


function sendPersistSignalToServer(formData) {
    $.ajax({
        url: "persist",
        type: "POST",
        data: {'formData': formData},
        dataType: 'json'
    })
        .done(function (data) {
            showResponseNoty(data)
        }); // end .done


}// end sendCreateSignalToServer