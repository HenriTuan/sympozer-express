$(document).ready(function () {

  $('.delete-pub-btn').click(function (event) {
    event.preventDefault()

    // get pubSlug
    const self = $(this);
    const pubId = self.data('pub-id').replace(/"/g, '');

    showUndoBox(
      'Your publication is being deleted ...',
      sendDeleteSignalToServer,
      pubId,
      'Your publication has been restored.'
    )


  }) // end .delete-button click


});// end document ready


function sendDeleteSignalToServer(pubId) {

  const confId = $('#conf-info').data('id').replace(/"/g, '');
  $.post('/conference/' + confId + '/publication/delete/' + pubId,
    null, function (response) {
      showResponseNoty(response)
    })


}// end sendDeleteSignalToServer