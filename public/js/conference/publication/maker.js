$(document).ready(function () {

    const listUserHolder = $('#list-user-holder');
    const getPersons = function (idConf, term) {
        return $.get({
            url: '/conference/' + idConf.replace(/^"?(.+?)"?$/, '$1') + "/person/rest",
            data: {
                'id_min': -1,
                'term': term,
                'page_size': 20
            },
            accept: "application/json",
        });
    };

    const sendAuthor = function (idPub, idAuthor) {
        return $.post(idPub + '/addauthor/' + idAuthor);
    };


    const idConf = $('#conf-info').data("id").replace(/['"]+/g, '');
    const idPub = $(".delete-pub-btn").data("pub-id").replace(/['"]+/g, '');
    const url = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
    const urlApi = url + "/conference/" + idConf;
    const rowRelatedEvents = $("#relatedEvents");
    let relatedEvents = [];
    let eventsSearched = [];

    let authors = [];
    $('.user-holder').each((i, el) => {
        authors.push($(el).data("person-id").replace(/['"]+/g, ''))
    });


    $('#authors-input').autocomplete({

      source: function (req, res) {
        var promise = getPersons(idConf, req.term);
        promise.done((data) => {
          result = [];
          data.forEach((el) => {
            if(!authors.includes(el._id)){
              result.push({label:el.displayname,value: el._id})
            }
          });
          res(result);
        })
          .fail((err) => res(""))
      },
      minLength: 0,
      delay: 500,
      select: function (event, ui) {
        event.preventDefault();
        $(event.target).val("");
        var promise = sendAuthor(idPub,ui.item.value);
        promise.done((data) => {
          console.log(authors);
          if(authors.length === 0){
            $('#authors-alert').toggle();
            $('#authors-panel').toggle();
          }
          authors.push(ui.item.value);
          $('.list-user-holder').append(data);
        })
          .fail((err) => console.log(err))
      }
    });


    // load list people that don't belong to conference
    $('#collapse').on('show.bs.collapse', function () {

        const confAcronym = $('#conf-info').data('acronym');

        $.ajax({
            url: "makers/get-list-avaiable-maker",
            method: "POST",
            data: {'confAcronym': confAcronym},
            dataType: "html"
        }) // end ajax
            .done(function (response) {
                listUserHolder.html(response);
            }) // end done
            .fail(function (response) {
                listUserHolder.html(response);
            }) // end fail


    });// end .on show.bs.collapse


    /*****************/
    $(document).on('click', '.close-button', function (event) {
        event.preventDefault();
        $('#collapse').collapse('hide');
    }); // end close-button .onclick


    /*****************/
    $('.add-button').on('click', function (event) {
        event.preventDefault();

        const listAddedUser = [];
        listUserHolder.find('input[type=checkbox]').each(function () {
            const self = $(this);
            if (self.is(':checked')) {
                listAddedUser.push(self.data('person-uri'))
            }

        }); // end .each

        if (listAddedUser.length <= 0) {
            return;
        }

        const self = $(this);
        const pubUri = self.data('pub-uri');
        const confAcronym = $('#conf-info').data('acronym');

        showInfoNoty('Adding author ...');

        $.ajax({
            url: "makers/add-maker-to-pub",
            method: "POST",
            data: {'pubUri': pubUri, 'listPersonUri': listAddedUser, 'confAcronym': confAcronym},
            dataType: "JSON"
        }) // end ajax
            .done(function (response) {

                if (response.state == 'success') {
                    location.reload()
                }
                else {
                    showErrorNoty(response)
                }
            }) // end done

    }); // end add-button .onclick


    $('.list-user-holder').on('click', '.remove-maker-button', function (event) {

        event.preventDefault();
        const self = $(this);

        const pubId = self.data('pub-id').replace(/['"]+/g, '');
        const personId = self.data('person-id').replace(/['"]+/g, '');

        $.ajax({
            url: pubId + '/removeauthor/' + personId,
            type: 'DELETE',
            success: function () {
              authors.splice(authors.indexOf(personId),1);
              self.parent().parent().remove();
              console.log(authors);
              if(authors.length === 0){
                $('#authors-alert').toggle();
                $('#authors-panel').toggle();
              }
            },
            error: null,
        });
    });


    const sendRemovePersonSignal = function (params) {

        $.ajax({
            url: "makers/remove-maker-from-pub",
            method: "POST",
            data: params,
            dataType: "JSON"
        }) // end ajax
            .done(function (response) {
                showResponseNoty(response)
            }) // end done


    };// end sendRemovePersonSignal

    $(document).on("click", ".btn-edit", (e) => {
        let target = $(e.target);
        let row = target.closest('tr');
        let field = row.find('.editable');
        target.find('span').toggleClass("glyphicon-pencil glyphicon-ok");
        if (field.is('span')) {
            let text = field.text();
            field.replaceWith(`<input class="editable" value="${text}">`);
        }
        else if (field.is('input')) {
            let text = field.val();
            let fieldname = row.data("attr-name");
            let formData = {};
            if(fieldname == "keywords"){
              formData[fieldname] = text.split(',').map(str => str.trim());
              console.log(formData[fieldname])
            }
            else{
              formData[fieldname] = text;
            }
            $.ajax({
                url: idPub + "/save",
                type: "POST",
                data: {'formData': formData},
                dataType: 'json'
            })
                .done(function (data) {
                    field.replaceWith(`<span class="editable">${text}</span>`);
                })
        }
    });

    //On charge les events associés à la publication
    $.get(urlApi + "/publication/" + idPub + "/events", (data) => {
        if (data && data.events && data.events.length > 0) {
            relatedEvents = data.events;
            paintRelatedEvents(relatedEvents);
        } else {
            showAlertNoEvents();
        }
    });

    $(document).on("click", ".btnRemoveRelatedEvent", function () {
        const id = $(this).data("id");
        if (!id) {
            return false;
        }

        $.ajax({
            url: urlApi + "/publication/" + idPub + "/events/" + id,
            type: 'DELETE',
            success: function () {
                relatedEvents = relatedEvents.filter((event) => {
                    return event._id !== id;
                });

                if (relatedEvents.length === 0) {
                    showAlertNoEvents();
                } else {
                    paintRelatedEvents(relatedEvents);
                    showTabEvents();
                }
            }
        });
    });

  $('.list-user-holder').sortable({
    delay: 100,
    stop: function (ev,ui) {
      console.log("bla");
      let newAuth = $(ev.target).children('.user-holder').map(function(){
        return JSON.parse($(this).data('person-id'));
      }).get();
      //.map((index,child) => newAuth.push(JSON.parse($(child).data('person-id'))));
      authors = newAuth;
      let formData = {};
      formData["idAuthors"] = authors;
      $.ajax({
        url: idPub + "/save",
        type: "POST",
        data: {'formData': formData},
        dataType: 'json'
      })
    }
  });

    function showAlertNoEvents() {
        rowRelatedEvents.find(".no-events").removeClass("hidden");
        rowRelatedEvents.find(".tab-events").addClass("hidden");
    }

    function showTabEvents() {
        rowRelatedEvents.find(".no-events").addClass("hidden");
        rowRelatedEvents.find(".tab-events").removeClass("hidden");
    }

    function paintRelatedEvents(events) {
        const tabEvents = rowRelatedEvents.find(".tab-events");

        if (tabEvents) {
            //On vide la tab
            tabEvents.empty();

            //On ajoute le tbody
            let tbody = "<tbody>";

            console.log(events);
            events.forEach((event) => {
                const name = event.name;
                const id = event._id;
                tbody += "<tr><td style='text-align: center;'>" + name + "</td>" +
                    "<td style='text-align: center;'><button class='btn btn-danger btnRemoveRelatedEvent' data-id='" + id + "'>Remove</button></td></tr>"
            });

            tbody += "</tbody>";

            //Création de la table
            const table = "<div class='panel panel-info'><div class='panel-heading'><h4 class='panel-title'>List events of publication</h4></div><table class='table table-striped' style='width: 50%' align='center'>" + tbody + "</table></div>";

            //On ajoute dans le dom
            tabEvents.html(table);
        }
    }

    const getEvents = function (idConf, term) {
        return $.get({
            url: '/conference/' + idConf + "/event/find",
            data: {
                'term': term,
                'page_size': 20
            },
            accept: "application/json",
        });
    };

    $('#events-input-add').autocomplete({
        source: function (req, res) {
            let promise = getEvents(idConf, req.term);
            promise.done((data) => {
                if (data && data.events) {
                    let result = [];
                    data.events.forEach((el) => {
                        result.push({label: el.name, value: el._id})
                    });
                    res(result);
                }
            })
                .fail((err) => res(""))
        },
        minLength: 0,
        delay: 500,
        select: function (e, ui) {
            e.preventDefault();
            $(e.target).val("");

            //On regarde si on a déjà l'event
            let find = false;
            if (relatedEvents.length > 0) {
                find = relatedEvents.find((event) => {
                    return event._id === ui.item.value;
                });
            }

            if (!find) {
                //On l'ajoute en bdd
                $.post('/conference/' + idConf + "/event/" + ui.item.value + "/publication/" + idPub,
                    null,
                    function (data, status) {
                        if (isSuccess(status)) {
                            //On l'ajoute au tableau local
                            relatedEvents.push({
                                name: ui.item.label,
                                _id: ui.item.value,
                            });

                            //On update le tableau
                            paintRelatedEvents(relatedEvents);
                            showTabEvents();
                        }
                    });
            }
        }
    });

    function isSuccess(status) {
        return status && status === "success";
    }
});