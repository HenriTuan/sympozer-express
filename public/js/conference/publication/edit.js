$(document).ready(function() {

	var publicationForm = $("#publication-form");

  var group = $("ol.authors-list").sortable({
    group: 'serialization',
    delay: 500,
    onDrop: function ($item, container, _super) {
      var data = group.sortable("serialize").get();
      selectedAuthors = data[0].map((el) => el.id);
      _super($item, container);
    }
  });

  let selectedAuthors = [];

  $('#authors-input').autocomplete({
    source: function (req, res) {
      var confId = $('#conf-info').data("id");
      $.get({
        url: '/conference/' + confId.replace(/^"?(.+?)"?$/,'$1') + "/person/rest",
        data: {
          'id_min': -1,
          'term': req.term,
          'page_size': 20
        },
        accept: "application/json",
        success: function (data) {
          result = [];
          data.forEach((el) => {
            if(!selectedAuthors.includes(el._id)){
              result.push({label:el.displayname,value: el._id})
            }
          });
          res(result);
        },
        error: function (err) {
          res("")
        }
      });
    },
    minLength: 0,
    delay: 500,
    select: function (event, ui) {
      event.preventDefault();
      $(event.target).val("");
      selectedAuthors.push(ui.item.value);
      $('.authors-list').append(`
				<li class="list-group-item" data-id="${ui.item.value}">
					<span>${ui.item.label}</span>
					<button class="remove-author"><span class="glyphicon glyphicon-remove"></span></button>
				</li>
			`)
    }
  });

  $('.authors-list').on('click','.remove-author',function (event) {
    event.preventDefault();
    const target = $(event.target);
    const id = target.parent().data("id");
    selectedAuthors.splice(selectedAuthors.indexOf(id),1);
    target.parent().remove();
  });

	publicationForm.submit(function(event){
		event.preventDefault();

		var formData = 
			publicationForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {});

		formData.idAuthors = selectedAuthors;
    formData.keywords = $('#keywords-input').val().split(",");

    console.log(formData);
    publicationForm.validate();
		if (publicationForm.valid()) {
			showUndoBox(
	                        'Your publication is being saved ...', 
	                        sendSaveSignalToServer, 
	                        formData,
	                        'Your publication has not been changed.'
                   		)

      	} // end formValidator.validate

	}) // end form submit


	
}) // end document ready



function sendSaveSignalToServer(formData){
	$.ajax({
  		url: "save",
  		type: "POST",
  		data: {'formData': formData},
  		dataType: 'json'
	})
	.done(function( data ) {
    	showResponseNoty(data)
	}) // end .done


}// end sendSaveSignalToServer