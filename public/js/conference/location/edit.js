$(document).ready(function() {
	const inputLatitude = $("#latitude");
    const inputLongitude = $("#longitude");
    let latitude, longitude, address;

    if(inputLatitude){
    	latitude = inputLatitude.val();
	}
	if(inputLongitude){
    	longitude = inputLongitude.val();
	}

	if(latitude && longitude){
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(latitude, longitude);
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                	address = results[1].formatted_address;
                	$("#geoplace").val(address);
                }
            }
        });
	}

    $("#geoplace").geocomplete().bind("geocode:result", function (event, result) {
        if(result && result.geometry && result.geometry.location){
            latitude = result.geometry.location.lat();
            longitude = result.geometry.location.lng();
            address = result.formatted_address;
        } else {
            latitude = null;
            longitude = null;
            address = null;
        }
    });

	var locationForm = $("#location-form")

    var commentAnchor = $("#comment-anchor")


	locationForm.submit(function(event){
		event.preventDefault();

        const name = $("#name").val();
        const nbPlace = $("#nbPlace").val();
        const description = $("#description").val();

        //On ajoute la liste des commentaires
        let list_comment = [];

        commentAnchor.find('input').each(function (index, el) {
            const text = $(el).val();
            if (typeof(text) !== 'undefined' && text !== null && text !== '') {
                list_comment.push(text);
            }
        });

        const location = {
            name: name,
            nb_place: nbPlace,
            equipements: list_comment,
            gps: [latitude, longitude],
            address: address,
            description: description,
        };

        showUndoBox(
            'Your location is being created ...',
            sendSaveSignalToServer,
            location,
            'Your location has not been created.'
        );

	});// end form submit



	/*** end ava url input handler **/


	/*** dynamic input init ****/
	commentAnchor.dynamicInput({
			'class'	   : 'form-control',
			'placeholder': 'There are 30 chairs left.',
			'label'		: 'Comments'
	})

	
});// end document ready




function sendSaveSignalToServer(formData){
	var confAcronym = $('#conf-info').data('acronym');

	$.ajax({
  		url: "save",
  		type: "POST",
  		data: {'formData': formData, 'confAcronym': confAcronym},
  		dataType: 'json'
	})
	.done(function( data ) {
    	showResponseNoty(data)
	}) // end .done



}// end sendSaveSignalToServer