$(document).ready(function () {

    let latitude, longitude, address;

    $("#geoplace").geocomplete().bind("geocode:result", function (event, result) {
        if(result && result.geometry && result.geometry.location){
            latitude = result.geometry.location.lat();
            longitude = result.geometry.location.lng();
            address = result.formatted_address;
        } else {
            latitude = null;
            longitude = null;
            address = null;
        }
    });

    const locationForm = $("#location-form");
    const commentAnchor = $("#comment-anchor");


    locationForm.submit(function (event) {
        event.preventDefault();

        const name = $("#name").val();
        const nbPlace = $("#nbPlace").val();
        const description = $("#description").val();

        //On ajoute la liste des commentaires
        let list_comment = [];

        commentAnchor.find('input').each(function (index, el) {
            const text = $(el).val();
            if (typeof(text) !== 'undefined' && text !== null && text !== '') {
                list_comment.push(text);
            }
        });

        const location = {
            name: name,
            nb_place: nbPlace,
            equipements: list_comment,
            gps: [latitude, longitude],
            address: address,
            description: description,
        };

        showUndoBox(
            'Your location is being created ...',
            sendPersistSignalToServer,
            location,
            'Your location has not been created.'
        );

    });// end #create-button click


    /*** dynamic input init ****/
    commentAnchor.dynamicInput({
        'class': 'form-control',
        'placeholder': 'There are 30 chairs left.',
        'label': 'Comments'
    })

    /*** end dynamic input init ***/


}) // end document ready


function sendPersistSignalToServer(formData) {

    const confAcronym = $('#conf-info').data('acronym');
    const confId = $('#conf-info').data('id');

    $.ajax({
        url: "persist",
        type: "POST",
        data: {'formData': formData, 'confAcronym': confAcronym},
        dataType: 'json'
    })
        .done(function (data) {
            showResponseNoty(data)
        }) // end .done


}// end sendCreateSignalToServer