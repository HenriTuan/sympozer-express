$(document).ready(function() {

	$(document).on("click", ".delete-location-btn", function(){
		const idLocation = $(this).data("id-location");
        const idConf = $(this).data("conf-id");
		if(!idLocation || !idConf){
			return false;
		}

        $.post('/conference/'+idConf+'/location/delete/' + idLocation, function(response) {
            showResponseNoty(response)
		});
	});
	
	
}) // end document ready



function sendDeleteSignalToServer(locationSlug){

	var confAcronym = $('#conf-info').data('acronym') 
	$.post('/conference/'+confAcronym+'/location/delete',
				{'locationSlug': locationSlug, 'confAcronym': confAcronym}, function(response) {
    	showResponseNoty(response)
   })



}// end sendDeleteSignalToServer