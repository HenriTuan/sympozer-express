$(document).ready(function() {

    // A corriger et utiliser
    function validUri(value,element) {
        console.log(value);
        var uriString = $.trim(value)
        var pattern =
            new RegExp('^(https?:\\/\\/)?'+ // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
                '(\\?[&a-z\\d%_.~+=-]*)?'+ // query string
                '(\\#[-a-z\\d_]*)?$','i') // fragment locator
        return pattern.test(uriString)
    }



    function validAcronym(value,element) {
        console.log(value);
        // check if string is null or whitespaced
        if(!value.length || /\s/.test(value)) return false
        // check if string contains special characters
        return ( !/[^a-zA-Z0-9]/.test(value))
    }

    $.validator.addMethod("validUri",validUri);
    $.validator.addMethod("validAcronym",validAcronym)
});

