$(document).ready(function() {

    var listSubEventHolder = $('#list-sub-event-holder')
    var listSuperEventHolder = $('#list-super-event-holder')

    // load list available sub event
    $('#collapse-sub-event').on('show.bs.collapse', function(){
        
        var self = $(this)
        var eventAcronym = self.data('event-acronym')
        var confAcronym = $('#conf-info').data('acronym')

        $.ajax({
                url: "sub-events/get-list-available-event",
                method: "POST",
                data: { 'eventAcronym' : eventAcronym, 'confAcronym': confAcronym },
                dataType: "html"
        }) // end ajax
        .done(function(response){
            listSubEventHolder.html(response)
        }) // end done
        .fail(function(response){
            listSubEventHolder.html(response)
            console.log(response)
        }) // end fail

    }) // end .on show.bs.collapse

    // load list available super event
    $('#collapse-super-event').on('show.bs.collapse', function(){

        var self = $(this)
        var eventAcronym = self.data('event-acronym')
        var confAcronym = $('#conf-info').data('acronym')

        $.ajax({
                url: "super-events/get-list-available-event",
                method: "POST",
                data: { 'eventAcronym' : eventAcronym, 'confAcronym': confAcronym },
                dataType: "html"
        }) // end ajax
        .done(function(response){
            listSuperEventHolder.html(response)
        }) // end done
        .fail(function(response){
            listSuperEventHolder.html(response)
            console.log(response)
        }) // end fail


    }) // end .on show.bs.collapse


    /*****************/
    $('.close-button').on('click', function(event){
        event.preventDefault()
        
        var self = $(this)
        if( self.data('event-type') == 'subEvent' ){
            $('#collapse-sub-event').collapse('hide')
        }
        if( self.data('event-type') == 'superEvent' ){
            $('#collapse-super-event').collapse('hide')
        }

    }) // end close-button .onclick


    /*****************/
    $('.add-button').on('click', function(event){
        event.preventDefault()

        var self = $(this)
        var eventType = self.data('event-type')
        var listAddedEvent = []

        var listEventHolder = null
        if(eventType == 'subEvent'){
            listEventHolder = listSubEventHolder
        }
        if(eventType == 'superEvent'){
            listEventHolder = listSuperEventHolder
        }

        listEventHolder.find('input[type=checkbox]').each(function(){
            var self = $(this)
            if(self.is(':checked')){
                listAddedEvent.push({'acronym': self.data('event-acronym'), uri: self.data('event-uri')})
            }
        })  // end .each

        if(listAddedEvent.length <= 0) return

        var eventAcronym = self.data('event-acronym')
        var params = {'eventAcronym': eventAcronym}
        var url = null

        if(eventType == 'subEvent'){
            showInfoNoty('Adding sub event ...')
            params.listSubEvent = listAddedEvent
            url = 'sub-events/add-sub-event'
        }
        if(eventType == 'superEvent'){
            showInfoNoty('Adding super event ...')
            params.listSuperEvent = listAddedEvent
            url = 'super-events/add-super-event'
        }        

        $.ajax({
                url: url,
                method: "POST",
                data: params,
                dataType: "JSON"
        }) // end ajax
        .done(function(response){

            if(response.state == 'success'){
               location.reload()
            }
            else{
                showErrorNoty(response)
            }
            
        }) // end done

    }) // end add-button .onclick



    // $('.remove-sub-button').on('click', function(event){

    //     event.preventDefault()
    //     var self = $(this)

    //     var eventAcronym = self.data('event-acronym')

    //     showUndoBox(
    //         'Your sub event is being removed ...', 
    //         sendRemoveEventSignal, 
    //         {
    //          'url': 'sub-events/remove-sub-event' 
    //          'eventAcronym': eventAcronym,
    //          'subEvent': {
    //                 'acronym': self.data('event-acronym'),
    //             }
    //         },
    //         'Your sub event has not been removed.'
    //     )

    // }) // end delete subEvent on click


    var sendRemoveEventSignal = function(params){

        $.ajax({
            url: params.url,
            method: "POST",
            data: params,
            dataType: "JSON"
        }) // end ajax
        .done(function(response){
            showResponseNoty(response)
        }) // end done

    } // end sendRemoveEventSignal



}) // end document ready