/**
 * Created by Mahmoud on 27/04/2017.
 */



$(document).ready(function () {



    //glob var for calendar select
    use_glob_var_date = false
    start_date = null
    end_date = null
    // end global var for calendar select


    confEvent = {
        title: confAcronym,
        slug: confAcronym,
    }


    if (typeof(superEvent) === 'undefined') {


        console.log('undefined')

        $("#navTab").append("<li role=presentation><img data-idcalendar="+confAcronym+" class='closeTab' src='/img/close.png' ><a href=#" + confAcronym + " aria-controls=settings role=tab data-toggle=tab >" + confAcronym + "</a></li>");
        $("#navTabContent").append("<div role=tabpanel class=tab-pane id=" + confAcronym + "><div id=" + confAcronym + "Calendar" + " > </div></div>")
        $("#navTab a[href=#" + confAcronym + "]").tab('show')
        newCalendar(confAcronym, confEvent)

    }
    else {
        console.log('defined' + superEvent.name)
        $("#navTab").append("<li role=presentation><img data-idcalendar="+superEvent.slug+" class='closeTab' src='/img/close.png' ><a href=#" + superEvent.slug + " aria-controls=settings role=tab data-toggle=tab >" + superEvent.name + "</a></li>");
        $("#navTabContent").append("<div role=tabpanel class=tab-pane id=" + superEvent.slug + "><div id=" + superEvent.slug + "Calendar" + " > </div></div>")
        $("#navTab a[href=#" + superEvent.slug + "]").tab('show')
        newCalendar(superEvent.slug, superEvent)

    }

    //append calendar elements

    //$("#navTab").append("<li role=presentation class=active><a href="+confAcronym+" aria-controls="+confAcronym+" role=tab data-toggle=tab>"+confAcronym+"</a></li>")
    //$("#navTabContent").append("<div id="+confAcronym+" role=tabpanel class="+"tab-pane active"+" ><div id="+confAcronym+"></div></div>")


    // Modal persist new event (saveBtnModal)
    $('#saveBtnModal').click(function (event) {
        event.preventDefault();


        //superEventSlug = $(".tab-pane.active").attr('id')

        // need test (name and summary)
        event = {
            title: $('#name').val(),
            summary: $('#summary').val(),
            category: $('#category').val(),
            date_start: null,
            date_end: null,
            time_start: $('#time-start').val(),
            time_end: $('#time-end').val()
        }




        persistEvent(confAcronym, event);
    });


// persist new event
    function persistEvent(confAcronym, event) {


        formData = {
            name: event.title,
            category: event.category,
            date_start: new Date(event.start),
            date_end: new Date(event.end),
            time_start: event.time_start,
            time_end: event.time_end,
        }


        if ($(".tab-pane.active").attr('id') != undefined) {
            console.log('icccciiiii')
            formData.super_event_slug = $(".tab-pane.active").attr('id')
        }
        console.log($(".tab-pane.active").attr('id'))


        if (use_glob_var_date) {
            formData.date_start = new Date(start_date)
            formData.date_end = new Date(end_date)
            use_glob_var_date = false
        }


        $.ajax({
            url: '/conference/' + confAcronym + '/event/persist',
            method: 'POST',
            data: {
                formData: formData,
                confAcronym: confAcronym,

            },
            success: function (data) {
                console.log('Event had been created !');
                event = data.event;
                if (event.date_start != 'Invalid Date' && event.date_end != 'Invalid Date') {

                    if (event._super_event != undefined) {

                        $("#" + event._super_event.slug + 'Calendar').fullCalendar('refetchEvents')
                    }
                    else {
                        $("#" + confAcronym + 'Calendar').fullCalendar('refetchEvents')
                    }
                }
                else {

                    if (event._super_event != undefined) {
                        $("#externalEventsList").append("<div data-superEvent=" + event._super_event + "  data-track =" + event._track.name + " name=" + event.slug +
                            " class='fc-event' >" + event.name + "</div>")
                    }

                    else {
                        $("#externalEventsList").append("<div   data-track =" + event._track.name + " name=" + event.slug +
                            " class='fc-event' >" + event.name + "</div>")
                    }


                    externalEventUpdate()();
                }
            },
            error: function () {
                console.log('error persist event!')
            }
        });

    }


    // save an existing event


})


// Calendar Object

function newCalendarEvent(event) {
    calendarEvent = {
        title: event.name,
        slug: event.slug,
        start: new moment(event.date_start),
        end: new moment(event.date_end),
        track: event._track.name,
        _super_event: event._super_event,
        conference: event.conference,
        editable: true,
        allDay: false
    }

    if (calendarEvent.track != 'Talk') calendarEvent.borderColor = 'green'
    else calendarEvent.borderColor = 'red'

    return calendarEvent

}


function saveEvent(confAcronym, event ,idCalendar) {
    formData = {
        slug: event.slug,
        name: event.title,
        date_start: new Date(event.start),
        date_end: new Date(event.end),
    }

    if ($(".tab-pane.active.in").attr('id') != undefined) {
        formData._super_event = $(".tab-pane.active.in").attr('id')
    }


    $.ajax({
        url: '/conference/' + confAcronym + '/event/' + event.slug + '/save',
        method: 'POST',
        data: {
            formData: formData,
            confAcronym: confAcronym
        },
        success: function (data) {
            console.log('Event had been updated !')
            $('#'+idCalendar).fullCalendar('refetchResources')
            $('#'+idCalendar).fullCalendar('rerenderEvents')
        },
        error: function () {
            console.log('error save event !')
        }
    });
}


function getEventsByConfAcronymAndEventSlug(idCalendar, confAcronym, superEvent) {

    var url = ""
    if (confAcronym === superEvent.title) url = '/conference/' + confAcronym + '/api'
    else url = '/conference/' + confAcronym + '/api/' + event.slug + '/subEvents'


    $.ajax({
        url: url,
        method: 'GET',
        data: {

            confAcronym: confAcronym
        },
        success: function (eventArray) {
            console.log('list sub events of : ' + eventArray)

            refreshCalendarEvents(idCalendar, eventArray)

            //test if is a talk event
            /*eventsTab = data
             refreshCalendarEvents(idCalendar,eventArray)
             superEvent = event.slug*/


        },
        error: function () {
            console.log('error !')
        }
    });


}



function clearExternalEvent(){
    $("#externalEventsList").empty()
}


// initialize Calendar
function newCalendar(idCalendar, superEvent) {


    $('#' + idCalendar + 'Calendar').fullCalendar({
        customButtons: {
            myCustomButton: {
                text: 'Form View',
                click: function() {
                    if(superEvent.slug === superEvent.title) {
                        window.location.href = "/conference/"+confAcronym ;
                    }
                    else {
                        window.location.href = "/conference/"+confAcronym+"/event/"+superEvent.slug ;
                    }
                }
            }
        },
        header: {
            left: 'prev,next today myCustomButton ',
            center: 'title',
            right: 'agendaWeek,agendaDay,month'
        },
        defaultView : 'agendaWeek',
        allDaySlot: true,
        minTime: "08:00:00",
        maxTime: "21:00:00",
        slotDuration: "00:07:30",
        events: function (start, end, timezone, callback) {
            console.log(confAcronym + " - " + superEvent.slug)
            var url = ""
            if (confAcronym === superEvent.slug) url = '/conference/' + confAcronym + '/api'
            else url = '/conference/' + confAcronym + '/api/' + superEvent.slug + '/subEvents'


            $.ajax({
                url: url,
                method: 'GET',
                data: {

                    confAcronym: confAcronym
                },
                success: function (data) {



                    clearExternalEvent()
                    eventArray = []

                    console.log('list sub events of : ' + data)

                    data.forEach(function (event) {


                        if (event.date_start != 'Invalid Date' && event.date_end != 'Invalid Date') {
                            console.log('valid')
                            calendarEvent = {
                                title: event.name,
                                slug: event.slug,
                                start: new Date(event.date_start),
                                end: new Date(event.date_end),
                                track: event._track.name,
                                _super_event: event._super_event,
                                conference: event.conference,
                                allDay: false
                            }
                            //console.log(calendarEvent)

                            if (calendarEvent.track != 'Talk') calendarEvent.borderColor = 'green'
                            else calendarEvent.borderColor = 'red'
                            eventArray.push(calendarEvent)
                            //console.log(event)
                        }

                        else {
                            console.log('date invalid')

                            if (event._super_event != undefined) {
                                console.log('hnaay')
                                $("#externalEventsList").append("<div data-superEvent=" + event._super_event + "  data-track =" + event._track.name + " name=" + event.slug +
                                    " class=fc-event >" + event.name + "</div>")
                            }

                            else {
                                $("#externalEventsList").append("<div   data-track =" + event._track.name + " name=" + event.slug +
                                    " class=fc-event >" + event.name + "</div>")
                            }
                            externalEventUpdate()
                        }


                    });

                    callback(eventArray)
                    //refreshCalendarEvents(idCalendar,eventArray)

                    //test if is a talk event
                    /*eventsTab = data
                     refreshCalendarEvents(idCalendar,eventArray)
                     superEvent = event.slug*/


                },
                error: function () {
                    console.log('error !')
                }
            });

        },
        eventLimit: true,
        editable: true,
        eventResize: function (event, delta, revertFunc) {
            // Ok
            console.log(event.title + " end is now from " + event.start.format() + " to " + event.end.format());
            if (!confirm("is this okay?")) {

                revertFunc();
            }
            else {
                saveEvent(confAcronym, event,idCalendar+'Calendar')
            }

        },
        eventDrop: function (event, delta, revertFunc) {
            console.log(event)
            alert(event.title + " was dropped on " + event.start.format() + " and end at " + event.end.format());

            if (!confirm("Are you sure about this change?")) {
                revertFunc();
            }
            else {
                console.log(event)
                saveEvent(confAcronym, event,idCalendar+'Calendar')
            }
        },
        selectable: true,
        select: function (start, end) {
            start_date = start
            end_date = end
            use_glob_var_date = true

            $("#myModal").modal('show')

            console.log($(".tab-pane.active").attr('id'))
            $('#' + superEvent + 'Calendar').fullCalendar('unselect');
        },
        droppable: true, // this allows things to be dropped onto the calendar
        drop: function (date, jsEvent, ui, resourceId) {

            console.log("trackkk : " + $(this).attr('data-track'))

            event = {
                slug: $(this).attr('name'),
                title: $(this).text(),
                start: date.toDate(),
                end: moment(date, "DD-MM-YYYY").add(1, 'hours').toDate()
            }

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }

            console.log(jsEvent)

            saveEvent(confAcronym, event,idCalendar+'Calendar');



        },
        eventClick: function (calEvent, jsEvent, view) {


            console.log(calEvent)

            console.log('Event clicked');


            if (calEvent.track != 'Talk') {
                $(this).css('border-color', 'green');
                //getEventsByConfAcronymAndEventSlug(confAcronym, calEvent);
                $("#navTab").append("<li role=presentation><img data-idcalendar="+idCalendar+" class='closeTab' src='/img/close.png' ><a href=#" + calEvent.slug + " aria-controls=settings role=tab data-toggle=tab >" + calEvent.title + "</a></li>");
                $("#navTabContent").append("<div role=tabpanel data-idcalendar="+idCalendar+" class=tab-pane id=" + calEvent.slug + "><div id=" + calEvent.slug + "Calendar" + " > </div></div>")
                $("#" + calEvent.slug).tab('show')
                newCalendar(calEvent.slug, calEvent)
            }
            else $(this).css('border-color', 'red');


            $("#navTab a[href=#" + calEvent.slug + "]").tab('show')
            $(".fc-today-button.fc-button.fc-state-default.fc-corner-left.fc-corner-right").trigger('click');
        }
    })


    //getEventsByConfAcronymAndEventSlug(idCalendar,confAcronym,superEvent)

    $("#"+idCalendar).fullCalendar('refetchEvents')


$(".closeTab").click(function(event){
    event.preventDefault()
    console.log($(this).attr('data-idcalendar'))
    $("#"+idCalendar+'Calendar').remove()
    console.log($(".active[role=presentation]").attr('role') + 'olala ')
    $(".active[role=presentation]").remove()
})

}


// refresh calendar events

function refreshCalendarEvents(idCalendar, eventTab) {
    console.log('hna')
    //$('#'+idCalendar).fullCalendar('removeEvents')
    eventTab.forEach(function (event) {
        console.log(event)
        event.title = event.name
        //$('#' + idCalendar).fullCalendar('addEvent', newCalendarEvent(event))

    });
    // $('#' + idCalendar).fullCalendar('renderEvent' , newCalendarEvent(eventTab[1]))


}


function externalEventUpdate() {
    $('#externalEventsList .fc-event').each(function() {
        console.log('hna')
        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true,      // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });
}

