/**
 * Created by pierremarsot on 20/11/2017.
 */
$(document).ready(function () {
    const url = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
    const idConf = $('#conf-info').data('id').replace(/"/g, '');
    const urlApi = url + "/conference/" + idConf;
    const urlRestApi = url + "/rest/conference/" + idConf;
    const addSessionModal = $("#addSessionModal");
    const updateSessionModal = $("#updateSessionModal");
    const divAlertError = $(".alertError");
    const selectAddSessionLocation = $("#sessionLocation");
    const selectUdpateSessionLocation = $("#sessionLocationUpdate");
    const tabsEvents = $(".tabsEvents");
    const classSelectDurationSlot = ".selectDurationSlot";
    const selectDurationSlot = $(classSelectDurationSlot);
    const selectFilterByTrack = $(".selectFilterByTrack");

    let locations = [];
    let tracks = [];
    let eventCategories = [];
    let conference = null;
    let currentCalendar = null;
    let contexte = [];
    let indexTabEvent = 0;
    let loadTracks = true;
    let loadLocation = true;
    let loadEventCategories = false;
    let haveInformationsConference = false;
    let durationSlot = getSelectDurationSlot(selectDurationSlot);
    let idTrackSelectedGlobal = "0";

    var tabContextEvent = function (options) {

        /*
         * Variables accessible
         * in the class
         */
        var vars = {
            url: url,
            idConf: idConf,
            urlApi: urlApi,
            idTab: -1,
            calendarDom: null,
            calendar: null,
            event: null,
            idCalendar: "",
            events: [],
        };

        /*
         * Can access this.method
         * inside other methods using
         * root.method()
         */
        const root = this;

        /*
         * Constructor
         */
        this.construct = function (options) {
            $.extend(vars, options);

            if (!vars.event._id || vars.event._id.length === 0) {
                return false;
            }

            //On va récupérer le div calendar du dom
            vars.calendarDom = $("#" + vars.idCalendar);

            root.initCalendar();
        };

        this.initCalendar = function () {
            vars.calendarDom.empty();

            //On récupère le nombre de jour de décalage pour voir si on switch de agendaWeek ou de agendaDay
            const daysDiff = root.getDaysDiff();

            //On init le fullcalendar
            vars.calendar = vars.calendarDom.fullCalendar({
                header: {
                    left: 'title',
                    center: 'agendaDay,agendaWeek,month',
                    right: 'prev,next today'
                },
                editable: true,
                firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
                selectable: true,
                defaultView: daysDiff > 1 ? 'agendaWeek' : 'agendaDay',
                firstHour: 5,
                lastHour: 22,
                axisFormat: 'h:mm',
                allDaySlot: false,
                selectHelper: true,
                droppable: true,
                slotMinutes: durationSlot,
                events: [],
                viewRender: function (view, element) {
                    $("#" + vars.idCalendar + " .fc-header-title").html("<h2>" + root.getMomentDateStart().format("MM/DD/YYYY h:mm a") +
                        " to " + root.getMomentDateEnd().format("MM/DD/YYYY h:mm a") + " </h2>");
                },
                select: function (start, end, allDay) {
                    if (!root.isIntoEvent(start, end)) {
                        const startDate = root.getDateStart();
                        const startEnd = root.getDateEnd();
                        const timeStart = root.getTimeStart();
                        const timeEnd = root.getTimeEnd();
                        showError(root.getEventName() + " begin at " + startDate + " " + timeStart + " and finish at " + startEnd + " " + timeEnd);
                        return false;
                    }

                    //Extract start date
                    const startMoment = moment(start);

                    //Extract end date
                    const endMoment = moment(end);

                    $("#dateStartAddSession").val(parseDateFromMoment(startMoment));
                    $("#dateEndAddSession").val(parseDateFromMoment(endMoment));
                    $("#timeStartAddSession").val(parseHeureFromMoment(startMoment));
                    $("#timeEndAddSession").val(parseHeureFromMoment(endMoment));
                    $("#nameSessionAddModal").val("");
                    $("#descriptionSessionAddModal").val("");

                    //On set le calendar en variable globale
                    currentCalendar = root;

                    //On regarde quel titre on va donner à la modal
                    const title = currentCalendar.getIdSupEvent() === idConf ? "Add session" : "Add event";

                    //On affiche la modal d'ajout d'event
                    showModalAddEvent(title, vars.id);
                },
                eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
                    const startMoment = moment(event.start);
                    const endMoment = moment(event.end);

                    const eventToUpdate = event.original_event;
                    if (eventToUpdate) {
                        eventToUpdate.date_start = parseDateFromMoment(startMoment);
                        eventToUpdate.date_end = parseDateFromMoment(endMoment);
                        eventToUpdate.time_start = parseHeureFromMoment(startMoment);
                        eventToUpdate.time_end = parseHeureFromMoment(endMoment);

                        $.post(urlApi + "/event/" + eventToUpdate._id, eventToUpdate, (data, status) => {
                            if (isSuccess(status) && data.event) {
                                root.removeEventFromCalendar(eventToUpdate._id);

                                root.addEventToCalendar(data.event);
                            }
                        });
                    }
                },
                eventDrop: function (event) {
                    const startMoment = moment(event.start);
                    const endMoment = moment(event.end);

                    const eventToUpdate = event.original_event;
                    if (eventToUpdate) {
                        eventToUpdate.date_start = parseDateFromMoment(startMoment);
                        eventToUpdate.date_end = parseDateFromMoment(endMoment);
                        eventToUpdate.time_start = parseHeureFromMoment(startMoment);
                        eventToUpdate.time_end = parseHeureFromMoment(endMoment);

                        $.post(urlApi + "/event/" + eventToUpdate._id, eventToUpdate, (data, status) => {
                            if (isSuccess(status) && data.event) {
                                root.removeEventFromCalendar(eventToUpdate._id);

                                root.addEventToCalendar(data.event);
                            }
                        });
                    }
                },
                eventClick: function (calEvent, jsEvent, view) {
                    //On récup l'event
                    const event = calEvent.original_event;

                    //On regarde si on a l'event
                    if (!event) {
                        return false;
                    }

                    //Extract start date
                    const startMoment = moment(event.date_start + " " + event.time_start);

                    //Extract end date
                    const endMoment = moment(event.date_end + " " + event.time_end);

                    $("#idSessionUpdateModal").val(event._id);
                    $("#btnGoIntoEvent").data("id", event._id);
                    $("#btnGoIntoEvent").data("id-tab", vars.idTab);
                    $("#nameSessionUpdateModal").val(event.name);
                    $("#descriptionSessionUpdateModal").val(event.summary);
                    $("#sessionLocationUpdate").val(event.idLocation);

                    if(event.idTrack && event.idTrack._id) {
                        $("#sessionTrackUpdate").val(event.idTrack._id);
                    }

                    $("#sessionUpdateCategoriesEvent").val(event.idEventCategory);
                    $("#dateStartSessionUpdate").val(parseDateFromMoment(startMoment));
                    $("#dateEndSessionUpdate").val(parseDateFromMoment(endMoment));
                    $("#timeStartSessionUpdate").val(parseHeureFromMoment(startMoment));
                    $("#timeEndSessionUpdate").val(parseHeureFromMoment(endMoment));

                    //On set le calendar en variable globale
                    currentCalendar = root;

                    //On regarde quel titre on va donner à la modal
                    const title = currentCalendar.getIdSupEvent() === idConf ? "Update session" : "Update event";

                    //On affiche la modal de modification d'un event
                    showModalUpdateEvent(title)
                }
            });

            //On ajoute deux events pour définir la zone accessible
            root.addNotAvalaibleEvents();

            //On charge les events
            $.get(urlApi + "/event/find/" + vars.event._id, (data, status) => {
                if (isSuccess(status) && data.events) {
                    vars.events = data.events;


                    for (const event of data.events) {
                        root.addEventToCalendar(event, false);
                    }

                    //On set le jour de départ du calendar
                    vars.calendarDom.fullCalendar('gotoDate', new Date(root.getDateStart()));
                } else {
                    showError("Error when get events");
                }
            });
        };

        this.addNotAvalaibleEvents = function () {
            //Event avant l'event courant
            vars.calendar.fullCalendar('renderEvent',
                {
                    title: "Not available",
                    start: moment(root.getDateStart() + " " + root.getTimeStart()).subtract(2, 'years').format(),
                    end: moment(root.getDateStart() + " " + root.getTimeStart()).format(),
                    allDay: false,
                    backgroundColor: "#DC2121",
                    className: ["red-event"],
                    alreadyEnable: true,
                },
                true
            );

            //Event après l'event courant
            vars.calendar.fullCalendar('renderEvent',
                {
                    title: "Not available",
                    start: moment(root.getDateEnd() + " " + root.getTimeEnd()).format(),
                    end: moment(root.getDateEnd() + " " + root.getTimeEnd()).add(2, 'years').format(),
                    allDay: false,
                    backgroundColor: "#DC2121",
                    className: ["red-event"],
                    alreadyEnable: true,
                },
                true
            );
        };

        /*
         * Public method
         * Can be called outside class
         */
        this.getCalendar = function () {
            return vars.calendar;
        };

        this.getIdSupEvent = function () {
            return vars.event._id;
        };

        this.getIdTab = function () {
            return vars.idTab;
        };

        this.getEventById = function (idEvent) {
            if (vars.events.length === 0) {
                return null;
            }

            return vars.events.find((e) => {
                return e._id === idEvent;
            });
        };

        this.getEventName = function () {
            return vars.event.name || vars.event.title;
        };

        this.getDateStart = function () {
            return vars.event.date_start || vars.event.date_begin;
        };

        this.getDateEnd = function () {
            return vars.event.date_end;
        };

        this.getTimeStart = function () {
            return vars.event.time_start || "00:00";
        };

        this.getTimeEnd = function () {
            return vars.event.time_end || "23:59";
        };

        this.getMomentDateStart = function () {
            return moment(root.getDateStart() + " " + root.getTimeStart());
        };

        this.getMomentDateEnd = function () {
            return moment(root.getDateEnd() + " " + root.getTimeEnd());
        };

        this.getDaysDiff = function () {
            const m1 = moment(root.getDateStart() + " " + root.getTimeStart());
            const m2 = moment(root.getDateEnd() + " " + root.getTimeEnd());
            return Math.abs(m2.diff(m1, 'days'));
        };

        this.isIntoEvent = function (start, end) {
            start = moment(start);
            end = moment(end);

            const startDate = root.getDateStart();
            const startEnd = root.getDateEnd();
            const timeStart = root.getTimeStart();
            const timeEnd = root.getTimeEnd();
            const startEvent = moment(startDate + " " + timeStart);
            const endEvent = moment(startEnd + " " + timeEnd);

            return !!(start.isSameOrAfter(startEvent) && start.isSameOrBefore(endEvent) && end.isSameOrAfter(startEvent) && end.isSameOrBefore(endEvent));
        };

        this.addEventToCalendar = function (event, addIntoLocalEvents = true) {
            if (!event) {
                return false;
            }

            //On récupère l'id de la track
            let idTrack = null;
            if(event && event.idTrack) {
                idTrack = event.idTrack._id;
            }

            //On l'ajoute dans le calendar
            vars.calendar.fullCalendar('renderEvent',
                {
                    id: event._id,
                    original_event: event,
                    title: event.name,
                    start: moment(event.date_start + " " + event.time_start).format(),
                    end: moment(event.date_end + " " + event.time_end).format(),
                    id_track: idTrack,
                    allDay: false,
                    alreadyEnable: false,
                },
                true // make the event "stick"
            );

            //On l'ajoute dans notre tableau d'events local
            if (addIntoLocalEvents) {
                root.addEventToTab(event);
            }
        };

        this.addEventToTab = function (event) {
            vars.events.push(event);
        };

        this.removeEventFromCalendar = function (idEvent, deleteLocalEvents = true) {
            //On le supprime du calendar
            vars.calendar.fullCalendar('removeEvents', idEvent);

            //On le supprime de notre tableau d'events local
            if (deleteLocalEvents) {
                vars.events = vars.events.filter((e) => {
                    return e._id !== idEvent;
                });
            }
        };

        this.filterByTrack = function () {
            //On supprime tous les events du calendar
            vars.calendar.fullCalendar('removeEvents', function (e) {
                return e.alreadyEnable === undefined || e.alreadyEnable !== true;
            });

            for (const event of vars.events) {
                if(!event.idTrack || event.idTrack === undefined) {
                    root.addEventToCalendar(event, false);
                } else if (event.idTrack._id === idTrackSelectedGlobal || idTrackSelectedGlobal === "0") {
                    root.addEventToCalendar(event, false);
                }
            }
        };

        /*
         * Pass options when class instantiated
         */
        this.construct(options);
    };

    function addContexte(event) {
        const ul = tabsEvents.find(".nav-tabs");
        const tabContent = tabsEvents.find(".tab-content");
        if (!ul || !tabContent) {
            showError("Error when get DOM");
            return false;
        }

        //On regarde si la tab doit être activée
        const active = contexte.length === 0 ? "active" : "";

        //On ajoute un ul
        const titleEvent = event.title || event.name;
        ul.append('<li role="presentation" class="' + active + '"><a href="#' + indexTabEvent +
            '" aria-controls="' + indexTabEvent + '" role="tab" data-toggle="tab" data-id="' + indexTabEvent + '">' + titleEvent + '</a></li>');

        //On regarde si on peut supprimer la tab
        let removeTab = "";
        if (event._id !== idConf) {
            removeTab = "<div class='row'><div class='col-md-12'><button class='btn btn-danger btnRemoveTabEvent pull-right m20' data-id='" + event._id + "' data-key='" + indexTabEvent + "''>Remove Tab</button> </div></div>"
        }

        //On ajoute un content pour la tab
        tabContent.append('<div role="tabpanel" class="tab-pane ' + active + '" id="' + indexTabEvent + '">'
            + removeTab + '<div id="calendar' + indexTabEvent + '"></div></div>');

        //On crée l'objet tab du context
        const context = new tabContextEvent({
            idTab: indexTabEvent,
            event: event,
            idCalendar: "calendar" + indexTabEvent,
        });

        //On ajoute notre contexte à notre liste
        contexte.push(context);

        //On incrémente l'index pour la prochaine tab
        indexTabEvent++;
    }

    //On charge les locations
    $.get(urlRestApi + "/location", (data, status) => {
        loadLocation = false;

        if (isSuccess(status) && data.locations) {
            locations = data.locations;
            for (const location of locations) {
                selectAddSessionLocation.append("<option value='" + location._id + "'>" + location.name + "</option>");
                selectUdpateSessionLocation.append("<option value='" + location._id + "'>" + location.name + "</option>");
            }

            canManageEvent();
        } else {
            showError("Error when get locations")
        }
    });

    //On charge toutes les tracks
    $.get(urlApi + "/tracks/find", function (data, status) {
        loadTracks = false;

        if (isSuccess(status)) {
            tracks = data.tracks;

            for (const track of tracks) {
                //On ajoute dans la modal
                $("#sessionTrack").append("<option value='" + track._id + "'>" + track.name + "</option>");
                $("#sessionTrackUpdate").append("<option value='" + track._id + "'>" + track.name + "</option>");

                //On ajoute dans le filter
                selectFilterByTrack.append("<option value='" + track._id + "'>" + track.name + "</option>")
            }

            canManageEvent();
        }
    });

    //On charge les informations de la conférence
    $.get(urlRestApi + "/informations", function (data, status) {
        if (isSuccess(status) && data.conference) {
            conference = data.conference;
            haveInformationsConference = true;
            canManageEvent();
        } else {
            showError("Error when get informations conference")
        }
    });

    //On charge les event catégories
    $.get(urlApi + "/event-categories", (data, status) => {
       if(isSuccess(status) && data.event_categories) {
           eventCategories = data.event_categories;

           for (const e of eventCategories) {
               //On ajoute dans la modal
               $("#sessionUpdateCategoriesEvent").append("<option value='" + e._id + "'>" + e.name + "</option>");
               $("#sessionCategoriesEvent").append("<option value='" + e._id + "'>" + e.name + "</option>");
           }
           canManageEvent();
       }  else {
           showError("Error when get event categories");
       }
    });

    function canManageEvent() {
        if (tracks && tracks.length > 0 && locations && locations.length > 0 && haveInformationsConference && eventCategories && eventCategories.length > 0) {
            //On ajoute le contexte par défaut
            addContexte(conference);
        } else if (!loadTracks && !loadLocation) {
            if (tracks.length === 0) {
                showError("You must add tracks");
            } else if (locations.length === 0) {
                showError("You must add locations");
            }
        }
    }

    function isSuccess(status) {
        return status && status === "success";
    }

    function showError(error) {
        divAlertError.html("<div class='col-md-12'><div class='alert alert-danger'>" + error + "</div></div>");
        divAlertError.show();

        setTimeout(function () {
            divAlertError.empty();
            divAlertError.hide();
        }, 5000)
    }

    function parseDateFromMoment(momentDate) {
        const year = momentDate.get('year');
        const month = momentDate.get('month') + 1;  // 0 to 11
        const date = momentDate.get('date');

        return month + "/" + date + "/" + year;
    }

    function parseHeureFromMoment(mom) {
        const heureMoment = moment(mom);
        let hour = heureMoment.get('hour').toString();
        let minute = heureMoment.get('minute').toString();

        if (!hour || !minute) {
            return "";
        }

        if (hour.length < 2) {
            hour = "0" + hour;
        }

        if (minute.length < 2) {
            minute = "0" + minute;
        }

        return hour + ":" + minute;
    }

    function hideModalAddEvent() {
        addSessionModal.modal("hide");
    }

    function hideModalUpdateEvent() {
        updateSessionModal.modal("hide");
    }

    function showModalAddEvent(title) {
        addSessionModal.find(".title-type-event").empty().html(title);

        //On regarde si c'est un ajout de session ou d'un event
        const isSession = currentCalendar.getIdSupEvent() === idConf;
        //addSessionModal.find("#isSessionAddEvent").val(isSession);

        if(!isSession){
            //Si ce n'est pas une session, on supprime la track et on affiche les catégories d'event
            addSessionModal.find("#sessionTrack").closest(".form-group").addClass("hidden");
            addSessionModal.find("#sessionCategoriesEvent").closest(".form-group").removeClass("hidden");
        } else {
            //Si c'est une session, on affiche les tracks
            addSessionModal.find("#sessionTrack").closest(".form-group").removeClass("hidden");
            addSessionModal.find("#sessionCategoriesEvent").closest(".form-group").addClass("hidden");
        }

        addSessionModal.modal("show");
    }

    function showModalUpdateEvent(title) {
        updateSessionModal.find(".title-type-event").empty().html(title);

        //On regarde si c'est un ajout de session ou d'un event
        const isSession = currentCalendar.getIdSupEvent() === idConf;
        //updateSessionModal.find("#idSessionUpdateModal").val(isSession);

        if(!isSession){
            //Si ce n'est pas une session, on supprime la track et on affiche les catégories d'event
            updateSessionModal.find("#sessionTrackUpdate").closest(".form-group").addClass("hidden");
            updateSessionModal.find("#sessionUpdateCategoriesEvent").closest(".form-group").removeClass("hidden");
        } else {
            //Si c'est une session, on affiche les tracks
            updateSessionModal.find("#sessionTrackUpdate").closest(".form-group").removeClass("hidden");
            updateSessionModal.find("#sessionUpdateCategoriesEvent").closest(".form-group").addClass("hidden");
        }

        //On affiche la modal
        updateSessionModal.modal("show");
    }

    function geContexteByIdTab(idTab) {
        if (contexte.length === 0) {
            return null;
        }

        return contexte.find((c) => {
            return c.getIdTab() === idTab;
        });
    }

    function getSelectDurationSlot(select) {
        const duration = select.find("option:selected").val();
        if (duration === undefined || duration.length === 0) {
            return 30;
        }

        return parseInt(duration);
    }

    function rerenderCalendar() {
        for (const c of contexte) {
            c.initCalendar();
        }
    }

    $(document).on("change", ".selectFilterByTrack", function () {
        idTrackSelectedGlobal = $(".selectFilterByTrack option:selected").val();

        for (const c of contexte) {
            c.filterByTrack();
        }
    });

    //For display calendar when click on event tab
    $(document).on('shown.bs.tab', '.tabsEvents a[role="tab"]', function (e) {

        const tab = e.target;
        if (!tab) {
            return false;
        }

        const idContexte = $(tab).data("id");

        if (idContexte === undefined || idContexte === null) {
            return false;
        }

        const context = contexte[idContexte];
        if (context.getCalendar()) {
            context.getCalendar().fullCalendar('render');
            context.filterByTrack();
        }
    });

    //Event to add an event
    $(document).on("click", ".btnAddSessionModal", function () {

        const name = $("#nameSessionAddModal").val();
        const description = $("#descriptionSessionAddModal").val();
        const location = $("#sessionLocation option:selected").val();

        //On regarde si la track / event category est hidden
        let track = null;
        let idEventCategory = null;

        if(!$("#sessionTrack").closest(".form-group").hasClass("hidden")){
            track = $("#sessionTrack option:selected").val();
        }
        if(!$("#sessionCategoriesEvent").closest(".form-group").hasClass("hidden")){
            idEventCategory = $("#sessionCategoriesEvent option:selected").val();
        } else {
            const session = getSessionEventCategory();
            if(session) {
                idEventCategory = session._id;
            }
        }

        const dateStart = $("#dateStartAddSession").val();
        const dateEnd = $("#dateEndAddSession").val();
        const timeStart = $("#timeStartAddSession").val();
        const timeEnd = $("#timeEndAddSession").val();

        $.post(urlApi + "/event", {
            name: name,
            description: description,
            id_location: location,
            date_start: dateStart,
            date_end: dateEnd,
            time_start: timeStart,
            time_end: timeEnd,
            id_track: track,
            idSupEvent: currentCalendar.getIdSupEvent(),
            idEventCategory: idEventCategory,
        }, function (data, status) {
            if (isSuccess(status) && data.event) {
                hideModalAddEvent();
                currentCalendar.addEventToCalendar(data.event);
                currentCalendar.addEventToTab(data.event);
            }
        });
    });

    function getSessionEventCategory(){
        for(const e of eventCategories){
            if(e.name === "Session"){
                return e;
            }
        }

        return null;
    }

    //Event to update event
    $(document).on("click", ".btnUpdateSessionModal", function () {
        const idEvent = $("#idSessionUpdateModal").val();
        const name = $("#nameSessionUpdateModal").val();
        const description = $("#descriptionSessionUpdateModal").val();
        const location = $("#sessionLocationUpdate").val();

        //On regarde si la track / event category est hidden
        let track = null;
        let idEventCategory = null;

        if(!$("#sessionTrackUpdate").closest(".form-group").hasClass("hidden")){
            track = $("#sessionTrackUpdate option:selected").val();
        }
        if(!$("#sessionUpdateCategoriesEvent").closest(".form-group").hasClass("hidden")){
            idEventCategory = $("#sessionUpdateCategoriesEvent option:selected").val();
        } else {
            const session = getSessionEventCategory();
            if(session) {
                idEventCategory = session._id;
            }
        }

        const dateStart = $("#dateStartSessionUpdate").val();
        const dateEnd = $("#dateEndSessionUpdate").val();
        const timeStart = $("#timeStartSessionUpdate").val();
        const timeEnd = $("#timeEndSessionUpdate").val();

        $.post(urlApi + "/event/" + idEvent, {
            name: name,
            description: description,
            id_location: location,
            date_start: dateStart,
            date_end: dateEnd,
            time_start: timeStart,
            time_end: timeEnd,
            idTrack: track,
            idEventCategory: idEventCategory,
        }, function (data, status) {
            if (isSuccess(status) && data.event) {
                //On supprime l'event du calendat
                currentCalendar.removeEventFromCalendar(idEvent);

                //On ajoute l'event modifié dans le calendar
                currentCalendar.addEventToCalendar(data.event);
            }
        });

        //On close la modal update
        hideModalUpdateEvent();
    });

    //Event to remove event
    $(document).on("click", ".btnRemoveEventModal", function () {
        const idEvent = $("#idSessionUpdateModal").val();

        if (idEvent) {
            $.ajax({
                url: urlApi + "/event/" + idEvent,
                type: 'DELETE',
                success: function (result) {
                    if (result && result.id_event) {
                        currentCalendar.removeEventFromCalendar(result.id_event);
                    }
                }
            });

            //On close la modal update
            hideModalUpdateEvent();
        }
    });

    //When we want go into an event
    $(document).on("click", "#btnGoIntoEvent", function () {
        const idEventToGo = $(this).data("id");
        const idTab = $(this).data("id-tab");
        if (idEventToGo === undefined || idTab === undefined) {
            return false;
        }

        //On récup le contexte
        const c = geContexteByIdTab(idTab);
        if (c === undefined || c === null) {
            return false;
        }

        //On récup l'event
        const e = c.getEventById(idEventToGo);
        if (e === undefined || e === null) {
            return false;
        }

        //On ajoute un contexte
        addContexte(e);

        //On close la modale
        hideModalUpdateEvent();
    });

    $(document).on("click", ".btnRemoveTabEvent", function () {
        const idSupEvent = $(this).data("id");
        const keyTab = $(this).data("key");

        if (!idSupEvent) {
            return false;
        }

        //On supprime la tab du contexte
        let newContexte = [];
        let idTab = -1;

        for (const key in contexte) {
            const ctx = contexte[key];

            if (ctx.getIdSupEvent() === idSupEvent) {
                idTab = ctx.getIdTab();
            } else {
                newContexte.push(ctx);
            }
        }

        //On supprime la tab content du dom
        $("#" + keyTab).remove();

        //On remove le tab link du dom
        const ul = tabsEvents.find(".nav-tabs li a[href='#" + keyTab + "']");
        if (ul) {
            ul.remove();
        }
    });

    $(document).on("change", classSelectDurationSlot, function () {
        durationSlot = getSelectDurationSlot($(this));
        rerenderCalendar();
    });
});