$(document).ready(function() {

	$('.delete-event-btn').click(function(event) {
		event.preventDefault()

		// get eventSlug
    const self = $(this);
		const idEvent = self.data('event-id').replace(/"/g, '');

		showUndoBox(
                        'Your event is being deleted ...', 
                        sendDeleteSignalToServer,
      idEvent,
                        'Your event has been restored.'
               		)


	}) // end .delete-button click	
	
	
}) // end document ready



function sendDeleteSignalToServer(idEvent){

  const confId = $('#conf-info').data('id').replace(/"/g, '');
	$.post('/conference/'+confId+'/event/delete/' + idEvent,
				null, function(response) {
    	showResponseNoty(response)
   })



}// end sendDeleteSignalToServer