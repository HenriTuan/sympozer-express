$(document).ready(function() {

	var eventForm = $("#event-form")


	eventForm.submit(function(event){
		
		event.preventDefault()
		var formData = 
			eventForm.serializeArray().reduce(function(obj, item) {
    			obj[item.name] = item.value;
    			return obj;
				}, {})

		eventForm.validate();
		if (eventForm.valid()) {

			showUndoBox(
	                        'Your event is being created ...', 
	                        sendPersistSignalToServer, 
	                        formData,
	                        'Your event has not been created.'
                   		)

      	} // end formValidator.validate

	}) // end #create-button click



    $('#newSessionBtn').click(function(event){
        event.preventDefault()

    })




    $('#newEventPersistBtn').click(function (event) {
        event.preventDefault();
        eventForm.submit();

    })




}) // end document ready




function sendPersistSignalToServer(formData){

	const confId = $('#conf-info').data('id').replace(/"/g, '');

	$.ajax({
  		url: "/conference/"+confId+"/event/persist",
  		type: "POST",
  		data: {'formData': formData},
  		dataType: 'json'
	})
	.done(function( data ) {
    	showResponseNoty(data)
	}) // end .done



}// end sendCreateSignalToServer