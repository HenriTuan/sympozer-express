/**
 * Created by pierremarsot on 20/11/2017.
 */
$(document).ready(function () {
    const url = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
    const idConf = $('#conf-info').data('id').replace(/"/g, '');
    const urlApi = url + "/conference/" + idConf;
    const urlRestApi = url + "/rest/conference/" + idConf;
    const divTableTracks = $(".tableTracks");
    const addTrackModal = $("#addTrackModal");
    const updateTrackModal = $("#updateTrackModal");
    const divAlertError = $(".alertError");

    let tracks = [];
    let chairs = [];
    let lastIdTrackUpdate = null;

    //On charge toutes les tracks
    $.get(urlApi + "/tracks/find", function (data, status) {
        if (isSuccess(status)) {
            tracks = data.tracks;
            paintTracks();
        }
    });

    function paintTracks() {
        if (tracks.length === 0) {
            divTableTracks.html("<div class='alert alert-danger'><span class='glyphicon glyphicon-exclamation-sign'> </span> There is no tracks in this conference.</div>");
        }
        else {
            //Dans tab tracks
            let ret = "<div class='panel panel-info'>";
            ret += "<div class='panel-heading'>";
            ret += "<h3 class='panel-title'>All tracks</h3>";
            ret += "</div>";
            ret += "<div class='panel-body'>";
            ret += "<table class='table table-user-information JColResizer' id='JColResizer0'>";
            ret += "<thead> <tr> <th>#</th> <th>Name</th> <th>Is votable</th> <th>Actions</th> </tr> </thead>";
            ret += "<tbody>";

            for (const key in tracks) {
                const track = tracks[key];

                ret += "<tr>";
                ret += "<td scope='row'>";
                ret += parseInt(key) + 1;
                ret += "</td>";
                ret += "<td>";
                ret += track.name;
                ret += "</td>";
                ret += "<td>";
                ret += track.is_votable ? "Yes" : "No";
                ret += "</td>";
                ret += "<td>";
                ret += "<button class='btn btn-info btnEditTrack' data-id='" + track._id + "'>Edit</button>";
                ret += "<button style='margin-left:5px' class='btn btn-danger btnDeleteTrack' data-id='" + track._id + "'>Delete</button>";
                ret += "</td>";
            }

            ret += "</tbody>";
            ret += "</table>";
            ret += "</div>";
            ret += "</div>";

            divTableTracks.html(ret);
        }
    }

    function isSuccess(status) {
        return status && status === "success";
    }

    function showError(error) {
        divAlertError.html("<div class='col-md-12'><div class='alert alert-danger'>" + error + "</div></div>");
        divAlertError.show();

        setTimeout(function () {
            divAlertError.empty();
            divAlertError.hide();
        }, 5000)
    }

    $(document).on("click", ".btnAddTrackModal", function () {
        const trackName = $("#trackName").val();
        const trackVotable = $("#trackVotable").is(':checked');

        if(!trackName || trackName.length === 0) {
            showError("You must specify a track name.");
            return false;
        }

        const idsPerson = chairs.map((chair) => {
            return chair.id;
        });

        $.post(urlApi + "/tracks", {
            name: trackName,
            idsPerson: idsPerson,
            trackVotable: trackVotable,
        }, function (data, status) {
            addTrackModal.modal("hide");

            if (isSuccess(status) && data.track) {
                tracks.push(data.track);
                paintTracks();
            } else {
                showError(data.error);
            }
        })
    });

    $(document).on("click", ".btnDeleteTrack", function () {
        const idTrack = $(this).data("id");
        if (idTrack) {
            $.ajax({
                url: urlApi + "/tracks/" + idTrack,
                type: 'DELETE',
                success: function (result) {
                    if (result && result.id_track) {
                        tracks = tracks.filter((track) => {
                            return track._id !== result.id_track;
                        });
                        paintTracks();
                    }
                }
            });
        }
    });

    $(document).on("click", ".btnEditTrack", function () {
        const idTrack = $(this).data("id");
        if (idTrack) {
            const track = tracks.find((t) => {
                return t._id === idTrack;
            });

            if (!track) {
                showError("Error when retreiving Track");
                return false;
            }

            $("#trackNameToUpdate").val(track.name);
            $("#trackVotableUpdate").prop("checked", track.is_votable);

            //paintChairs($("#editTableChairs"));
            $.get(urlApi + "/tracks/" + track._id + "/chairs", (data, status) => {
                if (isSuccess(status)) {
                    chairs = data.chairs;
                    paintChairs($("#editTableChairs"));
                }
            });
            updateTrackModal.modal("show");
            lastIdTrackUpdate = track._id;
        }
    });

    $(document).on("click", ".btnUpdateTrackModal", function () {
        const newTrackName = $("#trackNameToUpdate").val();
        const trackVotable = $("#trackVotableUpdate").is(':checked');
        const idsPerson = chairs.map((chair) => chair.id || chair._id);

        console.log(idsPerson, JSON.stringify({name: newTrackName, idsPerson: idsPerson}));
        console.log(lastIdTrackUpdate);
        $.ajax({
            url: urlApi + "/tracks/" + lastIdTrackUpdate,
            type: 'POST',
            data: JSON.stringify({name: newTrackName, idsPerson: idsPerson, trackVotable: trackVotable}),
            contentType: 'application/json',
            success: function (result) {
                if (result && result.track) {
                    tracks = tracks.map((track) => {
                        if (track._id === result.track._id) {
                            track = result.track;
                        }

                        return track;
                    });
                    updateTrackModal.modal("hide");
                    paintTracks();
                }
            }
        })
    });

    function paintChairs(chairsTable) {
        //On vide la table
        chairsTable.empty();

        if (chairs.length > 0) {
            //On ajoute l'entête
            chairsTable.append("<thead><tr><th>Chair name</th></tr></thead>");

            //On ajoute le contenu
            let tbody = "<tbody>";

            chairs.forEach((chair) => {
                const name = chair.name || chair.displayname;
                const id = chair.id || chair._id;
                tbody += "<tr><td>" + name + "</td><td><button class='btn btn-danger btnRemoveChair' data-id='" + id + "'>Remove</button></td></tr>"
            });

            tbody += "</tbody>";

            chairsTable.append(tbody);
        }
    }

    const getPersons = function (idConf, term) {
        return $.get({
            url: '/conference/' + idConf + "/person/rest",
            data: {
                'id_min': -1,
                'term': term,
                'page_size': 20
            },
            accept: "application/json",
        });
    };

    $('#chairs-input-add').autocomplete({
        source: function (req, res) {
            let promise = getPersons(idConf, req.term);
            promise.done((data) => {
                let result = [];
                data.forEach((el) => {
                    result.push({label: el.displayname, value: el._id})
                });
                res(result);
            })
                .fail((err) => res(""))
        },
        minLength: 0,
        delay: 500,
        select: function (event, ui) {
            event.preventDefault();
            $(event.target).val("");

            const find = chairs.find((chair) => {
                return chair.id === ui.item.value;
            });

            if (!find) {
                chairs.push({
                    name: ui.item.label,
                    id: ui.item.value,
                });

                paintChairs($("#addTableChairs"));
            }
        }
    });

    $('#chairs-input-edit').autocomplete({
        source: function (req, res) {
            var promise = getPersons(idConf, req.term);
            promise.done((data) => {
                let result = [];
                data.forEach((el) => {
                    result.push({label: el.displayname, value: el._id})
                });
                res(result);
            })
                .fail((err) => res(""))
        },
        minLength: 0,
        delay: 500,
        select: function (event, ui) {
            event.preventDefault();
            $(event.target).val("");

            const find = chairs.find((chair) => {
                return chair.id === ui.item.value;
            });

            if (!find) {
                chairs.push({
                    name: ui.item.label,
                    id: ui.item.value,
                });

                paintChairs($("#editTableChairs"));
            }
        }
    });

    $(document).on("click", ".btnRemoveChair", function () {
        const id = $(this).data("id");
        if (!id) {
            return false;
        }

        chairs = chairs.filter((chair) => {
            const idChair = chair.id || chair._id;

            return idChair !== id;
        });

        const table = $(this).closest("table");
        if(!table){
            return false;
        }

        paintChairs(table);
    });
});