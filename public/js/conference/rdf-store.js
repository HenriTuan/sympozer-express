$(document).ready(function() {

	// table pagination
    var paginator = $('#triple-pagination')
	var nPage = paginator.data('n-page')
	var acronym = paginator.data('conf-acronym')
    // var visiblePage = 13

	// paginator.twbsPagination({
 //        totalPages: nPage,
 //        visiblePages: visiblePage,
 //        startPage: 1,
 //        next: false,
 //        prev: false
 //    }) // end twbsPagination

 //    goToPage(1, acronym)

 //    paginator.on('click', function(event){
 //        event.preventDefault()

 //        var self = $(event.target)

 //        if(self.parent().hasClass('page')){
 //            var page = self.text()
 //            goToPage(page, acronym)
 //            updatePaginator(page, visiblePage)
 //            $('#goToPageInput').val(page)
 //        }

 //    })

    goToPage(1)

    // page number control
        // http://bootsnipp.com/snippets/featured/bootstrap-number-spinner-on-click-hold
    var action
    $(".number-spinner button").mousedown(function () {
        btn = $(this)
        input = btn.closest('.number-spinner').find('input')
        btn.closest('.number-spinner').find('button').prop("disabled", false)

        if (btn.attr('data-dir') == 'up') {
            action = setInterval(function(){
                if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                    input.val(parseInt(input.val())+1)
                }else{
                    btn.prop("disabled", true)
                    clearInterval(action)
                }
            }, 50)
        } else {
            action = setInterval(function(){
                if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                    input.val(parseInt(input.val())-1)
                }else{
                    btn.prop("disabled", true)
                    clearInterval(action)
                }
            }, 50)
        }
    }).mouseup(function(){
        clearInterval(action)
    })



    // go to page button control
    $('#goToPageBtn').on('click', function(event){
        event.preventDefault()

        var page = $('#goToPageInput').val()
        goToPage(page)


    }) // end #goToPageBtn onclick


    // next prev first last button control
    $('#first').on('click', function(event){
        event.preventDefault()
        goToPage(1)
    }) // end first.onclick

    $('#last').on('click', function(event){
        event.preventDefault()
        goToPage(nPage)
    }) // end first.onclick

    $('#prev').on('click', function(event){
        event.preventDefault()
        goToPage( parseInt($('#goToPageInput').val())-1 )
    }) // end first.onclick


    $('#next').on('click', function(event){
        event.preventDefault()
        goToPage( parseInt($('#goToPageInput').val())+1 )
    }) // end first.onclick


}) // end document ready


var goToPage = function(page){

    var nPage = $('#triple-pagination').data('n-page')

    if(page <= 0 || page > nPage){
        showErrorNoty({'message': 'Page number must be between 1 and '+nPage+'.'})
        return
    }else{

        var acronym = $('#triple-pagination').data('conf-acronym')
        $.ajax({
            url: "get-triple",
            method: "POST",
            data: { 'page' : page, 'acronym': acronym },
            dataType: "html"
        }) // end ajax
        .done(function(response){
            $('#tripleTable').html(response)
            $('#goToPageInput').val(page)
            $('#cur').find('a').text(page)
        }) // end done
        .fail(function(response){
            console.log(response)
        }) // end fail

    }

} // end goToPage


// var updatePaginator = function(page, visiblePage){
//     var paginator = $('#triple-pagination')
    

//     if(page <= visiblePage/2){
//         var pageIndex = page    
//         paginator.find('li.page').each(function(index, el) {
//             if(index == 0){
//                 $(el).addClass('active')
//             }else{
//                 $(el).removeClass('active')
//             }
//             $(el).find('a').text(pageIndex)
//             pageIndex++
//         })

//     }else{
//         var half = Math.round(visiblePage/2 - 1)
//         var pageIndex = page - half
//         paginator.find('li.page').each(function(index, el) {
//             if(index == half){
//                 $(el).addClass('active')
//             }else{
//                 $(el).removeClass('active')
//             }
//             $(el).find('a').text(pageIndex)
//             pageIndex++
//         })
//     }

// } // end updatePaginator