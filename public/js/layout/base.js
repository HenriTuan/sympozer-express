$(document).ready(function(){

	$('.date-picker').datepicker()

	// table resize	
	$('table').colResizable({
	    liveDrag:true,
	    gripInnerHtml:"<div class='grip'></div>", 
	    draggingClass:"dragging" 
	})

	// textarea autogrow
  	$('textarea').livequery(function(){ 
        var self = $(this)
        autosize(self)
    })
    

}) // end document ready