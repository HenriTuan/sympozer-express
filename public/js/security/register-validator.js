$(document).ready(function() {

      // cache references to input controls
      var password = $('#password')
      var confirmPassword = $('#confirm-password')
      var email = $('#email')
      var confirmEmail = $('#confirm-email')


    $('#register-form').validate({
        rules:{
            'confirm-password':{
                equalTo: '#password'
            }
        }
    })
}) // end document ready
