/* 
 * © 2016 Sympozer-express Project
 */

/*
 * @author HA Le Tuan <henri.tuan77@gmail.com>
 */

(function ( $ ) {

	 $.fn.dynamicInput = function(options) {
	  
		//Default Value Setting
		var settings = $.extend({
			 'type'         : 'text',
			 'class'        : 'form-control',
			 'debug'        : true,
			 'name' 	    : null,
			 'label'		: null,
			 'placeholder'  : 'Your text here ...',
			 'readonly'		: false
		}, options )
		

		return this.each(function() {
			var self = $(this)
			var nInput = 0
			// add label if existed
			if(settings.label != null){
				var label = '<label>'+settings.label+'</label>'		
				self.append(label)
			}

			var inputHolder = $('<div></div>')
			var input = $('<input>').attr({
				type: settings.type,
				placeholder: settings.placeholder,
				class: settings.class,
				readonly: settings.readonly
			})
			.css({
				width: '88%',
				display: 'inline-block'
			})

			if(settings.name !== null){
				input.attr('name', settings.name);
			}

			// add first input (with add button)
			if(settings.readonly == false){
				var firstInput = input.clone()
				// new input button 
				var addButton = $('<button></button>').addClass('btn btn-success')
				var plusIcon = $('<span></span>').addClass('glyphicon glyphicon-plus')
				addButton.append(plusIcon)			
				// add button and input in one line
				var firstInputHolder = inputHolder.clone()
				firstInputHolder.append(addButton).append(firstInput)
				// add this first input line to anchor
				self.append(firstInputHolder)
				nInput++
			}

			// add init inputs (if existed)
			var initInputs = self.data('init-inputs')
			
			if(typeof(initInputs) !== 'undefined' && initInputs !== null && initInputs.length > 0){
				for(var i in initInputs) {
					var newInput = input.clone()
					
					newInput.attr({
						value: initInputs[i],
						readonly: settings.readonly
					})

					if(settings.readonly == false){
						// remove button
						var removeButton = $('<button></button').addClass('btn btn-warning')
						var minusIcon = $('<span></span>').addClass('glyphicon glyphicon-minus')
						removeButton.append(minusIcon)					
						// add button and input in one line
						var newInputHolder = inputHolder.clone()
						newInputHolder.append(removeButton).append(newInput)
						//	add this line to anchor
						self.append(newInputHolder)

						// add listener for remove button
						removeButton.on('click', function(event) {
							event.preventDefault();
							var self = $(this)
							self.parent().remove() // remove this input holder
						}) // end removeButton .onclick
					}else{
						newInput.css({width:'100%'})
						self.append(newInput)
					}

					nInput++

				} // end for
			} // end if

			// add listener for newInput button
			if(settings.readonly == false){
				addButton.on('click', function(event) {
				
					event.preventDefault()
					/* Create new input with remove button */
					
					// new input
					var newInput = input.clone()

					// remove button
					var removeButton = $('<button></button').addClass('btn btn-warning')
					var minusIcon = $('<span></span>').addClass('glyphicon glyphicon-minus')
					removeButton.append(minusIcon)
					// add button and input in one line
					var newInputHolder = inputHolder.clone()
					newInputHolder.append(removeButton).append(newInput)
					//	add this line to anchor
					self.append(newInputHolder)
					nInput++

					// add listener for remove button
					removeButton.on('click', function(event) {
						event.preventDefault();
						var self = $(this)
						self.parent().remove() // remove this input holder
					}) // end removeButton .onclick

				}) // end addButton .onclick

			} // end if readonly == false



		}) // end this.each	 

	 } // end dynamicInput

	 

	
 
}( jQuery ))







