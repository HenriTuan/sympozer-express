function showResponseNoty(data){
	if(data.state == 'success') showSuccessNoty(data)
	else showErrorNoty(data)
}


function showInfoNoty(text){

	return noty({

		text: text, 
		layout: 'topRight',
		type: 'information',
		theme: 'relax',
		timeout: false,

	})// end errorBox

}


function showSuccessNoty(data){
	console.log("success");
	var successBox =
		noty({
			text: data.message, 
			layout: 'topRight',
			type: 'success',
			theme: 'relax',
			timeout: '1200',

			animation: {
		        open: 'animated slideInUp', // Animate.css class names
		        close: 'animated slideOutUp', // Animate.css class names
			}, // end animation

			callback:{
				onClose: function(){
					if( typeof(data.redirect) !== 'undefined' && data.redirect != null){
						if( data.redirect == 'reload' ){
							location.reload()
						}else{

							window.location = data.redirect
						}
					}
				} // end onClose
			}, // end callback

		})// noty

} // end showSuccessNoty



function showErrorNoty(data){

	window.stop();
	console.log(data.errormsg || "");
	var errorBox = noty({
		text: data.message, 
		layout: 'topRight',
		type: 'error',
		theme: 'relax',
		timeout: '5000',

		animation: {
	        open: 'animated slideInUp', // Animate.css class names
	        close: 'animated slideOutUp', // Animate.css class names
		}, // end animation
	})// end errorBox

} // end showErrorNoty



function showUndoBox(text, successCb, params, undoText){

		$.allowPersist = true
    	//show "do you want to undo" box
    	$.undoBox = noty({
    		layout: 'topRight',
    		type: 'success',
    		text: text,
    		timeout: '1200',
    		theme: 'relax',

    		animation: {
		        open: 'animated slideInUp', // Animate.css class names
		        close: 'animated slideOutUp', // Animate.css class names
			}, // end animation

    		callback:{

    			onClose: function(){
    				if($.allowPersist){
    					successCb(params)
    				}
    			}, // end onClose

    		}, // end callback
    		
    		buttons:[
    			{ // UNDO button
					addClass: 'btn btn-warning', 
					text: 'Undo', 
					onClick: function(){
						undoButtonClick(undoText)
					}
				}
			], // end buttons array

    	}) // end undoBox

}


function undoButtonClick(undoText){
	$.allowPersist = false
	// hide undoBox, show removedBox
	$.undoBox.close()
	$.removedBox = noty({
		text: undoText, 
		layout: 'topRight',
		type: 'success',
		theme: 'relax',
		timeout: '5000',

		animation: {
	        open: 'animated slideInUp', // Animate.css class names
	        close: 'animated slideOutUp', // Animate.css class names
		}, // end animation
	}) // end removedBox


} // end undoButtonClick