$(document).ready(function(){

	$('.collapse-panel').on('hidden.bs.collapse shown.bs.collapse', function(event) {

		var self = $(this)
		var icon = self.find('.glyphicon')

		icon.toggleClass('glyphicon-minus glyphicon-plus')

	}) // end collapse-button.onclick
    
  $('#pageselect').change(() => {
    var term = $('#search').val();
    searchPage(-1,term);
  });
    
	$('#btn-more').click(function (e) {
	    e.preventDefault();
	    var maxid = $('.panel:last').attr('data-id');
	    var term = $('#search').val();
	    searchPage(maxid,term);
    });

	function searchPage(maxid,term) {
	    let pageSize = $('#pageselect').val();
        $.get({
            url: window.location.pathname + '/page',
            data: {
                'id_min': maxid,
                'term': term,
                'page_size': pageSize
            },
            success: function (data) {
                data = JSON.parse(data);
                if(maxid == -1){
                    replacePage(data.html);
                    if(!data.empty && data.next){
                        $('#btn-more').show();
                    }
                    else{
                        $('#btn-more').hide();
                    }
                }
                else{
                    if(!data.empty){
                        addPage(data.html);
                    }
                    if(!data.next){
                        $('#btn-more').hide();
                    }
                }
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function addPage(data){
        $('#list-container').append(data);
    }

    function replacePage(data) {
        $('#list-container').html(data);
    }

	$('#search').autocomplete({
	    source: function (request,response) {
            searchPage(-1,request.term);
            response("");
        },
        minLength: 0,
        delay: 500

    });

    $('#btn-more').hide();
    searchPage(-1,"");
});