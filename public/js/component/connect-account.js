$(document).ready(function() {
	
	var matchedPersonHolder = $('#matched-person-holder')

	$('#collapse-person-holder').on('show.bs.collapse', function(){
        var self = $(this)

        var confAcronym = $('#conf-info').data('acronym')

        $.ajax({
                url: "/conference/get-matched-person",
                method: "POST",
                data: { 'confAcronym': confAcronym },
                dataType: "html"
        }) // end ajax

        .done(function(response){
            matchedPersonHolder.html(response)
        }) // end done

        .fail(function(response){
            matchedPersonHolder.html(response)
            console.log(response)
        }) // end fail


    }) // end .on show.bs.collapse




	matchedPersonHolder.on('click', '#connect-btn', function(event){
		event.preventDefault()
		
		var self = $(this)
		var person = {
					  'acronym'		: self.data('person-acronym'),
					  'displayname' : self.data('person-displayname'),
					  'sha'			: self.data('person-sha')
					 }
		var confAcronym = $('#conf-info').data('acronym')

		$.ajax({
                url: '/conference/connect-user',
                method: 'POST',
                data: { 'confAcronym': confAcronym, 'person': person },
                dataType: 'json'
        }) // end ajax
        .done(function(response){
            showResponseNoty(response)
        }) // end done


	}) // end connectBtn.onclick



}) // end document ready