$(document).ready(function() {

  // set nav active
  var nav = $('#nav')
  var activeCode = nav.data('active')


  nav.find('#'+activeCode).removeClass('btn-default').addClass('btn-primary')

}) // end document ready