/*******

	CREATE META DATA AND SAVE TO DATABASE

********/

var db = require('../config/db')
var publicationCategoryModel = require('../models/category/publication_category')
var metaData = require('../config/meta_data')


// --------------- publication cateogory

metaData.publicationCategory.forEach(function(elem, index, array){
    

 
    publicationCategoryModel.find({name : elem}, function (err, docs) {

        if (!docs.length){
            // element doesn't exist, save it into database
            var publicationCategory = 
                new publicationCategoryModel({
                                            'name': elem
                                            })
            publicationCategory.save(function(err){
                console.log(err)  
            })
        }
    }) // end find
    
}) // end forEach

// ------ end publication cateogory


