// var url = 'mongodb://localhost:27017/Sympozer'
// argv[2] is port of mongodb; argv[3] is database's name
const url = 'mongodb://' + process.argv[2] + ':' + process.argv[3] + '/Sympozer';
// var url = process.argv[2]
let conn = null;

const mongoose = require('mongoose');


exports.connect = function(done) {
  if (!conn){
    conn = mongoose.createConnection(url);
    mongoose.connect(url);
    console.log('Connect String: '+url);
    // console.log(conn)
  }
  return done()
};

exports.get = function(){
  return conn;
};

exports.close = function(done) {
  if (conn) {
    conn.close(function(err, result) {
      conn = null;
      done(err);
    });
  }
};

exports.url = url;
exports.conn = conn;

