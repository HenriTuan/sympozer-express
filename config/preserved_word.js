module.exports = {
	
	confAcronym: [
		'all',
		'user',
		'conference',
		'myconferences',
		'organization',
		'edit',
		'create',
		'rdf-store',
		'roles',
		'person'
	],

	userAcronym: [
		'all',
		'user',
		'conference',
		'organization',
		'edit',
		'create',
		'person'
	],

	orgaAcronym: [
		'all',
		'user',
		'conference',
		'organization',
		'edit',
		'create',
		'person',
		'member'
	]

} /// end module.exports