var app = require('../app')
/**************************************************************************/

var loginUrl = 'http://localhost:8089/login';
var registerUrl = 'http://localhost:8089/register';
var logoutUrl = 'http://localhost:8089/logout';
var moduleLoginUrl = 'http://localhost:8089';
if (process.env.ENV === "production") {
    loginUrl = 'http://login.sympozer.com/login'; // Url de module login
    registerUrl = 'http://login.sympozer.com/register'; // Url de registration
    logoutUrl = 'http://login.sympozer.com/logout'; // Url de déconnexion
    moduleLoginUrl = 'http://login.sympozer.com'; // Url de module login
}

app.use(function (req, res, next) {
    var loginCallbackUrl = encodeURIComponent(req.protocol + '://' + req.get('host') + '/login/callback');
    var logoutCallbackUrl = encodeURIComponent(req.protocol + '://' + req.get('host') + '/logout');

    res.locals.loginUrl = moduleLoginUrl + '/login?service=' + loginCallbackUrl;
    res.locals.registerUrl = moduleLoginUrl + '/register?service=' + loginCallbackUrl;
    res.locals.logoutUrl = moduleLoginUrl + '/logout?service=' + logoutCallbackUrl;
    res.locals.myAccountUrl = moduleLoginUrl + '/user/myAccount';
    return next();
});

app.use('/'			 	, require('../middlewares/global_midwre'))
// app.use('/organization'	, require('../middlewares/organization_midwre'))
app.use('*/conference'	, require('../middlewares/conference_midwre'))


/***************************************************************************/
module.exports = app