module.exports = {

	'publicationCategory':[
		'research',
		'technical',
		'industrial',
		'demo',
		'poster'
	],

	'eventCategory':[
		'conference',
		'track',
		'session',
		'keynote',
		'panel',
		'talk',
		'lunch'
	]

} // end module.exports