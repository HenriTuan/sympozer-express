var app = require('../app')
/**************************************************************************/


app.use('/users', require('../controllers/user_ctrler'))

// authentication controllers
app.use('/', require('../controllers/auth/local_auth_ctrler'))
//app.use('/auth/facebook', 	require('../controllers/auth/facebook_auth_ctrler'))
//app.use('/auth/google', 	require('../controllers/auth/google_auth_ctrler'))
//app.use('/auth/twitter', 	require('../controllers/auth/twitter_auth_ctrler'))
//app.use('/auth/linkedin', 	require('../controllers/auth/linkedin_auth_ctrler'))


// conference controllers
app.use('/conference', 			require('../controllers/conference/create_conf_ctrler'))
app.use('/conference', 			require('../controllers/conference/show_conf_ctrler'))
app.use('/conference',			require('../controllers/conference/delete_conf_ctrler'))
app.use('/conference',			require('../controllers/conference/edit_conf_ctrler'))
app.use('/conference', 			require('../controllers/conference/connect_account_ctrler'))

	//Informations conference
	app.use('/rest/conference/:id', 	require('../controllers/conference/index'));

	// person in conference
	app.use('/conference/:id/person',		require('../controllers/conference/person/create_person_ctrler'))
	app.use('/conference/:id/person',		require('../controllers/conference/person/show_person_ctrler'))
	app.use('/conference/:id/person',		require('../controllers/conference/person/delete_person_ctrler'))
	app.use('/conference/:id/person',		require('../controllers/conference/person/edit_person_ctrler'))

	// role in conference
	app.use('/conference/:id/role', 		require('../controllers/conference/role/create_role_ctrler'))
	app.use('/conference/:id/role', 		require('../controllers/conference/role/show_role_ctrler'))
	app.use('/conference/:id/role', 		require('../controllers/conference/role/edit_role_ctrler'))
	app.use('/conference/:id/role', 		require('../controllers/conference/role/delete_role_ctrler'))

	// organization in conference
	app.use('/conference/:id/organization',	require('../controllers/conference/organization/create_orga_ctrler'))
	app.use('/conference/:id/organization',	require('../controllers/conference/organization/show_orga_ctrler'))
	app.use('/conference/:id/organization',	require('../controllers/conference/organization/edit_orga_ctrler'))
	app.use('/conference/:id/organization',	require('../controllers/conference/organization/delete_orga_ctrler'))
	app.use('/conference/:id/organization',	require('../controllers/conference/organization/member_ctrler'))

	// publication in conference
	app.use('/conference/:id/publication',		require('../controllers/conference/publication/create_pub_ctrler'))
	app.use('/conference/:id/publication',		require('../controllers/conference/publication/show_pub_ctrler'))
	app.use('/conference/:id/publication',		require('../controllers/conference/publication/delete_pub_ctrler'))
	app.use('/conference/:id/publication',		require('../controllers/conference/publication/edit_pub_ctrler'))
	app.use('/conference/:id/publication',		require('../controllers/conference/publication/maker_ctrler'))

	// event in conference
	/*app.use('/conference/:id/event',		require('../controllers/conference/event/create_event_ctrler'))
	app.use('/conference/:id/event',		require('../controllers/conference/event/show_event_ctrler'))
	app.use('/conference/:id/event',		require('../controllers/conference/event/edit_event_ctrler'))
	app.use('/conference/:id/event',		require('../controllers/conference/event/delete_event_ctrler'))
	app.use('/conference/:id/event',		require('../controllers/conference/event/sub_super_event_ctrler'))
	app.use('/conference/:id/api',			require('../controllers/conference/event/api_event_ctrler'))*/
app.use('/conference/:id/event',		require('../controllers/conference/event/index'));
app.use('/conference/:id/event-categories',		require('../controllers/conference/eventCategory/index'));

	// location in conference
	app.use('/conference/:id/location', 	require('../controllers/conference/location/create_location_ctrler'))
	app.use('/conference/:id/location', 	require('../controllers/conference/location/show_location_ctrler'))
	app.use('/conference/:id/location', 	require('../controllers/conference/location/edit_location_ctrler'))
	app.use('/conference/:id/location', 	require('../controllers/conference/location/delete_location_ctrler'))
	app.use('/rest/conference/:id/location', 	require('../controllers/conference/location/rest/index'));


	//Track in conference
	app.use('/conference/:id/tracks' , require('../controllers/conference/track/index'));


// person controllers
// app.use('/person',			require('../controllers/person/create_person_ctrler'))
// app.use('/person',			require('../controllers/person/show_person_ctrler'))
// app.use('/person',			require('../controllers/person/delete_person_ctrler'))
// app.use('/person',			require('../controllers/person/edit_person_ctrler'))

// organization controllers
// app.use('/organization',	require('../controllers/organization/create_orga_ctrler'))
// app.use('/organization',	require('../controllers/organization/show_orga_ctrler'))
// app.use('/organization',	require('../controllers/organization/edit_orga_ctrler'))
// app.use('/organization',	require('../controllers/organization/delete_orga_ctrler'))
// app.use('/organization',	require('../controllers/organization/member_ctrler'))

app.use("/api/publication", require('../controllers/api/publication'));

// REST service controllers
app.use('/REST-service/xml', 	require('../controllers/REST-service/exportXML_ctrler'))


// filehandle controllers
app.use('/file-handle/reader',  require('../controllers/file-handle/reader_ctrler'))
app.use('/conference/:id/file-handle/writer', 	require('../controllers/file-handle/writer_ctrler'))
app.use('/file-handle/image', 	require('../controllers/file-handle/image_ctrler'))


//[TEST]
app.use('/test',  require('../controllers/test_ctrler'))


// error handler
app.use('*',					require('../controllers/error_ctrler'))

/***************************************************************************/
module.exports = app