import RoleDao from '../DAO/v2/role_DAO';
import Role from '../models/v2/role';
import TrackMetier from "./track";

const uuidv1 = require('uuid/v1');

export default class RoleMetier {
    constructor() {
        this.roleDao = new RoleDao();
    }

    /**
     * Permet de récupérer tous les rôles d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            this.roleDao
                .find(idConference)
                .then((roles) => {
                    return resolve(roles);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de role a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de role a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de roles a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.roleDao
                .findPage(idConference, idMin, searchTerm, pageSize)
                .then((roles) => {
                    return resolve(roles);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de role a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de roles a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.roleDao
                .findWithTerm(idConference, searchTerm, pageSize)
                .then((roles) => {
                    return resolve(roles);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer un rôle
     * @param idConference - id de la conférence
     * @param idRole - id du rôle
     * @returns {Promise}
     */
    get(idConference, idRole) {
        return new Promise((resolve, reject) => {
            //We get the role
            this.roleDao
                .get(idRole, idConference)
                .then((role) => {

                    //We get the track which attache to the role
                    new TrackMetier()
                        .getByRole(idConference, idRole)
                        .then((track) => {
                            role.track = track;
                            return resolve(role);
                        })
                        .catch((err) => {
                            return resolve(role);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer un role via son nom
     * @param idConference - id de la conférence
     * @param roleName - nom du role
     * @returns {Promise<any>}
     */
    getByName(idConference, roleName) {
        return new Promise((resolve, reject) => {
            //We get the role
            this.roleDao
                .getByName(idConference, roleName)
                .then((role) => {
                    return resolve(role);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récuperer plusieurs rôles
     * @param idConference - l'id de la conférence
     * @param idRoles - Une liste d'id de roles
     * @returns {Promise}
     */
    getMultiple(idConference, idRoles) {
        return new Promise((resolve, reject) => {
           this.roleDao.mget(idConference, idRoles)
               .then((roles) => {
                   return resolve(roles);
               })
               .catch((err) => {
                   return reject(err);
               });
        });
    }

    /**
     * Permet d'ajouter un rôle
     * @param idConference - l'id de la conférence
     * @param name - le nom du rôle
     * @returns {Promise}
     */
    add(idConference,
        name) {
        return new Promise((resolve, reject) => {
            const role = new Role({
                name: name,
                idConference: idConference,
            });

            this.roleDao
                .add(role)
                .then((role) => {
                    return resolve(role);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer un rôle
     * @param idConference - id de la conférence
     * @param idRole - id du rôle
     * @returns {Promise}
     */
    remove(idConference,
           idRole) {
        return new Promise((resolve, reject) => {
            this.roleDao
                .remove(idRole, idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de modifier un role
     * @param idConference - id de la conférence
     * @param idRole - id du role
     * @param name - nouveau nom du role
     * @returns {Promise}
     */
    update(idConference,
           idRole,
           name) {
        return new Promise((resolve, reject) => {
            const role = {
                name: name,
            };

            this.roleDao
                .update(
                    idRole,
                    idConference,
                    role
                )
                .then(() => {
                    this.get(
                        idConference,
                        idRole,
                    )
                        .then((role) => {
                            return resolve(role);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }
}