import EventDao from '../DAO/v2/event_DAO';
import Event from '../models/v2/event';
import ConferenceMetier from './conference';
import PublicationMetier from "./publication";
import EventCategorieMetier from "./event_category";

const uuidv1 = require('uuid/v1');

export default class EventMetier {
    constructor() {
        this.eventDao = new EventDao();
    }

    /**
     * Permet de récupérer les events liés à une conférence et à un super event
     * @param idConference - id de la conférence
     * @param idSuperEvent - id du super event
     * @returns {Promise}
     */
    find(idConference, idSuperEvent) {
        return new Promise((resolve, reject) => {
            this.eventDao
                .find(idConference, idSuperEvent)
                .then((events) => {
                    return resolve(events);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer des events via l'id d'une publication
     * @param idConference - id de la conférence
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    findByIdPublication(idConference, idPublication) {
        return new Promise((resolve, reject) => {
            this.eventDao.findByIdPublication(idConference, idPublication)
                .then((events) => {
                    return resolve(events);
                })
                .catch((err) => {
                    return reject(err);
                })
        });
    }

    /**
     * Permet de récupérer des events via un id de publication
     * @param idPublication - id d'une publication
     * @returns {Promise<any>}
     */
    findByIdPublicationWithoutConference(idPublication) {
        return new Promise((resolve, reject) => {
            this.eventDao.findByIdPublicationWithoutConference(idPublication)
                .then((events) => {
                    return resolve(events);
                })
                .catch((err) => {
                    return reject(err);
                })
        });
    }

    getTrackByEvent(idConference, idEvent) {
        return new Promise((resolve, reject) => {
            this.get(idConference, idEvent)
                .then((event) => {
                    if (!event) {
                        return reject();
                    }

                    //On regarde si il a une track
                    if (event.idTrack && event.idTrack.toHexString().length > 0) {
                        return resolve(event);
                    }

                    //Sinon on va chercher son superEvent
                    if (!event.idSuperEvent || event.idSuperEvent.length === 0) {
                        return reject();
                    }

                    this.getTrackByEvent(idConference, event.idSuperEvent)
                        .then((event) => {
                            return resolve(event);
                        })
                        .catch(() => {
                            return reject()
                        });
                })
                .catch(() => {
                    return reject();
                });
        });
    }

    /**
     * Permet d'ajouter un event
     * @param idConference - id de la conférence
     * @param data - les données de l'evenement
     * @returns {Promise}
     */
    add(idConference, data) {
        return new Promise(async (resolve, reject) => {
            if (!idConference || !data) {
                return reject("Erreur lors de la récupération des informations");
            }

            //Check data
            if (data.id_track === null && data.idSupEvent === null) {
                return reject("We need a track or super event");
            }

            if (data.idEventCategory === null || data.idEventCategory.length === 0) {
                return reject();
            }

            //On regarde si on doit remonter dans les superEvent pour trouver la track
            if (!data.id_track || data.id_track === null || data.id_track.length === 0) {
                try {
                    const track = await this.getTrackByEvent(idConference, data.idSupEvent);
                    if (!track || !track._id) {
                        return reject("Error get track");
                    }

                    data.id_track = track._id;
                }
                catch (e) {
                    return reject("Error get track");
                }
            }

            //On récupère le type de l'event
            let uri = null;

            try {
                const eventCategorie = await new EventCategorieMetier().get(data.idEventCategory);
                if (eventCategorie) {
                    uri = "https://sympozer.com/" + eventCategorie.name.toLowerCase() + "/" + uuidv1();
                }
            }
            catch (e) {
                return reject(e);
            }


            const newEvent = new Event({
                idConference: idConference,
                idLocation: data.id_location,
                idsSubEvent: [],
                idSuperEvent: data.idSupEvent,
                idTrack: data.id_track || null,
                idEventCategory: data.idEventCategory,
                name: data.name,
                date_end: data.date_end,
                date_start: data.date_start,
                time_start: data.time_start,
                time_end: data.time_end,
                summary: data.description,
                uri: uri,
            });

            //We add the event
            this.eventDao.add(newEvent)
                .then((event) => {
                    //We add this event into conference
                    new ConferenceMetier()
                        .addSubEvent(idConference, event._id)
                        .then(() => {
                            //We add this event into super event
                            this.addSubEvent(idConference, event.idSuperEvent, event._id)
                                .then(() => {
                                    return resolve(event);
                                })
                                .catch((err) => {
                                    return reject(err);
                                });
                            return resolve(event);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer un event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event
     * @returns {Promise}
     */
    get(idConference, idEvent) {
        return new Promise((resolve, reject) => {
            this.eventDao.get(idEvent, idConference)
                .then((event) => {
                    return resolve(event);
                })
                .catch((err) => {
                    return reject(err);
                })
        });
    }

    /**
     * Permet de récupérer un event via son id
     * @param idEvent - id de l'event
     * @returns {Promise<any>}
     */
    getById(idEvent) {
        return new Promise((resolve, reject) => {
            this.eventDao.getById(idEvent)
                .then((event) => {
                    return resolve(event);
                })
                .catch((e) => {
                    return reject(e);
                });
        });
    }

    /**
     * Permet de supprimer tous les event d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            if (!idConference || idConference.length === 0) {
                return reject();
            }

            this.eventDao.removeByConference(idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer un event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event
     * @returns {Promise}
     */
    remove(idConference, idEvent) {
        return new Promise((resolve, reject) => {
            //We get this event
            this.get(idConference, idEvent)
                .then((event) => {
                    //We remove this event
                    this.eventDao.remove(idEvent, idConference)
                        .then(() => {
                            //We remove the event id into conference document
                            new ConferenceMetier()
                                .removeSubEvent(idConference, idEvent)
                                .then(() => {
                                    if (event.idSuperEvent.toString() === idConference) {
                                        return resolve();
                                    } else {
                                        //We remove this event into sub event of super event
                                        this.removeSubEvent(idConference, event.idSuperEvent, idEvent)
                                            .then(() => {
                                                return resolve();
                                            })
                                            .catch((err) => {
                                                return reject(err);
                                            });
                                    }
                                })
                                .catch((err) => {
                                    return reject(err);
                                })
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de modifier un event
     * @param idConference
     * @param idEvent
     * @param data
     * @returns {Promise<any>}
     */
    update(idConference, idEvent, data) {
        return new Promise(async (resolve, reject) => {
            if (!idConference || !data) {
                return reject("Erreur lors de la récupération des informations");
            }

            //On regarde si on doit remonter dans les superEvent pour trouver la track
            if (!data.idTrack || data.idTrack === null || data.idTrack.length === 0) {
                try {
                    const track = await this.getTrackByEvent(idConference, idEvent);
                    if (!track || !track._id) {
                        return reject("Error get track");
                    }

                    data.idTrack = track._id;
                }
                catch (e) {
                    return reject("Error get track");
                }
            }

            //On récup l'event
            this.get(idConference, idEvent)
                .then((event) => {
                    event.idConference = idConference;
                    event.idLocation = data.id_location;
                    event.idTrack = data.idTrack;
                    event.name = data.name;
                    event.date_end = data.date_end;
                    event.date_start = data.date_start;
                    event.time_start = data.time_start;
                    event.time_end = data.time_end;
                    event.summary = data.description;
                    event.idEventCategory = data.idEventCategory;

                    this.eventDao.update(event, idEvent, idConference)
                        .then(() => {
                            return resolve(event);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter un event dans les sub event de l'event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event du super event
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    addSubEvent(idConference, idEvent, idSubEvent) {
        return new Promise((resolve, reject) => {
            this.eventDao.addSubEvent(idConference, idEvent, idSubEvent)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    findAll(idConference) {
        return new Promise((resolve, reject) => {
            this.eventDao.findAll(idConference)
                .then((events) => {
                    return resolve(events);
                })
                .catch((err) => {
                    return reject(err);
                })
        });
    }

    /**
     * Permet de supprimer un event dans les sub event de l'event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event du super event
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    removeSubEvent(idConference, idEvent, idSubEvent) {
        return new Promise((resolve, reject) => {
            this.eventDao.removeSubEvent(idConference, idEvent, idSubEvent)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une publication à un event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    addRelatedPublication(idConference, idEvent, idPublication) {
        return new Promise((resolve, reject) => {
            //On récupère l'event
            this.get(idConference, idEvent)
                .then((event) => {
                    if (!event) {
                        return reject();
                    }

                    //On récupère la publication
                    new PublicationMetier()
                        .get(idConference, idPublication)
                        .then((publication) => {
                            if (!publication) {
                                return reject();
                            }

                            //On regarde si l'event ne contient pas déjà la publication
                            if (event.isEventRelatedTo
                                && event.isEventRelatedTo.length > 0
                                && event.isEventRelatedTo.includes(publication._id)) {
                                return reject();
                            }

                            //On ajoute la publication à l'event
                            this.eventDao.addRelatedPublication(idConference, idEvent, idPublication)
                                .then(() => {
                                    return resolve();
                                })
                                .catch(() => {
                                    return reject();
                                });
                        })
                        .catch(() => {
                            return reject();
                        });
                })
                .catch(() => {
                    return reject();
                });
        });
    }

    /**
     * Permet de récupérer plusieurs events
     * @param idConference - id de la conférence
     * @param idsEvent - ids des events
     * @returns {Promise}
     */
    mget(idConference, idsEvent) {
        return new Promise((resolve, reject) => {
            this.eventDao.mget(idConference, idsEvent)
                .then((events) => {
                    return resolve(events);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une publication à des events
     * @param idConference - id de la conférence
     * @param idsEvent - id des events
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    addRelatedPublicationBulk(idConference, idsEvent, idPublication) {
        return new Promise((resolve, reject) => {
            //On récupère les events
            this.mget(idConference, idsEvent)
                .then((events) => {
                    if (!events || events.length === 0) {
                        return resolve();
                    }

                    //On récupère la publication
                    new PublicationMetier()
                        .get(idConference, idPublication)
                        .then((publication) => {

                            if (!publication) {
                                return reject();
                            }

                            //On ne garde que les events qui ne contiennent pas la publication
                            const eventsSaved = events.filter((event) => {
                                return !event.isEventRelatedTo
                                    || !event.isEventRelatedTo.includes(publication._id);
                            });

                            if (eventsSaved.length === 0) {
                                return resolve();
                            }

                            //On crée les requêtes bulk pour ajouter la publication dans les events
                            let bulks = [];

                            for (const event of eventsSaved) {
                                bulks.push({
                                    updateOne: {
                                        filter: {_id: event._id, idConference: idConference},
                                        update: {$push: {isEventRelatedTo: idPublication}},
                                    }
                                });
                            }

                            if (bulks.length === 0) {
                                return resolve();
                            }

                            Event.bulkWrite(bulks)
                                .then(() => {
                                    return resolve();
                                })
                                .catch(() => {
                                    return reject();
                                });
                        })
                        .catch(() => {
                            return reject();
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une publication à des events
     * @param idConference - id de la conférence
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    removeRelatedPublicationBulk(idConference, idPublication) {
        return new Promise((resolve, reject) => {
            //On récupère les events qui ont la publication
            this.findByIdPublication(idConference, idPublication)
                .then((events) => {
                    if (!events || events.length === 0) {
                        return resolve();
                    }

                    //On ne garde que les events qui contiennent la publication
                    const eventsSaved = events.filter((event) => {
                        return event.isEventRelatedTo
                            && event.isEventRelatedTo.length > 0
                            && event.isEventRelatedTo.some((element) => {
                                return element.equals(idPublication);
                            });
                    });

                    if (eventsSaved.length === 0) {
                        return resolve();
                    }

                    //On crée les requêtes bulk pour supprimer la publication dans les events
                    let bulks = [];

                    for (const event of eventsSaved) {
                        bulks.push({
                            updateOne: {
                                filter: {_id: event._id, idConference: idConference},
                                update: {$pull: {isEventRelatedTo: {$in: [idPublication]}}},
                            }
                        });
                    }

                    if (bulks.length === 0) {
                        return resolve();
                    }

                    Event.bulkWrite(bulks)
                        .then(() => {
                            return resolve();
                        })
                        .catch(() => {
                            return reject();
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer une publication à un event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    removeRelatedPublication(idConference, idEvent, idPublication) {
        return new Promise((resolve, reject) => {
            //On récupère l'event
            this.get(idConference, idEvent)
                .then((event) => {
                    if (!event) {
                        return reject();
                    }

                    //On récupère la publication
                    new PublicationMetier()
                        .get(idConference, idPublication)
                        .then((publication) => {
                            if (!publication) {
                                return reject();
                            }

                            //On regarde si l'event contient la publication
                            if (!event.isEventRelatedTo
                                || event.isEventRelatedTo.length === 0
                                || !event.isEventRelatedTo.some((element) => {
                                    return element.equals(publication._id);
                                })) {
                                return reject();
                            }

                            //On supprime la publication à l'event
                            this.eventDao.removeRelatedPublication(idConference, idEvent, idPublication)
                                .then(() => {
                                    return resolve();
                                })
                                .catch(() => {
                                    return reject();
                                });
                        })
                        .catch(() => {
                            return reject();
                        });
                })
                .catch(() => {
                    return reject();
                });
        });
    }

    /**
     * Permet de récupérer une page de publication a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de publications a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.eventDao
                .findWithTerm(idConference, searchTerm, pageSize)
                .then((events) => {
                    return resolve(events);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }
}