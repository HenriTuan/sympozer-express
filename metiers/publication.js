import PublicationDao from '../DAO/v2/publication_DAO';
import Publication from '../models/v2/publication';
import EventMetier from "./event";

const uuidv1 = require('uuid/v1');

export default class PublicationMetier {
    constructor() {
        this.publicationDao = new PublicationDao();
    }

    /**
     * Permet de récupérer toutes les publications d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            this.publicationDao
                .find(idConference)
                .then((publications) => {
                    return resolve(publications);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer toutes les publications d'une conférence en plus de peupler leur event associé (et leur catégorie)
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    findWithEvent(idConference) {
        return new Promise((resolve, reject) => {
            this.publicationDao
                .findWithEvent(idConference)
                .then((publications) => {
                    return resolve(publications);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }


    /**
     * Permet de récupérer une page de publication a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de publication a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de publications a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.publicationDao
                .findPage(idConference, idMin, searchTerm, pageSize)
                .then((publications) => {
                    return resolve(publications);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de publication a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de publications a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.publicationDao
                .findWithTerm(idConference, searchTerm, pageSize)
                .then((publications) => {
                    return resolve(publications);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une publication d'une conférence
     * @param idConference - id conférence
     * @param idPublication - id publication
     * @returns {Promise}
     */
    get(idConference, idPublication) {
        return new Promise((resolve, reject) => {
            //On récupère la publication
            this.publicationDao
                .get(
                    idPublication,
                    idConference
                )
                .then((publication) => {
                    return resolve(publication);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer une publication d'une conférence
     * @param idConference - id conférence
     * @param idPublication - id publication
     * @returns {Promise}
     */
    remove(idConference, idPublication) {
        return new Promise((resolve, reject) => {
            //On supprime la publication
            this.publicationDao
                .remove(
                    idPublication,
                    idConference
                )
                .then(async () => {
                    try{
                        //On supprime la liaison avec les events
                        await new EventMetier()
                            .removeRelatedPublicationBulk(idConference, idPublication);
                    }
                    catch(e){

                    }

                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une publication via son URI
     * @param uriPublication - URI de la publication
     * @returns {Promise<any>}
     */
    getByUri(uriPublication) {
        return new Promise(async (resolve, reject) => {
           if(!uriPublication || uriPublication.length === 0) {
               return reject("You must specify an URI");
           }

           try {
               const publication = await this.publicationDao.getByUri(uriPublication);
               if(!publication) {
                   return reject('Error when retrieving publication');
               }

               return resolve(publication);
           }
           catch(e) {
               return reject(e);
           }
        });
    }

    /**
     * Permet de supprimer toutes les publications d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            if (!idConference || idConference.length === 0) {
                return reject();
            }

            this.publicationDao.removeByConference(idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une publication dans une conférence
     * @param idConference - id de la conférence
     * @param title - titre de la publication
     * @param abstract - abstract de la publication
     * @param keywords - keywords de la publication
     * @param idAuthors - id des auteurs
     * @param idEvent - id de l'events lié a la publication
     * @param idsEventRelatedTo - ids des events liés à la publication
     * @returns {Promise}
     */
    add(idConference,
        title,
        abstract,
        keywords,
        idAuthors,
        idEvent,
        idsEventRelatedTo) {
        return new Promise((resolve, reject) => {

            const uri = "https://sympozer.com/inproceedings/" + uuidv1();

            //On crée l'objet de la publication
            const publication = new Publication({
                idConference: idConference,
                title: title,
                abstract: abstract,
                keywords: keywords,
                idAuthors: idAuthors,
                idEvent: idEvent,
                uri: uri,
            });

            //On ajoute la publication
            this.publicationDao
                .add(publication)
                .then(async (publication) => {
                    if (!publication) {
                        return reject();
                    }

                    //On ajoute la publication dans les events
                    if (idsEventRelatedTo && idsEventRelatedTo.length > 0) {
                        try {
                            await new EventMetier()
                                .addRelatedPublicationBulk(idConference, idsEventRelatedTo, publication._id);
                            return resolve(publication);
                        }
                        catch (e) {
                            return resolve(publication);
                        }
                    } else {
                        return resolve(publication);
                    }
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    update(idConference, idPublication, changes) {
        return new Promise((resolve, reject) => {
            this.get(idConference, idPublication)
                .then((oldPub) => {
                    const newPub = {
                        idConference: idConference,
                        title: changes.title || oldPub.title,
                        abstract: changes.abstract || oldPub.abstract,
                        keywords: changes.keywords || oldPub.keywords || [],
                        idAuthors: changes.idAuthors || oldPub.idAuthors || [],
                        idEvent: changes.idEvent || oldPub.idEvent
                    };
                    this.publicationDao
                        .update(idPublication, idConference, newPub)
                        .then(() => {
                            this.get(
                                idConference,
                                idPublication
                            )
                                .then((publication) => {
                                    return resolve(publication);
                                })
                                .catch((err) => {
                                    return reject(err);
                                });
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    reject(err)
                });
        });
    }

    removeAuthor(idConference, idPub, idAuthor) {
        return new Promise((resolve, reject) => {
            this.get(idConference, idPub)
                .then((pub) => {
                    this.update(idConference, idPub, {idAuthors: pub.idAuthors.filter((el) => el != idAuthor)})
                        .then((pub) => {
                            return resolve(pub)
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    addAuthors(idConference, idPub, idAuthors) {
        return new Promise((resolve, reject) => {
            this.get(idConference, idPub)
                .then((pub) => {
                    this.update(idConference, idPub, {idAuthors: pub.idAuthors.concat(idAuthors)})
                        .then((pub) => {
                            return resolve(pub)
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    getAllFromAuthor(idConference, idAuthor) {
        return new Promise((resolve, reject) => {
            this.publicationDao.getAllFromAuthor(idConference, idAuthor)
                .then((publications) => {
                    return resolve(publications);
                })
                .catch((err) => {
                    return reject(err);
                })
        })
    }
}