import ConferenceDao from '../DAO/v2/conference_DAO';
import EventMetier from './event';
import LocationMetier from './location';
import TrackMetier from './track';
import PublicationMetier from './publication';
import OrganizationMetier from './organization';
import PersonMetier from './person';

const underscore = require('underscore');
const countryLanguage = require('country-language');
const Conference = require('../models/v2/conference');
const uuidv1 = require('uuid/v1');

export default class ConferenceMetier {
    constructor() {
        this.conferenceDao = new ConferenceDao();
    }

    /**
     * Permet d'ajouter une conférence
     * @param title - le titre de la conférence
     * @param uri - l'uri de la conférence
     * @param acronym - l'acronym de la conférence
     * @param description - la description de la conférence
     * @param homepage - la homepage de la conférence
     * @param start - la date de début de la conférence
     * @param end - la date de fin de la conférence
     * @param country - le pays de la conférence
     * @param region - la région de la conférence
     * @param logoUrl - l'url du logo de la conférence
     * @param lang - la lang de la conférence
     * @param userId - l'id du créateur de la conférence
     * @returns {Promise}
     */
    add(title,
        uri,
        acronym,
        description,
        homepage,
        start,
        end,
        country,
        region,
        logoUrl,
        lang,
        userId) {
        return new Promise((resolve, reject) => {
            let langName = null;

            if (lang) {
                const l = countryLanguage.getLanguage(lang);
                if (l) {
                    langName = l.name[0];
                }
            }

            //On regarde si on a une URI
            if(uri === null || uri.length === 0){
                uri = "https://w3id.org/scholarlydata/conference/eswc2017/" + uuidv1();
            }


            const conferenceModel = new Conference({
                title: title,
                display_acronym: acronym,
                uri: uri,
                description: description,
                homepage: homepage,
                logo_path: logoUrl,
                date_begin: start,
                date_end: end,
                timezone: new Date().getTimezoneOffset() / 60,
                language: {
                    code: lang,
                    name: langName,
                },
                location:
                    {
                        country: country,
                        region: region,
                        formatted_address: country + ', ' + region,
                    },
                _user_id: userId,
                type: 'ConferenceEvent',
            });

            this.conferenceDao
                .add(conferenceModel)
                .then((conference) => {
                    return resolve(conference);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer toutes les conférences d'un utilisateurs
     * @param idUser - l'id de l'utilisateur
     * @returns {Promise}
     */
    findByAuthor(idUser) {
        return new Promise((resolve, reject) => {
            this.conferenceDao
                .getAllByUserAuthor(idUser)
                .then((conferences) => {
                    return resolve(conferences);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer une conférence
     * @param idConference - l'id de la conférence
     * @returns {Promise}
     */
    remove(idConference) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.conferenceDao.removeById(idConference);

                await new EventMetier().removeByConference(idConference);

                await new PersonMetier().removeByConference(idConference);

                await new LocationMetier().removeByConference(idConference);

                await new TrackMetier().removeByConference(idConference);

                await new PublicationMetier().removeByConference(idConference);

                await new OrganizationMetier().removeByConference(idConference);

                return resolve();
            }
            catch(e){
                return reject(e);
            }
        });
    }

    /**
     * Permet de récupérer une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idConference) {
        return new Promise((resolve, reject) => {
            this.conferenceDao
                .get(idConference)
                .then((conference) => {
                    return resolve(conference);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de modifier une conférence
     * @param idConference - id de la conférence à modifier
     * @param title - nouveau titre
     * @param uri - nouvel URI
     * @param acronym - nouvel acronym
     * @param description - nouvelle description
     * @param homepage - nouvelle homepage
     * @param start - nouvelle date de début
     * @param end - nouvelle date de fin
     * @param country - nouveau pays
     * @param region - nouvelle région
     * @param logoUrl - nouveau logo url
     * @param lang - nouvelle langue
     * @param userId - id de l'utilisateur qui modifie la conférence
     * @returns {Promise}
     */
    update(idConference,
           title,
           uri,
           acronym,
           description,
           homepage,
           start,
           end,
           country,
           region,
           logoUrl,
           lang,
           userId) {
        return new Promise((resolve, reject) => {
            let langName = null;

            if (lang) {
                const l = countryLanguage.getLanguage(lang);
                if (l) {
                    langName = l.name[0];
                }
            }

            const conference = {
                title: title,
                display_acronym: acronym,
                uri: uri,
                description: description,
                homepage: homepage,
                logo_path: logoUrl,
                date_begin: start,
                date_end: end,
                timezone: new Date().getTimezoneOffset() / 60,
                language: {
                    code: lang,
                    name: langName,
                },
                location:
                    {
                        country: country,
                        region: region,
                        formatted_address: country + ', ' + region,
                    },
                _user_id: userId,
                type: 'ConferenceEvent',
            };

            this.conferenceDao
                .update(conference, idConference, userId)
                .then(() => {
                    this.get(idConference)
                        .then((conference) => {
                            return resolve(conference);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de regrouper par author toutes les conférences
     * @returns {Promise}
     */
    findAndGroupByAuthor() {
        return new Promise((resolve, reject) => {
            this.conferenceDao
                .find()
                .then((conferences) => {
                    return resolve(underscore.groupBy(conferences, '_user_id'));
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter un event dans les sub event de la conférence
     * @param idConference - id de la conférence
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    addSubEvent(idConference, idSubEvent) {
        return new Promise((resolve, reject) => {
            this.conferenceDao.addSubEvent(idConference, idSubEvent)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer un event dans les sub event de la conférence
     * @param idConference - id de la conférence
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    removeSubEvent(idConference, idSubEvent) {
        return new Promise((resolve, reject) => {
            this.conferenceDao.removeSubEvent(idConference, idSubEvent)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupèrer une conférence par son acronym
     * @param idUser - Identifiant de l'utilisateur courant
     * @param acronym - acronym de la conférence
     * @returns {Promise<any>}
     */
    getByAcronym(idUser, acronym) {
        return new Promise((resolve, reject) => {
            this.conferenceDao.getByAcronym(idUser, acronym)
                .then((conference) => {
                    return resolve(conference);
                })
                .catch((err) => {
                    return reject(err);
                });
        })
    }
}