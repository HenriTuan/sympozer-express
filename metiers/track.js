/**
 * Created by pierremarsot on 20/11/2017.
 */
import TrackDao from '../DAO/v2/track_DAO';
import TrackModel from '../models/v2/track';
import RoleMetier from "./role";
import PersonMetier from "./person";

const uuidv1 = require('uuid/v1');

export default class TrackMetier {
    constructor() {
        this.trackDao = new TrackDao();
    }

    /**
     * Permet de récupérer les persons d'une track
     * @param idConference - id de la conférence
     * @param idTrack - id de la track
     * @returns {Promise}
     */
    getChairs(idConference, idTrack) {
        return new Promise((resolve, reject) => {
            this.get(idTrack, idConference)
                .then((track) => {
                    if (!track) {
                        return reject("Error when get track");
                    }

                    new PersonMetier()
                        .mget(idConference, track.idsPerson)
                        .then((chairs) => {
                            return resolve(chairs);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer toutes les tracks d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            this.trackDao.find(idConference)
                .then((tracks) => {
                    return resolve(tracks);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer toutes les tracks d'une conférence avec le nom de ses sous events
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    findWithSubEvents(idConference) {
        return new Promise((resolve, reject) => {
            this.trackDao.findWithSubEvents(idConference)
                .then((tracks) => {
                    return resolve(tracks);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    getByRole(idConference, idRole){
        return new Promise((resolve, reject) => {
            this.trackDao.getByRole(idConference, idRole)
                .then((tracks) => {
                    return resolve(tracks);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une track
     * @param idTrack - id de la track à récupérer
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idTrack, idConference) {
        return new Promise((resolve, reject) => {
            this.trackDao.get(idTrack, idConference)
                .then((track) => {
                    return resolve(track);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une track via son id
     * @param idTrack - id de la track
     * @returns {Promise<any>}
     */
    getById(idTrack) {
        return new Promise((resolve, reject) => {
            this.trackDao.getById(idTrack)
                .then((track) => {
                    return resolve(track);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une track dans une conférence
     * @param idConference - id de la conférence
     * @param nom - nom de la track
     * @param idsPerson - array of persons id
     * @param isVotable - Boolean pour dire si la track est votable ou non
     */
    add(idConference,
        nom,
        idsPerson,
        isVotable) {
        return new Promise((resolve, reject) => {
            const uri = "https://sympozer.com/track/" + uuidv1();

            isVotable = isVotable || false;

            const track = new TrackModel({
                idConference: idConference,
                name: nom,
                idsPerson: idsPerson || [],
                idsSubEvent: [],
                uri: uri,
                is_votable: isVotable,
            });

            //We add a role for this track
            const roleName = this.generateRoleName(nom);
            new RoleMetier()
                .add(idConference, roleName)
                .then((role) => {
                    //We set the role to the track
                    track.idRole = role._id;

                    //We add the track
                    this.trackDao.add(track)
                        .then((track) => {

                            //We add role into person model
                            new PersonMetier()
                                .addRoleFromPersons(idConference, idsPerson, role._id)
                                .then(() => {
                                    return resolve(track);
                                })
                                .catch(() => {
                                    return resolve(track);
                                });
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer une track
     * @param idTrack - id de la track
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    remove(idTrack, idConference) {
        return new Promise((resolve, reject) => {
            //On récupère la track
            console.log(idTrack, idConference);
            this.get(idTrack, idConference)
                .then((track) => {
                    //On supprime le role de la track
                    new RoleMetier()
                        .remove(idConference, track.idRole)
                        .then(async () => {
                            //On regarde si on a des personnes associées à la track
                            let promise = null;
                            if(track.idsPerson && track.idsPerson.length > 0){
                                console.log("ok");
                                //On supprime le role dans les personnes
                                promise = new PersonMetier()
                                    .removeRoleFromPersons(idConference, track.idsPerson, track.idRole);
                            }

                            try {
                                if(promise) {
                                    console.log("ok");
                                    await promise;
                                    console.log("oki");
                                }

                                //On supprime la track
                                this.trackDao.remove(idTrack, idConference)
                                    .then(() => {
                                        console.log("oki");
                                        //On supprime le role associé
                                        return resolve();
                                    })
                                    .catch(() => {
                                        return reject();
                                    });
                            }
                            catch(e){
                                return reject(e);
                            }
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer toutes les tracks d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            if (!idConference || idConference.length === 0) {
                return reject();
            }

            this.trackDao.removeByConference(idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une track dans une conférence
     * @param idConference - id de la conférence
     * @param idTrack - id de la track
     * @param nom - nouveau nom de la track
     * @param idsPerson - array of persons id
     * @param isVotable - Boolean pour dire si la track est votable ou non
     */
    update(idConference,
           idTrack,
           nom,
           idsPerson,
           isVotable) {
        return new Promise((resolve, reject) => {
            //We get the track
            this.get(idTrack, idConference)
                .then((track) => {
                    if (track) {
                        //We get the role
                        const roleMetier = new RoleMetier();
                        roleMetier.get(idConference, track.idRole)
                            .then((role) => {
                                if (role) {
                                    const personMetier = new PersonMetier();
                                    //We remove from all current chairs the role
                                    personMetier.removeRoleFromPersons(idConference, track.idsPerson, role._id)
                                        .then(() => {
                                            //We generate the new role name
                                            const roleName = this.generateRoleName(nom);

                                            //We update the role
                                            roleMetier.update(idConference, role._id, roleName)
                                                .then(() => {
                                                    //We update the track name and idsPersons
                                                    track.name = nom;
                                                    track.idsPerson = idsPerson || [];
                                                    track.is_votable = isVotable || false;

                                                    //We update in bdd
                                                    this.trackDao.update(idTrack, idConference, track)
                                                        .then(() => {
                                                            //We add the role to new chairs
                                                            personMetier.addRoleFromPersons(idConference, track.idsPerson, role._id)
                                                                .then(() => {
                                                                    return resolve(track);
                                                                })
                                                                .catch(() => {
                                                                    return resolve(track);
                                                                });
                                                        })
                                                        .catch((err) => {
                                                            return reject(err);
                                                        });
                                                })
                                                .catch((err) => {
                                                    return reject(err);
                                                });
                                        })
                                        .catch(() => {
                                            return reject();
                                        });
                                } else {
                                    return reject("Error when getting role");
                                }
                            })
                            .catch((err) => {
                                return reject(err);
                            });
                    } else {
                        return reject("Error when retreiving Track");
                    }
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    generateRoleName(trackName) {
        return "Role track " + trackName;
    }
}