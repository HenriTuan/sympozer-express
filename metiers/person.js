import PersonDao from '../DAO/v2/person_conference_DAO';
import RoleMetier from "./role";
import OrganisationMetier from "./organization";
import PersonConference from '../models/v2/personConference';
import OrganisationModel from '../models/v2/organisation';

const sha1 = require('sha1');
const uuidv1 = require('uuid/v1');

export default class PersonConferenceMetier {
    constructor() {
        this.personDao = new PersonDao();
    }

    /**
     * Permet de récupérer des persons via un array d'id
     * @param idConference - id de la conférence
     * @param idsPerson - array ids person
     * @returns {Promise}
     */
    mget(idConference, idsPerson) {
        return new Promise((resolve, reject) => {
            this.personDao.mget(idConference, idsPerson)
                .then((persons) => {
                    return resolve(persons);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer des persons via un array de label (nom des personnes)
     * @param idConference - id de la conférence
     * @param labels - array ids labels
     * @returns {Promise}
     */
    mgetLabels(idConference, labels) {
        return new Promise((resolve, reject) => {
            this.personDao.mgetLabel(idConference, labels)
                .then((persons) => {
                    return resolve(persons);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer toutes les personnes d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            this.personDao
                .find(idConference)
                .then((personsConference) => {
                    return resolve(personsConference);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de personnes a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de personne a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de personnes a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.personDao
                .findPage(idConference, idMin, searchTerm, pageSize)
                .then((persons) => {
                    return resolve(persons);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de personnes a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de personnes a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.personDao
                .findWithTerm(idConference, searchTerm, pageSize)
                .then((persons) => {
                    return resolve(persons);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une personne d'une conférence
     * @param idConference - id conférence
     * @param idPersonConference - id person conference
     * @returns {Promise}
     */
    get(idConference, idPersonConference) {
        return new Promise((resolve, reject) => {
            this.personDao
                .get(
                    idPersonConference,
                    idConference
                )
                .then((personConference) => {
                    return resolve(personConference)
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer une personne d'une conférence
     * @param idConference - id conférence
     * @param idPersonConference - id person conference
     * @returns {Promise}
     */
    remove(idConference, idPersonConference) {
        return new Promise((resolve, reject) => {
            let organisationMetier = new OrganisationMetier();
            this.personDao
                .remove(
                    idPersonConference,
                    idConference
                )
                .then(() => {
                    organisationMetier.getByPerson(idConference, idPersonConference)
                        .then(organisations => {
                            if (organisations) {
                                const idOrganisations = organisations.map(orga => orga.id);
                                let bulks = [];

                                idOrganisations.forEach(idOrga => {
                                    bulks.push({
                                        updateOne: {
                                            filter: {_id: idOrga, idConference: idConference},
                                            update: {$pull: {idPersons: {$in: [idPersonConference]}}}
                                        }
                                    });
                                });

                                if (bulks.length === 0) {
                                    return resolve();
                                }

                                OrganisationModel.bulkWrite(bulks)
                                    .then(() => {
                                        return resolve();
                                    })
                                    .catch(() => {
                                        return reject();
                                    });
                            }
                            else
                                return resolve();
                        })
                        .catch(err => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer toutes les personnes d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            if (!idConference || idConference.length === 0) {
                return reject();
            }

            this.personDao.removeByConference(idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une personne dans une conférence
     * @param idConference - id de la conférence
     * @param firstname - firstname
     * @param lastname - lastname
     * @param avaPath - path de l'avatar
     * @param email - email
     * @param homepage - url homepage
     * @param country - pays
     * @param socialAccount - lien réseau social
     * @returns {Promise}
     */
    add(idConference,
        firstname,
        lastname,
        avaPath,
        email,
        homepage,
        country,
        socialAccount,
        idRoles,) {
        return new Promise((resolve, reject) => {
            const sha = sha1(email);
            const displayname = firstname + ' ' + lastname;
            if (socialAccount === null || socialAccount === undefined) {
                socialAccount = [];
            }

            const uri = "https://sympozer.com/person/" + uuidv1();

            const personConference = new PersonConference({
                idConference: idConference,
                idRoles: idRoles,
                displayname: displayname,
                firstname: firstname,
                lastname: lastname,
                ava_path: avaPath,
                email: email,
                sha: sha,
                homepage: homepage,
                country: country,
                social_account: socialAccount,
                uri: uri,
            });

            this.personDao
                .add(personConference)
                .then((personConference) => {
                    return resolve(personConference);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de modifier une personne d'une conférence
     * @param idConference - id de la conférence
     * @param idPersonConference - id de la personne
     * @param firstname - firstname
     * @param lastname - lastname
     * @param avaPath - path de l'avatar
     * @param email - email
     * @param homepage - url homepage
     * @param country - pays
     * @param socialAccount - lien réseau social
     * @param idRoles - les roles associés
     * @returns {Promise}
     */
    update(idConference,
           idPersonConference,
           firstname,
           lastname,
           avaPath,
           email,
           homepage,
           country,
           socialAccount,
           idRoles,) {
        return new Promise((resolve, reject) => {
            const sha = sha1(email);
            const displayname = firstname + ' ' + lastname;
            if (socialAccount === null || socialAccount === undefined) {
                socialAccount = [];
            }

            const personConference = {
                idConference: idConference,
                displayname: displayname,
                firstname: firstname,
                lastname: lastname,
                ava_path: avaPath,
                email: email,
                sha: sha,
                homepage: homepage,
                country: country,
                social_account: socialAccount,
                idRoles: idRoles
            };

            this.personDao
                .update(idPersonConference, idConference, personConference)
                .then(() => {
                    this.get(
                        idConference,
                        idPersonConference
                    )
                        .then((personConference) => {
                            return resolve(personConference);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer un role à une liste de personnes
     * @param idConference - id de la conférence
     * @param idsPerson - ids des personnes
     * @param idRole - id du role à supprimer
     * @returns {Promise}
     */
    removeRoleFromPersons(idConference, idsPerson, idRole) {
        return new Promise((resolve, reject) => {
            let bulks = [];

            for (const id of idsPerson) {
                bulks.push({
                    updateOne: {
                        filter: {_id: id, idConference: idConference},
                        update: {$pull: {idRoles: {$in: [idRole]}}}
                    }
                });
            }

            if (bulks.length === 0) {
                return resolve();
            }

            PersonConference.bulkWrite(bulks)
                .then(() => {
                    return resolve();
                })
                .catch(() => {
                    return reject();
                });
        });
    }

    /**
     * Permet d'ajouter un role à une liste de personnes
     * @param idConference - id de la conférence
     * @param idsPerson - array ids person
     * @param idRole - id du role
     * @returns {Promise}
     */
    addRoleFromPersons(idConference, idsPerson, idRole) {
        return new Promise((resolve, reject) => {
            let bulks = [];

            for (const id of idsPerson) {
                bulks.push({
                    updateOne: {
                        filter: {_id: id, idConference: idConference},
                        update: {$addToSet: {idRoles: idRole}},
                    }
                });
            }

            if (bulks.length === 0) {
                return resolve();
            }

            PersonConference.bulkWrite(bulks)
                .then(() => {
                    return resolve();
                })
                .catch(() => {
                    return reject();
                });
        });
    }

    /**
     * Permet de récuperer plusieurs personnes
     * @param idConference - l'id de la conférence
     * @param idPersons - Une liste d'id de personnes
     * @returns {Promise}
     */
    getMultiple(idConference, idPersons) {
        return new Promise((resolve, reject) => {
            this.mget(idConference, idPersons)
                .then((persons) => {
                    return resolve(persons);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récuperer plusieurs personnes avec un même rôle
     * @param idConference - l'id de la conférence
     * @param idRole - L'id du role
     * @returns {Promise}
     */
    getAllWithRole(idConference, idRole) {
        return new Promise((resolve, reject) => {
            this.personDao.findByRole(idConference, idRole)
                .then((persons) => {
                    return resolve(persons);
                })
                .catch((err) => {
                    return reject(err);
                })
        });
    }
}