import LocationDao from '../DAO/v2/location_DAO';

const Location = require('../models/v2/location');
const uuidv1 = require('uuid/v1');

export default class LocationMetier {
    constructor() {
        this.locationDao = new LocationDao();
    }

    /**
     * Permet d'ajouter une location à une conférence
     * @param idConference - id de la conférence
     * @param name - name de la location
     * @param nbPlace - Nombre de place dans la salle
     * @param equipements - Liste (string) des équipements disponibles
     * @param gps - Coordonnées GPS [latitude, longitude]
     */
    add(idConference,
        name,
        nbPlace,
        equipements,
        gps,
        address,
        description) {
        return new Promise((resolve, reject) => {
            if (gps && gps.length !== 2) {
                return reject();
            }

            let uri = "https://sympozer.com/location/" + uuidv1();

            const location = new Location({
                idConference: idConference,
                name: name,
                nb_place: nbPlace,
                equipements: equipements,
                gps: gps,
                address: address,
                description: description,
                uri: uri,
            });

            this.locationDao
                .add(location)
                .then((location) => {
                    return resolve(location);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer toutes les locations d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            if (!idConference || idConference.length === 0) {
                return reject();
            }

            this.locationDao.removeByConference(idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer une location d'une conférence
     * @param idConference - l'id de la conférence
     * @param idLocation - id de la location
     * @returns {Promise}
     */
    remove(idConference, idLocation) {
        return new Promise((resolve, reject) => {
            this.locationDao
                .remove(idLocation, idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une location d'une conférence
     * @param idConference - id de la conférence
     * @param idLocation - id de la location
     * @returns {Promise}
     */
    get(idConference, idLocation) {
        return new Promise((resolve, reject) => {
            this.locationDao
                .get(idLocation, idConference)
                .then((location) => {
                    return resolve(location);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer toutes les locations d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            this.locationDao
                .find(idConference)
                .then((locations) => {
                    return resolve(locations);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de location a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de location a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de locations a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.locationDao
                .findPage(idConference, idMin, searchTerm, pageSize)
                .then((locations) => {
                    return resolve(locations);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de location a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de locations a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.locationDao
                .findWithTerm(idConference, searchTerm, pageSize)
                .then((locations) => {
                    return resolve(locations);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de modifier une conférence
     * @param idConference - id de la conférence à modifier
     * @param idLocation - id de la location à modifier
     * @param name - nouveau name de la location
     * @param nbPlace - Nombre de place dans la salle
     * @param equipements - Liste (string) des équipements disponibles
     * @param gps - Coordonnées GPS [latitude, longitude]
     */
    update(idConference,
           idLocation,
           name,
           nbPlace,
           equipements,
           gps,
           address,
           description) {
        return new Promise((resolve, reject) => {
            const location = {
                idConference: idConference,
                name: name,
                nb_place: nbPlace,
                equipements: equipements,
                gps: gps || [],
                address: address,
                description: description,
            };

            this.locationDao
                .update(idLocation, idConference, location)
                .then(() => {
                    console.log("then update");
                    this.get(idConference, idLocation)
                        .then((location) => {
                            return resolve(location);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    console.log(err);
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une location via son nom
     * @param idConference - id de la conférence
     * @param nameLocation - nom de la location
     * @returns {Promise<any>}
     */
    getByName(idConference, nameLocation) {
        return new Promise((resolve, reject) => {
            this.locationDao.getByName(idConference, nameLocation)
                .then((location) => {
                    return resolve(location);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }
}