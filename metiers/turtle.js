/*
    Create by Pierre Marsot on 16/01/2018
*/
import Event from "../models/v2/event";
import Track from '../models/v2/track';
import Role from '../models/v2/role';
import Organisation from "../models/v2/organisation";
import PersonConference from "../models/v2/personConference";
import Publication from "../models/v2/publication";
import Location from "../models/v2/location";
import PersonMetier from './person';
import CategorieEvent from './event_category';
import TrackMetier from './track';
import RoleMetier from "./role";
import LocationMetier from "./location";
import PublicationMetier from "./publication";
import ConferenceMetier from './conference';
import EventMetier from './event';

const mongoose = require('mongoose');
const rdflib = require('rdflib');
const fs = require('fs');
const momentjs = require('moment');

export default class TurtleMetier {
    constructor() {
        this._store = null;
        this._$rdf = rdflib;
        this._baseUrl = "https://w3id.org";
        this._prefixScholary = "PREFIX scholary: <https://w3id.org/scholarlydata/ontology/conference-ontology.owl#> \n";
        this._prefixSchema = "PREFIX schema: <http://www.w3.org/2000/01/rdf-schema#> \n";
        this._prefixSchemaUri = "http://www.w3.org/2000/01/rdf-schema";
        this._prefixScholaryUri = "https://w3id.org/scholarlydata/ontology/conference-ontology.owl";
        this._prefixFoafUri = "http://xmlns.com/foaf/0.1/";
        this._prefixFoaf = "PREFIX foaf: <http://xmlns.com/foaf/0.1/> \n";
        this.uriConference = null;
        this.idConference = null;
        this.mapTracks = new Map();
        this.mapEvents = new Map();
        this.mapPublicationEvent = new Map();
        this.mapPublicationsBdd = new Map();
        this.mapTracksBdd = new Map();
        this.mapEventsBdd = new Map();
        this.mapSPO = new Map();
    }

    __addSpo(sujet, predicat, object) {
        if (!sujet || !predicat || !object) {
            return false;
        }

        let sujetMap;

        if (this.mapSPO.has(sujet) === false) {
            this.mapSPO.set(sujet, new Map());
        }

        sujetMap = this.mapSPO.get(sujet);

        let predicatMap;
        if (sujetMap.has(predicat) === false) {
            sujetMap.set(predicat, new Set());
        }

        predicatMap = sujetMap.get(predicat);

        if (predicatMap.has(object) === false) {
            switch (typeof object) {
                case "string":
                    predicatMap.add("\"" + object + "\"");
                    break;
                default:
                    predicatMap.add(object);
                    break;
            }
        }

        return true;
    }

    ___haveSpo(sujet, predicat, object) {
        if (!sujet || !predicat || !object) {
            return true;
        }

        if (this.mapSPO.has(sujet) === false) {
            return false;
        }

        let sujetMap = this.mapSPO.get(sujet);

        if (sujetMap.has(predicat) === false) {
            return false;
        }

        const predicatMap = sujetMap.get(predicat);

        return predicatMap.has(object);
    }

    __getDateByFormat(date) {
        const m = momentjs(date);
        if (!m) {
            return null;
        }

        return m.format("MM/DD/YYYY");
    }

    __getTimeByFormat(date) {
        const m = momentjs(date);
        if (!m) {
            return null;
        }

        return m.format("hh:mm");
    }

    /**
     * Permet de parser et initialiser le store de rdflib
     * @param path - chemin relatif du fichier (à partir du root du projet)
     * @returns {boolean}
     * @private
     */
    _loadDataset(path) {
        const that = this;
        const mimeType = 'text/turtle';
        const store = that._$rdf.graph();

        //On lit le fichier en string
        const rdfData = fs.readFileSync(__basedir + "/" + path).toString();

        try {
            //On parse le string
            that._$rdf.parse(rdfData, store, that._baseUrl, mimeType);
            that._store = store;
            that._store.fetcher = null;

            return true;
        }
        catch (e) {
            return false;
        }
    }

    /**
     * Permet de run une query sparql dans le store
     * @param query - la query en string
     * @param callbackResult - une fonction de callback (result) => ...
     * @param callbackDone - callback quand la query est finie
     * @private
     */
    _runQuery(query, callbackResult, callbackDone) {
        try {
            const that = this;
            const querySparql = that._$rdf.SPARQLToQuery(query, false, that._store);

            that._store.query(querySparql, callbackResult, null, callbackDone);
        }
        catch (e) {
            throw e;
        }
    }

    /**
     * Permet d'extraire un champ d'un resultat d'une query
     * @param result - l'objet retourné dans le callback de la query
     * @param field - le champ cherché
     * @returns {null}
     */
    __extractValue(result, field) {
        //On regarde si on a un résultat
        if (!result) {
            return null;
        }

        //On regarde si le champ spécifié est valide
        if (!field || field.length === 0 || !field.includes("?")) {
            return null;
        }

        //on récupère le noeud
        const nodeField = result[field];

        if (!nodeField) {
            return null;
        }

        //On retourne la valeur
        return nodeField.value;
    }

    /**
     * Permet d'extraire l'objet de la conférence dans le dataset
     */
    extractConference(idUser) {
        return new Promise((resolve, reject) => {
            if (!idUser || idUser.length === 0) {
                return reject();
            }

            const that = this;

            const query =
                that._prefixScholary +
                that._prefixSchema +
                "SELECT DISTINCT ?id ?label ?acronym ?endDate ?startDate ?location \n" +
                "WHERE {\n" +
                " ?id a scholary:Conference . \n" +
                " ?id schema:label ?label . \n" +
                " ?id scholary:acronym ?acronym . \n" +
                " ?id scholary:endDate ?endDate . \n" +
                " ?id scholary:location ?location . \n" +
                " ?id scholary:startDate ?startDate . \n" +
                "} limit 1";

            let haveConference = false;
            let conference = null;
            let promise = null;

            const resultFunction = async (result) => {
                if (!haveConference) {
                    haveConference = true;

                    promise = new Promise(async (resolve, reject) => {
                        //On récupère les informations de la conf
                        const uri = that.__extractValue(result, '?id');
                        const label = that.__extractValue(result, '?label');
                        let acronym = that.__extractValue(result, '?acronym');
                        let endDate = that.__extractValue(result, '?endDate');
                        const location = that.__extractValue(result, '?location');
                        let startDate = that.__extractValue(result, '?startDate');

                        let pays = null;
                        let ville = null;

                        //On regarde si on peut récupérer la ville / pays dans le location
                        if (location && location.length > 0 && location.includes(',')) {
                            const split = location.split(',');
                            if (split.length > 1) {
                                ville = split[0];
                                pays = split[1];
                            }
                        }

                        if (!acronym || acronym.length === 0) {
                            return reject();
                        }

                        acronym = acronym.toLowerCase();

                        //On parse les dates
                        startDate = that.__getDateByFormat(startDate);
                        endDate = that.__getDateByFormat(endDate);

                        //On regarde si on a déjà la conférence
                        const conferenceMetier = new ConferenceMetier();

                        try {
                            conference = await conferenceMetier.add(
                                label,
                                uri,
                                acronym,
                                "",
                                null,
                                startDate,
                                endDate,
                                pays,
                                ville,
                                null,
                                null,
                                idUser
                            );

                            //On set les variables locales
                            this.uriConference = uri;
                            this.idConference = conference._id;

                            //On set notre SPO
                            that.__addSpo(uri, "a", that._prefixScholaryUri + "#Conference");
                            that.__addSpo(uri, that._prefixSchemaUri + "#label", label);
                            that.__addSpo(uri, that._prefixScholaryUri + "#acronym", acronym);
                            that.__addSpo(uri, that._prefixScholaryUri + "#endDate", endDate);
                            that.__addSpo(uri, that._prefixScholaryUri + "#location", location);
                            that.__addSpo(uri, that._prefixScholaryUri + "#startDate", startDate);

                            return resolve(conference);
                        }
                        catch (e) {
                            return reject(e);
                        }
                    });
                }
            };

            const doneFunction = () => {
                Promise.all([promise])
                    .then(() => {
                        return resolve(conference);
                    })
                    .catch(() => {
                        return reject();
                    });
            };

            that._runQuery(query, resultFunction, doneFunction);
        });
    }

    /**
     * Permet d'extraire les tracks du dataset
     * * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    extractTrack(idConference) {
        return new Promise((resolve) => {
            const that = this;

            const query =
                this._prefixScholary +
                this._prefixSchema +
                " SELECT DISTINCT ?id ?label ?description \n" +
                " WHERE { \n" +
                " ?id a scholary:Track . \n" +
                " ?id schema:label ?label . \n" +
                " ?id scholary:description ?description . \n" +
                "}";

            //On initialise le bulk des tracks / roles
            let bulk = Track.collection.initializeUnorderedBulkOp();
            let bulkRole = Role.collection.initializeUnorderedBulkOp();
            const mapTrackRole = new Map();

            //Fonction qui récolte les tracks
            const resultFunction = (result) => {
                if (result) {
                    //On extrait les données
                    const uri = that.__extractValue(result, '?id');
                    const label = that.__extractValue(result, '?label');
                    const description = that.__extractValue(result, '?description');

                    //On vérifie les données
                    if (!uri || uri.length === 0 || !label || label.length === 0 || !description || description.length === 0) {
                        return false;
                    }

                    const trackMetier = new TrackMetier();
                    const roleName = trackMetier.generateRoleName(label);

                    if (!roleName || roleName.length === 0) {
                        return false;
                    }

                    //On ajoute dans la map le nom de la track et le nom généré du role
                    mapTrackRole.set(label, {
                        roleName: roleName,
                        uriTrack: uri,
                    });

                    //On ajoute à notre map SPO
                    that.__addSpo(uri, "a", that._prefixScholaryUri + "#Track");
                    that.__addSpo(uri, that._prefixSchemaUri + "#label", label);
                    that.__addSpo(uri, that._prefixScholaryUri + "#description", description);
                }
            };

            //Fonction lorsque la récolte des tracks est finie
            const doneFunction = async () => {
                const roleMetier = new RoleMetier();
                for (const [key, value] of mapTrackRole) {
                    if (!value || value.length === 0) {
                        continue;
                    }

                    //Key => label track
                    //Value => role name

                    //On va chercher le role en bdd
                    try {
                        const role = await roleMetier.getByName(idConference, value.roleName);
                        let idRole = null;

                        //Si on a pas le role, alors il faut le créer
                        if (!role) {
                            //On génère un id pour le role
                            idRole = new mongoose.Types.ObjectId();

                            //On ajoute un bulk role
                            bulkRole.find({
                                idConference: idConference,
                                name: value,
                            })
                                .upsert()
                                .updateOne({
                                    $setOnInsert: {
                                        _id: idRole,
                                    },
                                    $set: {
                                        name: value.roleName,
                                        idConference: idConference,
                                    }
                                });
                        }
                        //Sinon, on ajoute le role dans la track
                        else {
                            idRole = role._id;
                        }

                        //On génère ou récupère l'id
                        let idTrack = new mongoose.Types.ObjectId();
                        if (that.mapTracksBdd.has(value.uriTrack)) {
                            const t = that.mapTracksBdd.get(value.uriTrack);
                            if (t !== null) {
                                idTrack = t._id;
                            }
                        }

                        //On ajoute le bulk pour la track
                        bulk.find({
                            uri: value.uriTrack,
                            idConference: idConference
                        })
                            .upsert()
                            .updateOne({
                                $setOnInsert: {
                                    _id: idTrack
                                },
                                $set: {
                                    name: key,
                                    idConference: idConference,
                                    idRole: idRole,
                                    uri: value.uriTrack,
                                }
                            });

                        //On ajoute la track dans notre map
                        that.mapTracks.set(value.uriTrack, idTrack);
                    }
                    catch (e) {

                    }
                }

                //On execute notre bulk role
                bulkRole.execute()
                    .then(() => {
                        //On execute notre bulk track
                        bulk.execute()
                            .then(() => {
                                return resolve();
                            })
                            .catch(() => {
                                return resolve();
                            });
                    })
                    .catch(() => {
                        return resolve();
                    });
            };

            //On lance la query dans le store
            that._runQuery(query, resultFunction, doneFunction);
        });
    }

    /**
     * Permet d'extraire les organisations du dataset
     * @param idConference - id de la conférence où ajouter / modifier les organisations
     * @returns {Promise<any>}
     */
    extractOrganisation(idConference) {
        return new Promise((resolve) => {
            const that = this;

            const query =
                that._prefixScholary +
                that._prefixSchema +
                " SELECT DISTINCT ?id ?label ?name \n" +
                " WHERE {\n" +
                " ?id a scholary:Organisation . \n" +
                " ?id schema:label ?label . \n" +
                " ?id scholary:name ?name . \n" +
                '}';

            //TODO => différence entre label et name ?

            //On initialise le bulk des organisations
            let bulk = Organisation.collection.initializeUnorderedBulkOp();
            const promises = [];

            //Fonction qui récolte les organisation
            const resultFunction = (result) => {
                promises.push(new Promise((resolveOrganisation) => {
                    if (result) {
                        //On extrait les données de l'organisation
                        let uriOrganisation = that.__extractValue(result, '?id');
                        const label = that.__extractValue(result, '?label');
                        const name = that.__extractValue(result, '?name');

                        //On set notre SPO
                        that.__addSpo(uriOrganisation, "a", "<" + that._prefixScholaryUri + "#Organisation>");
                        that.__addSpo(uriOrganisation, that._prefixSchemaUri + "#label", label);
                        that.__addSpo(uriOrganisation, that._prefixScholaryUri + "#name", name);

                        //On vérifie les données
                        if (!uriOrganisation || uriOrganisation.length === 0 || !label || label.length === 0 || !name || name.length === 0) {
                            return resolveOrganisation();
                        }

                        if (uriOrganisation.includes("'")) {
                            uriOrganisation = uriOrganisation.replace(/'/g, "");
                        }

                        //On extrait les personnes qui sont affiliées à l'organisation
                        //La query
                        const queryAffiliationDuringEvent =
                            that._prefixScholary +
                            that._prefixSchema +
                            " SELECT DISTINCT ?id ?isAffiliationOf \n " +
                            " WHERE {\n" +
                            " ?id a scholary:AffiliationDuringEvent . \n " +
                            " ?id scholary:withOrganisation <" + uriOrganisation + "> . \n " +
                            " ?id scholary:isAffiliationOf ?isAffiliationOf . \n" +
                            "}";

                        //Tableau des promises pour récupérer les personnes affiliées à une orga
                        const promisesAffiliationDuringEvent = [];
                        const labelsPerson = [];

                        const resultAffiliationDuringEvent = (result) => {
                            promisesAffiliationDuringEvent.push(new Promise((resolveAffiliationDuringEvent) => {
                                if (result) {
                                    const uriPersonAffiliationDuringEvent = that.__extractValue(result, '?isAffiliationOf');
                                    const uriAffiliationDuringEvent = that.__extractValue(result, '?id');

                                    if (uriPersonAffiliationDuringEvent && uriPersonAffiliationDuringEvent.length > 0) {
                                        //On set dans notre spo
                                        that.__addSpo(uriAffiliationDuringEvent, "a", "<" + that._prefixScholaryUri + "#AffiliationDuringEvent>");
                                        that.__addSpo(uriAffiliationDuringEvent, that._prefixScholaryUri + "#withOrganisation", "<" + uriOrganisation + ">");
                                        that.__addSpo(uriAffiliationDuringEvent, that._prefixScholaryUri + "#isAffiliationOf", "<" + uriPersonAffiliationDuringEvent + ">");

                                        //Query pour récupérer la personne
                                        const queryPerson =
                                            that._prefixScholary +
                                            that._prefixSchema +
                                            " SELECT DISTINCT ?label \n" +
                                            " WHERE {\n" +
                                            " <" + uriPersonAffiliationDuringEvent + "> a scholary:Person . \n" +
                                            " <" + uriPersonAffiliationDuringEvent + "> schema:label ?label . \n" +
                                            "}";


                                        //On récupère les labels des personnes
                                        const resultFunctionPerson = (result) => {
                                            if (result) {
                                                const label = that.__extractValue(result, "?label");
                                                if (label && label.length > 0) {

                                                    //On ajoute dans le SPO
                                                    that.__addSpo(uriPersonAffiliationDuringEvent, "a", "<" + that._prefixScholaryUri + "#Person>");
                                                    that.__addSpo(uriPersonAffiliationDuringEvent, that._prefixSchemaUri + "#label", label);

                                                    labelsPerson.push(label);

                                                    //On set dans notre SPO
                                                    //TODO => that.__addSpo(uri)
                                                }
                                            }
                                        };

                                        //On récupère les personnes pour en extraire les ids
                                        const doneFunctionPerson = () => {
                                            return resolveAffiliationDuringEvent();
                                        };

                                        //On lance la query pour récupérer la personne
                                        that._runQuery(queryPerson, resultFunctionPerson, doneFunctionPerson);
                                    } else {
                                        return resolveAffiliationDuringEvent();
                                    }
                                } else {
                                    return resolveAffiliationDuringEvent();
                                }
                            }));
                        };

                        const doneAffiliationDuringEvent = () => {
                            Promise.all(promisesAffiliationDuringEvent)
                                .then(() => {
                                    if (labelsPerson.length > 0) {
                                        new PersonMetier()
                                            .mgetLabels(idConference, labelsPerson)
                                            .then((persons) => {
                                                if (!persons || persons.length === 0) {
                                                    return resolveOrganisation();
                                                }

                                                //On extrait les ids
                                                const idsPersonsAffiliate = persons.map((p) => p._id);

                                                //Sinon on bulk l'orga
                                                bulk.find({
                                                    name: label,
                                                    idConference: idConference
                                                })
                                                    .upsert()
                                                    .updateOne({
                                                        $setOnInsert: {
                                                            _id: new mongoose.Types.ObjectId()
                                                        },
                                                        $set: {
                                                            uri: uriOrganisation,
                                                            name: label,
                                                            description: name,
                                                            idConference: idConference,
                                                            idPersons: idsPersonsAffiliate,
                                                        }
                                                    });

                                                //On met fin à la promise
                                                return resolveOrganisation();
                                            })
                                            .catch(() => {
                                                return resolveOrganisation();
                                            });
                                    } else {
                                        //Sinon on bulk l'orga
                                        bulk.find({
                                            name: label,
                                            idConference: idConference
                                        })
                                            .upsert()
                                            .updateOne({
                                                $setOnInsert: {
                                                    _id: new mongoose.Types.ObjectId()
                                                },
                                                $set: {
                                                    uri: uriOrganisation,
                                                    name: label,
                                                    description: name,
                                                    idConference: idConference,
                                                }
                                            });
                                        return resolveOrganisation();
                                    }
                                })
                                .catch(() => {
                                    return resolveOrganisation();
                                });
                        };

                        //Lancement de la query pour récupérer les personnes affiliées
                        that._runQuery(queryAffiliationDuringEvent, resultAffiliationDuringEvent, doneAffiliationDuringEvent);
                    } else {
                        return resolveOrganisation();
                    }
                }));
            };

            //Fonction lorsque la récolte des organisations est finie
            const doneFunction = () => {
                Promise.all(promises)
                    .then(() => {
                        //On execute notre bulk
                        bulk.execute()
                            .then(() => {
                                return resolve();
                            })
                            .catch(() => {
                                return resolve();
                            });
                    })
                    .catch(() => {
                        return resolve();
                    });
            };

            //On run la query dans le dataset
            that._runQuery(query, resultFunction, doneFunction);
        });
    }

    /**
     * Permet d'extraire les Person du dataset
     * @param idConference - id de la conférence dans laquelle ajouter les personnes
     * @returns {Promise<any>}
     */
    extractPerson(idConference) {
        return new Promise((resolve, reject) => {
            const that = this;

            const query =
                that._prefixScholary +
                that._prefixSchema +
                that._prefixFoaf +
                " SELECT DISTINCT ?id ?label ?mbox_sha1sum ?name \n" +
                " WHERE {\n" +
                " ?id a scholary:Person . \n" +
                " ?id schema:label ?label . \n" +
                " ?id scholary:name ?name . \n" +
                " ?id foaf:mbox_sha1sum ?mbox_sha1sum . \n" +
                '}';

            //TODO => différence entre label et name ?

            //On initialise le bulk des organisations
            let bulk = PersonConference.collection.initializeUnorderedBulkOp();

            //Fonction qui récolte les tracks
            const resultFunction = (result) => {
                if (result) {
                    //On extrait les données
                    const uri = that.__extractValue(result, '?id');
                    const label = that.__extractValue(result, '?label');
                    const name = that.__extractValue(result, '?name');
                    const mboxSha1sum = that.__extractValue(result, '?mbox_sha1sum');

                    //On vérifie les données
                    if (!uri || uri.length === 0 || !label || label.length === 0 || !name || name.length === 0) {
                        return false;
                    }

                    if (!mboxSha1sum || mboxSha1sum.length === 0) {
                        return false;
                    }

                    const tmp = name.split(" ");
                    let lastName, firstName;

                    if (tmp.length > 1) {
                        lastName = tmp.pop();
                        firstName = tmp.join(" ");
                    } else {
                        firstName = name;
                        lastName = "";
                    }

                    //On ajoute dans le SPO
                    that.__addSpo(uri, "a", "<" + that._prefixScholaryUri + "#Person>");
                    that.__addSpo(uri, that._prefixSchemaUri + "#label", label);
                    that.__addSpo(uri, that._prefixScholaryUri + "#name", name);
                    that.__addSpo(uri, that._prefixFoafUri + "mbox_sha1sum", mboxSha1sum);

                    //On ajoute au bulk pour add / update la track
                    bulk.find({
                        name: label,
                        idConference: idConference
                    })
                        .upsert()
                        .updateOne({
                            $setOnInsert: {
                                _id: new mongoose.Types.ObjectId()
                            },
                            $set: {
                                uri: uri,
                                firstname: firstName,
                                lastname: lastName,
                                displayname: name,
                                mboxSha1sum: mboxSha1sum,
                                idConference: idConference,
                            }
                        });
                }
            };

            //Fonction lorsque la récolte des organisations est finie
            const doneFunction = () => {
                //On execute notre bulk
                bulk.execute()
                    .then(() => {
                        return resolve();
                    })
                    .catch(() => {
                        return resolve();
                    });
            };

            //On run la query dans le dataset
            that._runQuery(query, resultFunction, doneFunction);
        });
    }

    /**
     * Permet d'extraire les publications dans le dataset
     * @param idConference - id de la conférence dans laquelle ajouter les publications
     * @returns {Promise<any>}
     */
    extractInProceedings(idConference) {
        return new Promise((resolve, reject) => {
            const that = this;

            const queryPersons =
                that._prefixScholary +
                that._prefixSchema +
                that._prefixFoaf +
                " SELECT DISTINCT ?id ?label ?abstract ?title ?relatesToEvent \n" +
                " WHERE {\n" +
                " ?id a scholary:InProceedings . \n" +
                " ?id schema:label ?label . \n" +
                " ?id scholary:title ?title . \n" +
                " ?id scholary:abstract ?abstract . \n" +
                " OPTIONAL { ?id scholary:relatesToEvent ?relatesToEvent . } \n" +
                "}";

            //TODO => différence entre label et name ?

            //On initialise le bulk des publications
            let bulk = Publication.collection.initializeUnorderedBulkOp();
            let promisesRoot = [];

            //Fonction qui récolte les organisations
            const resultFunction = (result) => {
                promisesRoot.push(new Promise((resolve2) => {
                    if (result) {
                        //On extrait les données
                        const uri = that.__extractValue(result, '?id');
                        const label = that.__extractValue(result, '?label');
                        const abstract = that.__extractValue(result, '?abstract');
                        const title = that.__extractValue(result, '?title');
                        const relatesToEvent = that.__extractValue(result, '?relatesToEvent');

                        //On vérifie les données
                        if (!uri || uri.length === 0 || !label || label.length === 0 || !abstract || abstract.length === 0) {
                            return resolve2();
                        }

                        if (!title || title.length === 0) {
                            return resolve2();
                        }

                        //On ajoute dans le SPO
                        that.__addSpo(uri, "a", "<" + that._prefixScholaryUri + "#InProceedings>");
                        that.__addSpo(uri, that._prefixSchemaUri + "#label", label);
                        that.__addSpo(uri, that._prefixScholaryUri + "#title", title);
                        that.__addSpo(uri, that._prefixScholaryUri + "#abstract", abstract);

                        if(relatesToEvent) {
                            that.__addSpo(uri, that._prefixScholaryUri + "#relatesToEvent", "<" + relatesToEvent + ">");
                        }

                        //On va récupérer les keywords
                        let keywords = [];

                        //Query pour récupérer les keywords
                        const queryKeywords =
                            this._prefixScholary +
                            this._prefixSchema +
                            " SELECT DISTINCT ?keyword \n" +
                            " WHERE {\n" +
                            " <" + uri + "> a scholary:InProceedings . \n" +
                            " <" + uri + "> scholary:keyword ?keyword . \n" +
                            "}";

                        //Function qui va streamer les keywords
                        const resultFunctionKeyword = (result) => {
                            if (result) {
                                const keyword = that.__extractValue(result, '?keyword');
                                if (!keyword) {
                                    return false;
                                }

                                //On ajoute au SPO
                                that.__addSpo(uri, that._prefixScholaryUri + "#keyword", keyword);

                                //On ajoute au tableau
                                keywords.push(keyword);
                            }
                        };

                        //Function de fin de récupération des keywords
                        const doneFunctionKeyword = () => {

                            //On va récupérer les authorList
                            const queryAuthorList =
                                that._prefixScholary +
                                that._prefixFoaf +
                                " SELECT DISTINCT ?authorList \n" +
                                " WHERE {\n" +
                                " <" + uri + "> a scholary:InProceedings . \n" +
                                " <" + uri + "> scholary:hasAuthorList ?authorList . \n" +
                                "}";

                            let uriDebutAuthorList = null;

                            //Fonction qui va récupérer la ressource authorList de début
                            const resultFunctionAuthorList = (result) => {
                                if (result) {

                                    uriDebutAuthorList = that.__extractValue(result, '?authorList');

                                    if(uriDebutAuthorList) {
                                        that.__addSpo(uri, that._prefixScholaryUri + "#hasAuthorList", "<" + uriDebutAuthorList + ">");
                                    }
                                }
                            };

                            //Fonction de fin de récupération de la ressource authorList de début
                            const doneFunctionAuthorList = () => {
                                //On regarde si on a bien un authorList
                                if (uriDebutAuthorList && uriDebutAuthorList.length > 0) {

                                    //Query pour récupérer la premier auteur dans les authorsList
                                    const queryGetFirstAuthorListItem =
                                        that._prefixScholary +
                                        " SELECT DISTINCT ?hasFirstItem \n" +
                                        "WHERE {\n" +
                                        " <" + uriDebutAuthorList + "> a scholary:List . \n" +
                                        " <" + uriDebutAuthorList + "> scholary:hasFirstItem ?hasFirstItem . \n" +
                                        "}";

                                    let uriFirstAuthorListItem = null;

                                    //Fonction qui va récupérer le stream de l'uri de la première personne (ie : normalement que 1)
                                    const resultFunctionGetFirstAuthorListItem = (result) => {
                                        if (result) {
                                            uriFirstAuthorListItem = that.__extractValue(result, '?hasFirstItem');

                                            if(uriFirstAuthorListItem) {
                                                that.__addSpo(uriDebutAuthorList, "a", "<" + that._prefixScholaryUri + "#List>");
                                                that.__addSpo(uriDebutAuthorList, that._prefixScholaryUri + "#hasFirstItem", "<" + uriFirstAuthorListItem + ">");
                                            }
                                        }
                                    };

                                    //Fonction de fin qui va récupérer le stream de l'uri de la première personne
                                    const doneFunctionGetFirstAuthorListItem = async () => {
                                        try {
                                            //Création du tableau de fin des labels des personnes
                                            let labels = [];

                                            //On récupère les labels des personnes
                                            await that.getAuthorsLabelFromAuthorList(uriFirstAuthorListItem, labels, idConference);

                                            //On récupère les personnes via les labels
                                            new PersonMetier()
                                                .mgetLabels(idConference, labels)
                                                .then((personnes) => {
                                                    //On regarde si on a bien des personnes
                                                    if (!personnes || personnes.length === 0) {
                                                        that.addBulkPublication(bulk, idConference, title, abstract, keywords, [], uri, relatesToEvent);
                                                        return resolve2();
                                                    }

                                                    //On ordonne les ids
                                                    const idsPersons = [];

                                                    labels.forEach((l) => {
                                                        if (l && l.length > 1) {
                                                            personnes.forEach((p) => {
                                                                if (p && p.displayname) {
                                                                    if (p.displayname === l) {
                                                                        idsPersons.push(p._id);
                                                                        return false;
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    });

                                                    //Ajout du bulk
                                                    that.addBulkPublication(bulk, idConference, title, abstract, keywords, idsPersons, uri, relatesToEvent);

                                                    //On met fin à la promesse
                                                    return resolve2();
                                                })
                                                .catch(() => {
                                                    return resolve2();
                                                });

                                        }
                                        catch (e) {
                                            //Ajout du bulk
                                            that.addBulkPublication(bulk, idConference, title, abstract, keywords, []);

                                            //On met fin à la promesse
                                            return resolve2();
                                        }
                                    };

                                    that._runQuery(queryGetFirstAuthorListItem, resultFunctionGetFirstAuthorListItem, doneFunctionGetFirstAuthorListItem);
                                } else {
                                    //Ajout du bulk
                                    that.addBulkPublication(bulk, idConference, title, abstract, keywords, []);

                                    //On met fin à la promesse
                                    return resolve2();
                                }
                            };

                            //On run la query pour récupérer la ressource de début des authorList
                            that._runQuery(queryAuthorList, resultFunctionAuthorList, doneFunctionAuthorList);
                        };

                        //On run la query pour récupérer les keywords
                        that._runQuery(queryKeywords, resultFunctionKeyword, doneFunctionKeyword);

                    }
                }));
            };

            //Fonction lorsque la récolte des organisations est finie
            const doneFunction = () => {
                Promise.all(promisesRoot)
                    .then(() => {
                        //On execute notre bulk
                        bulk.execute()
                            .then(() => {
                                return resolve();
                            })
                            .catch(() => {
                                return resolve();
                            });
                    })
                    .catch(() => {
                        return reject();
                    });
            };

            //On run la query dans le dataset
            that._runQuery(queryPersons, resultFunction, doneFunction);
        });
    }

    /**
     * Permet d'ajouter au bulk d'insert des publications
     * @param bulk - le bulk des publications
     * @param idConference - id de la conférence où ajouter la conférence
     * @param title - le titre de la publication
     * @param abstract - l'abstract de la publication
     * @param keywords - les keywords de la publication
     * @param idsAuthorList - les ids des model person des authro list
     * @param uri - uri de la publication
     * @param relatesToEvent - uri de l'event lié à la publication
     */
    addBulkPublication(bulk, idConference, title, abstract, keywords, idsAuthorList, uri, relatesToEvent) {
        const that = this;

        //On init l'id de la publication
        let idPublication = null;

        //On regarde si on a pas déjà la publication dans notre BDD
        if (that.mapPublicationsBdd.has(uri)) {
            const p = that.mapPublicationsBdd.get(uri);
            if (!p) {
                return;
            }

            idPublication = p._id;
        }
        //Sinon on génère un id mongo
        else {
            idPublication = new mongoose.Types.ObjectId();
        }

        //Création du bulk
        bulk.find({
            uri: uri,
            idConference: idConference
        })
            .upsert()
            .updateOne({
                $setOnInsert: {
                    _id: idPublication,
                },
                $set: {
                    uri: uri,
                    title: title,
                    abstract: abstract,
                    keywords: keywords,
                    idAuthors: idsAuthorList || [],
                }
            });

        //On regarde si la publication est liée à un event
        if (relatesToEvent && relatesToEvent.length > 0) {
            const hasPublicationsRelatedTo = that.mapPublicationEvent.has(relatesToEvent);
            if (hasPublicationsRelatedTo === true) {
                const arrayPublicationsRelatedTo = that.mapPublicationEvent.get(relatesToEvent);
                arrayPublicationsRelatedTo.push(idPublication);
                that.mapPublicationEvent.set(relatesToEvent, arrayPublicationsRelatedTo);
            } else {
                that.mapPublicationEvent.set(relatesToEvent, [idPublication]);
            }
        }
    }

    /**
     * Permet de récupérer les labels des personnes à partir d'un AuthorList
     * @param hasFirstItem - l'uri de la première personne de l'AuthorList
     * @param labels - un tableau vide
     * @param idConference - l'id de la conférence
     * @returns {Promise<any>}
     */
    getAuthorsLabelFromAuthorList(hasFirstItem, labels, idConference) {
        return new Promise((resolve, reject) => {
            if (hasFirstItem == null) {
                return resolve();
            }

            if (hasFirstItem.length === 0) {
                return resolve();
            }

            const that = this;

            //Création de la query pour récupérer le ListItem
            const query =
                that._prefixScholary +
                " SELECT DISTINCT ?hasContent ?next \n" +
                " WHERE {\n" +
                " <" + hasFirstItem + "> a scholary:ListItem . \n" +
                " <" + hasFirstItem + "> scholary:hasContent ?hasContent . \n" +
                " OPTIONAL { <" + hasFirstItem + "> scholary:next ?next . } \n" +
                "}";

            let hasContent = null;
            let next = null;

            const resultFunction = (result) => {
                if (result) {
                    hasContent = that.__extractValue(result, '?hasContent');
                    next = that.__extractValue(result, '?next');

                    //On ajoute au SPO
                    that.__addSpo(hasFirstItem, "a", "<" + that._prefixScholaryUri + "#ListItem>");
                    that.__addSpo(hasFirstItem, that._prefixScholaryUri + "#hasContent", "<" + hasContent + ">");
                    if(next) {
                        that.__addSpo(hasFirstItem, that._prefixScholaryUri + "#next", "<" + next + ">");
                    }
                }
            };

            const doneFunction = () => {
                if (!hasContent) {
                    return resolve();
                }

                const queryPerson =
                    that._prefixScholary +
                    that._prefixSchema +
                    " SELECT DISTINCT ?label \n" +
                    " WHERE {\n" +
                    " <" + hasContent + "> a scholary:Person . \n" +
                    " <" + hasContent + "> schema:label ?label . \n" +
                    "}";

                //Fonction qui récolte la personne
                const resultFunctionPerson = (result) => {
                    if (result) {
                        //On extrait le label
                        const label = that.__extractValue(result, '?label');

                        //On vérifie les données
                        if (!label || label.length === 0) {
                            return resolve();
                        }

                        //On ajoute le label de la personne
                        labels.push(label);

                        //On ajoute au SPO
                        that.__addSpo(hasContent, "a", "<"  + that._prefixScholaryUri + "#Person>");
                        that.__addSpo(hasContent, that._prefixSchemaUri + "#label", label);
                    }
                };

                //Fonction lorsque la récolte de la personne est finie
                const doneFunctionPerson = () => {
                    //On regarde si on a un authorList suivant
                    if (next && next.length > 0) {
                        //Si c'est le cas, on rapelle la fonction pour récupérer la personne suivante
                        that.getAuthorsLabelFromAuthorList(next, labels, idConference)
                            .then(() => {
                                return resolve();
                            })
                            .catch(() => {
                                return reject();
                            });
                    } else {
                        return resolve();
                    }
                };

                //On run la query dans le dataset
                that._runQuery(queryPerson, resultFunctionPerson, doneFunctionPerson);
            };

            //On run la query pour récupérer le ListItem
            that._runQuery(query, resultFunction, doneFunction);
        });
    }

    /**
     * Permet de récupérer les catégories d'event
     * @returns {Promise<any>}
     */
    getCategorieEvent() {
        return new Promise((resolve, reject) => {
            new CategorieEvent()
                .find()
                .then((categoriesEvent) => {
                    return resolve(categoriesEvent);
                })
                .catch(() => {
                    return reject();
                });
        });
    }

    /**
     * Permet d'extraire un type d'event dans le dataset
     * @param idConference - id de la conférence
     * @param originalTypeEvent - type d'event
     * @param bulkLocation - bulk des locations
     * @returns {Promise<any>}
     */
    extractEvent(idConference, originalTypeEvent, bulkLocation) {
        return new Promise(async (resolve, reject) => {
            const that = this;
            let haveObjectBulkLocation = false;
            let mapLocations = new Map();
            let categoriesEvent = [];

            //On regarde si on a des tracks en mémoire
            if (this.mapTracks.size === 0) {
                return reject();
            }

            //On récupère les catégories d'event
            try {
                categoriesEvent = await that.getCategorieEvent();
                if (!categoriesEvent || categoriesEvent.length === 0) {
                    return reject();
                }
            }
            catch (e) {
                return reject();
            }

            const queryEventOrganisedEvent =
                that._prefixScholary +
                that._prefixSchema +
                " SELECT ?id ?label ?startDate ?endDate ?description ?location ?isSubEventOf \n " +
                " WHERE {\n" +
                " ?id a scholary:" + originalTypeEvent + " . \n" +
                " ?id schema:label ?label . \n" +
                " ?id scholary:startDate ?startDate . \n" +
                " ?id scholary:endDate ?endDate . \n" +
                " OPTIONAL { ?id scholary:location ?location . } \n" +
                " OPTIONAL { ?id scholary:description ?description . } \n" +
                "}";

            const promises = [];
            let events = [];

            const resultFunctionOrganisedEvent = (result) => {
                promises.push(new Promise(async (resolveSession, rejectSession) => {
                    if (result) {

                        const id = that.__extractValue(result, '?id');
                        const label = that.__extractValue(result, '?label');
                        const description = that.__extractValue(result, '?description');
                        const locationName = that.__extractValue(result, '?location');
                        let idLocation = mapLocations.get(locationName);
                        const startDateTime = that.__extractValue(result, '?startDate');
                        const endDateTime = that.__extractValue(result, '?endDate');

                        if (!id || id.length === 0) {
                            return rejectSession();
                        }

                        if (!label || label.length === 0) {
                            return rejectSession();
                        }

                        if (!startDateTime || startDateTime.length === 0) {
                            return rejectSession();
                        }

                        if (!endDateTime || endDateTime.length === 0) {
                            return rejectSession();
                        }

                        //On ajoute au SPO
                        that.__addSpo(id, "a", "<" + that._prefixScholaryUri + "#" + originalTypeEvent + ">");
                        that.__addSpo(id, that._prefixSchemaUri + "#label", label);
                        that.__addSpo(id, that._prefixScholaryUri + "#startDate", startDateTime);
                        that.__addSpo(id, that._prefixScholaryUri + "#endDate", endDateTime);
                        if(locationName && locationName.length > 0) {
                            that.__addSpo(id, that._prefixScholaryUri + "#location", locationName);
                        }
                        if(description && description.length > 0) {
                            that.__addSpo(id, that._prefixScholaryUri + "#description", description);
                        }

                        //On regarde si on a la location
                        if (locationName && locationName.length > 0 && !mapLocations.has(locationName)) {
                            idLocation = new mongoose.Types.ObjectId();

                            bulkLocation.find({
                                name: locationName,
                                idConference: idConference
                            })
                                .upsert()
                                .updateOne({
                                    $setOnInsert: {
                                        _id: idLocation
                                    },
                                    $set: {
                                        name: locationName,
                                    }
                                });
                            mapLocations.set(locationName, idLocation);
                            haveObjectBulkLocation = true;
                        }

                        //On récupère les types de l'event
                        let typesEvent = null;
                        let categorieEvent = null;
                        try {
                            typesEvent = await that.extractTypesRessource(id);
                            if (!typesEvent || typesEvent.length === 0) {
                                return resolveSession();
                            }

                            if (typesEvent.includes("Conference")) {
                                return resolveSession();
                            }

                            //On récupère le type d'event mongoose
                            for (const typeEvent of typesEvent) {
                                for (const c of categoriesEvent) {
                                    if (c.name === typeEvent && typesEvent !== "Conference") {
                                        categorieEvent = c;
                                        break;
                                    }
                                }

                                if (categorieEvent) {
                                    break;
                                }
                            }

                            if (!categorieEvent) {
                                return resolveSession();
                            }
                        }
                        catch (e) {
                            return resolveSession();
                        }

                        //On formate les dates
                        const startDate = that.__getDateByFormat(startDateTime);
                        const endDate = that.__getDateByFormat(endDateTime);

                        const startTime = that.__getTimeByFormat(startDateTime);
                        const endTime = that.__getTimeByFormat(endDateTime);

                        //On va récupérer les events parent
                        const queryIsSubEventOf =
                            that._prefixSchema +
                            that._prefixScholary +
                            " SELECT ?isSubEventOf \n " +
                            " WHERE { \n" +
                            " <" + id + "> a scholary:" + originalTypeEvent + " . \n " +
                            " <" + id + "> scholary:isSubEventOf ?isSubEventOf . \n" +
                            "}";

                        const promisesIsSubEventOf = [];
                        let subEventsOf = [];

                        const resultFunctionIsSubEventOf = (result) => {
                            promisesIsSubEventOf.push(new Promise((resolveIsSubEventOf) => {
                                if (!result) {
                                    return resolveIsSubEventOf();
                                }

                                const isSubEventOf = that.__extractValue(result, '?isSubEventOf');

                                if (!isSubEventOf || isSubEventOf.length === 0) {
                                    return resolveIsSubEventOf();
                                }

                                //On ajoute au tableau
                                subEventsOf.push(isSubEventOf);

                                //On ajoute au SPO
                                that.__addSpo(id, that._prefixScholaryUri + "#isSubEventOf", "<" + isSubEventOf + ">");

                                return resolveIsSubEventOf();
                            }));
                        };

                        const doneFunctionIsSubEventOf = () => {
                            Promise.all(promisesIsSubEventOf)
                                .then(() => {

                                    //On supprime la conférence dans les isSubEventOf
                                    subEventsOf = subEventsOf.filter((e) => {
                                        if (!e) {
                                            return false;
                                        }

                                        return e !== that.uriConference;
                                    });

                                    //On va récupérer les events fils
                                    const queryHasSubEvent =
                                        that._prefixSchema +
                                        that._prefixScholary +
                                        " SELECT ?hasSubEvent \n " +
                                        " WHERE { \n" +
                                        " <" + id + "> a scholary:" + originalTypeEvent + " . \n " +
                                        " <" + id + "> scholary:hasSubEvent ?hasSubEvent . \n" +
                                        "}";

                                    const promisesHasSubEventOf = [];
                                    const hasSubEvents = [];

                                    //Fonction pour récupérer les sub event
                                    const resultFunctionHashSubEvent = (result) => {
                                        promisesHasSubEventOf.push(new Promise((resolveHasSubEvent) => {
                                            if (!result) {
                                                return resolveHasSubEvent();
                                            }

                                            const hasSubEvent = that.__extractValue(result, '?hasSubEvent');

                                            if (!hasSubEvent || hasSubEvent.length === 0) {
                                                return resolveHasSubEvent();
                                            }

                                            //On ajoute au tableau
                                            hasSubEvents.push(hasSubEvent);

                                            //On ajoute au SPO
                                            that.__addSpo(id, that._prefixScholaryUri + "#hasSubEvent", "<" + hasSubEvent + ">");

                                            return resolveHasSubEvent();

                                        }));
                                    };

                                    //Fonction de fin de récupération des sub events
                                    const doneFunctionHashSubEvent = () => {
                                        Promise.all(promisesHasSubEventOf)
                                            .then(async () => {
                                                //On va chercher la Track (si on en a une)
                                                let uriTrack = null;
                                                let uriSuperEvent = null;

                                                //On regarde si l'event est lié à une Track
                                                for (const uriSubEvent of subEventsOf) {
                                                    if (that.mapTracks.has(uriSubEvent) === true) {
                                                        uriTrack = uriSubEvent;//that.mapTracks.get(uriSubEvent);
                                                        break;
                                                    }
                                                }

                                                //Si on a récupéré uen Track, on la supprime du tableau
                                                if (uriTrack !== null && uriTrack.length > 0) {
                                                    subEventsOf = subEventsOf.filter((e) => {
                                                        return e !== uriTrack;
                                                    });
                                                }

                                                //On regarde si on a un super event
                                                if (subEventsOf.length > 0) {
                                                    uriSuperEvent = subEventsOf.shift();
                                                }

                                                //On regarde si c'est une Session
                                                let isSession = categorieEvent.name === "Session";
                                                if (isSession) {
                                                    uriSuperEvent = idConference;
                                                }

                                                //On génère l'id de l'event
                                                let idEvent = new mongoose.Types.ObjectId();

                                                //On regarde si on a pas déjà l'event en BDD
                                                if (that.mapEventsBdd.has(id)) {
                                                    const e = that.mapEventsBdd.get(id);
                                                    if (e !== null) {
                                                        idEvent = e._id;
                                                    }
                                                }

                                                //On ajoute l'uri de l'event et son label dans la map
                                                that.mapEvents.set(id, idEvent);

                                                let isEventRelatedTo = [];

                                                if (that.mapPublicationEvent.has(id) === true) {
                                                    isEventRelatedTo = that.mapPublicationEvent.get(id);
                                                }

                                                events.push({
                                                    idEvent: idEvent,
                                                    hasSubEvents: hasSubEvents,
                                                    isSession: isSession,
                                                    uriSuperEvent: uriSuperEvent,
                                                    name: label,
                                                    idConference: idConference,
                                                    idLocation: idLocation,
                                                    idTrack: that.mapTracks.get(uriTrack),
                                                    idEventCategory: categorieEvent._id,
                                                    date_end: endDate,
                                                    date_start: startDate,
                                                    summary: description,
                                                    idsSubEvent: [],
                                                    isEventRelatedTo: isEventRelatedTo,
                                                    idSuperEvent: null,
                                                    time_end: endTime,
                                                    time_start: startTime,
                                                    uri: id,
                                                });

                                                return resolveSession();
                                            })
                                            .catch((e) => {
                                                return rejectSession();
                                            });
                                    };

                                    that._runQuery(queryHasSubEvent, resultFunctionHashSubEvent, doneFunctionHashSubEvent);
                                })
                                .catch(() => {
                                    return rejectSession();
                                });
                        };

                        that._runQuery(queryIsSubEventOf, resultFunctionIsSubEventOf, doneFunctionIsSubEventOf);
                    } else {
                        return rejectSession();
                    }
                }));
            };

            const doneFunctionOrganisedEvent = () => {
                if (promises.length === 0) {
                    return resolve(events);
                }
                Promise.all(promises)
                    .then(async () => {
                        return resolve(events);
                    })
                    .catch((e) => {
                        return reject();
                    });
            };

            that._runQuery(queryEventOrganisedEvent, resultFunctionOrganisedEvent, doneFunctionOrganisedEvent);
        });
    }

    hasTrack(uriEvents) {
        return new Promise((resolve, reject) => {
            if (!uriEvents || uriEvents.length === 0) {
                return reject();
            }

            const that = this;

            const uri = uriEvents.shift();
            if (!uri || uri.length === 0) {
                return reject();
            }

            const query =
                that._prefixScholary +
                that._prefixSchema +
                " SELECT ?label \n " +
                " WHERE {\n" +
                " <" + uri + "> a scholary:Track . \n" +
                " <" + uri + "> schema:label ?label . \n" +
                "}";

            const resultFunction = (result) => {
                if (result) {
                    const label = that.__extractValue(result, '?label');
                    if (label && label.length > 0) {
                        //On ajoute au SPO
                        that.__addSpo(uri, "a", "<" + that._prefixScholaryUri + "#Track>");
                        that.__addSpo(uri, that._prefixSchemaUri + "#label", label);

                        return resolve(label);
                    }
                }
            };

            const doneFunction = () => {
                that.hasTrack(uriEvents)
                    .then((label) => {
                        return resolve(label);
                    })
                    .catch(() => {
                        return reject();
                    });
            };

            that._runQuery(query, resultFunction, doneFunction);

        });
    }

    extractTypesRessource(uriRessource) {
        return new Promise((resolve, reject) => {
            const that = this;

            if (!uriRessource || uriRessource.length === 0) {
                return reject();
            }

            const query =
                that._prefixScholary +
                that._prefixSchema +
                " SELECT ?type \n " +
                " WHERE { \n" +
                " <" + uriRessource + "> a ?type . \n" +
                "}";

            const promises = [];
            const types = [];

            const resultFunction = (result) => {
                promises.push(new Promise((resolveQuery, rejectQuery) => {
                    if (!result) {
                        return rejectQuery();
                    }

                    const type = that.__extractValue(result, '?type');
                    if (!type || type.length === 0) {
                        return rejectQuery();
                    }

                    if (!type.includes("#")) {
                        return rejectQuery();
                    }

                    const splits = type.split('#');

                    types.push(splits[splits.length - 1]);

                    return resolveQuery();
                }));
            };

            const doneFunction = () => {
                Promise.all(promises)
                    .then(() => {
                        return resolve(types);
                    })
                    .catch(() => {
                        return reject();
                    });
            };

            that._runQuery(query, resultFunction, doneFunction);
        });
    }

    __extractResiduelTuple() {
        const that = this;
        const query = that._prefixScholary +
            that._prefixSchema +
            " SELECT ?s ?p ?o \n " +
            " WHERE { \n" +
            " ?s ?p ?o . \n" +
            "}";


        let spo = [];

        const resultFunction = (result) => {
            if (result) {
                const s = that.__extractValue('?s');
                const p = that.__extractValue('?p');
                const o = that.__extractValue('?o');

                if(!that.___haveSpo(s, p, o)) {
                    spo.push({
                        s,
                        p,
                        o,
                    });
                }
            }
        };

        const doneFunction = () => {
            //On génère le string
            let str = "";

            for (const objSpo of spo) {
                str += "<" + objSpo.s + "> <" + objSpo.p + "> " + objSpo.o + "\n";
            }

            //TODO => ajouter le résidu dans Mongo
        };

        this.runQuery(query, resultFunction, doneFunction);
    }

    /**
     * Permet d'importer un dataset en bdd
     * @param idUser
     * @param path - chemin relatif du fichier (à partir du root du projet)
     * @returns {Promise<boolean>}
     */
    importDataset(idUser, path) {
        return new Promise(async (resolve, reject) => {
            const that = this;
            if (!path || path.length === 0) {
                return reject();
            }

            if (!this._loadDataset(path)) {
                return reject();
            }

            try {
                //On ajoute ou modifie la conférence puis on la récupère
                const conference = await this.extractConference(idUser);

                //On vérifie que l'on ait la conférence
                if (!conference) {
                    return reject();
                }

                //On récupère toutes les publications de la conférence
                const publicationsAlreadyStore = await new PublicationMetier().find(conference._id);

                //On ajoute dans notre map locale les publications
                if (publicationsAlreadyStore !== null && publicationsAlreadyStore.length > 0) {
                    for (const publication of publicationsAlreadyStore) {
                        that.mapPublicationsBdd.set(publication.uri, publication);
                    }
                }

                //On récupère toutes les tracks de la conférence
                const tracks = await new TrackMetier().find(conference._id);
                if (tracks !== null && tracks.length > 0) {
                    for (const track of tracks) {
                        that.mapTracksBdd.set(track.uri, track);
                    }
                }

                //On va chercher tous les events de la conférence
                const eventsAlreadyStore = await new EventMetier().findAll(conference._id);
                if (eventsAlreadyStore !== null && eventsAlreadyStore.length > 0) {
                    for (const e of eventsAlreadyStore) {
                        that.mapEventsBdd.set(e.uri, e);
                    }
                }

                //On ajoute ou modifie les personnes
                await this.extractPerson(conference._id);
                console.log("bulk person done");

                //On ajoute ou modifie les tracks
                await this.extractTrack(conference._id);
                console.log("bulk track done");

                //On ajoute ou modifier les organisations
                await this.extractOrganisation(conference._id);
                console.log("bulk organisation done");

                //On ajoute ou modifie les publications
                await this.extractInProceedings(conference._id);
                console.log("bulk inProceedings done");

                const typesEvent = ["Talk", "OrganisedEvent"];
                let bulkSession = Event.collection.initializeUnorderedBulkOp();
                let bulkLocation = Location.collection.initializeUnorderedBulkOp();
                let events = [];
                for (const typeEvent of typesEvent) {
                    events = events.concat(await this.extractEvent(conference._id, typeEvent, bulkLocation));
                }

                if (events.length > 0) {
                    for (const e of events) {
                        if (!e || e === undefined) {
                            continue;
                        }

                        //On récupère l'id de l'event
                        const idEvent = e.idEvent;
                        delete e.idEvent;

                        //On va récupérer l'id du super event
                        if (e.isSession === true) {
                            e.idSuperEvent = e.uriSuperEvent;
                            delete e.isSession;
                            delete e.uriSuperEvent;
                        }
                        else if (e.uriSuperEvent !== null && e.uriSuperEvent.length > 0) {
                            e.idSuperEvent = that.mapEvents.get(e.uriSuperEvent);
                            delete e.uriSuperEvent;
                        }

                        //On va chercher les sous events
                        if (e.hasSubEvents && e.hasSubEvents.length > 0) {
                            e.idsSubEvent = [];
                            for (const subEvent of e.hasSubEvents) {
                                e.idsSubEvent.push(that.mapEvents.get(subEvent));
                            }

                            delete e.hasSubEvents;
                        }

                        //On ajoute l'event dans notre bulk d'event
                        bulkSession.find({
                            uri: e.uri,
                            idConference: conference._id
                        })
                            .upsert()
                            .updateOne({
                                $setOnInsert: {
                                    _id: idEvent
                                },
                                $set: {
                                    ...e
                                }
                            });
                    }

                    //On bulk les locations
                    try {
                        await bulkLocation.execute();
                    }
                    catch (e) {

                    }

                    //On bulk les events
                    try {
                        await bulkSession.execute();
                    }
                    catch (e) {
                        return reject();
                    }

                    console.log("end event");

                    /*that.mapSPO.forEach((key, value) => {
                        console.log(key);
                        console.log(value);
                    });*/

                    this.__extractResiduelTuple();

                    return resolve(conference);
                } else {
                    return resolve(conference);
                }
            }
            catch (e) {
                console.log(e, "error");
                return reject(e);
            }
        });
    }
}
