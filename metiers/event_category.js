import EventCategoryDao from '../DAO/v2/event_category_DAO';
import EventCategory from '../models/category/event_category';

const ID_EVENT_CATEGORY_SESSION = 1;
const ID_EVENT_CATEGORY_TRACK = 2;
const ID_EVENT_CATEGORY_TUTORIAL = 3;
const ID_EVENT_CATEGORY_WORKSHOP = 4;

export default class EventCategorieMetier {
  constructor() {
    this.eventCategorieDao = new EventCategoryDao();
  }

  /**
   * Permet de récupérer tous les event category
   * @returns {Promise}
   */
  find() {
    return new Promise((resolve, reject) => {
      this.eventCategorieDao
        .find()
        .then((eventsCategory) => {
          return resolve(eventsCategory);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /**
   * Permet de récupérer un event category
   * @param idEventCategory - id de l'event category
   * @returns {Promise}
   */
  get(idEventCategory) {
    return new Promise((resolve, reject) => {
      this.eventCategorieDao
        .get(idEventCategory)
        .then((eventCategory) => {
          return resolve(eventCategory);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /**
   * Permet de supprimer un event category
   * @param idEventCategory - id de l'event category
   * @returns {Promise}
   */
  remove(idEventCategory) {
    return new Promise((resolve, reject) => {
      this.eventCategorieDao
        .remove(idEventCategory)
        .then(() => {
          return resolve();
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /**
   * Permet d'ajouter un event category
   * @param name - name de l'event category
   * @returns {Promise}
   */
  add(name) {
    return new Promise((resolve, reject) => {
      const eventCategory = new EventCategory({
        name: name,
      });

      this.eventCategorieDao
        .add(eventCategory)
        .then((eventCategory) => {
          return resolve(eventCategory);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /**
   * Permet de modifier un event category
   * @param idEventCategory - id de l'evetn category
   * @param name - nouveau nom de l'event category
   * @returns {Promise}
   */
  update(idEventCategory,
         name,) {
    return new Promise((resolve, reject) => {
      const eventCategory = {
        name: name,
      };

      this.eventCategorieDao
        .update(
          idEventCategory,
          eventCategory,
        )
        .then(() => {
          this.get(idEventCategory)
            .then((eventCategory) => {
              return resolve(eventCategory);
            })
            .catch((err) => {
              return reject(err);
            });
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  initBdd() {
    return new Promise((resolve, reject) => {

    });
  }
}