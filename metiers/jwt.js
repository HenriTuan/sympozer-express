const jwt = require('jsonwebtoken');
const config = require('../config/config');

export default class JwtMetier {

    constructor() {

    };

    verifyToken(token) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, config.key_jwt_login, (err, decoded) => {
                if (err)
                    return reject(err);
                return resolve(decoded);
            });
        });
    }
}