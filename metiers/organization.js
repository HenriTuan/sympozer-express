import OrganizationDao from '../DAO/v2/organization_DAO';

const Organization = require('../models/v2/organisation');
const uuidv1 = require('uuid/v1');

export default class OrganizationMetier {
    constructor() {
        this.organizationDao = new OrganizationDao();
    }

    /**
     * Permet de récupérer toutes les organisations d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            this.organizationDao
                .find(idConference)
                .then((organizations) => {
                    return resolve(organizations);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page d'organisation a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum d'organisation a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre d'organisations a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.organizationDao
                .findPage(idConference, idMin, searchTerm, pageSize)
                .then((organizations) => {
                    return resolve(organizations);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page d'organisation a partir
     * d'un id de conférence, d'un id min, d'un nom et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre d'organisations a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            this.organizationDao
                .findWithTerm(idConference, searchTerm, pageSize)
                .then((organizations) => {
                    return resolve(organizations);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une organisation
     * @param idConference - id de la conférence
     * @param idOrganization - id de l'organisation
     * @returns {Promise}
     */
    get(idConference, idOrganization) {
        return new Promise((resolve, reject) => {
            this.organizationDao
                .get(idOrganization, idConference)
                .then((organization) => {
                    return resolve(organization);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de supprimer une organisation d'une conférence
     * @param idConference - id de la conférence
     * @param idOrganization - id de l'organisation
     * @returns {Promise}
     */
    remove(idConference, idOrganization) {
        return new Promise((resolve, reject) => {
            this.organizationDao
                .remove(idOrganization, idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject();
                });
        });
    }

    /**
     * Permet de supprimer toutes les organisations d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            if (!idConference || idConference.length === 0) {
                return reject();
            }

            this.organizationDao.removeByConference(idConference)
                .then(() => {
                    return resolve();
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet d'ajouter une organisation
     * @param name - le nom de l'organisation
     * @param logoPath - le logo de l'organisation
     * @param homepage - l'url de la homepage de l'organisation
     * @param description - le description de l'organisation
     * @param idConference - l'id de la conférence
     * @returns {Promise}
     */
    add(name,
        logoPath,
        homepage,
        description,
        idConference) {
        return new Promise((resolve, reject) => {
            let uri = "https://sympozer.com/organisation/" + uuidv1();
            const organization = new Organization({
                name: name,
                logo_path: logoPath,
                homepage: homepage,
                description: description,
                idConference: idConference,
                idPersons: [],
                uri: uri,
            });

            this.organizationDao
                .add(organization)
                .then((organization) => {
                    return resolve(organization);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de mettre à jour une organisation
     * @param idOrganization - l'id de l'organisation
     * @param idConference - l'id de la conférence
     * @param name - le nom de l'organisation
     * @param logoPath - le logo de l'organisation
     * @param homepage - l'url de la homepage de l'organisation
     * @param description - le description de l'organisation
     * @returns {Promise}
     */
    update(idOrganization,
           idConference,
           name,
           logoPath,
           homepage,
           description,) {
        return new Promise((resolve, reject) => {
            const organization = {
                name: name,
                logo_path: logoPath,
                homepage: homepage,
                description: description,
            };

            this.organizationDao
                .update(idOrganization, idConference, organization)
                .then(() => {
                    this.get(
                        idConference,
                        idOrganization,
                    )
                        .then((organization) => {
                            return resolve(organization);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de mettre à jour la liste des personnes d'une organisation
     * @param idOrganization - l'id de l'organisation
     * @param idConference - l'id de la conférence
     * @param listPersons - la liste de l'id des personnes à mettre à jour
     * @returns {Promise}
     */
    updateListPersons(idOrganization, idConference, listPersons) {
        return new Promise((resolve, reject) => {
            const organization = {
                idPersons: listPersons
            }

            this.organizationDao
                .update(idOrganization, idConference, organization)
                .then(() => {
                    this.get(
                        idConference,
                        idOrganization,
                    )
                        .then((organization) => {
                            return resolve(organization);
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer les organisations d'une personne
     * @param idConference - l'id de la conférence
     * @param idPerson - l'id de la personne
     * @returns {Promise}
     */
    getByPerson(idConference, idPerson) {
        return new Promise((resolve, reject) => {
            this.organizationDao.findByPerson(idConference, idPerson)
                .then(organizations => {
                    return resolve(organizations);
                })
                .catch(err => {
                    return reject(err);
                });
        });
    }
}