// load dependencies
import Track from "./models/v2/track";

require('dotenv').load();
var express = require('express');
var app = module.exports = express();
var bodyParser = require('body-parser');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var cors = require('cors');
var fs = require('fs');
const EventCategory = require('./models/v2/event_categorie');
const mongoose = require('mongoose');

global.__basedir = __dirname;

/*new ConferenceDao().removeById("59eded7e3006b940c12c2f15")
	.then(() => {
	console.log("ok");
	})
	.catch((err) => {
	console.log(err);
	});*/
// use dependencies as middlewares
// bodyParser
app.use(bodyParser.json({
    limit: '100mb'
}));

app.use(bodyParser.urlencoded({
    extended: true,
    limit: '100mb'
}));

app.use(cors());
//app.use(bodyParser({ keepExtensions: true, uploadDir: '/public/upload' }))
// passport (authientication security)
app.use(passport.initialize())
// cookie
var cookieSecret = 'be like Henri'
app.use(cookieParser(cookieSecret))
// session
app.use(expressSession({
    secret: cookieSecret,
    cookie: {maxAge: 6000 * 60 * 24 * 100}, // 100 days
    expire: new Date(Date.now() + 2400), // 100 days
    proxy: true,
    resave: true,
    saveUninitialized: true
}))

app.use(function (req, res, next) {
    res.locals.session = req.session
    next()
})

app.set('json spaces', 40);


// database config
var port = process.env.PORT || 3000
// var port = process.argv[5] || 3000
var db = require('./config/db')

// connect to mongo
db.connect(function (err) {
    if (err) {
        console.log(err)
        console.log('Unable to connect to Mongo.')
        process.ext(1)
    } else {
        app.listen(port, function () {
            console.log('Listening on port ' + port)

            //On attend que la connexion à la bdd soit OK
            mongoose.connection.on("open",function(err) {
                if(err){
                    return false;
                }

                //On charge les données par default
                let bulkEventCategory = EventCategory.collection.initializeUnorderedBulkOp({useLegacyOps: true});
                const nameEventCategory = [
                    {
                        name: "Conference",
                        link: true,
                    },
                    {
                        name: "Panel",
                        link: true,
                    },
                    {
                        name: "Session",
                        link: true
                    },
                    {
                        name: "Talk",
                        link: true,
                    },
                    {
                        name: "Tutorial",
                        link: true,
                    },
                    {
                        name: "Workshop",
                        link: true,
                    }];

                nameEventCategory.forEach((e) => {
                    bulkEventCategory.find({
                        name: e.name,
                    })
                        .upsert()
                        .updateOne({
                            $setOnInsert: {
                                _id: new mongoose.Types.ObjectId()
                            },
                            $set: {
                                name: e.name,
                                linkWithEvent: e.link,
                            }
                        });
                });

                bulkEventCategory.execute()
                    .then(() => {
                        console.log("Event category added");
                    })
                    .catch(() => {
                        console.log("Event category not added");
                    });
            });
        });
    }
})


// midlleware (filter)
var filter = require('./config/filter')

// routing
var router = require('./config/route')

// controller
app.use(require('./controllers'))


// initilize metadata
//require('./init/load_meta_data')

// view engine setup
app.set('views', __dirname + '/views')
app.engine('jade', require('jade').__express)
app.set('view engine', 'jade')

app.use(express.static(__dirname + '/public'))
//app.use(bodyParser.json())
//app.use(bodyParser.urlencoded({extended: true}))

//On regarde si le dossier d'upload d'image existe
function checkDirectory(directory) {
    fs.stat(directory, function (err, stats) {
        //Check if error defined and the error code is "not exists"
        if (err && err.errno === 34) {
            //Create the directory, call the callback.
            fs.mkdir(directory, callback);
        }
    });
}

checkDirectory("./public/upload");


/*
- id du dernier object
- limit

{_id {$gt objectid}}.limit(..)
 */