var express = require('express')
var router = express.Router()


import ConferenceMetier from '../metiers/conference';

var preservedWord = require('../config/preserved_word')


// check user right
router.all(['/:id', '/:id/*'], function (req, res, next) {

  var sess = req.session;

  if (req.params.id) {
    new ConferenceMetier()
      .get(req.params.id)
      .then((conference) => {
        req.conf = conference;
        req.id_conference = req.params.id;
        req.isOwner = true;
        req.author = "toto";
        next();
      })
      .catch((err) => {
        next();
      });
  } else {
    req.id_conference = req.params.id;
    req.isOwner = true;
    req.author = "toto";
    next();
  }

  /*confDAO.getConfModelByAcronym(req.params.acronym, function(err, confModel){

    if(err || confModel==null){
      return res.render('message/page-not-found', {'message': 'Conference does not exist.'})
    }
    else{

      confDAO.getAuthorByAcronym(req.params.acronym, function(err, author){

        if(err) {
          res.render('message/page-not-found', {'message': 'Something has gone wrong in our server. Pleas try again later'})
        }else{
          var isOwner = false
          if(typeof(req.session.user) !== 'undefined' && req.session.user != null )
            if(confModel._user_id == req.session.user._id)
              isOwner = true

          req.confModel = confModel
          req.isOwner = isOwner
          req.author = author
          next()
        }

      }) // end getAuthorByAcronym

    }

  }) // end getconfByAcronym
*/


});// end /conference/:acronym


/**********************************/
module.exports = router