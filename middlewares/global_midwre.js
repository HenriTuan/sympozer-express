var express = require('express')
var router  = express.Router()


// check user right
router.get(['*/create', '*/edit'], function(req, res, next){
	
	var sess = req.session
	
	if( typeof(sess.user) !== 'undefined' && sess.user !== null ){
		next()
	}else{
		return res.render('message/access-denied', {'message': 'Please login to gain access to this page'})
	}

}) // end /edit, /create



/**********************************/
module.exports = router