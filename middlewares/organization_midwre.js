var express = require('express')
var router  = express.Router()

var orgaDAO = require('../DAO/organization_DAO')
var preservedWord = require('../config/preserved_word')


// check user right
router.get(['/:acronym', '/:acronym/*'], function(req, res, next){

	for(var i in preservedWord.orgaAcronym){
		if(req.params.acronym == preservedWord.orgaAcronym[i])
			return next()
	}

	var sess = req.session

	orgaDAO.getOrgaByAcronym(req.params.acronym, function(err, orgaModel){

		if(err || orgaModel==null){
			return res.render('message/page-not-found', {'message': 'Organization does not exist.'})
		}
		else{

			var isOwner = false
			if(typeof(req.session.user) !== 'undefined' && req.session.user != null )
				if(orgaModel._user_id == req.session.user._id) 
					isOwner = true
			
			req.orgaModel = orgaModel
			req.isOwner = isOwner
			next()
		}

	}) // end getOrgaByAcronym

	


}) // end /organization/:acronym



/**********************************/
module.exports = router