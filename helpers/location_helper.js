
var locationHelper = []


locationHelper.htmlFormToModelFormat = function(body, userId){

	var htmlFormFormat = body.formData

	var modelFormat = {
		'_user_id'		: userId,
		'name'			: htmlFormFormat.name,
		'default_name'  : htmlFormFormat.name,
		'uri'			: htmlFormFormat.uri,
		'list_comment'		: htmlFormFormat.listComment,
		'_conference_acronym': body.confAcronym
	}

	if( typeof(htmlFormFormat.acronym) !== 'undefined' && htmlFormFormat.acronym !== null )
		modelFormat.acronym = htmlFormFormat.acronym

	return modelFormat
} // end htmlFormToModelFormat




/*****************************************/
module.exports = locationHelper