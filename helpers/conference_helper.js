var countryLanguage = require('country-language')
var underscore = require('underscore')

var n3 = require('n3')
var n3Util = n3.Util

var geocoder = require('node-geocoder')('google', 'http');



var confHelper = []


confHelper.htmlFormToModelFormat = function(body, userId){

	var htmlFormFormat = body.formData
	var logoUrl = body.logoUrl

	var _id = null
	if( typeof(htmlFormFormat._id) !== 'undefined' && htmlFormFormat._id !== null )
		_id = htmlFormFormat._id

	var langName = countryLanguage.getLanguage(htmlFormFormat.lang).name[0]

	var modelFormat = {
		'title'			 : htmlFormFormat.title,
		'display_acronym': htmlFormFormat.acronym,
		'uri'			 : htmlFormFormat.uri,
		'description'	 : htmlFormFormat.description,
		'date_begin'	 : htmlFormFormat.date_begin,
		'date_end'		 : htmlFormFormat.date_end,
		'_user_id'		 : userId,
		'_location_id'	 : null,
		'type'			 : 'ConferenceEvent',
		'timezone'		 : new Date().getTimezoneOffset()/60,
		'language'		 : {
							'code': htmlFormFormat.lang,
							'name': langName
						  },
		'location'		: 
						 {
						  'country': htmlFormFormat.country,
						  'region': htmlFormFormat.region,
						  'formatted_address': htmlFormFormat.country+', '+htmlFormFormat.region
						 },
		'logo_path'		: logoUrl,
		'homepage'		: htmlFormFormat.homepage,
		'user_person'	: {
							is_in_conference: false,
							_person_acronym: null
						}
	}
	return modelFormat
}


confHelper.modelToHtmlFormFormat = function(confModel){
	
	var htmlFormat = {
		'_id'		  : confModel._id,
		'title'		  : confModel.title,
		'acronym'	  : confModel.acronym,
		'uri'		  : confModel.uri,
		'homepage'	  : confModel.homepage,
		'description' : confModel.description,
		'GMT'		  : confModel.timezone,
		'language'	  : confModel.language.name,
		'logo_path'	  : confModel.logo_path,
		'user_person' : confModel.user_person
	}

	// if(confModel.date_begin != null){
	// 	htmlFormat.date_begin = confModel.date_begin.toLocaleDateString()
	// }

	// if(confModel.date_end != null){
	// 	htmlFormat.date_end = confModel.date_end.toLocaleDateString()
	// }

	if(confModel.location != null){
		htmlFormat.location = confModel.location.formatted_address
	}


	return htmlFormat
}


confHelper.editFormToModelFormat = function(body, userId){

	const editFormFormat = body.formData;
  const langName = countryLanguage.getLanguage(editFormFormat.lang).name[0];
  const logoUrl = body.logoUrl;

  return {
    'title': editFormFormat.title,
    'acronym': editFormFormat.acronym,
    'uri': editFormFormat.uri,
    'description': editFormFormat.description,
    'date_begin': editFormFormat.date_begin,
    'date_end': editFormFormat.date_end,
    '_user_id': userId,
    '_location_id': null,
    'type': 'conference',
    'timezone': new Date().getTimezoneOffset() / 60,
    'language': {
      'code': editFormFormat.lang,
      'name': langName
    },
    'location':
      {
        'country': editFormFormat.country,
        'region': editFormFormat.region
      },
    'logo_path': logoUrl,
    'homepage': editFormFormat.homepage
  };

}; // end editFormToModelFormat



confHelper.modelToEditFormFormat = function(confModel){
	
	var editFormFormat = {
		'title'		  : confModel.title,
		'acronym'	  : confModel.acronym,
		'uri'		  : confModel.uri,
		'description' : confModel.description,
		'timezone'	  : confModel.timezone,
		'language'	  : confModel.language,
		'logo_path'	  : confModel.logo_path,
		'homepage'	  : confModel.homepage
	}

	if(confModel.date_begin != null){
		// editFormFormat.date_begin = confModel.date_begin.toLocaleDateString()
		editFormFormat.date_begin = confModel.date_begin
	}

	if(confModel.date_end != null){
		// editFormFormat.date_end = confModel.date_end.toLocaleDateString()
		editFormFormat.date_end = confModel.date_end
	}

	if(confModel.location != null){
		editFormFormat.location = confModel.location
	}

	return editFormFormat
} // end modelToEditFormFormat



confHelper.rdfStoreToConfModel = function(rdfStore, userId, cb){

	var confModel = {}
	var allTriple = rdfStore.source

	confModel.title   	  = confHelper.getTripleObjectbyPredicate(allTriple, 'http://www.w3.org/2000/01/rdf-schema#label')	
	confModel.display_acronym 	  = confHelper.getTripleObjectbyPredicate(allTriple, 'http://data.semanticweb.org/ns/swc/ontology#hasAcronym')	
	confModel.uri     	  = confHelper.getTripleObjectbyPredicate(allTriple, 'http://data.semanticweb.org/ns/swc/ontology#completeGraph').replace('/complete', '')	
	confModel.logo_path   = confHelper.getTripleObjectbyPredicate(allTriple, 'http://data.semanticweb.org/ns/swc/ontology#hasLogo')
	confModel.homepage    = confHelper.getTripleObjectbyPredicate(allTriple, 'http://xmlns.com/foaf/0.1/homepage')
	confModel.date_begin  = confHelper.getTripleObjectbyPredicate(allTriple, 'http://www.w3.org/2002/12/cal/icaltzd#dtstart') // icaltzd:dtstart
	confModel.date_end    = confHelper.getTripleObjectbyPredicate(allTriple, 'http://www.w3.org/2002/12/cal/icaltzd#dtend') // icaltzd:dtend
	confModel.description = null

	confModel._user_id    = userId
	confModel.timezone 	  = new Date().getTimezoneOffset()/60
	confModel.type = 'ConferenceEvent'

	confModel.user_person = {
								is_in_conference: false,
								_person_acronym: null
							}

	confModel.list_person = []
	confModel.list_organization = []

	if(confModel.logo_path == null){
		confModel.logo_path = '/img/sympozer_logo.png' // sympozer default logo
	}

	/* location */
	var location = {}
	location.lat  = confHelper.getTripleObjectbyPredicate(allTriple, 'http://www.w3.org/2003/01/geo/wgs84_pos#lat')
	location.long = confHelper.getTripleObjectbyPredicate(allTriple, 'http://www.w3.org/2003/01/geo/wgs84_pos#long')

	if(location.lat !== null && location.long !== null){

		geocoder.reverse({'lat': location.lat, 'lon': location.long}, function(err, res) {
	    
	    	if(err){
	    		cb(err)
	 	   	}else{
	    		location.city = res[0].city
	    		location.country = res[0].country
	    		location.street_name = res[0].streetName
	    		location.street_number = res[0].streetNumber
	    		
	    		location.formatted_address = res[0].formattedAddress

	    		confModel.location = location
	    		cb(null, confModel) // success, cb err=null
	    	}
		
		}) // end geocoder.reverse

	}else{
		cb(null, confModel) // success, cb err=null
	}
	
	/* end location  */

} // end rdfStoreToConfModel



confHelper.updateRdfStore = function(source, confModel){

	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	confHelper.updateTriple(allTriple, 'http://www.w3.org/2000/01/rdf-schema#label', confModel.title)
	confHelper.updateTriple(allTriple, 'http://www.w3.org/2002/12/cal/icaltzd#dtstart', confModel.date_begin)
	confHelper.updateTriple(allTriple, 'http://www.w3.org/2002/12/cal/icaltzd#dtend', confModel.date_end)
	confHelper.updateTriple(allTriple, 'http://data.semanticweb.org/ns/swc/ontology#hasLogo', confModel.logo_path)
	confHelper.updateTriple(allTriple, 'http://xmlns.com/foaf/0.1/homepage', confModel.homepage)
	/****************/

	return allTriple
} // end updateRdfStore


confHelper.updateTriple = function(allTriple, predicate, newObject){

		var triple = underscore.find(allTriple, function(_triple){
			return _triple.predicate.nominalValue == predicate
		})

		triple.object.nominalValue = newObject
		
} // end updateTripe



confHelper.getTripleObjectbyPredicate = function(allTriple, predicate){
	
	var triple = underscore.find(allTriple, function(_triple){
		return _triple.predicate.nominalValue == predicate
	})

	for(var i in allTriple){

		if(allTriple[i].predicate.nominalValue == predicate){
			return allTriple[i].object.nominalValue
		}

	} // end for

	// no predicate matched
	return null

} // end getTripleObjectbyPredicate


confHelper.createTriple = function(object, predicateSurfix, uri){
	
	var nodeType = 'NamedNode'
	if(n3Util.isBlank(object)){
		nodeType = 'BlankNode'
	}

	var predicateVal = null
	switch(predicateSurfix){
		
		case 'hasAcronym':
		case 'hasLogo':
		case 'completeGraph': // swc:hasLogo, swc:hasAcronym ...
			predicateVal = 'http://data.semanticweb.org/ns/swc/ontology#'+predicateSurfix
			break

		case 'label':
			predicateVal = 'http://www.w3.org/2000/01/rdf-schema#label'
			break

		case 'type':
			predicateVal = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
			break

		case 'description':
		case 'creator': // xmlns:dce, dce:creator
			predicateVal = 'http://purl.org/dc/elements/1.1/'+predicateSurfix
			break

		case 'dtstart':
		case 'dtend':
			predicateVal = 'http://www.w3.org/2002/12/cal/icaltzd#'+predicateSurfix
			break

		case 'homepage':
			predicateVal = 'http://xmlns.com/foaf/0.1/homepage'
			break

		default:
			break

	} // end switch

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : uri
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : predicateVal
		},

		'object':{
			'interfaceName' : nodeType,
			'nominalValue'  : object
		}

	} // end triple

	return triple
} // end createTriple

/*****************************************/
module.exports = confHelper