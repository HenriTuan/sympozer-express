import LocationMetier from "../metiers/location";

const n3 = require('n3');
const n3Util = n3.Util;
const sha1 = require('sha1');

import ConferenceMetier from '../metiers/conference';
//import LocationMetier from '../metiers/location';
import EventMetier from '../metiers/event';
import PersonMetier from '../metiers/person';
import RoleMetier from '../metiers/role';
import OrganisationMetier from '../metiers/organization'
import PublicationMetier from '../metiers/publication'
import TrackMetier from "../metiers/track";

export default class turtleHelper {
    constructor(filePath) {

        if (arguments.length) {
            this.filePath = filePath;
        }
        this.a = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';
        this.label = 'http://www.w3.org/2000/01/rdf-schema#label';
        this.conferenceOntology = 'https://w3id.org/scholarlydata/ontology/conference-ontology.owl#';
        this.roleURI = 'https://w3id.org/scholarlydata/role/';
        this.meetingRoomPrefix = "http://schema.org/MeetingRoom";
    }

    static filterSpecialChar(string){
        //tout ce qui n'est pas alpha-num ou "_"
        return string.replace(/\W/g, '-').toLowerCase();

    }

    //méthode de génération de l'URI d'une ressource
    static generatorURI(ressource){
        const scholarPrefix = 'https://w3id.org/scholarlydata/';

        //On regarde si la ressource & déjà une URI
        if(ressource !== null && ressource.uri !== undefined && ressource.uri.length > 0) {
            return ressource.uri;
        }



        switch (ressource.constructor.modelName){
            case 'Conference':
                return ressource.uri;

            case 'Event':
                if (ressource.idEventCategory.name){
                    return scholarPrefix + this.filterSpecialChar(ressource.idEventCategory.name) + '/' + ressource._id;
                }else{
                    throw ('[Error Ressource URI creation] : Event\'s type is not populated');
                }

            case 'PersonConference':
                return scholarPrefix + 'person/' + ressource._id;

            case 'Organisation':
                return scholarPrefix + 'organisation/' + ressource._id;

            case 'Track':
                return scholarPrefix + 'track/' + ressource._id;

            case 'Publication':
                return scholarPrefix + 'inproceedings/' + ressource._id;

            case 'Role':
                return scholarPrefix + 'role/' + ressource._id; //TODO : exact ???

            default:
                throw ('[Error Ressource URI creation] : ressource not in database');
        }
    }

    static formatDateTime(date, time){
        let result, tmp;
        tmp = date.split("/");

        if(tmp.length === 3){

            while(tmp[0].length < 2){
                tmp[0] = "0" + tmp[0];
            }

            while(tmp[1].length < 2){
                tmp[1] = "0" + tmp[1];
            }

            result = tmp[2] + '-' + tmp[0] + '-' + tmp[1];
        }else{
            throw "Error : Date format";
        }
        if(time){
            result += 'T' + time + ':00';
        }
        return result;
    }

    /**
     * Generate main conference tuples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getConferenceTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {

            let store, subject;

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            new ConferenceMetier()
                .get(conferenceId)
                .then((conference) => {
                    //On regarde si on a la conférence
                    if (!conference) {
                        return reject("there is no conference");
                    }

                    this.currentConference = conference;
                    console.log("conf uri : " + turtleHelper.generatorURI(conference) );

                    //subject = 'https://w3id.org/scholarlydata/conference/' + conferences.uri;
                    subject = conference.uri;

                    //type
                    store.addTriple(subject,
                        this.a,
                        this.conferenceOntology + 'Conference');

                    //label
                    store.addTriple(subject,
                        this.label,
                        n3Util.createLiteral(this.currentConference.title));

                    //acronyme
                    if (this.currentConference.display_acronym && this.currentConference.display_acronym.length > 0) {
                        this.currentConference.display_acronym = turtleHelper.filterSpecialChar(this.currentConference.display_acronym);

                        store.addTriple(subject,
                            this.conferenceOntology + 'acronym',
                            n3Util.createLiteral(this.currentConference.display_acronym));
                    }else{
                        return reject("Error : Conference's informations invalid - No acronym");
                    }

                    //date début
                    if (this.currentConference.date_begin && this.currentConference.date_begin.length > 0) {
                        this.currentConference.date_begin = turtleHelper.formatDateTime(this.currentConference.date_begin, null);

                        store.addTriple(subject,
                            this.conferenceOntology + 'startDate',
                            n3Util.createLiteral(this.currentConference.date_begin));
                    }else{
                        return reject("Error : Conference's informations invalid - No start date");

                    }

                    //date fin
                    if (this.currentConference.date_end && this.currentConference.date_end.length > 0) {
                        this.currentConference.date_end = turtleHelper.formatDateTime(this.currentConference.date_end, null);

                        store.addTriple(subject,
                            this.conferenceOntology + 'endDate',
                            n3Util.createLiteral(this.currentConference.date_end));
                    }else {
                        return reject("Error : Conference's informations invalid - No end date");
                    }

                    //description
                    if(this.currentConference.description){
                        store.addTriple(subject,
                            this.conferenceOntology + 'description',
                            n3Util.createLiteral(this.currentConference.description));
                    }else{
                        store.addTriple(subject,
                            this.conferenceOntology + 'description',
                            n3Util.createLiteral(""));
                        console.log("[Warning] Export : conference has no description");
                    }


                    //location //TODO à modif si refactoring des locations
                    if (this.currentConference.location &&
                        this.currentConference.location.formatted_address &&
                        this.currentConference.location.formatted_address.length > 0) {

                        store.addTriple(subject,
                            this.conferenceOntology + "location",
                            n3Util.createLiteral(this.currentConference.location.formatted_address));
                    }else{
                        return reject("Error : Conference's informations invalid - No location");
                    }

                    //proceedings
                    if(this.currentConference.uri){
                        store.addTriple(subject,
                            this.conferenceOntology + 'hasProceedings',
                            this.currentConference.uri + '/proceedings');
                    }else{
                        return reject("Error : Conference's informations invalid - No URI");
                    }

                    //series TODO : à modifier avec la vraie serie (need edit backend)
                    // (et uri chelou dans le dataset original généré, normal ?)
                    store.addTriple(subject,
                        this.conferenceOntology + 'hasSeries',
                        'https://w3id.org/scholarlydata//conferenceseries/eswc');

                    return resolve(store);

                    /*new TrackMetier() //récupération des events
                        .find(conferenceId)
                        .then((tracks) => {
                            let trackURI;
                            if (tracks.length && tracks.length > 0) {
                                for (const track of tracks) {
                                    trackURI = 'https://w3id.org/scholarlydata/track/' + track.name.replace(/ /g, '-');

                                    store.addTriple(subject,
                                        this.conferenceOntology +'hasSubEvent',
                                        trackURI);
                                }
                            }
                        })
                        .then(() => {
                            new EventMetier()
                                .find(conferenceId, conferenceId)
                                .then((sessions) => {
                                    let sessionURL;
                                    for(const session of sessions){
                                        sessionURL = 'https://w3id.org/scholarlydata/session/' + session.name.replace(/ /g, '-');
                                        store.addTriple(subject,
                                            this.conferenceOntology + 'hasSubEvent',
                                            sessionURL);

                                    }
                                })
                                .then(() => {
                                    resolve(store);
                                })
                        })
                        .catch((err) => {
                            return reject(err);
                        });
                        */

                    // ------------------------ version si refactoring des locations ------------------------
                    /*
                    new LocationMetier()
                        .find(conferenceId) //récupération des locations
                        .then((locations) => {
                            if (locations.length && locations.length > 0) {
                                for (let i = 0; i < locations.length; i++) {
                                    store.addTriple(subject,
                                        this.conferenceOntology +'location',
                                        n3Util.createLiteral(locations[i].name)); //TODO à modif avec le lien de la location ou du Site ?
                                }
                            }
                        })
                        .then(() => {
                            new EventMetier() //récupération des events
                                .find(conferenceId, null)
                                .then((events) => {
                                    if (events.length && events.length > 0) {
                                        for (let i = 0; i < events.length; i++) {
                                            store.addTriple(subject,
                                                this.conferenceOntology +'hasSubEvent',
                                                n3Util.createLiteral(events[i].uri));
                                        }
                                    }
                                })
                                .then(() => {
                                    resolve(store.getTriples(null, null, null));
                                })
                                .catch((err) => {
                                    return reject(err);
                                });

                        })
                        .catch((err) => {
                            return reject(err);
                        });
                        */
                    // ------------------------------------------------------------------------------------------

                })

                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Generate Persons tuples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getPersonsTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {
            let store, subject, j, filteredLastname, filteredFirstname; //TODO:pb de maj sur les noms

            filteredFirstname ='';
            filteredLastname = '';

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            new PersonMetier()
                .find(conferenceId)
                .then((personsConference) => {

                    if(personsConference === null || personsConference.length === 0){
                        return resolve();
                    }

                    for (const person of personsConference) {
                        if (!person) {
                            continue;
                        }

                        //On fait un lowerCase sur le firstname
                        if (person.firstname && person.firstname.length > 0) {
                            filteredFirstname = turtleHelper.filterSpecialChar(person.firstname);
                        }else{
                            console.log("Error : Person's informations invalid - No firstname");
                        }

                        //On fait un lowerCase sur le lastname
                        if (person.lastname && person.lastname.length > 0) {
                            filteredLastname = turtleHelper.filterSpecialChar(person.lastname);
                        }else{
                            console.log("Error : Person's informations invalid - No lastname");
                        }

                        subject = 'https://w3id.org/scholarlydata/person/' + filteredFirstname + '-' + filteredLastname;
                        console.log("person uri : " + turtleHelper.generatorURI(person) );


                        j = 1;
                        while (store.countTriples(subject, null, null) > 0){
                            subject = 'https://w3id.org/scholarlydata/person/' + filteredFirstname + '-' + filteredLastname + '-' + j;
                            j++;
                        }

                        //console.log("uri person : " + subject);
                        //type
                        store.addTriple(subject,
                            this.a,
                            this.conferenceOntology + 'Person');

                        //label
                        store.addTriple(subject,
                            this.label,
                            n3Util.createLiteral(person.firstname + ' ' + person.lastname));


                        //name
                        store.addTriple(subject,
                            this.conferenceOntology + 'name',
                            n3Util.createLiteral(person.firstname + ' ' + person.lastname));

                        //id
                        store.addTriple(subject,
                            this.conferenceOntology + 'id',
                            n3Util.createLiteral(person._id));

                        //checksum
                        if(person.mboxSha1sum){
                            store.addTriple(subject,
                                'http://xmlns.com/foaf/0.1/mbox_sha1sum',
                                n3Util.createLiteral(person.mboxSha1sum));
                        }else{
                            if(person.email){
                                //TODO : bonne version du sha1
                                /*store.addTriple(subject,
                                    'http://xmlns.com/foaf/0.1/mbox_sha1sum',
                                    n3Util.createLiteral(sha1("mailto:" + person.email)));*/
                                //version temporaire
                                store.addTriple(subject,
                                    'http://xmlns.com/foaf/0.1/mbox_sha1sum',
                                    n3Util.createLiteral(sha1(person.email)));
                            }else{
                                console.log("Error : Person's informations invalid - No email");

                            }

                        }

                    }

                    return resolve(store);
                })
                .catch((err) => {
                    return reject(err);
                });


        });
    }

    /**
     * Generate Organisations tuples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getOrganisationTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {
            let store, subject;

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            new OrganisationMetier()
                .find(conferenceId)
                .then((organisationConference) => {
                    if (organisationConference === null || organisationConference.length === 0) {
                        return resolve(store);
                    }

                    for (const organisation of organisationConference) {
                        //On regarde si on a l'orga
                        if (!organisation) {
                            continue;
                        }

                        //On regarde si on a un nom d'orga
                        if (!organisation.name || organisation.name.length === 0) {
                            continue;
                        }

                        console.log("orga uri : " + turtleHelper.generatorURI(organisation) );

                        subject = 'https://w3id.org/scholarlydata/organisation/' + turtleHelper.filterSpecialChar(organisation.name);

                        store.addTriple(subject,
                            this.a,
                            this.conferenceOntology + 'Organisation');

                        store.addTriple(subject,
                            this.label,
                            n3Util.createLiteral(organisation.name));

                        store.addTriple(subject,
                            this.conferenceOntology + 'name',
                            n3Util.createLiteral(organisation.name));

                    }

                    return resolve(store);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Generate affiliation tuples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getAffiliationTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {

            let store, subject,conference;

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            let personPromesses = [];

            const personMetier = new PersonMetier();

            if (!this.currentConference){
                return reject("Error : there is no current conference");
            }else{
                conference = this.currentConference;
            }

            new OrganisationMetier()
                .find(conferenceId)
                .then((organisationsConference) => {

                    //pour toutes les organistions
                    for (let i = 0; i < organisationsConference.length; i++) {
                        //pour chaque personne appartenant à l'organisation
                        personPromesses.push(personMetier.getMultiple(conferenceId, organisationsConference[i].idPersons))
                    }

                    Promise.all(personPromesses)
                        .then((values) => {
                            let acronymConference = conference.acronym;
                            if (!acronymConference || acronymConference.length === 0) {
                                return reject("there is no conference acronym");
                            }

                            acronymConference = turtleHelper.filterSpecialChar(acronymConference);

                            for (let i = 0; i < values.length; i++) {
                                for (let j = 0; j < values[i].length; j++) {

                                    /*let personURI = 'https://w3id.org/scholarlydata/person/'
                                        + values[i][j].displayname.replace(/ /g,'-'); */

                                    //On récupère l'URI de la personne avec son id (si 2 personnes ont le même nom)
                                    const persons = store.getTriples(null,
                                        this.conferenceOntology + 'id',
                                        n3Util.createLiteral(values[i][j]._id)
                                    );
                                    if (!persons || persons.length === 0 || persons.length > 1) {
                                        return reject("No (or too many) person(s) matched for this id : " + values[i][j]._id);
                                    }

                                    const person = persons[0];
                                    if (!person || !person.subject || person.subject.length === 0) {
                                        return reject("error in retrieving person");
                                    }

                                    let personURI = person.subject;

                                    const orgaConference = organisationsConference[i];
                                    if (!orgaConference) {
                                        return reject("error : retrieving orgaconference");
                                    }

                                    if (!orgaConference.name || orgaConference.name.length === 0) {
                                        return reject("error : organisation have no name");
                                    }

                                    const nameOrgaConferenceWithReplace = turtleHelper.filterSpecialChar(orgaConference.name);

                                    if (!values[i][j] || !values[i][j].displayname || values[i][j].displayname.length === 0) {
                                        return reject("error : retrieving name of person(s) in organisation");
                                    }

                                    const displaynameReplace = turtleHelper.filterSpecialChar(values[i][j].displayname);

                                    let affiliationURI = 'https://w3id.org/scholarlydata/affiliation-during-event/'
                                        + acronymConference + '-'
                                        + nameOrgaConferenceWithReplace + '-'
                                        + displaynameReplace;


                                    //ajout du lien dans les tuples de la personne vers l'affiliation
                                    store.addTriple(personURI,
                                        this.conferenceOntology + 'hasAffiliation',
                                        affiliationURI);

                                    subject = affiliationURI;

                                    //type
                                    store.addTriple(subject,
                                        this.a,
                                        this.conferenceOntology + 'AffiliationDuringEvent');

                                    //label
                                    store.addTriple(subject,
                                        this.label,
                                        n3Util.createLiteral('Affiliation held by ' + values[i][j].displayname
                                            + ' with ' + organisationsConference[i].name
                                            + ' during ' + conference.title));

                                    //during conference
                                    store.addTriple(subject,
                                        this.conferenceOntology + 'during',
                                        conference.uri);

                                    //link affiliation -> person
                                    store.addTriple(subject,
                                        this.conferenceOntology + 'isAffiliationOf',
                                        personURI);

                                    //link affiliation -> organisation
                                    store.addTriple(subject,
                                        this.conferenceOntology + 'withOrganisation',
                                        'https://w3id.org/scholarlydata/organisation/' + nameOrgaConferenceWithReplace);
                                }
                            }

                            return resolve(store);

                        })
                        .catch((err) => {
                            return reject(err);
                        })
                })
                .catch((err) => {
                    return reject(err);
                });
        })
    }

    /**
     * Generate proceedings tuples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getPublicationTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {
            let store,
                subject,
                conference,
                authorPromesses = [];

            const personMetier = new PersonMetier();

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            if(!this.currentConference) {
                return reject("Error : there is no current conference");
            }else{
                conference = this.currentConference;
            }

            new PublicationMetier()
                .findWithEvent(conferenceId)
                .then((publications) => {

                    if(!publications || publications.length === 0) {
                        return resolve();
                    }

                    // ajout de la liste totale des documents

                    subject = conference.uri + '/proceedings'; //TODO format a vérifier

                    store.addTriple(subject,
                        this.a,
                        this.conferenceOntology + 'Proceedings');

                    store.addTriple(subject,
                        this.label,
                        n3Util.createLiteral('Proceedings of ' + conference.title));

                    store.addTriple(subject,
                        this.conferenceOntology + 'isProceedingsOf',
                        conference.uri);

                    for (let i = 0; i < publications.length; i++) {
                        authorPromesses.push(personMetier.getMultiple(conferenceId, publications[i].idAuthors));
                    }

                    Promise.all(authorPromesses)
                        .then((authors) => {
                            for (let i = 0; i < publications.length; i++) {

                                const publication = publications[i];
                                if(!publication) {
                                    continue;
                                }

                                if(!publication.title || publication.title.length === 0) {
                                    continue;
                                }

                                const titlePublicationReplace = turtleHelper.filterSpecialChar(publications[i].title);

                                let publicationSubject = 'https://w3id.org/scholarlydata/inproceedings/' + titlePublicationReplace;

                                console.log("publi uri : " + turtleHelper.generatorURI(publication) );

                                store.addTriple(subject,
                                    this.conferenceOntology + 'hasPart',
                                    publicationSubject); //TODO verif format

                                //ajout individuel des publications

                                store.addTriple(publicationSubject,
                                    this.a,
                                    this.conferenceOntology + 'InProceedings');

                                store.addTriple(publicationSubject,
                                    this.label,
                                    n3Util.createLiteral(publications[i].title));

                                store.addTriple(publicationSubject,
                                    this.conferenceOntology + 'abstract',
                                    n3Util.createLiteral(publications[i].abstract));

                                store.addTriple(publicationSubject,
                                    this.conferenceOntology + 'isPartOf',
                                    subject);

                                publications[i].keywords.forEach((keyword) => {
                                    store.addTriple(publicationSubject,
                                        this.conferenceOntology + 'keyword',
                                        n3Util.createLiteral(keyword));
                                });

                                //console.log("===================================");
                                //console.log(publications[i]);
                                //console.log("===================================");

                                /* //INUTILE car application de la relation inverse dans les events
                                //si la publication est reliée à un event
                                if(publications[i].idEvent != null){

                                    store.addTriple(publicationSubject,
                                        this.conferenceOntology + 'relatesToEvent',
                                        'https://w3id.org/scholarlydata/' +
                                        publications[i].idEvent.idEventCategory.name.toLowerCase().replace(/ /g, '') +
                                        '/' + confAcronym + '-' + publications[i].idEvent.name.replace(/ /g, '-')); // attention si 2 event ont le même nom -> trouver le bon à partir d'un ID ou autre
                                }
                                */

                                store.addTriple(publicationSubject,
                                    this.conferenceOntology + 'title',
                                    n3Util.createLiteral(publications[i].title));

                                //author list
                                let listURI = 'https://w3id.org/scholarlydata/authorlist/' + titlePublicationReplace;

                                store.addTriple(publicationSubject,
                                    this.conferenceOntology + 'hasAuthorList',
                                    listURI);

                                store.addTriple(listURI,
                                    this.a,
                                    this.conferenceOntology + 'List');

                                store.addTriple(listURI,
                                    this.label,
                                    n3Util.createLiteral('Authors list of the paper \"' + publications[i].title + '\"'));

                                store.addTriple(listURI,
                                    this.conferenceOntology + 'hasFirstItem',
                                    'https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-1');

                                store.addTriple(listURI,
                                    this.conferenceOntology + 'hasLastItem',
                                    'https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (authors[i].length));

                                for (let j = 0; j < authors[i].length; j++) {

                                    //let authorURI = 'https://w3id.org/scholarlydata/person/' + authors[i][j].firstname.toLowerCase() + '-' + authors[i][j].lastname.toLowerCase();

                                    //on récupère l'URI de la personne avec son id (si 2 personnes ont le même nom)
                                    const persons = store.getTriples(null,
                                        this.conferenceOntology + 'id',
                                        n3Util.createLiteral(authors[i][j]._id)
                                    );
                                    if(!persons || persons.length === 0) {
                                        continue;
                                    }

                                    const person = persons[0];
                                    if(!person) {
                                        continue;
                                    }

                                    let authorURI = person.subject;

                                    //ajout des auteurs dans la publication
                                    store.addTriple(publicationSubject,
                                        'http://purl.org/dc/elements/1.1/creator',
                                        authorURI);

                                    //ajout "made" des tuples des auteurs
                                    store.addTriple(authorURI,
                                        'http://xmlns.com/foaf/0.1/made',
                                        publicationSubject);

                                    //ajout des item dans la liste
                                    store.addTriple(listURI,
                                        this.conferenceOntology + 'hasItem',
                                        'https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (j + 1));

                                    //type d'un item
                                    store.addTriple('https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (j + 1),
                                        this.a,
                                        'https://w3id.org/scholarlydata/ontology/conference-ontology.owl#ListItem');

                                    //liaison personne-item
                                    store.addTriple('https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (j + 1),
                                        this.conferenceOntology + 'hasContent',
                                        authorURI);

                                    //on ajoute le previous si ce n'est pas le premier item
                                    if (j > 0) {
                                        store.addTriple('https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (j + 1),
                                            this.conferenceOntology + 'previous',
                                            'https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (j - 1));
                                    }

                                    //on ajoute le next si ce n'est pas le dernier item
                                    if (j < authors[i].length - 1) {
                                        store.addTriple('https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (j + 1),
                                            this.conferenceOntology + 'next',
                                            'https://w3id.org/scholarlydata/authorlistitem/' + titlePublicationReplace + '-item-' + (j + 2));
                                    }
                                }
                            }

                            return resolve(store);
                        })
                        .catch((err) => {
                            return reject(err);
                        });

                })
                .catch((err) => {
                    return reject(err);
                });
        });

    }

    /**
     * Generate roleduringevent tuples and link with the persons tuples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getRoleTuples(conferenceId, previousStore) //Role during event
    {
        return new Promise((resolve, reject) => {

            let store, subject;

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }
            let rolePromesses = [];
            let authorPromesses = [];
            let publicationName;

            const roleMetier = new RoleMetier();
            const publiMetier = new PublicationMetier();

            new PersonMetier()
                .find(conferenceId)
                .then((personsConference) => {

                    //on recupère les roles et les publications de chaque personne
                    for (let i = 0; i < personsConference.length; i++) {

                        rolePromesses.push(roleMetier.getMultiple(conferenceId, personsConference[i].idRoles));
                        authorPromesses.push(publiMetier.getAllFromAuthor(conferenceId, personsConference[i]._id));
                    }

                    Promise.all(rolePromesses)
                        .then((rolesValues) => {

                            //console.log("rolelength :" + rolesValues.length);

                            Promise.all(authorPromesses)
                                .then((publicationsValues) => {
                                    //console.log("publi length :" + publicationsValues.length);
                                    //console.log("--------------------");
                                    //console.log(publicationsValues);
                                    //console.log("--------------------");

                                    if(!this.currentConference || !this.currentConference.display_acronym || this.currentConference.display_acronym.length === 0) {
                                        return reject("error : there is no current conference");
                                    }

                                    const acronymReplaceConference = turtleHelper.filterSpecialChar(this.currentConference.display_acronym);

                                    //pour chaque personnes
                                    for (let i = 0; i < personsConference.length; i++) {

                                        //Check data
                                        const person = personsConference[i];
                                        if(!person) {
                                            continue;
                                        }

                                        if(!person.firstname || person.firstname.length === 0) {
                                            continue;
                                        }

                                        if(!person.lastname || person.lastname.length === 0) {
                                            continue;
                                        }

                                        const firstnameLowercase = turtleHelper.filterSpecialChar(personsConference[i].firstname);
                                        const lastnameLowercase = turtleHelper.filterSpecialChar(personsConference[i].lastname);

                                        subject = 'https://w3id.org/scholarlydata/person/' + firstnameLowercase + '-' + lastnameLowercase;
                                        //console.log(" rolesvalues.length : " + rolesValues[i].length);
                                        //pour chaque role
                                        for (let j = 0; j < rolesValues[i].length; j++) {

                                            if(!rolesValues[i][j]) {
                                                continue;
                                            }

                                            //On récupère le nom du role
                                            const roleName = rolesValues[i][j].name;
                                            if(!roleName || roleName.length === 0){
                                                continue;
                                            }

                                            //On fait un replace sur le nom du role
                                            const roleNameReplace = turtleHelper.filterSpecialChar(roleName);

                                            //on récupère l'uri de la bonne personne
                                            const persons = store.getTriples(null,
                                                this.conferenceOntology + 'id',
                                                n3Util.createLiteral(personsConference[i]._id)
                                            );
                                            if(!persons || persons.length === 0) {
                                                continue;
                                            }

                                            const person = persons[0];
                                            if(!person) {
                                                continue;
                                            }

                                            let personURI = person.subject;
                                            if(!personURI || personURI.length === 0) {
                                                continue;
                                            }

                                            //pour le role d'auteur
                                            if(roleNameReplace.includes("author") && publicationsValues[i] && publicationsValues[i].length > 0){
                                                //on créé des tuples pour chaque publication
                                                for(let publication of publicationsValues[i]){

                                                    publicationName = turtleHelper.filterSpecialChar(publication.title);

                                                    //on crée un tuple correspondant au "role-during-event" dans les tuples de la personne (avec la publication)
                                                    store.addTriple(subject,
                                                        this.conferenceOntology + 'holdsRole',
                                                        'https://w3id.org/scholarlydata/role-during-event/'
                                                        + acronymReplaceConference + '-'
                                                        + roleNameReplace + '-' + firstnameLowercase
                                                        + '-' + lastnameLowercase
                                                        + '-' + publicationName);

                                                    subject = 'https://w3id.org/scholarlydata/role-during-event/'
                                                        + acronymReplaceConference
                                                        + '-' + roleNameReplace + '-'
                                                        + firstnameLowercase + '-' + lastnameLowercase
                                                        + '-' + publicationName;

                                                    //type
                                                    store.addTriple(subject,
                                                        this.a,
                                                        this.conferenceOntology + 'RoleDuringEvent');

                                                    //label
                                                    store.addTriple(subject,
                                                        this.label,
                                                        n3Util.createLiteral('Role of ' + rolesValues[i][j].name
                                                            + ' held by ' + personsConference[i].displayname
                                                            + ' during ' + this.currentConference.title
                                                            + ' for paper titled: ' + publication.title
                                                        ));

                                                    //during
                                                    store.addTriple(subject,
                                                        this.conferenceOntology + 'during',
                                                        this.currentConference.uri);

                                                    store.addTriple(subject,
                                                        this.conferenceOntology + 'isHeldBy',
                                                        //'https://w3id.org/scholarlydata/person/' + personsConference[i].firstname.toLowerCase() + '-' + personsConference[i].lastname.toLowerCase());
                                                        personURI);

                                                    //withRole
                                                    store.addTriple(subject,
                                                        this.conferenceOntology + 'withRole',
                                                        this.roleURI + roleNameReplace);

                                                    store.addTriple(subject,
                                                        this.conferenceOntology + 'withDocument',
                                                        'https://w3id.org/scholarlydata/inproceedings/' + publicationName);
                                                }
                                            }else{
                                                //on crée un tuple correspondant au "role-during-event" dans les tuples de la personne
                                                store.addTriple(subject,
                                                    this.conferenceOntology + 'holdsRole',
                                                    'https://w3id.org/scholarlydata/role-during-event/' + acronymReplaceConference + '-' + roleNameReplace + '-' + firstnameLowercase + '-' + lastnameLowercase); //TODO : +withdomument ?

                                                subject = 'https://w3id.org/scholarlydata/role-during-event/' + acronymReplaceConference + '-' + roleNameReplace + '-' + firstnameLowercase + '-' + lastnameLowercase; // +withdomument ?

                                                //type
                                                store.addTriple(subject,
                                                    this.a,
                                                    this.conferenceOntology + 'RoleDuringEvent');

                                                //label
                                                store.addTriple(subject,
                                                    this.label,
                                                    n3Util.createLiteral('Role of ' + rolesValues[i][j].name
                                                        + ' held by ' + personsConference[i].displayname
                                                        + ' during ' + this.currentConference.title
                                                    ));

                                                //during
                                                store.addTriple(subject,
                                                    this.conferenceOntology + 'during',
                                                    this.currentConference.uri);

                                                store.addTriple(subject,
                                                    this.conferenceOntology + 'isHeldBy',
                                                    //'https://w3id.org/scholarlydata/person/' + personsConference[i].firstname.toLowerCase() + '-' + personsConference[i].lastname.toLowerCase());
                                                    personURI);

                                                //withRole
                                                store.addTriple(subject,
                                                    this.conferenceOntology + 'withRole',
                                                    this.roleURI + roleNameReplace
                                                    //TODO ou values[i][j].uri ?
                                                );
                                            }
                                        }
                                    }
                            })
                            .then(() => {
                                roleMetier.find(conferenceId)
                                    .then((roles) => {
                                        roles.forEach((role) => {
                                            if(role.name && role.name.length > 0){
                                                subject = this.roleURI + turtleHelper.filterSpecialChar(role.name);

                                                store.addTriple(subject,
                                                    this.a,
                                                    'https://w3id.org/scholarlydata/ontology/conference-ontology.owl#Role');

                                                store.addTriple(subject,
                                                    this.label,
                                                    n3Util.createLiteral(role.name));
                                            }

                                        });

                                        return resolve(store);
                                    })
                                    .catch((err) => {
                                        return reject(err);
                                    });
                            })
                            .catch((err) => {
                                return reject(err);
                            });
                        })
                })
                .catch((err) => {
                    return reject(err);
                });


        });
    }

    /**
     * Generate tracks triples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getTracksTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {

            let store, subject, i;
            const confAcronym = turtleHelper.filterSpecialChar(this.currentConference.display_acronym);


            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            new TrackMetier()
                .findWithSubEvents(conferenceId)
                .then((tracks) => {
                    if(tracks === null || tracks.length === 0) {
                        return resolve();
                    }

                    //console.log(tracks);
                    for (const track of tracks) {

                        // Track triples
                        subject = 'https://w3id.org/scholarlydata/track/' + confAcronym + '-' + turtleHelper.filterSpecialChar(track.name);
                        console.log("track uri : " + turtleHelper.generatorURI(track) );

                        i = 1;
                        while (store.countTriples(subject, null, null) > 0) {
                            subject = 'https://w3id.org/scholarlydata/track/' + confAcronym + '-' + turtleHelper.filterSpecialChar(track.name) + '-' + i;
                            i++;
                        } //TODO : vérif que ca marche bien avec les conf

                        store.addTriple(subject,
                            this.a,
                            this.conferenceOntology + 'Track');

                        store.addTriple(subject,
                            this.label,
                            n3Util.createLiteral(track.name));

                        store.addTriple(subject,
                            this.conferenceOntology + 'description',
                            n3Util.createLiteral(track.name));

                        store.addTriple(subject,
                            this.conferenceOntology + 'isSubEventOf',
                            this.currentConference.uri); //TODO inutile ? a cause de la boucle finale de event

                        store.addTriple(this.currentConference.uri,
                            this.conferenceOntology + 'hasSubEvent',
                            subject);


                        for (const subEvent of track.idsSubEvent) { //TODO (il n'y a pas des sous-event dans les tracks)
                            store.addTriple(subject,
                                this.conferenceOntology + 'hasSubEvent',
                                'https://w3id.org/scholarlydata/event/' + confAcronym + '-' + turtleHelper.filterSpecialChar(subEvent.name));
                        }
                    }
                })
                .then(() => {
                    resolve(store);
                })
                .catch((err) => {

                    return reject(err);
                })

            /*new EventMetier()
                .find(conferenceId)
                .then((events) =>{
                    events.forEach((element) =>{
                    })
                })*/


        })
    }

    /**
     * Generate Events triples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getEventsTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {

            let store, subject, subEvent, i = 1, subEventType,subEventTypeURIFormat;

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            new EventMetier()
                .findAll(conferenceId)
                .then((events) => {


                    let eventType, eventTypeFormat;
                    const confAcronym = turtleHelper.filterSpecialChar(this.currentConference.display_acronym);
                    for (const event of events) {

                        console.log("event uri : " + turtleHelper.generatorURI(event) );

                        //console.log("------------------------");
                        //console.log(event._id, event.idSuperEvent);
                        //console.log("------------------------");
                        //console.log(event.name + ' superevent :' + event.idSuperEvent);

                        //on récupère le type d'évent
                        eventType = event.idEventCategory.name;
                        eventTypeFormat = turtleHelper.filterSpecialChar(eventType);

                        if (eventType === "Session") //si on a une session
                        {
                            //console.log("null");
                            subject = 'https://w3id.org/scholarlydata/session/' + confAcronym + '-' + turtleHelper.filterSpecialChar(event.name);

                            //on vérifie s'il n'y a pas d'autres tuples du même nom
                            i = 1;
                            //console.log(i, event);
                            while (store.countTriples(subject, null, null) > 0) {
                                console.log(i, "while");
                                subject = 'https://w3id.org/scholarlydata/session/' + confAcronym + '-' + turtleHelper.filterSpecialChar(event.name) + '-' + i;
                                i++;
                            }

                            //console.log(i, "end while");

                            //ajout de la conférence en super-event //TODO inutile ? a cause de la boucle finale
                            store.addTriple(subject,
                                this.conferenceOntology + 'isSubEventOf',
                                this.currentConference.uri);

                            //ajout aux sous-event de la conférence
                            store.addTriple(this.currentConference.uri,
                                this.conferenceOntology + 'hasSubEvent',
                                subject);

                            //console.log("end fonction");

                        }
                        else {
                            //build de URI subject selon le type d'event
                            //console.log("not null", event);

                            subject = 'https://w3id.org/scholarlydata/' + eventTypeFormat + '/' + confAcronym + '-' + turtleHelper.filterSpecialChar(event.name);

                            //on vérifie s'il n'y a pas d'autre tuples du même nom
                            i = 2;
                            //console.log(i);
                            while (store.countTriples(subject, null, null) > 0) {
                                //console.log(i);
                                subject = 'https://w3id.org/scholarlydata/' + eventTypeFormat + '/' + confAcronym + '-' + turtleHelper.filterSpecialChar(event.name) + '-' + i;
                                i++;
                            }

                            //console.log(i);
                        }

                        //console.log("---------------------------------------");
                        //console.log(event);
                        //console.log("*******");
                        //console.log(event.idsSubEvent);


                        store.addTriple(subject,
                            this.label,
                            n3Util.createLiteral(event.name));

                        store.addTriple(subject,
                            this.conferenceOntology + 'description',
                            n3Util.createLiteral(event.summary));

                        store.addTriple(subject,
                            this.a,
                            this.conferenceOntology + 'OrganisedEvent'); //TODO : pas pour les Talk ?

                        //type
                        store.addTriple(subject,
                            this.a,
                            this.conferenceOntology + eventType);

                        store.addTriple(subject,
                            this.conferenceOntology + 'startDate',
                            n3Util.createLiteral(turtleHelper.formatDateTime(event.date_start, event.time_start)));

                        store.addTriple(subject,
                            this.conferenceOntology + 'endDate',
                            n3Util.createLiteral(turtleHelper.formatDateTime(event.date_end, event.time_end)));

                        //console.log("---------------before to do-------------------");
                        //ajout de la location si présente
                        if(event.idLocation != null){
                            store.addTriple(subject,
                                this.conferenceOntology + 'location',
                                //'https://w3id.org/scholarlydata/location/' + event.idLocation._id);
                                n3Util.createLiteral(event.idLocation.name));
                        }

                        //ajout de la publication associée si présente
                        for (const publication of event.isEventRelatedTo){
                            store.addTriple(subject,
                                this.conferenceOntology + 'isEventRelatedTo',
                                'https://w3id.org/scholarlydata/inproceedings/' + turtleHelper.filterSpecialChar(publication.title));
                        }


                        //ajout de la Track si présente
                        if(event.idTrack != null){
                            //track
                            store.addTriple(subject,
                                this.conferenceOntology + 'isSubEventOf',
                                'https://w3id.org/scholarlydata/track/' + confAcronym + '-'+ turtleHelper.filterSpecialChar(event.idTrack.name));//TODO inutile ? a cause de la boucle finale
                        }

                        //ajout du lien vers les sous-event
                        for (const subevent of event.idsSubEvent) {

                            //recupération du type du sous-event
                            subEventType = subevent.idEventCategory.name;
                            //formatage pour build l'URI (replace spécifique car "Social Event" devient "socialevent" dans l'URI)
                            subEventTypeURIFormat = subEventType.toLowerCase().replace(/ /g, '');

                            //build URI du sous-event
                            subEvent = 'https://w3id.org/scholarlydata/' + subEventTypeURIFormat + '/' + confAcronym + '-' + turtleHelper.filterSpecialChar(subevent.name);

                            i = 1;
                            while (store.countTriples(subject, this.conferenceOntology + 'hasSubEvent', subEvent) > 0) {
                                subEvent = 'https://w3id.org/scholarlydata/' + subEventTypeURIFormat + '/' + confAcronym + '-' + turtleHelper.filterSpecialChar(subevent.name) + '-' + i;
                                i++;
                            } //TODO : moyen, pour etre sûr de build la bonne URI du sous-event, il faudrait identifier le sous-event dans le dataset (par un id peu etre)
                            // ici marche si tous les event de nom similaire ont le même super-event

                            store.addTriple(subject,
                                this.conferenceOntology + 'hasSubEvent',
                                subEvent);
                        }
                    }

                    //console.log("ok");

                    //on ajoute les isSubEventOf en parcourant les 'hasSubEvent' du store
                    store.forEach((triple) => {
                        store.addTriple(triple.object,
                            this.conferenceOntology + 'isSubEventOf',
                            triple.subject);
                    }, null, this.conferenceOntology + 'hasSubEvent', null);

                    //si un event est relié à une publication, on ajoute cet event à la publication
                    store.forEach((triple) => {
                        store.addTriple(triple.object,
                            this.conferenceOntology + 'relatesToEvent',
                            triple.subject);
                    }, null, this.conferenceOntology + 'isEventRelatedTo', null);



                    return store;
                })
                .then(() => {
                    resolve(store);
                })
                .catch((err) => {
                    reject(err);
                })
        });
    }


    /**
     * Generate locations tuples
     * @param conferenceId - Main conference ID
     * @param previousStore - Previous N3 triples storage, can be null
     * @returns {Promise} N3 store
     */
    getLocationTuples(conferenceId, previousStore) {
        return new Promise((resolve, reject) => {

            let store, subject;

            if (previousStore != null) {
                store = previousStore;
            } else {
                store = n3.Store();
            }

            new LocationMetier()
                .find(conferenceId)
                .then((locations) => {
                    //On regarde si on a la conférence

                    for (let location of locations){

                        if (location._id){
                            subject = 'https://w3id.org/scholarlydata/location/' + location._id;
                        }else{
                            return reject('[Errror] : no location id')
                        }

                        //type
                        store.addTriple(subject,
                            this.a,
                            this.meetingRoomPrefix);

                        //name
                        store.addTriple(subject,
                            "http://schema.org/name",
                            n3Util.createLiteral(location.name));

                        //label
                        store.addTriple(subject,
                            this.label,
                            n3Util.createLiteral(location.name));

                        console.log(location);

                        //GPS data
                        if(location.gps && location.gps.length === 2){
                            store.addTriple(subject,
                                'http://schema.org/geo',
                                n3Util.createLiteral(location.gps[0] + " - " + location.gps[1]));
                        }

                        //nombre de places
                        if(location.nb_place){
                            store.addTriple(subject,
                                this.conferenceOntology + 'Capacity',
                                n3Util.createLiteral(location.nb_place));
                        }

                        //description
                        if(location.description){
                            store.addTriple(subject,
                                this.conferenceOntology + 'Description',
                                n3Util.createLiteral(location.description));
                        }

                        //address
                        if(location.address){
                            store.addTriple(subject,
                                this.conferenceOntology + 'Address',
                                n3Util.createLiteral(location.address));
                        }

                        //equipement
                        if (location.equipements){
                            for (let equipement of location.equipements){
                                store.addTriple(subject,
                                    this.conferenceOntology + 'Devices',
                                    n3Util.createLiteral(equipement));
                            }
                        }


                    }

                    return resolve(store);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }
/*
    buildEventSubjetURI(typeEvent, store){
        let result = 'https://w3id.org/scholarlydata/' + typeEvent +'/' + this.currentConference.acronym.replace(/ /g, '-') + '-' + event.name.replace(/ /g, '-');

        //on vérifie s'il n'y a pas d'autre tuples du même nom
        i = 2;
        console.log(i);
        while (store.countTriples(result, null, null) > 0) {
            console.log(i);
            result = 'https://w3id.org/scholarlydata/' + typeEvent + '/' + this.currentConference.acronym.replace(/ /g, '-') + '-' + event.name.replace(/ /g, '-') + '-' + i;
            i++;
        }
        return result;
    }
    */

    // ------------------------------------- Import TTL -------------------------------------
/*
    parseFile() {
        return new Promise((resolve, reject) => {

            let store = new n3.Store();
            try {
                let parser = n3.Parser(),
                    rdfStream = fs.createReadStream(this.filePath);
                parser.parse(rdfStream, (error, triple, prefixes) => {
                    if (triple) {
                        store.addTriple(triple);
                        resolve(store);
                    }
                    else
                        reject(error)
                });
            }
            catch (err) {
                reject(err);
            }
        })
    }

    setEntities(store) {
        return new Promise((resolve, reject) => {
            let tuples = store.getTriples(null, this.a, 'https://w3id.org/scholarlydata/ontology/conference-ontology.owl#Conference');

            if (!tuples.length) {

            }
            //console.log(tuples);
        });
    }

    setLinks(store) {

    }

    persist(userId, cb) {
        this.parseFile()
            .then((store) => {
                this.setEntities(store)

                /*
                const conferenceMetier = new ConferenceMetier();
                conferenceMetier.add(info.name, info.uri, info.acronym, null, null, info.datebegin, info.dateend, info.location, info.region, null, info.language, userId)
                    .then((conference) =>{
                        return cb(null, conference);
                    })
                    .catch((err) =>{
                        console.log(err);
                    })*//*
            })
            .catch((err) => {
                return cb(err, null);
            })

    }
    */
}

//TODO : vérifier que 2 uri de ressources soient différentes (fait normalement)
//TODO : les roles génériques ?
//TODO : conserver les majuscules sur les values (ok ?)
//TODO : locations : http://schema.org/MeetingRoom
//TODO : push sur github
