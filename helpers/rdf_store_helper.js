var underscore = require('underscore')

var n3 = require('n3')
var n3Util = n3.Util

var dateHelper = require('./date_helper')
var util = require('./util')
var sha1 = require('sha1')

var rdfStoreHelper = []


rdfStoreHelper.loadN3Store = function(rdfStore){

	var store = n3.Store()

	for(var i in rdfStore.source){
		var triple = rdfStore.source[i]
		store.addTriple({
			'subject': triple.subject.nominalValue, 
			'predicate': triple.predicate.nominalValue, 
			'object': triple.object.nominalValue
		})
	}

	return store

} // end loadN3Store


/***************************** conference (main dataset) ***************************************************************/

rdfStoreHelper.getConferenceModel = function(rdfStore){

	// var store = rdfStoreHelper.loadN3Store(rdfStore)

	// var triple = store.find(null, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 'http://xmlns.com/foaf/0.1/Person')
	
	console.log(triple)

	return triple

} // end getConferenceModel




/***************************** end conference (main dataset) ***************************************************************/






/***************************** location ***************************************************************/
rdfStoreHelper.getListLocation = function(rdfStore){

	var allLocationSubject = []
	for(var i in rdfStore.source){
		
		var triple = rdfStore.source[i]

		if(  triple.object.nominalValue == 'http://data.semanticweb.org/ns/swc/ontology#MeetingRoomPlace'
		  && triple.predicate.nominalValue == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' ){

			allLocationSubject.push(triple.subject.nominalValue)

		} // end if

	} // end for


	allLocationSubject = underscore.uniq(allLocationSubject)

	var allLocation = []

	for(var i in allLocationSubject){		

		var subject = allLocationSubject[i]
		console.log('reading location: '+subject)

		var location = {}
		location.uri = subject
		location.slug = util.encodeUri(location.uri)

		for( var j in rdfStore.source ){

			var triple = rdfStore.source[j]
			if(  triple.subject.nominalValue == subject ){
				rdfStoreHelper.tripleToSimpleLocationModel(location, triple)
			} // end if

		} // end for j
		allLocation.push(location)

	} // end for i

	return allLocation

} // end getListLocation



rdfStoreHelper.tripleToSimpleLocationModel = function(location, triple){

	var predicate = triple.predicate.nominalValue

	if( /http:\/\/www.w3.org\/.*rdf-schema#label/.test(predicate) ){
		location.name = triple.object.nominalValue
	}	

} // end tripleToSimpleLocationModel



rdfStoreHelper.tripleToLocationModel = function(location, triple){

	var predicate = triple.predicate.nominalValue

	if( /http:\/\/www.w3.org\/.*rdf-schema#comment/.test(predicate) ){
		location.list_comment.push(triple.object.nominalValue)
	}

	if( /http:\/\/www.w3.org\/.*rdf-schema#label/.test(predicate) ){
		location.name = triple.object.nominalValue
	}

} // end tripleToLocationModel


rdfStoreHelper.locationModelToTriples = function(location){

	var listTriple = []

	for(var key in location){
		
		switch(key){

			case 'name':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = location.uri
				triple.predicate.nominalValue = 'http://www.w3.org/2000/01/rdf-schema#label'
				triple.object.nominalValue = location[key]
				listTriple.push(triple)
				break

			case 'list_comment':
				for(var i in location.list_comment){
					var triple = {'subject': {}, 'predicate': {}, 'object': {} }
					triple.subject.nominalValue = location.uri
					triple.predicate.nominalValue = 'http://www.w3.org/2000/01/rdf-schema#comment'
					triple.object.nominalValue = location.list_comment[i]
					listTriple.push(triple)
				} // end for
				break
		
		} // end switch

	} // end for

	return listTriple

} // end roleModelToTriples



rdfStoreHelper.addLocationTriple = function(rdfStore, location){

	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

	var locationTriple = []

	locationTriple = [
	
		rdfStoreHelper.createLocationTriple(location.uri, 'http://www.w3.org/2000/01/rdf-schema#label', location.name)

	] // end locationTriple

	for(var i in location.list_comment){
		locationTriple.push(rdfStoreHelper.createLocationTriple(location.uri, 'http://www.w3.org/2000/01/rdf-schema#comment', location.list_comment[i]))
	}


	return allTriple.concat(locationTriple)

} // end addLocationTriple


rdfStoreHelper.createLocationTriple = function(uri, predicate, object){

	var nodeType = 'NamedNode'
	if(n3Util.isBlank(object)){
		nodeType = 'BlankNode'
	}

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : uri,
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : predicate
		},

		'object':{
			'interfaceName' : nodeType,
			'nominalValue'  : object
		}
	}
	return triple
} // end createLocationTriple



rdfStoreHelper.removeLocationTriple = function(source, location){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	for( var i in allTriple ){
		if(allTriple[i].subject.nominalValue == location.uri)
		{
			allTriple[i] = null
		} // end if
	} // end for


	return underscore.compact(allTriple)

} // end removeOrganizationTriple



/***************************** end location ***************************************************************/






/***************************** event ***************************************************************/
rdfStoreHelper.getListEvent = function(rdfStore){

	var listEventUri = []

	// top most event: conferenceEvent
	listEventUri.push(rdfStore.uri)

	for( var i in rdfStore.source ){
		var triple = rdfStore.source[i]

		if(	triple.predicate.nominalValue == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type' ){
			
			if(    triple.object.nominalValue == 'http://data.semanticweb.org/ns/swc/ontology#TrackEvent'
			    || triple.object.nominalValue == 'http://data.semanticweb.org/ns/swc/ontology#TalkEvent' 
			    || triple.object.nominalValue == 'http://data.semanticweb.org/ns/swc/ontology#SessionEvent')
			{
				listEventUri.push(triple.subject.nominalValue)
			} // end if

		} // end if
	
	} // end for


	listEventUri = underscore.uniq(listEventUri)

	var allEvent = []

	for(var i in listEventUri){		

		var subject = listEventUri[i]
		console.log('reading event: '+subject)

		var event = {}
		event.uri = subject
		event.slug = util.encodeUri(event.uri)

		for( var j in rdfStore.source ){

			var triple = rdfStore.source[j]
			if( triple.subject.nominalValue == subject ){
				rdfStoreHelper.tripleToSimpleEventModel(event, triple)
			} // end if

		} // end for j
		allEvent.push(event)

	} // end for i

	return allEvent

} // end getListEvent


rdfStoreHelper.eventModelToTriples = function(event){

	var listTriple = []

	for(var key in event){
		
		switch(key){

			case 'name':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = event.uri
				triple.predicate.nominalValue = 'http://www.w3.org/2000/01/rdf-schema#label'
				triple.object.nominalValue = event[key]
				listTriple.push(triple)
				break

			case 'summary':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = event.uri
				triple.predicate.nominalValue = 'http://www.w3.org/2002/12/cal/icaltzd#summary'
				triple.object.nominalValue = event[key]
				listTriple.push(triple)
				break

			case 'location':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = event.uri
				triple.predicate.nominalValue = 'http://data.semanticweb.org/ns/swc/ontology#hasLocation'
				triple.object.nominalValue = event[key]
				listTriple.push(triple)
				break
		
			case 'date_end':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = event.uri
				triple.predicate.nominalValue = 'http://www.w3.org/2002/12/cal/icaltzd#dtend'
				triple.object.nominalValue = event[key]
				listTriple.push(triple)
				break

			case 'date_start':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = event.uri
				triple.predicate.nominalValue = 'http://www.w3.org/2002/12/cal/icaltzd#dtstart'
				triple.object.nominalValue = event[key]
				listTriple.push(triple)
				break

			case 'list_sub_event':
				for(var i in event.list_sub_event){
					var triple = {'subject': {}, 'predicate': {}, 'object': {} }
					triple.subject.nominalValue = event.uri
					triple.predicate.nominalValue = 'http://data.semanticweb.org/ns/swc/ontology#isSuperEventOf'
					triple.object.nominalValue = event.list_sub_event[i]
					listTriple.push(triple)
				} // end for
				break

			case 'list_super_event':
				for(var i in event.list_super_event){
					var triple = {'subject': {}, 'predicate': {}, 'object': {} }
					triple.subject.nominalValue = event.uri
					triple.predicate.nominalValue = 'http://data.semanticweb.org/ns/swc/ontology#isSubEventOf'
					triple.object.nominalValue = event.list_super_event[i]
					listTriple.push(triple)
				} // end for
				break

		} // end switch

	} // end for

	return listTriple

} // end eventModelToTriples


rdfStoreHelper.tripleToSimpleEventModel = function(event, triple){

	switch(triple.predicate.nominalValue){

		case 'http://www.w3.org/2000/01/rdf-schema#label':
			event.name = triple.object.nominalValue
			break

		case 'http://www.w3.org/2002/12/cal/icaltzd#dtend':
			event.date_end = dateHelper.UTCtoString(triple.object.nominalValue)
			break

		case 'http://www.w3.org/2002/12/cal/icaltzd#dtstart':
			event.date_start = dateHelper.UTCtoString(triple.object.nominalValue)
			break

		case 'http://www.w3.org/2002/12/cal/icaltzd#summary':
			event.summary = triple.object.nominalValue
			break

 	} // end switch


} // end tripleToSimpleEventModel



rdfStoreHelper.tripleToEventModel = function(event, triple){

	switch(triple.predicate.nominalValue){

		case 'http://www.w3.org/2000/01/rdf-schema#label':
			event.name = triple.object.nominalValue
			break

		case 'http://www.w3.org/2002/12/cal/icaltzd#summary':
			event.summary = triple.object.nominalValue
			break

		case 'http://data.semanticweb.org/ns/swc/ontology#hasLocation':
			event.location = {'location_uri': triple.object.nominalValue}
			break

		case 'http://www.w3.org/2002/12/cal/icaltzd#dtend':
			event.date_end = dateHelper.UTCtoString(triple.object.nominalValue)
			break

		case 'http://www.w3.org/2002/12/cal/icaltzd#dtstart':
			event.date_start = dateHelper.UTCtoString(triple.object.nominalValue)
			break

		case 'http://data.semanticweb.org/ns/swc/ontology#isSuperEventOf':
			event.list_sub_event.push({'event_uri': triple.object.nominalValue, '_event_slug': util.encodeUri(triple.object.nominalValue)})
			break	

		case 'http://data.semanticweb.org/ns/swc/ontology#isSubEventOf':
			event.list_super_event.push({'event_uri': triple.object.nominalValue, '_event_slug': util.encodeUri(triple.object.nominalValue)})
			break

 	} // end switch


} // end tripleToEventModel





rdfStoreHelper.addEventTriple = function(rdfStore, event){

	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

	var eventTriple = []

	eventTriple = [
	
		rdfStoreHelper.createEventTriple(event.uri, 'http://www.w3.org/2000/01/rdf-schema#label', event.name),
		rdfStoreHelper.createEventTriple(event.uri, 'http://www.w3.org/2002/12/cal/icaltzd#summary', event.summary),
		rdfStoreHelper.createEventTriple(event.uri, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 
															'http://data.semanticweb.org/ns/swc/ontology#TrackEvent'),

	] // end eventTriple

	return allTriple.concat(eventTriple)

} // end addEventTriple


rdfStoreHelper.createEventTriple = function(uri, predicate, object){

	var nodeType = 'NamedNode'
	if(n3Util.isBlank(object)){
		nodeType = 'BlankNode'
	}

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : uri,
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : predicate
		},

		'object':{
			'interfaceName' : nodeType,
			'nominalValue'  : object
		}
	}
	return triple
} // end createEventTriple



rdfStoreHelper.removeEventTriple = function(source, event){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	for( var i in allTriple ){
		if(allTriple[i].subject.nominalValue == event.uri)
		{
			allTriple[i] = null
		} // end if
	} // end for


	return underscore.compact(allTriple)

} // end removeEventTriple


rdfStoreHelper.createSubAndSuperEventTriple = function(parentEvent, childEvent){

	var triple1 = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : parentEvent.uri
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://data.semanticweb.org/ns/swc/ontology#isSuperEventOf'
		},

		'object':{
			'interfaceName' : 'NamedNode',
			'nominalValue'	: childEvent.uri
		}

	}

	var triple2 = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : childEvent.uri
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://data.semanticweb.org/ns/swc/ontology#isSubEventOf'
		},

		'object':{
			'interfaceName' : 'NamedNode',
			'nominalValue'	: parentEvent.uri
		}

	}

	return [triple1, triple2]

} // end createSubAndSuperEventTriple


/**************************** end event ***********************************************************/





/***************************** person ***************************************************************/
rdfStoreHelper.getListPerson = function(rdfStore){

	var allPersonSubject = []
	for(var i in rdfStore.source){
		
		var triple = rdfStore.source[i]

		if(  triple.predicate.nominalValue == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
		  && triple.object.nominalValue == 'http://xmlns.com/foaf/0.1/Person'){

			allPersonSubject.push(triple.subject.nominalValue)

		} // end if

	} // end for

	allPersonSubject = underscore.uniq(allPersonSubject)

	var allPerson = []

	for(var i in allPersonSubject){		

		var subject = allPersonSubject[i]
		console.log('reading person: '+subject)

		var person = {}
		person.uri = subject
		person.ava_path = '/img/sympozer_logo.png' // default avatar
		person.slug = util.encodeUri(person.uri)

		for( var j in rdfStore.source ){

			var triple = rdfStore.source[j]
			if(  triple.subject.nominalValue == subject ){
				rdfStoreHelper.tripleToSimplePersonModel(person, triple)
			} // end if

		} // end for j
		allPerson.push(person)

	} // end for i

	return allPerson

} // end getListPerson




rdfStoreHelper.getAllPerson = function(rdfStore, userId){

	var allPersonSubject = []
	for(var i in rdfStore.source){
		
		var triple = rdfStore.source[i]

		if(  triple.predicate.nominalValue == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
		  && triple.object.nominalValue == 'http://xmlns.com/foaf/0.1/Person'){

			allPersonSubject.push(triple.subject.nominalValue)

		} // end if

	} // end for

	allPersonSubject = underscore.uniq(allPersonSubject)

	var allPerson = []

	for(var i in allPersonSubject){		

		var subject = allPersonSubject[i]
		// console.log('reading person: '+subject)

		var person = {}
		// person._conference_acronym = rdfStore.acronym
		person._user_id = userId
		person.location = {}
		// person.social_account = []
		// person.list_publication = []
		// person._list_id_organization = []
		person.uri = subject

		for( var j in rdfStore.source ){
			var triple = rdfStore.source[j]

			if(  triple.subject.nominalValue == subject ){

				rdfStoreHelper.tripleToSimplePersonModel(person, triple)

			} // end if

		} // end for j
		allPerson.push(person)

	} // end for i

	return allPerson


} // end getAllPerson


rdfStoreHelper.tripleToSimplePersonModel = function(person, triple){

	switch(triple.predicate.nominalValue){

		case 'http://xmlns.com/foaf/0.1/lastName':
			person.lastname = triple.object.nominalValue
			break

		case 'http://xmlns.com/foaf/0.1/firstName':
			person.firstname = triple.object.nominalValue
			break

		case 'http://xmlns.com/foaf/0.1/name':
			person.displayname = triple.object.nominalValue
			break

		case 'http://xmlns.com/foaf/0.1/profilePhoto':
			person.ava_path = triple.object.nominalValue
			break

		case 'http://xmlns.com/foaf/0.1/mbox_sha1sum':
			person.sha = triple.object.nominalValue
			break

 	} // end switch

} // end tripleToSimplePersonModel



rdfStoreHelper.tripleToPersonModel = function(person, triple){

	switch(triple.predicate.nominalValue){

		case 'http://xmlns.com/foaf/0.1/lastName':
			person.lastname = triple.object.nominalValue;
			break;

		case 'http://xmlns.com/foaf/0.1/firstName':
			person.firstname = triple.object.nominalValue;
			break;

		case 'http://xmlns.com/foaf/0.1/name':
			person.displayname = triple.object.nominalValue;
			person.default_name = triple.object.nominalValue;
			break;

		case 'http://xmlns.com/foaf/0.1/homepage':
			person.homepage = triple.object.nominalValue;
			break;

		case 'http://xmlns.com/foaf/0.1/mbox_sha1sum':
			person.sha = triple.object.nominalValue;
			break;

		case 'http://xmlns.com/foaf/0.1/based_near':
			person.country = triple.object.nominalValue.replace('http://dbpedia.org/resource/', '');
			break;

		case 'http://xmlns.com/foaf/0.1/OnlineAccount':
			person.social_account.push(triple.object.nominalValue);
			break;

		case 'http://data.semanticweb.org/ns/swc/ontology#holdsRole':
			person.list_role.push(triple.object.nominalValue);
			break;

		case 'http://xmlns.com/foaf/0.1/profilePhoto':
			person.ava_path = triple.object.nominalValue;
			break;

 	} // end switch


} // end tripleToPersonModel



rdfStoreHelper.personModelToTriples = function(person){

	var listTriple = []

	for(var key in person){
		
		switch(key){

			case 'firstname':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = person.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/firstName'
				triple.object.nominalValue = person[key]
				listTriple.push(triple)
				break

			case 'lastname':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = person.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/lastName'
				triple.object.nominalValue = person[key]
				listTriple.push(triple)
				break

			case 'displayname':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = person.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/name'
				triple.object.nominalValue = person[key]
				listTriple.push(triple)
				break

			case 'homepage':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = person.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/homepage'
				triple.object.nominalValue = person[key]
				listTriple.push(triple)
				break

			case 'email':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = person.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/mbox_sha1sum'
				triple.object.nominalValue = sha1(person[key])
				listTriple.push(triple)
				break

			case 'country':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = person.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/based_near'
				triple.object.nominalValue = 'http://dbpedia.org/resource/' + person[key]
				listTriple.push(triple)
				break

			case 'ava_path':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = person.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/profilePhoto'
				triple.object.nominalValue = person[key]
				listTriple.push(triple)
				break

			case 'social_account':
				for(var i in person.social_account){
					var triple = {'subject': {}, 'predicate': {}, 'object': {} }
					triple.subject.nominalValue = person.uri
					triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/OnlineAccount'
					triple.object.nominalValue = person.social_account[i]
					listTriple.push(triple)
				} // end for
				break
		
		} // end switch


	} // end for

	return listTriple

} // end personModelToTriples




rdfStoreHelper.removePersonTriple = function(source, person){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))
	var fullname = null

	if(typeof(person.firstname) != 'undefined'){
		fullname = person.firstname.replace(' ', '-').toLowerCase()
	}

	if(typeof(person.lastname) != 'undefined'){
		fullname = fullname + '-' + person.lastname.replace(' ', '-').toLowerCase()
	}

	if(fullname == null){
		fullname = person.acronym
	}

	for( var i in allTriple ){
		if(allTriple[i].subject.nominalValue == 'http://data.semanticweb.org/person/'+fullname){
			allTriple[i] = null
		} // end if
	} // end for


	return underscore.compact(allTriple)

} // end removePersonTriple


/************************** end person *******************************/


/************************** role *******************************/

rdfStoreHelper.getListRole = function(rdfStore){

	var allRoleSubject = []
	for(var i in rdfStore.source){
		
		var triple = rdfStore.source[i]

		if(  triple.predicate.nominalValue == 'http://data.semanticweb.org/ns/swc/ontology#isRoleAt'
		 	 && triple.object.nominalValue == rdfStore.uri ){

			allRoleSubject.push(triple.subject.nominalValue)

		} // end if

	} // end for


	allRoleSubject = underscore.uniq(allRoleSubject)

	var allRole = []

	for(var i in allRoleSubject){		

		var subject = allRoleSubject[i]
		console.log('reading role: '+subject)

		var role = {}
		role.uri = subject
		role.slug = util.encodeUri(role.uri)

		for( var j in rdfStore.source ){

			var triple = rdfStore.source[j]
			if(  triple.subject.nominalValue == subject ){
				rdfStoreHelper.tripleToRoleModel(role, triple)
			} // end if

		} // end for j
		allRole.push(role)

	} // end for i

	return allRole

} // end getListRole



rdfStoreHelper.tripleToRoleModel = function(role, triple){

	switch(triple.predicate.nominalValue){

		case 'http://www.w3.org/2000/01/rdf-schema#label':
			role.name = triple.object.nominalValue;
			break;

 	} // end switch

}; // end tripleToPublicationModel


rdfStoreHelper.roleModelToTriples = function(role){

	var listTriple = []

	for(var key in role){
		
		switch(key){

			case 'name':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = role.uri
				triple.predicate.nominalValue = 'http://www.w3.org/2000/01/rdf-schema#label'
				triple.object.nominalValue = role[key]
				listTriple.push(triple)
				break
		
		} // end switch


	} // end for

	return listTriple

} // end roleModelToTriples



rdfStoreHelper.addRoleTriple = function(rdfStore, person, listRole){

	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

	var fullname = null

	if(typeof(person.firstname) != 'undefined'){
		fullname = person.firstname.replace(' ', '-').toLowerCase()
	}

	if(typeof(person.lastname) != 'undefined'){
		fullname = fullname + '-' + person.lastname.replace(' ', '-').toLowerCase()
	}

	if(fullname == null){
		fullname = person.acronym
	}

	// remove old roles
	for( var i in allTriple ){
		if( allTriple[i].subject.nominalValue == 'http://data.semanticweb.org/person/'+fullname &&
		 	allTriple[i].predicate.nominalValue == 'http://data.semanticweb.org/ns/swc/ontology#holdsRole'){
			allTriple[i] = null
		}
	} // end for
	allTriple = underscore.compact(allTriple)

	// add new roles
	var roleTriple = []
	for(var i in listRole){
		roleTriple.push(rdfStoreHelper.createRoleTriple(fullname, 'holdsRole', ''+rdfStore.uri+listRole[i]))
	}

	return underscore.uniq(allTriple.concat(roleTriple))

} // end addRoleTriple


rdfStoreHelper.getAllRoleHolderUri = function(rdfStore, role){

	var listRoleHolderUri = []
	for( var i in rdfStore.source ){
		var triple = rdfStore.source[i]

		if(	 triple.subject.nominalValue == role.uri
		  && triple.predicate.nominalValue == 'http://data.semanticweb.org/ns/swc/ontology#heldBy')
		{
			listRoleHolderUri.push(triple.object.nominalValue)
		} // end if
	
	} // end for

	return listRoleHolderUri

} // end getAllRoleHolderUri



rdfStoreHelper.createRoleTriple = function(fullname, predicate, object){

	var nodeType = 'NamedNode'
	if(n3Util.isBlank(object)){
		nodeType = 'BlankNode'
	}

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://data.semanticweb.org/person/'+fullname
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://data.semanticweb.org/ns/swc/ontology#'+predicate
		},

		'object':{
			'interfaceName' : nodeType,
			'nominalValue'  : object
		}
	}
	return triple
} // end createRoleTriple


/************************** end role *******************************/




/************************** publication *******************************/


rdfStoreHelper.getAllMakerUri = function(rdfStore, pub){

	var listMakerUri = []
	for( var i in rdfStore.source ){
		var triple = rdfStore.source[i]

		if(	 triple.subject.nominalValue == pub.uri
		  && triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/maker')
		{
			listMakerUri.push(triple.object.nominalValue)
		} // end if
	
	} // end for

	return listMakerUri

} // end getAllMakerUri



rdfStoreHelper.getListPublication = function(rdfStore){

	var allPubSubject = []
	for(var i in rdfStore.source){
		
		var triple = rdfStore.source[i]

		if(  triple.predicate.nominalValue == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
		  && triple.object.nominalValue == 'http://swrc.ontoware.org/ontology#InProceedings' ){

			allPubSubject.push(triple.subject.nominalValue)

		} // end if

	} // end for

	allPubSubject = underscore.uniq(allPubSubject)
	var allPub = []

	for(var i in allPubSubject){		

		var subject = allPubSubject[i]
		console.log('reading publication: '+subject)

		var pub = {}
		pub.uri = subject
		pub.slug = util.encodeUri(pub.uri)

		for( var j in rdfStore.source ){

			var triple = rdfStore.source[j]
			if( triple.subject.nominalValue == subject ){
				rdfStoreHelper.tripleToPublicationModel(pub, triple)
			} // end if

		} // end for j
		allPub.push(pub)

	} // end for i

	return allPub

} // end getListPublication



rdfStoreHelper.tripleToPublicationModel = function(pub, triple){

	switch(triple.predicate.nominalValue){

		case 'http://purl.org/dc/elements/1.1/title':
			pub.title = triple.object.nominalValue
			break

		case 'http://swrc.ontoware.org/ontology#abstract':
			pub.abstract = triple.object.nominalValue
			break

 	} // end switch

} // end tripleToPublicationModel


rdfStoreHelper.publicationModelToTriples = function(pub){

	var listTriple = []

	for(var key in pub){
		
		switch(key){

			case 'title':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = pub.uri
				triple.predicate.nominalValue = 'http://purl.org/dc/elements/1.1/title'
				triple.object.nominalValue = pub[key]
				listTriple.push(triple)
				break

			case 'abstract':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = pub.uri
				triple.predicate.nominalValue = 'http://swrc.ontoware.org/ontology#abstract'
				triple.object.nominalValue = pub[key]
				listTriple.push(triple)
				break
		
		} // end switch


	} // end for

	return listTriple

} // end publicationModelToTriples



rdfStoreHelper.addPublicationTriple = function(rdfStore, pub){

	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

	var publicationTriple = []

	publicationTriple = [
	
		rdfStoreHelper.createPublicationTriple(pub.uri, 'http://purl.org/dc/elements/1.1/title', pub.title),
		rdfStoreHelper.createPublicationTriple(pub.uri, 'http://swrc.ontoware.org/ontology#abstract', pub.resume),
		rdfStoreHelper.createPublicationTriple(pub.uri, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 
															'http://swrc.ontoware.org/ontology#InProceedings'),

	] // end publicationTriple

	return allTriple.concat(publicationTriple)

} // end addPublicationTriple



rdfStoreHelper.removePublicationTriple = function(source, pub){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	for( var i in allTriple ){
		if(allTriple[i].subject.nominalValue == pub.uri)
		{
			allTriple[i] = null
		} // end if
	} // end for

	return underscore.compact(allTriple)

} // end removePublicationTriple



rdfStoreHelper.createPublicationTriple = function(uri, predicate, object){

	var nodeType = 'NamedNode'
	if(n3Util.isBlank(object)){
		nodeType = 'BlankNode'
	}

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : uri,
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : predicate
		},

		'object':{
			'interfaceName' : nodeType,
			'nominalValue'  : object
		}
	}
	return triple
}



rdfStoreHelper.createMakerTriple = function(pubUri, personUri){


	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : pubUri
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://xmlns.com/foaf/0.1/maker'
		},

		'object':{
			'interfaceName' : 'NamedNode',
			'nominalValue'	: personUri
		}

	}
	return triple

} // end createMakerTriple


rdfStoreHelper.removeMakerTriple = function(source, pub, person){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	for( var i in allTriple ){
		if(	 allTriple[i].subject.nominalValue == pub.uri
		  && allTriple[i].predicate.nominalValue == 'http://xmlns.com/foaf/0.1/maker'
		  && allTriple[i].object.nominalValue == person.uri)
		{
			allTriple[i] = null
		} // end if
	} // end for


	return underscore.compact(allTriple)

} // end removeMakerTriple

// rdfStoreHelper.createPublicationTriple = function(fullname, predicate, object){

// 	var nodeType = 'NamedNode'
// 	if(n3Util.isBlank(object)){
// 		nodeType = 'BlankNode'
// 	}

// 	var triple = {

// 		'subject':{
// 			'interfaceName' : 'NamedNode',
// 			'nominalValue'  : 'http://data.semanticweb.org/person/'+fullname
// 		},

// 		'predicate':{
// 			'interfaceName' : 'NamedNode',
// 			'nominalValue'  : 'http://xmlns.com/foaf/0.1/'+predicate
// 		},

// 		'object':{
// 			'interfaceName' : nodeType,
// 			'nominalValue'  : object
// 		}
// 	}
// 	return triple
// }



/************************** end publication *******************************/




/************************** organization *******************************/

rdfStoreHelper.getListOrganization = function(rdfStore){

	var allOrgaSubject = []
	for(var i in rdfStore.source){
		
		var triple = rdfStore.source[i]

		if( triple.predicate.nominalValue == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
		  && triple.object.nominalValue == 'http://xmlns.com/foaf/0.1/Organization' ){

			allOrgaSubject.push(triple.subject.nominalValue)

		} // end if

	} // end for

	allOrgaSubject = underscore.uniq(allOrgaSubject)
	var allOrga = []

	for(var i in allOrgaSubject){		

		var subject = allOrgaSubject[i]
		console.log('reading organization: '+subject)

		var orga = {}
		orga.uri = subject
		orga.slug = util.encodeUri(orga.uri)
		orga.logo_path = '/img/sympozer_logo.png'

		for( var j in rdfStore.source ){

			var triple = rdfStore.source[j]
			if( triple.subject.nominalValue == subject ){
				rdfStoreHelper.tripleToSmpileOrganizationModel(orga, triple)
			} // end if

		} // end for j
		allOrga.push(orga)

	} // end for i

	return allOrga

} // end getListOrganization


rdfStoreHelper.tripleToSmpileOrganizationModel = function(orga, triple){

	switch(triple.predicate.nominalValue){

		case 'http://xmlns.com/foaf/0.1/name':
			orga.name = triple.object.nominalValue
			break

		case 'http://data.semanticweb.org/ns/swc/ontology#hasLogo':
			orga.logo_path = triple.object.nominalValue
			break

		case 'http://xmlns.com/foaf/0.1/homepage':
			orga.homepage = triple.object.nominalValue
			break

		case 'http://purl.org/dc/elements/1.1/description':
			orga.description = triple.object.nominalValue
			break

 	} // end switch

} // end tripleToSmpileOrganizationModel



rdfStoreHelper.tripleToOrganizationModel = function(orga, triple){

	switch(triple.predicate.nominalValue){

		case 'http://xmlns.com/foaf/0.1/name':
			orga.name = triple.object.nominalValue
			break

		case 'http://data.semanticweb.org/ns/swc/ontology#hasLogo':
			orga.logo_path = triple.object.nominalValue
			break

		case 'http://xmlns.com/foaf/0.1/homepage':
			orga.homepage = triple.object.nominalValue
			break

		case 'http://purl.org/dc/elements/1.1/description':
			orga.description = triple.object.nominalValue
			break

 	} // end switch

} // end tripleToOrganizationnModel



rdfStoreHelper.organizationModelToTriples = function(orga){

	var listTriple = []

	for(var key in orga){
		
		switch(key){

			case 'name':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = orga.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/name'
				triple.object.nominalValue = orga[key]
				listTriple.push(triple)
				break

			case 'homepage':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = orga.uri
				triple.predicate.nominalValue = 'http://xmlns.com/foaf/0.1/homepage'
				triple.object.nominalValue = orga[key]
				listTriple.push(triple)
				break

			case 'description':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = orga.uri
				triple.predicate.nominalValue = 'http://purl.org/dc/elements/1.1/description'
				triple.object.nominalValue = orga[key]
				listTriple.push(triple)
				break

			case 'logo_path':
				var triple = {'subject': {}, 'predicate': {}, 'object': {} }
				triple.subject.nominalValue = orga.uri
				triple.predicate.nominalValue = 'http://data.semanticweb.org/ns/swc/ontology#hasLogo'
				triple.object.nominalValue = orga[key]
				listTriple.push(triple)
				break
		
		} // end switch


	} // end for

	return listTriple

} // end organizationModelToTriples



rdfStoreHelper.removeOrganizationTriple = function(source, orga){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	for( var i in allTriple ){
		if(allTriple[i].subject.nominalValue == 'http://data.semanticweb.org/organization/'+orga.acronym
		  && allTriple[i].predicate.nominalValue == 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type'
		  && allTriple[i].object.nominalValue == 'http://xmlns.com/foaf/0.1/Organization')
		{
			allTriple[i] = null
		} // end if
	} // end for


	return underscore.compact(allTriple)

} // end removeOrganizationTriple



rdfStoreHelper.addOrganizationTriple = function(rdfStore, orga){

	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

	var triples = [
		rdfStoreHelper.createOrganizationTriple(orga, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 'http://xmlns.com/foaf/0.1/Organization'),
		rdfStoreHelper.createOrganizationTriple(orga, 'http://xmlns.com/foaf/0.1/name', orga.name)
	]

	return underscore.uniq(allTriple.concat(triples))

} // end addOrganizationTriple



rdfStoreHelper.createOrganizationTriple = function(orga, predicate, object){

	var nodeType = 'NamedNode'
	if(n3Util.isBlank(object)){
		nodeType = 'BlankNode'
	}

	return 	{

				'subject':{
					'interfaceName' : 'NamedNode',
					'nominalValue'  : 'http://data.semanticweb.org/organization/'+orga.acronym
				},

				'predicate':{
					'interfaceName' : 'NamedNode',
					'nominalValue'  : predicate
				},

				'object':{
					'interfaceName' : nodeType,
					'nominalValue'  : object
				}

			}

} // end createOrganizationTriple




/************************** end organization *******************************/


/************************* affiliation *****************************/



rdfStoreHelper.removeAffiliationTriple = function(source, person, orga){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	for( var i in allTriple ){
		if(	 allTriple[i].subject.nominalValue == 'http://data.semanticweb.org/person/'+fullname
		  && allTriple[i].predicate.nominalValue == 'http://swrc.ontoware.org/ontology#affiliation'
		  && allTriple[i].object.nominalValue == 'http://data.semanticweb.org/organization/'+orga.acronym)
		{
			allTriple[i] = null
		} // end if
	} // end for


	return underscore.compact(allTriple)

} // end removeAffiliationTriple



rdfStoreHelper.createAffiliationTriple = function(personUri, orgaUri){

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : personUri
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://swrc.ontoware.org/ontology#affiliation'
		},

		'object':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : orgaUri
		}

	}
	return triple

} // end createAffiliationTriple


/************************* end affiliation *******************************/



/************************ member *****************************************/


rdfStoreHelper.getAllMemberUri = function(rdfStore, orga){

	var listMemberUri = []
	for( var i in rdfStore.source ){
		var triple = rdfStore.source[i]

		if(	 triple.subject.nominalValue == orga.uri
		  && triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/member')
		{
			listMemberUri.push(triple.object.nominalValue)
		} // end if
	
	} // end for

	return listMemberUri

} // end getAllMemberUri


rdfStoreHelper.removeMemberTriple = function(source, orga, person){
	/*
		use low-frills deep copy. don't use shallow copy
		http://stackoverflow.com/questions/5055746/cloning-an-object-in-node-js
	*/
	var allTriple = JSON.parse(JSON.stringify(source))

	var fullname = null

	if(typeof(person.firstname) != 'undefined'){
		fullname = person.firstname.replace(' ', '-').toLowerCase()
	}

	if(typeof(person.lastname) != 'undefined'){
		fullname = fullname + person.lastname.replace(' ', '-').toLowerCase()
	}

	if(fullname == null){
		fullname = person.acronym
	}


	for( var i in allTriple ){
		if(	 allTriple[i].subject.nominalValue == orga.uri
		  && allTriple[i].predicate.nominalValue == 'http://xmlns.com/foaf/0.1/member'
		  && allTriple[i].object.nominalValue == person.uri)
		{
			allTriple[i] = null
		} // end if
	} // end for


	return underscore.compact(allTriple)

} // end removeMemberTriple



rdfStoreHelper.createMemberTriple = function(orgaUri, personUri){

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : orgaUri
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://xmlns.com/foaf/0.1/member'
		},

		'object':{
			'interfaceName' : 'NamedNode',
			'nominalValue'	: personUri
		}

	}
	return triple

} // end createMemberTriple


/************************** end member *******************************/






/*****************************************/
module.exports = rdfStoreHelper