var util = []
var underscore = require('underscore')


util.encodeUri = function(uri){

	return uri.replace('http://', '').replace(/\//g, '-').replace(/\./g, '-')

} // end endCodeUri


util.removeEleOfObjectIdArray = function(objectArray, idKey, str){

	for(var i in objectArray){
		var objStr = '"'+objectArray[i][idKey].toString()+'"'

		if(objStr == str){
			objectArray[i] = null
		}

	}
	return underscore.compact(objectArray)

} // end removeEleOfObjectIdArray



util.removeEleAcronymOfArray = function(array, acronymKey, acronym){

	for(var i in array){
	
		if(array[i][acronymKey] == acronym){
			array[i] = null
		}

	}

	return underscore.compact(array)

} // end removeEleAcronymOfArray



util.removeEleOfArray = function(array, ele){

	for(var i in array){
		var index = array.indexOf(ele)
		if(index>-1){
			array.splice(index, 1)
		}
	}

	return array

} // end removeEleOfArray



util.removeEleOfArrayByKey = function(array, key, ele){

	for(var i in array){
		if(array[i][key] == ele){
			array[i] = null
		}
	}

	return underscore.compact(array)

} // end removeEleOfArrayByKey

util.makeComp = (function(){

  var accents = {
      a: 'àáâãäåæ',
      c: 'ç',
      e: 'èéêëæ',
      i: 'ìíîï',
      n: 'ñ',
      o: 'òóôõöø',
      s: 'ß',
      u: 'ùúûü',
      y: 'ÿ'
    },
    chars = /[aceinosuy]/g;

  return function makeComp(input) {
    return input.toLowerCase().replace(chars, function(c){
      return '[' + c + accents[c] + ']';
    });
  };

}());


/********************/
module.exports = util