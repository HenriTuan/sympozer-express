var userModel = require('../models/user')
var preservedWord = require('../config/preserved_word')
var jquery = require
var accountHelper = []


accountHelper.standardlizeSocialAccount = function(newUser, newSocialAccount, socialProfile, token) {
    
    newSocialAccount._id    = socialProfile.id                 
    newSocialAccount.token = token                
    newSocialAccount.displayname  = socialProfile.displayName
    newUser.displayname = newSocialAccount.displayname

    if( typeof(socialProfile.name) != "undefined")
        if( typeof(socialProfile.name.givenName) != "undefined" 
            && typeof(socialProfile.name.familyName) != "undefined" ){
            newSocialAccount.displayname = socialProfile.name.givenName + ' ' + socialProfile.name.familyName
            newUser.displayname = newSocialAccount.displayname
        }

    if( typeof(socialProfile.emails) != "undefined"){
        newSocialAccount.email = socialProfile.emails
        newUser.email = socialProfile.emails[0].value
    }

    newUser.is_verified = true
} // end standardlizeSocialAccount



var generateAcronym = function(originAcronym, count, cb){  
    var currentAcronym
    if(count == null){
        count = 1   
        currentAcronym = originAcronym
    }
    else{
        currentAcronym = originAcronym + '.' + count
    }

    userModel.findOne({'acronym': currentAcronym}, function(err, user){
        if(user == null){ // no acronym in database == currentAcronym
            for(var i in preservedWord.userAcronym){ 
                if(preservedWord.userAcronym[i] == currentAcronym) // check if currentAcronym is preserved
                    generateAcronym(originAcronym, count+1, cb )
            } // end for
            cb(currentAcronym) // got this far -> currentAcronym is find
         }
        else {
            generateAcronym(originAcronym, count+1, cb )
        }
    }) // end userModel.findOne


} // end generateAcronym


accountHelper.createAcronym = function(userId, cb){

    userModel.findOne({'_id': userId}, function(err, user){

        if(err){
            console.log(err)
            cb(err)
        }else{
            var originAcronym = user.displayname.trim().replace(' ', '-')
            generateAcronym(originAcronym, null, function(generatedAcronym){
                user.acronym = generatedAcronym
                user.save(function(err){
                    if(err){
                        console.log(err)
                        cb(err)
                    }
                }) // end save  
            })

        } // end else

    }) // end findOne

} // end createAcronym


/****************************/
module.exports = accountHelper