
var pubHelper = []


pubHelper.htmlFormToModelFormat = function(body, userId){

	var htmlFormFormat = body.formData

	var modelFormat = {
		'_user_id'		: userId,
		'title'			: htmlFormFormat.title,
		'default_name'  : htmlFormFormat.title,
		'uri'			: htmlFormFormat.uri,
		'resume'		: htmlFormFormat.resume,
		'_conference_acronym': body.confAcronym
	}

	if( typeof(htmlFormFormat.acronym) !== 'undefined' && htmlFormFormat.acronym !== null )
		modelFormat.acronym = htmlFormFormat.acronym

	return modelFormat
} // end htmlFormToModelFormat




/*****************************************/
module.exports = pubHelper