var key = "85446011-e5da-4c3f-9c60-745cc99b24f7"
var postmark = require("postmark")(key)
var ejs = require("ejs")

 
function sendVerificationEmail(options, done) {
    
    var deliver = function (textBody, htmlBody) {
        postmark.send({
            "From": "contact@henrituan.com",
            "To": options.email,
            "Subject": "Please verify your account at sympozer.fr",
            "TextBody": textBody,
            "HtmlBody": htmlBody
        }, done)
    }
    

    ejs.renderFile("views/mailer/confirm_account.ejs", options, function (err, textBody) {
        if (err) {
            console.log(err)
            return done(err)
        }
        ejs.renderFile("views/mailer/confirm_account.ejs", options, function (err, htmlBody) {
            if (err) {
                console.log(err)
                return done(err)
            }
            deliver(textBody, htmlBody)
        })
    })

}

 
module.exports.sendVerificationEmail = sendVerificationEmail