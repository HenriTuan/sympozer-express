var preservedWord = require('../config/preserved_word')
var sha1 = require('sha1')
var personHelper = []

var n3 = require('n3')
var n3Util = n3.Util

var util = require('./util')


personHelper.htmlFormToModelFormat = function(body, userId){

	var htmlFormFormat = body.formData
	var displayname = htmlFormFormat.firstname+' '+htmlFormFormat.lastname

	var sha = sha1(htmlFormFormat.email)
	var uri = 'http://data.semanticweb.org/person/'+displayname.toLowerCase().replace(/ /g, '-')
	var slug = util.encodeUri(uri)

	var modelFormat = {
		'uri'			: uri,
		'firstname'		: htmlFormFormat.firstname,
		'lastname'		: htmlFormFormat.lastname,
		'email'			: htmlFormFormat.email,
		'sha'           : sha,
		'displayname'	: displayname,
		'slug'			: slug,

		'country'		: htmlFormFormat.country,
		'ava_path'		: htmlFormFormat.ava_path,
		'homepage'		: htmlFormFormat.homepage,
		'social_account': htmlFormFormat.social_account
	}

	return modelFormat

} // end htmlFormToModelFormat



personHelper.modelToEditFormFormat = function(personModel){
	
	var eidtFormFormat = {
		'firstname'   : personModel.firstname,
		'lastname'    : personModel.lastname,
		'acronym'     : personModel.acronym,
		'email'       : personModel.email,
		'homepage'    : personModel.homepage,
		'location'    : personModel.location,
		'ava_path'    : personModel.ava_path,
		'homepage'    : personModel.homepage,
		'social_account': personModel.social_account
	}

	return eidtFormFormat
} // end modelToEditFormFormat


personHelper.editFormToModelFormat = function(body, userId){



} // end editFormToModelFormat


var generateAcronym = function(originAcronym, count, cb){  
	var currentAcronym
	if(count == null){
		count = 1   
		currentAcronym = originAcronym
	}
	else{
		currentAcronym = originAcronym + '.' + count
	}

	personModel.findOne({'acronym': currentAcronym}, function(err, person){
		if(person == null){ // no acronym in database == currentAcronym
			for(var i in preservedWord.userAcronym){ 
				if(preservedWord.userAcronym[i] == currentAcronym) // check if currentAcronym is preserved
					generateAcronym(originAcronym, count+1, cb )
			} // end for
			cb(currentAcronym) // got this far -> currentAcronym is fine
		 }
		else {
			generateAcronym(originAcronym, count+1, cb )
		}
	}) // end personModel.findOne


} // end generateAcronym


personHelper.createAcronym = function(personId, cb){

	personModel.findOne({'_id': personId}, function(err, person){

		if(err){
			console.log(err)
			cb(err)
		}else{
			var originAcronym = person.displayname.trim().replace(' ', '-')
			generateAcronym(originAcronym, null, function(generatedAcronym){
				person.acronym = generatedAcronym
				person.save(function(err){
					if(err){
						console.log(err)
						cb(err)
					}else{
						cb(null, generatedAcronym)
					}
				}) // end save  
			})

		} // end else

	}) // end findOne

} // end createAcronym


personHelper.createAllTriple = function(person){

	var fullname = person.firstname.replace(' ', '-').toLowerCase() + '-' + person.lastname.trim().replace(' ','-').toLowerCase()

	var allTriple = [

		personHelper.createTriple(fullname, 'http://xmlns.com/foaf/0.1/lastName', person.lastname),
		personHelper.createTriple(fullname, 'http://xmlns.com/foaf/0.1/firstName', person.firstname),
		personHelper.createTriple(fullname, 'http://xmlns.com/foaf/0.1/name', person.displayname),
		personHelper.createTriple(fullname, 'http://www.w3.org/2000/01/rdf-schema#label', person.displayname),
		personHelper.createTriple(fullname, 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type', 'http://xmlns.com/foaf/0.1/Person'),
		personHelper.createTriple(fullname, 'http://xmlns.com/foaf/0.1/homepage', person.homepage),
		personHelper.createTriple(fullname, 'http://xmlns.com/foaf/0.1/mbox_sha1sum', person.sha),
		personHelper.createTriple(fullname, 'http://xmlns.com/foaf/0.1/based_near', 'http://dbpedia.org/resource/'+person.location.country.replace(' ', '_')),

	] // end allTriple


	for(var i in personHelper.social_account){
		allTriple.push( personHelper.createTriple(fullname, 'http://xmlns.com/foaf/0.1/OnlineAccount', personHelper.social_account[i]) )			
	}

	return allTriple

}


personHelper.createTriple = function(fullname, predicate, object){

	var nodeType = 'NamedNode'
	if(n3Util.isBlank(object)){
		nodeType = 'BlankNode'
	}

	var triple = {

		'subject':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : 'http://data.semanticweb.org/person/'+fullname
		},

		'predicate':{
			'interfaceName' : 'NamedNode',
			'nominalValue'  : predicate
		},

		'object':{
			'interfaceName' : nodeType,
			'nominalValue'  : object
		}
	}
	return triple
}



/*****************************************/
module.exports = personHelper