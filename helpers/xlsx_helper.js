const XLSX = require('xlsx');

import ConferenceMetier from '../metiers/conference';
import LocationMetier from '../metiers/location';
import EventMetier from '../metiers/event';
import PersonMetier from '../metiers/person';
import RoleMetier from '../metiers/role';
import OrganisationMetier from '../metiers/organization'
import PublicationMetier from '../metiers/publication'

export default class xlsxHelper {
    constructor(filePath, fileName) {
        this.filePath = filePath;
        this.fileName = fileName;
    }

    parseFile() {
        return new Promise((resolve, reject) => {

            try{
                const workbook = XLSX.readFile(this.filePath);
                const worksheet = workbook.Sheets[workbook.SheetNames[1]];
                let confInfo = {};

                //informations of the conference
                confInfo.name = worksheet['A1'].v.split('-')[0].trim();
                console.log('name : ' + confInfo.name);

                const regex = /\d{2}\/\d{2}/;
                let tmp = worksheet['C3'].v.match(regex)[0].split("/");
                confInfo.datebegin = tmp[1] + "/" + tmp[0] + "/" + "2018";

                tmp =  worksheet['Q3'].v.match(regex)[0].split("/");
                confInfo.dateend = tmp[1] + "/" + tmp[0] + "/" + "2018";

                //raw informations // TODO : recup from document ?
                confInfo.uri = "https://w3id.org/scholarlydata/conference/eswc2018";
                confInfo.acronym = "ESWC2018";
                confInfo.location = "France";
                confInfo.region = "Rhone-Alpes";
                confInfo.language = "FR";

                //informations of events
                //TODO

                resolve(confInfo);
            }
            catch (err){
                reject(err);
            }
        })
    }

    persist(userId, cb){
        this.parseFile()
            .then((info)=>{
                const conferenceMetier = new ConferenceMetier();
                conferenceMetier.add(info.name, info.uri, info.acronym, null, null, info.datebegin, info.dateend, info.location, info.region, null, info.language, userId)
                    .then((conference) =>{
                        return cb(null, conference);
                    })
                    .catch((err) =>{
                    console.log(err);
                })
            })
            .catch((err)=>{
                return cb(err, null);
            })

    }

}