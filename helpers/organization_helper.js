var preservedWord = require('../config/preserved_word')
var sha1 = require('sha1')


var orgaHelper = []


orgaHelper.htmlFormToModelFormat = function(body, userId){

	var htmlFormFormat = body.formData
	var logoUrl = body.logoUrl

	var modelFormat = {
		'_user_id'		: userId,
		'name'			: htmlFormFormat.name,
		'default_name'  : htmlFormFormat.name,
		'uri'			: 'http://data.semanticweb.org/organization/'+htmlFormFormat.name.toLowerCase().replace(' ', '-'),
		'_conference_acronym': body.confAcronym,
		'description'	: htmlFormFormat.description,
		'logo_path'		: logoUrl,
		'homepage'		: htmlFormFormat.homepage,
	}

	if( typeof(htmlFormFormat.acronym) !== 'undefined' && htmlFormFormat.acronym !== null )
		modelFormat.acronym = htmlFormFormat.acronym

	return modelFormat
} // end htmlFormToModelFormat



orgaHelper.modelToHtmlFormFormat = function(orgaModel){
	

	var htmlFormat = {
		'_id'		  : orgaModel._id,
		'name'		  : orgaModel.name,
		'acronym'	  : orgaModel.acronym,
		'homepage'	  : orgaModel.homepage,
		'description' : orgaModel.description,
		'logo_path'	  : orgaModel.logo_path
	}

	return htmlFormat
}





/*****************************************/
module.exports = orgaHelper