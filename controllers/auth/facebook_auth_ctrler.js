var passport = require('passport')
var express = require('express')
var router = express.Router()
var configAuth = require('../../config/auth')

var User = require('../../models/user')
var accountHelper = require('../../helpers/account_helper')

var facebookStrategy = require('passport-facebook').Strategy

passport.serializeUser(function(user, done) {
  done(null, user)
})

passport.deserializeUser(function(user, done) {
  done(null, user)
})

passport.use(new facebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL
    },

// facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // find the user in the database based on their facebook id
            User.findOne({ 'facebook.id' : profile.id }, function(err, user) {

                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err)

                // if the user is found, then log them in
                if (user) {
                    return done(null, user) // user found, return that user
                } else {
                    // if there is no user found with that facebook id, create them
                    var newUser            = new User()
                  //  accountHelper.standardlizeFBaccount(newUser, profile, token)
                    accountHelper.standardlizeSocialAccount(newUser, newUser.facebook, profile, token)
                    // save our user to the database
                    newUser.save(function(err) {
                        if (err)
                            throw err

                        // if successful, return the new user
                        return done(null, newUser)
                    })
                }

            })
        }) // end callback function
    })
) // end passport.use


// =====================================
// FACEBOOK ROUTES =====================
// =====================================
// route for facebook authentication and login
router.get('/', passport.authenticate('facebook', {scope: 'email'}))


// handle the callback after facebook has authenticated the user
router.get('/callback',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.

    // store user in session
    var sess = req.session
    sess.user = req.user

    res.redirect('/')
  })


// route for logging out
router.get('/logout', function(req, res) {
    req.logout()
    res.redirect('/')
})



// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next()
    console.log('in isLogged')
    // if they aren't redirect them to the home page
    res.redirect('/')
}

module.exports = router