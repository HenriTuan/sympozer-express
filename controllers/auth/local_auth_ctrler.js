var express = require('express');
var router = express.Router();
import JwtMetier from '../../metiers/jwt';

var User = require('../../models/user')

var VerificationTokenModel = require('../../models/security/verification_token')

var emailHelper = require('../../helpers/email_helper')
var accountHelper = require('../../helpers/account_helper')

var sha1 = require('sha1')

// config 'passport' for security
var passport = require('passport')
var passport = require('passport')
var localStrategy = require('passport-local').Strategy

passport.use(new localStrategy(User.authenticate()))
passport.serializeUser(User.serializeUser())
passport.deserializeUser(User.deserializeUser())

// global variable
var verificationToken = []

// controllers
router.get('/', function (req, res) {
    var sess = req.session
    res.render('index', { user : sess.user })
})

/*router.get('/register', function(req, res) {
    res.render('register', { user : req.user })
})

router.post('/register', function(req, res) {
    
    var sha = sha1(req.body.username)

    User.register(new User({ username : req.body.username,
                             firstname: req.body.firstname,
                             lastname : req.body.lastname,
                             displayname: req.body.firstname + ' ' + req.body.lastname,
                             email    : req.body.username,
                             sha: sha,
                             homepage : req.body.homepage
                          }), 
                             req.body.password, 
                             function(err, user) {
        if (err) {
            console.log(err)
            return res.render('register', { user : user, errMessage : err })
        }
        
        passport.authenticate('local')(req, res, function () {
            // send verfication email with token

            if(verificationToken.length == 0)
               verificationToken = new VerificationTokenModel({_userId: req.user._id})
              
            verificationToken.createVerificationToken(function (err, token) {
                if (err) 
                return console.log("Couldn't create verification token", err)
                
                var message = {
                    email: user.email,
                    name: user.displayname,
                    // verifyURL: req.protocol + "://" + req.get('host') + "/verify/" + req.user._id+"/"+ token
                    // verifyURL: req.protocol + "://" + process.argv[4] + "/verify/" + req.user._id+"/"+ token
                    verifyURL: process.argv[4] + "/verify/" + req.user._id+"/"+ token
                }

                // if( typeof(process.argv[5]) !== 'undefined' && process.argv[5] !== null ){ // if port exist
                //   message.verifyURL = req.protocol + "://" + process.argv[4] + ':' + process.argv[5] + "/verify/" + req.user._id+"/"+ token
                // }else{
                //   message.verifyURL = req.protocol + "://" + process.argv[4] + "/verify/" + req.user._id+"/"+ token
                // }
                console.log('verify URL: '+message.verifyURL)

                emailHelper.sendVerificationEmail(message, function (error, success) {
                    if (error) {
                        // not much point in attempting to send again, so we give up
                        // will need to give the user a mechanism to resend verification
                        console.error("Unable to send via postmark: " + error.message)
                        return
                    }
                    console.info("Sent to postmark for delivery")
                }) // end sendVerificationEmail
            }) // end createVerificationToken
            //return res.redirect('/verification-mail-sent')
            return res.render('index', {'message': 'An email has been sent to '
                                                  +req.user.email
                                                  +'. Please follow the instruction in the email to complete your registration.'
                                        })
        }) // end passport authenticate

    }) // end user.register
})// end register post


router.get("/verify/:user_id/:token", function (req, res, next) {
    var token = req.params.token
    var user_id = req.params.user_id

    var verificationToken = new VerificationTokenModel({_userId: user_id})
    
    verificationToken.verifyUser(token, function(err) {
        if (err) return res.redirect("verification-failure")
        else{
            accountHelper.createAcronym(user_id)
            res.redirect("/verification-success")
        }
    })
})

router.get("/verification-success", function(req, res, next){
    res.render('message/verification-success')
})

router.get("/verification-failure", function(req, res, next){
    res.render('message/verification-failure')
})

router.get("/unverified", function(req, res, next){
    res.render('message/unverified')
})

router.get('/login', function(req, res) {
    res.render('login', { user : req.user })
}) // end login get

router.post('/login', function(req, res, next) {

  passport.authenticate('local', function(err, user, info) {

    if (err) { 
      console.log(err)
      return next(err)
    }
    if (!user) {
      console.log('can\'t find user')
      return res.render('login', { errMessage : 'Email address or password wrong.'})
    }
    req.logIn(user, function(err) {
      if (err) { 
        console.log(err)
        return next(err)
    }
    if(!user.is_verified){
       res.redirect('/unverified')
    }

      // store user in session
      req.session.user = req.user
      return res.redirect('/')
    })
  })(req, res, next)

}) // end login post */

router.get('/login/callback', function(req, res) {
    const token = req.query.token;

    if (token) {
        new JwtMetier().verifyToken(token)
            .then(decoded => {
                const id = decoded.id;

                req.session.user = {
                    _id: id
                };
                return res.redirect('/');
            })
            .catch(err => {
                return res.render('message/access-denied', {'message': err})
            });
    } else {
        return res.render('message/access-denied', {'message': 'No token provided'});
    }
});

router.get('/logout', function(req, res) {
    req.logout()
    req.session.user = null
    delete req.session['user']
  
    res.redirect('/')
}) // end logout

/*router.get('/ping', function(req, res){
    res.status(200).send("pong!")
})*/

module.exports = router