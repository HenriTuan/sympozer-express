/*
    Create by Pierre Marsot on 26/02/2018
*/
const express = require('express');
const router = express.Router();
import PublicationMetier from '../../metiers/publication';
import EventMetier from '../../metiers/event';
import TrackMetier from '../../metiers/track';

/**
 * Permet d'afficher la page pour gérer les tracks
 */
router.get("/:uri", async function (req, res) {
    const uri = req.params.uri;

    if(!uri || uri.length === 0) {
        return res.json({error: 'You must to specify an URI.'});
    }

    try {
        const publicationMetier = new PublicationMetier();

        //On récupère la publication
        const publication = await publicationMetier.getByUri(req.params.uri);

        if(!publication) {
            return res.json(403, {error: 'Error when retrieving publication'});
        }

        //On récupère l'event
        const eventMetier = new EventMetier();

        const events = await eventMetier.findByIdPublicationWithoutConference(publication._id);

        if(!events || events.length === 0) {
            return res.json(403, {error: 'Error when retrieving events'});
        }

        //Normalement on ne devrait avoir qu'un event
        const event = events[0];

        //On regarde si l'event est lié à une track
        if(!event.idTrack) {
            return res.json(403, {error: 'Error when retrieving track'});
        }

        //On récupère la track
        const trackMetier = new TrackMetier();
        const track = await trackMetier.getById(event.idTrack);

        if(!track) {
            return res.json(403, {error: 'Error when retrieving track'});
        }

        //On regarde si la track est votable
        if(!track.is_votable) {
            return res.json(403, {error: 'Track not votable'})
        }

        //On envoie
        return res.json({
            track_uri: track.uri,
        });
    }
    catch(e) {
        return res.json(403, {error: e});
    }
});

module.exports = router;