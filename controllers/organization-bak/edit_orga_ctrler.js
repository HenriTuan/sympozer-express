var express = require('express')

var router  = express.Router()

var orgaHelper = require('../../helpers/organization_helper')
var orgaDAO = require('../../DAO/organization_DAO')



router.get('/:acronym/edit', function(req, res) {

	if(req.orgaModel._user_id != req.session.user._id){
		return res.render('message/access-denied', {'message' : 'This is not your organization, you don\'t have right to edit it.'})
	}else{
		var orga = orgaHelper.modelToHtmlFormFormat(req.orgaModel)
		return res.render('organization/edit', {'orga': orga,
												'isOwner': true,
										  		'activeCode': 'edit'
										  		}
						)
	}

	
}) // end get /:acronym/edit


router.post('/:acronym/save', function(req, res){
	
	var sess = req.session
	
	// update orga
	var newOrgaModel = orgaHelper.htmlFormToModelFormat(req.body, sess.user._id)
	
	orgaDAO.save(newOrgaModel, function(err){

		if(err){
			console.log(err)
			switch(err.code){
				case 11000: 
					res.send({'state'	: 'error',
							  'message' : 'This URI has already been used. Please take another one.',
							  'redirect': '/'})
					break
				default:
					throw err
			}
		}else{
			// return persist success signal
			return res.send({'state'	: 'success',
							 'message'	: 'Organization saved!',
							 'redirect'	: '/organization/'+newOrgaModel.acronym})
		}
	}) // end update orga
	

}) // end /save


/**********************************/
module.exports = router