var express = require('express')

var router  = express.Router()
var orgaDAO = require('../../DAO/organization_DAO')
var orgaHelper = require('../../helpers/organization_helper')




// show all organizations
router.get('/all', function(req, res){

	var userId = null
	if( typeof(req.session.user) !== 'undefined' && req.session.user != null )
		userId = req.session.user._id

	orgaDAO.getAll(userId, function(err, listOrga){

		if(err){
			return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
		}else{
			return res.render('organization/show-all', {'listOrga': listOrga})
		}


	}) // end getAll

}) // end get /all



// show 1 organization
router.get('/:acronym', function(req, res) {
	var orga = orgaHelper.modelToHtmlFormFormat(req.orgaModel)

	return res.render('organization/overview', {'orga': orga, 
										  		'isOwner': req.isOwner,
										  		'activeCode': 'overview'
							 			 		}
				 )

}) // end get /:acronym





/**********************************/
module.exports = router