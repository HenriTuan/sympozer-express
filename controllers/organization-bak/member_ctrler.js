var express = require('express')
var router  = express.Router()

var orgaHelper = require('../../helpers/organization_helper')
var orgaDAO = require('../../DAO/organization_DAO')


router.get('/:acronym/members', function(req, res){
	
	orgaDAO.getListMemberByAcronym(req.params.acronym, function(err, listMember){

		if(err) 
			res.render('message/page-not-found', {'message': 'Something has gone wrong in our server. Pleas try again later'})
		else{
			var orga = orgaHelper.modelToHtmlFormFormat(req.orgaModel)
			return res.render('organization/member', {'orga': orga,
												  'isOwner': req.isOwner,
												  'activeCode': 'member',
												  'listMember': listMember
												}
						 )
		}

	}) // end getListMemberByAcronym


}) // end get/:acronym/member



// post get-list-avaiable-member
router.post('/:acronym/members/get-list-avaiable-member', function(req, res){

	var orgaId = req.body.orgaId

	orgaDAO.getListAvaiableMember(orgaId, function(err, listAvailablePerson){
		if(err){
			console.log(err)
			return res.render('message/page-not-found', {'message': 'We can\'t load list of avaiable people. Please try again later.'})
		}else{
			return res.render('component/list-user.jade', {'listAvailablePerson': listAvailablePerson})
		}

	})// end getConfRdfStoreByAcronym	

}) // end post /get-list-member


// post add member to orga
router.post('/:acronym/members/add-member-to-orga', function(req, res){

	var listPersonId = req.body.listPersonId
	orgaDAO.addlistMemberIdByAcronym(req.params.acronym, listPersonId, function(err){

		if(err){
			console.log(err)
			return res.send({'state': 'error', 'message': 'We can\'t add member. Please try again later.'})
		}else{
			return res.send({'state': 'success'})
		}

	})// end getConfRdfStoreByAcronym	

}) // end post /get-list-member



// post remove member from orga
router.post('/:acronym/members/remove-member-from-orga', function(req, res){

	orgaDAO.removePersonById(req.params.acronym, req.body, function(err){

		if(err){
			console.log(err)
			return res.send({'state': 'error', 'message': 'We can\'t remove this member. Please try again later.'})
		}else{
			return res.send({'state': 'success', 'message': 'Person removed.', 'redirect': 'reload'})
		}

	})// end removePersonById

}) // end post remove member from orga




/**********************************/
module.exports = router