var express = require('express')
var router  = express.Router()

var orgaDAO = require('../../DAO/organization_DAO')
var orgaHelper = require('../../helpers/organization_helper')


// create new organization
router.get('/create', function(req, res){

	return res.render('organization/create')

}) // end get /create



router.post('/persist', function(req, res){
	
	var sess = req.session

	// persist newOrga
	var newOrgaModel = orgaHelper.htmlFormToModelFormat(req.body, sess.user._id)

	orgaDAO.persist(newOrgaModel, req.session.user, function(err, orgaAcronym){
		if(err){
			switch(err.code){
				case 11000: 
					res.send({'state'	: 'error',
							  'message' : 'This name has already been used. Please take another one.',
							  'redirect': '/'})
					break
				default:
					console.log(err)
					res.send({'state'	: 'error',
							  'message' : 'Something in server has gone wrong, please try again later, thankyou for your comprehension.',
							  'redirect': '/'})
			}
		}else{
			// return persist success signal
			return res.send({'state'	: 'success',
							 'message'	: 'Orga saved!',
							 'redirect'	: '/organization/'+orgaAcronym})
		}
	}) // end persist
		
}) // end /persist



/**********************************/
module.exports = router