var express = require('express')

var router  = express.Router()
var orgaDAO = require('../../DAO/organization_DAO')



router.post('/delete/persist', function(req, res){
	
	orgaDAO.deleteByAcronym( req.body.acronym, function(err){

		if(err){
			switch(err){
				case -1: 
					res.send({'state'	: 'error',
							  'message' : 'This organization is not found in database.',
							  'redirect': '/'})
					break
				default:
					throw err
			}
		}else{
			// return persist success signal
			return res.send({'state'	: 'success',
							 'message'	: 'Organization deleted!',
							 'redirect'	: '/organization/all'})
		}

	}) // end delete


}) // end /persist


/**********************************/
module.exports = router