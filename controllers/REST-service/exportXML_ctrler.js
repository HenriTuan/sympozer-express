var express = require('express')
var router = express.Router()
var userModel = require('../../models/user')

var xml2js = require('xml2js')

// route for get user in xml
router.get('/user', function(req, res) {
	userModel.schema.methods.getAll(function(listUser){
		var builder = new xml2js.Builder()
		var xml = builder.buildObject(listUser)

	  	res.set('Content-Type', 'application/rss+xml');
		res.send(xml)
	}) // end getAll methods

}) // end /user route



module.exports = router