var express = require('express')

var router  = express.Router()

var personHelper = require('../../helpers/person_helper')
var personDAO = require('../../DAO/person_DAO')



// create new person
router.get('/:acronym/edit', function(req, res) {

	personDAO.getByAcronym(req.params.acronym, null, function(err, personModel){
		if(err) throw err
		else
			if(personModel == null) 
				return res.render('message/page-not-found', {'message' : 'Can\'t find person.'})
			else{
				if(personModel._user_id != req.session.user._id){
				// if(false){
					return res.render('message/access-denied', {'message' : 'This is not your person, you don\'t have right to edit her/him'})
				}else{
					var person = personHelper.modelToEditFormFormat(personModel)
					return res.render('person/edit', {'person': person})
				}
			}
	}) // end getByAcronym

	
}) // end get /:acronym/create


router.post('/:acronym/save', function(req, res){
	
	var sess = req.session
	// update person
	var newPersonModel = personHelper.htmlFormToModelFormat(req.body, sess.user._id)
	
	personDAO.save(newPersonModel, req.session.user, function(err){

		if(err){
			console.log(err)
			switch(err.code){
				case 11000: 
					res.send({'state'	: 'error',
							  'message' : 'This URI has already been used. Please take another one.',
							  'redirect': '/'})
					break
				default:
					throw err
			}
		}else{
			// return persist success signal
			return res.send({'state'	: 'success',
							 'message'	: 'Conference saved!',
							 'redirect'	: '/person/'+newPersonModel.acronym})
		}
	}) // end update person
	

}) // end /save


/**********************************/
module.exports = router