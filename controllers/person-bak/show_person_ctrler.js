var express = require('express')
var router  = express.Router()

var personDAO = require('../../DAO/person_DAO')



// show all persons
router.get('/all', function(req, res){

	var userId = null
	if( typeof(req.session.user) !== 'undefined' && req.session.user != null )
		userId = req.session.user._id

	personDAO.getAll(userId, function(err, listPerson){

		if(err){
			return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
		}else{
			return res.render('person/show-all', {'listPerson': listPerson})
		}


	}) // end getAll

}) // end get /all


router.get('/:acronym', function(req, res){

	var personAcronym = req.params.acronym
	
	var userId = null
	if( typeof(req.session.user) !== 'undefined' && req.session.user != null )
		userId = req.session.user._id	


	personDAO.getByAcronym(personAcronym, userId, function(err, person){

		if(err){
			return res.render('message/page-not-found', {'message': 'Person not existed.'})
		}else{

			return res.render('person/show', {'person': person})
		}

	}) // end getByAcronym


}) // end get /creates





/**********************************/
module.exports = router