var express = require('express')

var router  = express.Router()
var personDAO = require('../../DAO/person_DAO')


router.post('/delete/persist', function(req, res){
	
	personDAO.deleteByAcronym( req.body.acronym, function(err){

		if(err){
			switch(err){
				case -1: 
					res.send({'state'	: 'error',
							  'message' : 'This person is not found in database.',
							  'redirect': '/'})
					break
				default:
					throw err
			}
		}else{
			// return persist success signal
			return res.send({'state'	: 'success',
							 'message'	: 'Person deleted!',
							 'redirect'	: '/person/all'})
		}

	}) // end delete


}) // end /persist


/**********************************/
module.exports = router