var express = require('express')
var router  = express.Router()

var personDAO = require('../../DAO/person_DAO')
var personHelper = require('../../helpers/person_helper')


// create new person
router.get('/create', function(req, res){

	return res.render('person/create')

}) // end get /create



router.post('/persist', function(req, res){
	
	var sess = req.session

	// persist newPerson
	var newPersonModel = personHelper.htmlFormToModelFormat(req.body, sess.user._id)

	personDAO.persist(newPersonModel, req.session.user, function(err, personAcronym){
		if(err){
			switch(err.code){
				case 11000: 
					res.send({'state'	: 'error',
							  'message' : 'This email has already been used. Please take another one.',
							  'redirect': '/'})
					break
				default:
					console.log(err)
					res.send({'state'	: 'error',
							  'message' : 'Something in server has gone wrong, please try again later, thankyou for your comprehension.',
							  'redirect': '/'})
			}
		}else{
			// return persist success signal
			return res.send({'state'	: 'success',
							 'message'	: 'Person saved!',
							 'redirect'	: '/person/'+personAcronym})
		}
	}) // end persist
		
}) // end /persist



/**********************************/
module.exports = router