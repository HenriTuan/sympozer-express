var express = require('express')
var router  = express.Router()


var multer = require('multer')
var upload = multer({ dest: 'public/upload' }).single('image')


router.post('/upload-image', upload, function(req, res){

	if( typeof(req.file) !== "undefined"){

		upload(req, res, function (err) {
		 	if(err){
		 		return res.send({'state': 'err', 'message': err})
	    	}else{
	    		var imgUrl = req.file.path.replace('public\\upload', '/upload').replace('\\','/').replace('public', '')
	    		return res.send({'state': 'success', 'imgUrl': imgUrl})
	    	}
	  	}) // end upload

	}else{
		return res.send({'state': 'success', 'imgUrl': '/img/sympozer_logo.png'})	// default image
	} // end if req.file !== undefined

}) // end post /upload-image



/**********************************/
module.exports = router