var express = require('express');
var router  = express.Router();

var fs 		= require('fs');
var n3 		= require('n3');
var RdfXmlParser = require('rdf-parser-rdfxml');
var async = require('async');

var confHelper = require('../../helpers/conference_helper');
var conferenceModel = require('../../models/conference');

var rdfStoreHelper = require('../../helpers/rdf_store_helper');
var rdfStoreModel = require('../../models/dataset/rdf_store');

var jsonldHelper = require('../../helpers/jsonld_helper');

var multer = require('multer');
var upload = multer({ dest: 'public/upload' }).single('datasetFile');

import TurtleMetier from '../../metiers/turtle';
import TurtleHelper from '../../helpers/turtle_helper'


// read rdf, jsonld file
router.post('/import-dataset', function(req, res) {

    const sess = req.session;

    if(!sess || !sess.user || !sess.user._id) {
        return res.send({'state': 'err', 'message': 'error'});
	}

	upload(req, res, function (err) {

		if(err){
            return res.send({'state': 'err', 'message': err});
		}else{

			// var path = req.file.path.replace('public\\upload', '/upload').replace('\\','/')
			const fileExt = req.file.originalname.split('.').pop();

			switch(fileExt){
                case 'ttl':
					new TurtleMetier()
						.importDataset(sess.user._id, req.file.path)
						.then((conf) => {
                            res.send({
                                'state': 'success',
                                'message': 'File imported!',
                                'redirect': '/conference/'+conf._id
                            });
						})
						.catch((e) => {
							console.log("catch import", e);
                            return res.send({'state': 'err', 'message': e});
						});
                    break;
				case 'rdf':
					persistRdf(req.file.path, req.file.originalname, req.session.user._id, function(err, conf){

						if(err){
							return res.send({'state': 'err', 'message': err})
						}else{
							res.send({
                                'state': 'success',
                                'message': 'File imported!',
                                'redirect': '/conference/'+conf.acronym
                            });
						}

					}); // end persistRdf
					break;

/*				case 'jsonld':
					persistJsonld(req.file.path, req.file.originalname, req.session.user._id, function(err, conf){

						if(err){
							console.log(err);
							return res.send({'state': 'err', 'message': err});
						}else{
							var response = {
								'state': 'success',
								'message': 'File imported!',
								'redirect': '/conference/'+conf.acronym   
							};
							res.send(response);
						}

					}); // end persistJsonld
					break;

				case 'xlsx':
					//console.log(req.file.path + ' ::: ' + req.file.originalname);
					reader = new xlsxHelper(req.file.path, req.file.originalname);
					reader.persist(req.session.user._id, (error, result) =>{
						if(error){
                            console.log(error);
                            return res.send({'state': 'err', 'message': error});
						}else{
							console.log(result);
							var response = {
                                'state': 'success',
                                'message': 'File imported!',
                                'redirect': '/conference/'+result.id
                            }
                            res.send(response);
						}
					});
					break;

                case 'ttl':
                	let turtleHelper = new TurtleHelper(req.file.path);
                	turtleHelper.persist(req.session.user._id, (error, result) =>{
                        if(error){
                            console.log(error);
                            return res.send({'state': 'err', 'message': error});
                        }else{
                            console.log(result);
                            var response = {
                                'state': 'success',
                                'message': 'File imported!',
                                'redirect': '/conference/'+result.id
                            };
                            res.send(response);
                        }
					});
                	break;
                	*/

				default :
					return res.send({'state': 'err', 'message': 'file not supported'});
			} // end switch

		} // end else

	}) // end upload

});// end /rdf



function persistJsonld(path, fileName, userId, cb){

	var acronym = fileName;
	var shortUri = fileName.replace("www", "www.");

	fs.readFile(path, 'utf8', function(err, data){
		
		if(err){
			return cb(err, null);
		}

		var dataset = JSON.parse(data.toString());
		var graph = dataset["@graph"];
		var triples = [];

		for(var i in graph){

			var node = graph[i];

			switch(node['@type']){

				case 'cofunc:InProceedings':
					var triple = jsonldHelper.parsePublication(node);
					break;
			}

			triples.push(triple);

		} // end for graph

		// console.log(triples);

		

		/*
		var rdfStore = 
			new rdfStoreModel({
							'file_name' : fileName,
							'file_path' : path,
							'acronym'	: acronym,
							'source' 	: result,
							'uri'		: uri
							});

		async.waterfall([

				function saveConference(cb){

					confHelper.rdfStoreToConfModel(rdfStore, userId, function(err, newConfModel){
						
						if(err){
							console.log(err)
							return cb(err)
						}else{
							conferenceModel.create(newConfModel, function(err, conf){
								if(err){
									console.log(err)
									return cb(err)
								}else{
									return cb(null, conf)
								}
							}) // end conferenceModel.create
						}

					}) // end rdfStoreToConfModel

				}, // end saveConference

				function saveRdfStore(conf, cb){

					rdfStore.acronym = conf.acronym
					rdfStore._user_id = conf._user_id
								 
					rdfStore.save(function(err){
						if(err) {
							console.log(err)
							return cb(err)
						}
						else{
							return cb(null, conf) // sucess, return err=null           
							// return cb(null, conf.acronym)
						}
					}) // end rdfStore.save

				}, // end saveRdfStore

				function saveElement(rdfStore, conf, cb){

					conf.list_person = rdfStoreHelper.getListPerson(rdfStore)
					conf.list_location = rdfStoreHelper.getListLocation(rdfStore)
					conf.list_role = rdfStoreHelper.getListRole(rdfStore)
					conf.list_publication = rdfStoreHelper.getListPublication(rdfStore)
					conf.list_event = rdfStoreHelper.getListEvent(rdfStore)
					conf.list_organization = rdfStoreHelper.getListOrganization(rdfStore)

					cb(null, conf)

				} // end saveElement


			], function(err, rdfStore, conf){

				conf.save(function(err){

					console.log('file imported: '+conf.acronym)
					return cb(err, conf)

				}) // end conf.save

				
			}) // end async.waterfall

			*/

	}); // end fs.readFile

} // end persistJsonld



function persistRdf(path, fileName, userId, cb){

	var acronym = fileName
	var shortUri = fileName.replace("www", "www.")

	// var store = n3.Store()
	fs.readFile(path, 'utf8', function(err, data) {

		var result = []

		var parser = new RdfXmlParser()
		parser.process(data.toString(), function (triple) {

			result.push(triple)

		}, shortUri) // <-- www.example.org
		// end parser.process
		.then(function () {

			var acronym = confHelper.getTripleObjectbyPredicate(result, 'http://data.semanticweb.org/ns/swc/ontology#hasAcronym')
			var uri = confHelper.getTripleObjectbyPredicate(result, 'http://data.semanticweb.org/ns/swc/ontology#completeGraph').replace('/complete', '')

			// save new store to db
			var rdfStore = 
				new rdfStoreModel({
								'file_name' : fileName,
								'file_path' : path,
								'acronym'	: acronym,
								'source' 	: result,
								'uri'		: uri
								})

			async.waterfall([

				function saveConference(cb){

					confHelper.rdfStoreToConfModel(rdfStore, userId, function(err, newConfModel){
						
						if(err){
							console.log(err)
							return cb(err)
						}else{
							conferenceModel.create(newConfModel, function(err, conf){
								if(err){
									console.log(err)
									return cb(err)
								}else{
									return cb(null, conf)
								}
							}) // end conferenceModel.create
						}

					}) // end rdfStoreToConfModel

				}, // end saveConference

				function saveRdfStore(conf, cb){

					rdfStore.acronym = conf.acronym
					rdfStore._user_id = conf._user_id
								 
					rdfStore.save(function(err){
						if(err) {
							console.log(err)
							return cb(err)
						}
						else{
							return cb(null, conf); // sucess, return err=null           
							// return cb(null, conf.acronym)
						}
					}) // end rdfStore.save

				}, // end saveRdfStore

				function saveElement(conf, cb){

					conf.list_person = rdfStoreHelper.getListPerson(rdfStore)
					conf.list_location = rdfStoreHelper.getListLocation(rdfStore)
					conf.list_role = rdfStoreHelper.getListRole(rdfStore)
					conf.list_publication = rdfStoreHelper.getListPublication(rdfStore)
					conf.list_event = rdfStoreHelper.getListEvent(rdfStore)
					conf.list_organization = rdfStoreHelper.getListOrganization(rdfStore)

					cb(null, rdfStore, conf); // sucess, return err=null

				} // end saveElement


			], function(err, rdfStore, conf){

				conf.save(function(err){

					console.log('file imported: '+conf.acronym)
					return cb(err, conf)

				}) // end conf.save

				
			}) // end async.waterfall


 
		}) // parser.then

	}) // end fs.readFile

} // end persistRdf



/**********************************/
module.exports = router