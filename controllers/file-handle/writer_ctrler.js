var express = require('express');
var router = express.Router();

var fs = require('fs');
var n3 = require('n3');
var RdfXmlParser = require('rdf-parser-rdfxml');
var async = require('async');

var confHelper = require('../../helpers/conference_helper');
var conferenceModel = require('../../models/conference');
var confDAO = require('../../DAO/conference_DAO');

var orgaDAO = require('../../DAO/organization_DAO');

var rdfStoreHelper = require('../../helpers/rdf_store_helper');
var rdfStoreModel = require('../../models/dataset/rdf_store');

var xml2js = require('xml2js');
var mime = require('mime');

var path = require('path');
var jBuilder = require('jbuilder');

import TurtleHelper from '../../helpers/turtle_helper'

/*
// export dataset to JSON file
router.get('/json', function (req, res) {

  rdfStoreModel.findOne({'acronym': req.confModel.acronym}, function (err, rdfStore) {

    if (err || typeof(rdfStore) === 'undefined' || rdfStore === null) {
      return res.send({'state': 'err', 'message': err});
    }

    // initiate JSON file with all necessary categories
    var result = jBuilder.create(function (json) {

      json.set('organizations', []);
      json.set('persons', []);
      json.set('publications', []);
      json.set('events', []);
      json.set('locations', []);
      json.set('categories', []);

    }); // end jbuilder.encode


    result.set('organizations', function (json) {

      req.confModel.list_organization.forEach(function (organization) {

        var orga = organization;
        var organizationId = orga.uri === null ? orga.uri.replace('/', '\/') : null;
        json.child(function (json) {
          json.set('members', []);
          json.set('id', organizationId);
          json.set('label', orga.name);
        });

      });

    }); // end result.set('organizations')


    result.set('persons', function (json) {

      for (var i in req.confModel.list_person) {

        var person = req.confModel.list_person[i];
        json.child(function (json) {
          json.set('id', person.uri);
          json.set('name', person.displayname);

          json.set('depiction', null);
          json.set('locations', []);
          json.set('holdsRole', []);
          json.set('made', []);
          json.set('affiliation', []);
          json.set('mbox', []);
          json.set('accounts', []);
          json.set('websites', []);

        });

      }

    }); // end result.set('persons')


    result.set('publications', function (json) {

      for (var i in req.confModel.list_publication) {

        var pub = req.confModel.list_publication[i];
        json.child(function (json) {
          json.set('id', pub.uri);
          json.set('title', pub.title);
          json.set('uri', pub.uri);
          json.set('abstract', pub.abstract);

          json.set('track', null);
          json.set('thumbnail', null);

          json.set('authors', []);
        });

      }

    }); // end result.set('publications')


    result.set('events', function (json) {

      for (var i in req.confModel.list_event) {

        var event = req.confModel.list_event[i];
        json.child(function (json) {
          json.set('id', event.uri);
          json.set('name', event.name);
          json.set('uri', event.uri);
          json.set('description', event.summary);
          json.set('startsAt', event.date_start);
          json.set('endsAt', event.date_end);

          json.set('resource', null);
          json.set('locations', []);
          json.set('parent', []);
          json.set('children', []);
          json.set('topics', null);
          json.set('roles', []);
          json.set('homepage', null);
          json.set('lastModifiedAt', null);
          json.set('duration', null);

          json.set('categories', []);
          json.set('papers', []);

        });

      }

    }); // end result.set('events')


    result.set('locations', function (json) {

      for (var i in req.confModel.list_location) {

        var location = req.confModel.list_location[i];
        json.child(function (json) {
          json.set('id', location.uri);
          json.set('name', location.name);
        });

      }

    }); // end result.set('locations')


    result.set('roles', function (json) {

      for (var i in req.confModel.list_role) {

        var location = req.confModel.list_role[i];
        json.child(function (json) {
          json.set('id', location.uri);
          json.set('name', location.name);
          json.set('heldBy', []);
        });

      }

    }); // end result.set('locations')


    result.set('categories', function (json) {

      json.child(function (json) {
        json.set('id', 'http://data.semanticweb.org/ns/swc/ontology#ConferenceEvent');
        json.set('name', 'conferenceEvent');
        json.set('label', 'Conference event')
      });

      json.child(function (json) {
        json.set('id', 'http://data.semanticweb.org/ns/swc/ontology#TrackEvent');
        json.set('name', 'trackEvent');
        json.set('label', 'Track event')
      });

      json.child(function (json) {
        json.set('id', 'http://data.semanticweb.org/ns/swc/ontology#TalkEvent');
        json.set('name', 'talkEvent');
        json.set('label', 'Talk event')
      });

      json.child(function (json) {
        json.set('id', 'http://data.semanticweb.org/ns/swc/ontology#SessionEvent');
        json.set('name', 'sessionEvent');
        json.set('label', 'Session event')
      });

      json.child(function (json) {
        json.set('id', 'http://data.semanticweb.org/ns/swc/ontology#WorkshopEvent');
        json.set('name', 'workshopEvent');
        json.set('label', 'Workshop event')
      });

    }); // end result.set('categories')

    // add sub elements
    for (var i in rdfStore.source) {

      var triple = rdfStore.source[i];
      var content = result.content;

      // add member to organization
      if (triple.predicate.nominalValue === 'http://xmlns.com/foaf/0.1/member') {
        var rslt = content.organizations.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var orga = rslt[0];
        if (typeof(orga) !== 'undefined') {
          orga.members.push(triple.object.nominalValue);
        }
      }

      // add affiliation to person
      if (triple.predicate.nominalValue === 'http://swrc.ontoware.org/ontology#affiliation') {
        var rslt = content.persons.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var person = rslt[0];
        if (typeof(person) !== 'undefined') {
          person.affiliation.push(triple.object.nominalValue);
        }
      }

      // add location to person
      if (triple.predicate.nominalValue === 'http://xmlns.com/foaf/0.1/based_near') {
        var rslt = content.persons.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var person = rslt[0];
        if (typeof(person) !== 'undefined' && person.hasOwnProperty('locations')) {
          person.locations.push(triple.object.nominalValue);
        }
      }

      // add role to person
      if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#holdsRole') {
        var rslt = content.persons.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var person = rslt[0];
        if (typeof(person) !== 'undefined') {
          person.holdsRole.push(triple.object.nominalValue);
        }
      }

      // add person to role
      if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#heldBy') {
        var rslt = content.roles.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var role = rslt[0];
        if (typeof(role) !== 'undefined') {
          role.heldBy.push(triple.object.nominalValue);
        }
      }

      // add maker to publication
      if (triple.predicate.nominalValue === 'http://xmlns.com/foaf/0.1/maker') {
        var rslt = content.publications.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var pub = rslt[0];
        if (typeof(pub) !== 'undefined') {
          pub.authors.push(triple.object.nominalValue);
        }
      }

      // add location to event
      if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#hasLocation') {
        var rslt = content.events.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var event = rslt[0];
        if (typeof(event) !== 'undefined' && event.hasOwnProperty('locations')) {
          event['locations'].push(triple.object.nominalValue);
        }
      }

      // add subEvent to event
      if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#isSuperEventOf') {
        var rslt = content.events.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var event = rslt[0];
        if (typeof(event) !== 'undefined') {
          event.children.push(triple.object.nominalValue);
        }
      }

      // add superEvent to event
      if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#isSubEventOf') {
        var rslt = content.events.filter(function (obj) {
          return obj.id === triple.subject.nominalValue;
        }); // end filter
        var event = rslt[0];
        if (typeof(event) !== 'undefined') {
          event.parent.push(triple.object.nominalValue);
        }
      }

    } // end sub elements


    var filename = req.confModel.acronym + '.json';

    fs.writeFile('public/download/' + filename, JSON.stringify(result.content, null, 4), {flag: 'w'}, function (err) {

      if (err) {
        console.log(err);
        return res.send({'state': 'err', 'message': err});
      }
      res.setHeader('Content-Type', mime.lookup('json'));
      var appRootDir = require('app-root-dir').get();
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      res.header('Access-Control-Allow-Headers', 'Content-Type');
      res.json(result.content);
      // res.sendFile(filename, {root: path.join(__dirname, '../../public/download')});

    }); // end writefile

  }); // end rdfStoreModel.findOne

}); // end get /json
*/

/*
// export dataset to JSONLD file
router.get('/jsonld', function (req, res) {
  //Find Conference
  rdfStoreModel.findOne({'acronym': req.confModel.acronym}, function (err, rdfStore) {
    if (err || typeof(rdfStore) === 'undefined' || rdfStore === null) {
      console.log(err);
      return res.send({'state': 'err', 'message': err});
    }
    // Init graph
    var graph = [];
    // Process people information
    console.log('Begin to process people information');
    req.confModel.list_person.forEach(function (person) {
      graph.push({
        '@id': person.uri,
        // '@type': 'foaf:Person',
        // 'foaf:name': person.displayname,
        // 'foaf:mbox_sha1sum': person.sha,
        // 'foaf:based_near': [],
        // 'swrc:affiliation': [],
        // 'swc:holdsRole': [],
        '@type': 'cofunc:Person',
        'cofunc:name': person.displayname,
        'cofunc:mbox_sha1sum': person.sha,
        'cofunc:hasAffiliation': [],
        'cofunc:holdsRole': []

      });
    }); // end for
    console.log('Process ended successfully');
    // Process organizations information
    console.log('Begin to process organizations information');
    req.confModel.list_organization.forEach(function (organization) {
      var orga = organization;
      graph.push({
        '@id': orga.uri,
        // '@type': 'foaf:Organization',
        // 'rdfs:label': orga.name,
        'foaf:member': [],
        '@type': 'cofunc:Organisation',
        'cofunc:name': orga.name
        // 'rdfs:label': orga.label
      }); // end push

    }); // end for
    console.log('Process ended successfully');
    // Process events information
    console.log('Begin to process events information');
    req.confModel.list_event.forEach(function (event) {
      graph.push({

        '@id': event.uri,

        // '@type': 'http://data.semanticweb.org/ns/swc/ontology#TrackEvent',
        // 'rdfs:label': event.name,
        // 'icalztd:dtstart': event.date_start,
        // 'icaltzd:dtend': event.date_end,
        'swc:hasLocation': [],
        'swc:isSuperEventOf': [],
        'swc:isSubEventOf': [],
        '@type': 'cofunc:OrganisedEvent',
        'cofunc:name': event.name,
        'cofunc:description': event.summary,
        'cofunc:endDate': event.date_start,
        'cofunc:startDate': event.date_end,

        /* don't know what is it, but still copy from ESWC2016:
         "rdfs:label": [
         "Full-text Support for Publish/Subscribe Ontology Systems",
         "Lefteris Zervakis, Christos Tryfonopoulos, Spiros Skiadopoulos and Manolis Koubarakis"
         ]
         */
/*
        'rdfs:label': [event.name]

      }); // end push

    }); // end for
    console.log('Process ended successfully');
    // Process publications information
    console.log('Begin to process publications information');
    req.confModel.list_publication.forEach(function (publication) {
      var pub = publication;
      graph.push({
        '@id': pub.uri,
        // '@type': 'swrc:InProceedings',
        // 'rdfs:label': pub.title,
        // 'swrc:abstract': pub.abstract,
        // 'foaf:maker': []
        '@type': 'cofunc:InProceedings',
        'cofunc:title': pub.title,
        'cofunc:abstract': pub.abstract,
        'dc:creator': []
      }); // end push
    }); // end for
    console.log('Process ended successfully');
    // role
    console.log('Begin to process roles information');
    req.confModel.list_role.forEach(function (role) {
      graph.push({
        '@id': role.uri,
        // '@type': 'swc:Role',
        // 'rdfs:label': role.name,
        // 'swc:heldBy': []
        '@type': 'cofunc:RoleDuringEvent',
        'cofunc:name': role.name,
        'cofunc:isHeldBy': []
      }); // end push
    }); // end for
    console.log('Process ended successfully');
    // Process location
    console.log('Begin to process locations information');
    req.confModel.list_location.forEach(function (location) {
      graph.push({
        '@id': location.uri,
        '@type': 'icaltzd:location',
        'rdfs:label': location.name
      }); // end push
    }); // end for
    console.log('Process ended successfully');

    var customPush = function (object, property, value) {
      if (object[property] === null || typeof object[property] === 'undefined') {
        object[property] = [];
      }
      object[property].push(value);
    };
    // add sub elements
    rdfStore.source.forEach(function (element) {
      var triple = element;
      var predicateNominal = triple.predicate.nominalValue;
      var subjectNominal = triple.subject.nominalValue;
      var objectNominal = triple.object.nominalValue;
      if (predicateNominal === null || subjectNominal === null || objectNominal === null) {
        return; //Error
      }
      var object = null;
      for (var i = 0; i < graph.length; i++) {
        if (graph[i]['@id'] === subjectNominal) {
          object = graph[i];
          break;//Stop looking
        }
      }
      /*
       Change to for loop for better performance
       var object = graph.filter(function (element) {
       return element['@id'] === subjectNominal;
       });
       */
/*
      if (object === null) {
        return; // Stop the for each iteration
      }
      switch (predicateNominal) {
        case 'http://xmlns.com/foaf/0.1/member':
          customPush(object, 'foaf:member', objectNominal);
          // object['foaf:member'].push(objectNominal);
          break;
        case 'http://swrc.ontoware.org/ontology#affiliation':
          customPush(object, 'cofunc:hasAffiliation', objectNominal);
          // object['cofunc:hasAffiliation'].push(objectNominal);
          break;
        case 'http://xmlns.com/foaf/0.1/based_near':
          if (object.hasOwnProperty('foaf:based_near')) {
            customPush(object, 'foaf:based_near', objectNominal);
            // object['foaf:based_near'].push(objectNominal);
          }
          break;
        case 'http://data.semanticweb.org/ns/swc/ontology#holdsRole':
          // object['cofunc:holdsRole'].push(objectNominal);
          customPush(object, 'cofunc:holdsRole', objectNominal);
          break;
        case 'http://data.semanticweb.org/ns/swc/ontology#heldBy':
          // object['cofunc:isHeldBy'].push(objectNominal);
          customPush(object, 'cofunc:isHeldBy', objectNominal);
          break;
        case 'http://xmlns.com/foaf/0.1/maker':
          // object['dc:creator'].push(objectNominal);
          customPush(object, 'dc:creator', objectNominal);
          break;
        case 'http://data.semanticweb.org/ns/swc/ontology#hasLocation':
          if (object.hasOwnProperty('swc:hasLocation')) {
            // object['swc:hasLocation'].push(objectNominal);
            customPush(object, 'swc:hasLocation', objectNominal);
          }
          break;
        case 'http://data.semanticweb.org/ns/swc/ontology#isSuperEventOf':
          // object['swc:isSuperEventOf'].push(objectNominal);
          customPush(object, 'swc:isSuperEventOf', objectNominal);
          break;
        case 'http://data.semanticweb.org/ns/swc/ontology#isSubEventOf':
          // object['swc:isSubEventOf'].push(objectNominal);
          customPush(object, 'swc:isSubEventOf', objectNominal);
          break;
      }
      // add member to organization
      /*if (triple.predicate.nominalValue === 'http://xmlns.com/foaf/0.1/member') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var orga = rslt[0];
       if (typeof(orga) !== 'undefined') {
       orga['foaf:member'].push(triple.object.nominalValue);
       }
       }*/

      // add affiliation to person
      /*if (triple.predicate.nominalValue === 'http://swrc.ontoware.org/ontology#affiliation') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var person = rslt[0];
       if (typeof(person) !== 'undefined') {
       // person['swrc:affiliation'].push(triple.object.nominalValue);
       person['cofunc:hasAffiliation'].push(triple.object.nominalValue);
       }
       }*/

      // add location to person
      /*     if (triple.predicate.nominalValue === 'http://xmlns.com/foaf/0.1/based_near') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var person = rslt[0];
       if (typeof(person) !== 'undefined' && person.hasOwnProperty('foaf:based_near')) {
       person['foaf:based_near'].push(triple.object.nominalValue);
       }
       }*/

      // add role to person
      /*if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#holdsRole') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var person = rslt[0];
       if (typeof(person) !== 'undefined') {
       // person['swc:holdsRole'].push(triple.object.nominalValue);
       if (typeof(person['cofunc:holdsRole']) != 'undefined')
       person['cofunc:holdsRole'].push(triple.object.nominalValue);
       }
       }*/

      // add person to role
      /*if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#heldBy') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var role = rslt[0];
       if (typeof(role) !== 'undefined') {
       // role['swc:heldBy'].push(triple.object.nominalValue);
       role['cofunc:isHeldBy'].push(triple.object.nominalValue);
       }
       }*/

      // add maker to publication
      /* if (triple.predicate.nominalValue === 'http://xmlns.com/foaf/0.1/maker') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var pub = rslt[0];
       if (typeof(pub) !== 'undefined') {
       // pub['foaf:maker'].push(triple.object.nominalValue);
       pub['dc:creator'].push(triple.object.nominalValue);
       }
       }*/

      // add location to event
      /*if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#hasLocation') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var event = rslt[0];
       if (typeof(event) !== 'undefined' && event.hasOwnProperty('swc:hasLocation')) {
       event['swc:hasLocation'].push(triple.object.nominalValue);
       }
       }*/

      // add subEvent to event
      /*     if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#isSuperEventOf') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var event = rslt[0];
       if (typeof(event) !== 'undefined') {
       event['swc:isSuperEventOf'].push(triple.object.nominalValue);
       }
       }*/

      // add superEvent to event
      /*  if (triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#isSubEventOf') {
       var rslt = graph.filter(function (obj) {
       return obj['@id'] === triple.subject.nominalValue;
       }); // end filter
       var event = rslt[0];
       if (typeof(event) !== 'undefined') {
       // event['swc:isSubEventOf'].push(triple.object.nominalValue);
       }
       }*/
/*
    }); // end for rdfStore.source


    // initiate JSON file with all necessary categories
    var result = jBuilder.create(function (json) {

      json.set('@context', {

        'bibo': 'http://purl.org/ontology/bibo/',
        'dce': 'http://purl.org/dc/elements/1.1/',
        'dct': 'http://purl.org/dc/temrs/',
        'event': 'http://purl.org/NET/c4dm/event.owl#',
        'tl': 'http:/purl.org/NET/c4dm/timeline.owl#',

        'foaf': 'http://xmlds.com/foaf/0.1',

        'geo': 'http://www.w3.org/2003/01/geo/wgs84_pos#',
        'ical': 'http://www.w3.org/2002/12/cal/ical#',
        'icaltzd': 'http://www.w3.org/2002/12/cal/icaltzd#',

        'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'rdfs': 'http://www.w3.org/2000/01/rdf-schema#',
        'owl': 'http://www.w3.org/2002/07/owl#',

        'swc': 'http://data.semanticweb.org/ns/swc/ontology#',
        'swrc': 'http://swrc.ontoware.org/ontology#',

        'cofunc': 'https://w3id.org/scholarlydata/ontology/conference-ontology.owl#',
        'dc': 'http://purl.org/dc/elements/1.1/'

      }); // end set @context

      json.set('@graph', graph);

    }); // end jbuilder.encode


    var filename = req.confModel.acronym + '.jsonld';

    fs.writeFile('public/download/' + filename, JSON.stringify(result.content, null, 4), {flag: 'w'}, function (err) {

      if (err) {
        console.log(err);
        return res.send({'state': 'err', 'message': err});
      }
      res.setHeader('Content-Type', mime.lookup('json'));
      var appRootDir = require('app-root-dir').get();
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      res.header('Access-Control-Allow-Headers', 'Content-Type');
      res.json(result.content);

    }); // end writefile

  }); // end rdfStoreHelper

}); // end get /jsonld
*/

// export dataset to RDF file
router.get('/ttl', function (req, res) {

  var writer = n3.Writer();
  var idConf = req.id_conference;
  const turtlehelper = new TurtleHelper();

  /*turtlehelper.getTracksTuples(idConf)
      .then((store) =>{
          writer.addTriples(store.getTriples(null, null, null));
      })
      .then(() => {
          res.writeHead(200, {'Content-Type': 'application/force-download','Content-disposition':'attachment; filename=dataset.ttl'});
          writer.end(function (error, result) { res.end( result ); });
      })*/

  turtlehelper.getConferenceTuples(idConf, null)
  .then((store) => {
      console.log("[Export ttl] retrieval of conference informations done");
      turtlehelper.getPersonsTuples(idConf, store)
          .then((store) => {

              console.log("[Export ttl] retrieval of persons informations done");
              turtlehelper.getOrganisationTuples(idConf, store)
                  .then((store) =>{

                      console.log("[Export ttl] retrieval of organisations informations done");
                      turtlehelper.getAffiliationTuples(idConf, store)
                          .then((store) =>{

                              console.log("[Export ttl] retrieval of affiliations informations done");
                              turtlehelper.getRoleTuples(idConf, store)
                                  .then((store) => {

                                      console.log("[Export ttl] retrieval of roles informations done");
                                      turtlehelper.getPublicationTuples(idConf, store)
                                          .then((store) =>{

                                            console.log("[Export ttl] retrieval of publications informations done");
                                            turtlehelper.getTracksTuples(idConf, store)
                                                .then((store) => {

                                                    console.log("[Export ttl] retrieval of tracks informations done");
                                                  turtlehelper.getEventsTuples(idConf, store)
                                                      .then((store) => {

                                                          console.log("[Export ttl] retrieval of events informations done");
                                                          /*turtlehelper.getLocationTuples(idConf, store)
                                                              .then((store) => {

                                                                  console.log("[Export ttl] retrieval of locations informations done");

                                                                  writer.addTriples(store.getTriples(null, null, null));
                                                                  res.writeHead(200, {'Content-Type': 'application/force-download','Content-disposition':'attachment; filename=dataset.ttl'});
                                                                  writer.end(function (error, result) { res.end( result ); });
                                                              })
                                                              .catch((err) => {
                                                                  console.log(err);
                                                              })*/
                                                          writer.addTriples(store.getTriples(null, null, null));
                                                          res.writeHead(200, {'Content-Type': 'application/force-download','Content-disposition':'attachment; filename=dataset.ttl'});
                                                          writer.end(function (error, result) { res.end( result ); });
                                                      })
                                                      .catch((err) => {
                                                          console.log(err);
                                                      });
                                                })
                                                .catch((err) => {
                                                    console.log(err);
                                                });

                                          })
                                          .catch((err) => {
                                              console.log(err);
                                          });

                                  })
                                  .catch((err) => {
                                      console.log(err);
                                  });

                          })
                          .catch((err) => {
                              console.log(err);
                          });

                  })
                  .catch((err) => {
                      console.log(err);
                  });
          })
          .catch((err) => {
              console.log(err);
          });
  })
  .catch((err) => {
      console.log(err);
  });



  /*
  rdfStoreModel.findOne({'acronym': req.params.confAcronym}, function (err, rdfStore) {

    if (err) {
      console.log(err);
      return res.send({'state': 'err', 'message': err});
    }

    // var result = jBuilder.encode(function(json){

    // 	json.set('key', 'value');
    // }); // end jBuilder.encode


    // var jsonName = rdfStore.file_name.replace('.rdf', '.json');

    // fs.writeFile('public/download/'+jsonName, result, { flag: 'w' }, function(err) {
    // 	if(err) {
    // 		console.log(err)
    // 		return res.send({'state': 'err', 'message': err})
    // 	}
    // 	res.setHeader('Content-Type', mime.lookup('json'));
    // 	// console.log(path.dirname(require.main.filename));

    // 	var appRootDir = require('app-root-dir').get();
    // 	res.sendFile(rdfStore.file_name, {'root': appRootDir+'/public/download'})
    // });

    // end writerFile
    var writer = n3.Writer({
      format: mime.lookup('rdf'),
      foaf: 'http://xmlns.com/foaf/0.1/'
      // swc: 'http://data.semanticweb.org/ns/swc/ontology#',
      // prefixes: { c: 'http://example.org/cartoons#' }
    });

    for (var i in rdfStore.source) {
      var triple = rdfStore.source[i]
      writer.addTriple({
        subject: triple.subject.nominalValue,
        predicate: triple.predicate.nominalValue,
        object: triple.object.nominalValue
      })
    }

    writer.end(function (error, result) {
      //Get app directory
      var appRootDir = require('app-root-dir').get();

      //Set name file
      const nameFile = rdfStore.file_name + '.ttl';

      //Set directory when we want to save the file .ttl
      const dirDownload = '/public/download/';

      //Check if the directory download exist
      if (!fs.existsSync(appRootDir + dirDownload)){
        //If not, we create it
        fs.mkdirSync(appRootDir + dirDownload);
      }

      fs.writeFile(path.join(appRootDir, dirDownload + nameFile), result, {flag: 'w'}, function (err) {
        if (err) {
          console.log(err)
          return res.send({'state': 'err', 'message': err})
        }
        res.setHeader('Content-Type', mime.lookup('rdf'));
        // console.log(path.dirname(require.main.filename));

        res.sendFile(nameFile, {'root': appRootDir + dirDownload})

      }); // end writerFile

    }); // end writer

  }) // end .findOne
*/
}); // end /rdf


/**********************************/
module.exports = router;