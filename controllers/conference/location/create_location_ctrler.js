const express = require('express');
const router = express.Router();

import LocationMetier from '../../../metiers/location';

// create new location
router.get('/create', function (req, res) {

    return res.render('conference/location/create', {
        'isOwner': req.isOwner,
        'conf': req.conf,
        'author': req.author,
        'activeCode': 'location'
    });

}); // end get /create


router.post('/persist', function (req, res) {

    const data = req.body.formData;

    new LocationMetier()
        .add(
            req.id_conference,
            data.name,
            data.nb_place,
            data.equipements,
            data.gps,
            data.address,
            data.description,
        )
        .then((location) => {
            return res.send({
                'state': 'success',
                'message': 'Location saved!',
                'redirect': '/conference/' + req.id_conference + '/location',
            });
        })
        .catch((err) => {
            return res.send({
                'state': 'error',
                'message': 'This name has already been used. Please take another one.',
                'redirect': '/'
            });
        });


    /*locationDAO.persistToConf(req.body.formData, req.body.confAcronym, function (err, locationSlug) {
      if (err) {
        switch (err.code) {
          case 11000:
            res.send({
              'state': 'error',
              'message': 'This name has already been used. Please take another one.',
              'redirect': '/'
            })
            break
          default:
            console.log(err)
            res.send({
              'state': 'error',
              'message': 'Something in server has gone wrong, please try again later, thankyou for your comprehension.',
              'redirect': '/'
            })
        }
      } else {
        // return persist success signal
        return res.send({
          'state': 'success',
          'message': 'Location saved!',
          'redirect': '/conference/' + req.body.confAcronym + '/location/' + locationSlug
        })
      }
    }) // end persist*/

});// end /persist


/**********************************/
module.exports = router;