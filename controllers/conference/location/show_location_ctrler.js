const express = require('express');
const router = express.Router();

import LocationMetier from '../../../metiers/location';

// show all locations
router.get('/', function (req, res) {

  return res.render('conference/location/show-all', {
      'author': req.author,
      'isOwner': req.isOwner,
      'activeCode': 'location',
      'conf': req.conf
    }
  );
});// end get /all


router.get('/page', function (req, res) {
    let id_min = req.query.id_min;
    const pageSize = parseInt(req.query.page_size);
    if (id_min != '-1') {
        new LocationMetier()
            .findPage(req.id_conference, req.query.id_min, req.query.term, pageSize + 1)
            .then((locations) => {
                res.render('conference/location/location-partial',
                    {
                        'listLocation': locations.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'location',
                        'conf': req.conf,
                        'firstPage': false
                    }, (err, html) => {
                        let resObj = {
                          html: html,
                          empty: locations.length === 0,
                          next: locations.length > pageSize
                        };
                        res.send(JSON.stringify(resObj));
                    });
            })
            .catch((err) => {
              res.json({error: err})
            });
    }
    else {
        new LocationMetier()
            .findWithTerm(req.id_conference, req.query.term, pageSize + 1)
            .then((locations) => {
                res.render('conference/location/location-partial',
                    {
                        'listLocation': locations.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'location',
                        'conf': req.conf,
                        'firstPage': true
                    }, (err, html) => {
                        let resObj = {
                          html: html,
                          empty: locations.length === 0,
                          next: locations.length > pageSize
                        };
                        res.send(JSON.stringify(resObj));
                    });
            })
            .catch((err) => {
                return res.json({error: err})
            });
    }

});

// show 1 location of conference
router.get('/:id', function (req, res) {

  new LocationMetier()
    .get(
      req.id_conference,
      req.params.id,
    )
    .then((location) => {
      return res.render('conference/location/show', {
          'location': location,
          'author': req.author,
          'isOwner': req.isOwner,
          'conf': req.conf,
          'activeCode': 'location'
        }
      );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Location not existed.'})
    });
  /*var slug = req.params.slug
  var conf = confHelper.modelToHtmlFormFormat(req.confModel)

  locationDAO.getBySlug(req.confModel.acronym, slug, function (err, location) {

    if (err) {
      return res.render('message/page-not-found', {'message': 'Location not existed.'})
    } else {
      return res.render('conference/location/show', {
          'location': location,
          'author': req.author,
          'isOwner': req.isOwner,
          'conf': conf,
          'activeCode': 'location'
        }
      )
    }

  }) // end getBySlug*/
});// end get /:slug


/**********************************/
module.exports = router;