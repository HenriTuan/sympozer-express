/**
 * Created by pierremarsot on 20/11/2017.
 */
const express = require('express');
const router = express.Router();

import LocationMetier from '../../../../metiers/location';

router.get("", (req, res) => {
  new LocationMetier()
    .find(req.id_conference)
    .then((locations) => {
      return res.json({
        locations: locations,
      });
    })
    .catch((err) => {
      return res.json({
        error: err,
      });
    });
});

module.exports = router;