const express = require('express');
const router = express.Router();

import LocationMetier from '../../../metiers/location';

router.get('/:id/edit', function (req, res) {

    new LocationMetier()
        .get(
            req.id_conference,
            req.params.id,
        )
        .then((location) => {
            return res.render('conference/location/edit', {
                    'location': location,
                    'author': req.author,
                    'isOwner': req.isOwner,
                    'conf': req.conf,
                    'activeCode': 'location'
                }
            );
        })
        .catch((err) => {
            return res.render('message/page-not-found', {'message': 'Location not existed.'})
        });
});// end get /:slug/edit


router.post('/:id/save', function (req, res) {

    const data = req.body.formData;

    console.log(data);

    new LocationMetier()
        .update(
            req.id_conference,
            req.params.id,
            data.name,
            data.nb_place,
            data.equipements,
            data.gps,
            data.address,
            data.description,
        )
        .then((location) => {
            return res.send({
                'state': 'success',
                'message': 'Location saved!',
                'redirect': '/conference/' + req.id_conference + '/location/' + location._id,
            });
        })
        .catch((err) => {
            return res.send({
                'state': 'error',
                'message': 'An error occured while creating the location.',
                'errormsg': err,
                'redirect': '/'
            });
        });
});// end /save


/**********************************/
module.exports = router;