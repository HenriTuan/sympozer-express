const express = require('express');
const router = express.Router();

import LocationMetier from '../../../metiers/location';

router.post('/delete/:id', function (req, res) {

  new LocationMetier()
    .remove(
      req.id_conference,
      req.params.id,
    )
    .then(() => {
      return res.send({
        'state': 'success',
        'message': 'Location deleted!',
        'redirect': '/conference/' + req.id_conference + '/location'
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This location is not found in database.',
        'redirect': '/'
      });
    });
  /*locationDAO.deleteBySlug(req.body.locationSlug, req.body.confAcronym, function (err) {

    if (err) {
      switch (err) {
        case -1:
          res.send({
            'state': 'error',
            'message': 'This location is not found in database.',
            'redirect': '/'
          })
          break
        default:
          throw err
      }
    } else {
      // return persist success signal
      return res.send({
        'state': 'success',
        'message': 'Location deleted!',
        'redirect': '/conference/' + req.body.confAcronym + '/location'
      })
    }

  }) // end delete*/
});// end /persist


/**********************************/
module.exports = router;