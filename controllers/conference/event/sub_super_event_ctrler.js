var express = require('express')
var router  = express.Router()

var eventHelper = require('../../../helpers/event_helper')
var eventDAO = require('../../../DAO/event_DAO')

var confDAO = require('../../../DAO/conference_DAO')
var confHelper = require('../../../helpers/conference_helper')
var confModel = require('../../../models/conference')
var eventModel = require('../../../models/event_type/event')










/*********** sub event ****************/

// post get-list-available-sub-event
router.post('/persist-sub-event', function(req, res){

    eventDAO.persistAsSubEvent(req.body.formData, req.body.confAcronym, function(err, eventSlug){
        if(err){
            switch(err.code){
                case 11000:
                    res.send({'state'	: 'error',
                        'message' : 'This name has already been used. Please take another one.',
                        'redirect': '/'})
                    break
                default:
                    console.log(err)
                    res.send({'state'	: 'error',
                        'message' : 'Something in server has gone wrong, please try again later, thankyou for your comprehension.',
                        'redirect': '/'})
            }
        }else{
            // return persist success signal
            return res.send({'state'	: 'success',
                'message'	: 'Event saved!',
                'redirect'	: '/conference/'+req.body.confAcronym+'/event/'+eventSlug,
                'eventSlug' : eventSlug})
        }
    }) // end persist

}) // end post /get-list-available-sub-event


// post add sub-event to event
router.post('/sub-events/add-sub-event', function(req, res){

	var listSubEvent= req.body.listSubEvent
	eventDAO.addListSubEventByAcronym(req.body.eventAcronym, listSubEvent, function(err){

		if(err){
			console.log(err)
			return res.send({'state': 'error', 'message': 'We can\'t add sub event. Please try again later.'})
		}else{
			return res.send({'state': 'success'})
		}

	})// end addlistMemberByAcronym	

}) // end post /get-list-sub-event




/*********** super event ****************/

// post get-list-available-super-event
router.post('/super-events/get-list-available-event', function(req, res){

	var confAcronym = req.body.confAcronym

	confDAO.getConfModelByAcronym(req.body.confAcronym, function(err, conf){

		if( err || conf == null || typeof(conf.list_event) === 'undefined' ){
			return res.render('message/page-not-found', {'message': 'Can\'t find event. Pleas try again later, thankyou for your comprehension.'})
		}else{
			return res.render('component/list-event.jade', {'listAvailableEvent': conf.list_event, 'confAcronym': req.body.confAcronym})
		}

	})	

}) // end post /get-list-available-super-event

// post add super-event to event
router.post('/super-events/add-super-event', function(req, res){

	var listSuperEvent= req.body.listSuperEvent
	eventDAO.addListSuperEventByAcronym(req.body.eventAcronym, listSuperEvent, function(err){

		if(err){
			console.log(err)
			return res.send({'state': 'error', 'message': 'We can\'t add super event. Please try again later.'})
		}else{
			return res.send({'state': 'success'})
		}

	})// end addlistMemberByAcronym	

}) // end post /get-list-super-event



// post remove sub-event from event
router.post('/sub-events/remove-sub-event', function(req, res){

	eventDAO.removeSubEvent(req.body.eventAcronym, req.body.event, function(err){

		if(err){
			console.log(err)
			return res.send({'state': 'error', 'message': 'We can\'t remove this sub event. Please try again later.'})
		}else{
			return res.send({'state': 'success', 'message': 'Sub event removed.', 'redirect': 'reload'})
		}

	})// end removeSubEvent

}) // end post remove sub-event from event




/**********************************/
module.exports = router