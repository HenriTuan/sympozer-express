var express = require('express')
var router = express.Router()

var eventCatDAO = require('../../../DAO/eventCategory_DAO')
var confHelper = require('../../../helpers/conference_helper')

import EventMetier from '../../../metiers/event';

// create new event
router.get('/create', function (req, res) {

    eventCatDAO.getAll(function (err, listEventCat) {
        if (err) console.log('error')
        else {
            var conf = confHelper.modelToHtmlFormFormat(req.confModel)
            return res.render('conference/event/create', {
                'isOwner': req.isOwner,
                'conf': conf,
                'author': req.author,
                'activeCode': 'event',
                'listEventCat': listEventCat
            })
        }
    })


}) // end get /create


router.post('/persist', function (req, res) {
    const idConference = req.id_conference;
    const data = req.body.formData;

    new EventMetier()
      .add(idConference, data)
      .then((event) => {
        return res.send({
          'state': 'success',
          'message': 'Event saved!',
          'redirect': '/conference/' + idConference + '/event/' + event._id,
          'event': event,
        });
      })
      .catch((err) => {
        console.log(err);
        return res.send({
          'state': 'error',
          'message': 'Something in server has gone wrong, please try again later, thankyou for your comprehension.',
          'redirect': '/'
        });
      });
    /*eventDAO.persistToConf(req.body.formData, req.body.confAcronym, function (err, addedEvent , addedEventCat) {
        if (err) {
            switch (err.code) {
                case 11000:
                    res.send({
                        'state': 'error',
                        'message': 'This name has already been used. Please take another one.',
                        'redirect': '/'
                    })
                    break
                default:
                    console.log(err)
                    res.send({
                        'state': 'error',
                        'message': 'Something in server has gone wrong, please try again later, thankyou for your comprehension.',
                        'redirect': '/'
                    })
            }
        } else {
            // return persist success signal

            return res.send({
                'state': 'success',
                'message': 'Event saved!',
                'redirect': '/conference/' + req.body.confAcronym + '/event/' + addedEvent.slug,
                'eventSlug': addedEvent.slug,
                'event': addedEvent,
            })
        }
    }) // end persist*/

}); // end /persist


/**********************************/
module.exports = router;