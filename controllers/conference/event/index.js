/**
 * Created by pierremarsot on 20/11/2017.
 */
const express = require('express');
const router = express.Router();

import EventMetier from '../../../metiers/event';

router.get('/', function (req, res) {
    return res.render('conference/event/showAll', {

            'author': req.author,
            'isOwner': req.isOwner,
            'activeCode': 'event',
            'conf': req.conf
        }
    );
});

/**
 * Permet de trouver les sub-event d'unevent
 */
router.get("/find/:idSuperEvent", (req, res) => {
    new EventMetier()
        .find(req.id_conference, req.params.idSuperEvent)
        .then((events) => {
            return res.json({
                events: events,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
});

/**
 * Permet de créer un event
 */
router.post("", (req, res) => {
    new EventMetier()
        .add(req.id_conference, req.body)
        .then((event) => {
            return res.json({
                event: event,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
});

/**
 * Permet de modifier un event
 */
router.post("/:id", (req, res) => {
    new EventMetier()
        .update(req.id_conference, req.params.id, req.body)
        .then((event) => {
            return res.json({
                event: event,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
});

/**
 * Permet de supprimer un event
 */
router.delete("/:id", function (req, res) {
    new EventMetier()
        .remove(req.id_conference, req.params.id)
        .then(() => {
            return res.json({
                id_event: req.params.id,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        })
});

/**
 * Permet d'ajouter une publication à un event
 */
router.post("/:id/publication/:idPublication", (req, res) => {
    new EventMetier()
        .addRelatedPublication(
            req.id_conference,
            req.params.id,
            req.params.idPublication,
        )
        .then(() => {
            return res.json({});
        })
        .catch(() => {
            return res.json({});
        });
});

/**
 * Permet de supprimer une publication à un event
 */
router.delete("/:id/publication/:idPublication", (req, res) => {
    new EventMetier()
        .removeRelatedPublication(
            req.id_conference,
            req.params.id,
            req.params.idPublication,
        )
        .then(() => {
            return res.json({});
        })
        .catch(() => {
            return res.json({});
        });
});

/**
 * Permet de récupérer des events par autocomplete
 */
router.get("/find", (req, res) => {
    new EventMetier()
        .findWithTerm(req.id_conference, req.query.term, parseInt(req.query.page_size))
        .then((events) => {
            return res.json({
                events: events,
            });
        })
        .catch((err) => {
            return res.json({error: err});
        });
});

module.exports = router;