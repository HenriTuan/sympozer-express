var express = require('express')
var router = express.Router()

var eventDAO = require('../../../DAO/event_DAO')
var eventCatDAO = require('../../../DAO/eventCategory_DAO')
var eventModel = require('../../../models/event_type/event')
var confHelper = require('../../../helpers/conference_helper')















// gets events by conference Acronym and events (Super_event) slug
router.get('/:slug/subEvents', function (req, res) {

    var eventSlug = req.params.slug
    var conf = confHelper.modelToHtmlFormFormat(req.confModel)


    eventCatDAO.getAll(function (err, listEventCat) {
        if (err) console.log('error')
        else {


            eventDAO.getAllByConfAcronym( eventSlug,req.confModel, function (err, events) {

                if (err) {
                    return res.send({err :'not'})
                } else {
                    res.setHeader('Content-Type', 'application/json');
                    return res.send(JSON.stringify(events));


                }

            }) // end getBySlug
        }
    })

})







router.get('/', function (req, res) {

    var eventSlug = null
    var conf = confHelper.modelToHtmlFormFormat(req.confModel)


    eventCatDAO.getAll(function (err, listEventCat) {
        if (err) console.log('error')
        else {


            eventDAO.getAllByConfAcronym( eventSlug,req.confModel, function (err, events) {

                if (err) {
                    return res.send({err :'not'})
                } else {
                    res.setHeader('Content-Type', 'application/json');
                    return res.send(JSON.stringify(events));


                }

            }) // end getBySlug
        }
    })

})







module.exports = router