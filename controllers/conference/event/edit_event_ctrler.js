var express = require('express')

var router = express.Router()

var eventHelper = require('../../../helpers/event_helper')
var eventDAO = require('../../../DAO/event_DAO')
var eventCatDAO = require('../../../DAO/eventCategory_DAO')
var confHelper = require('../../../helpers/conference_helper')
var dateHelper = require('../../../helpers/date_helper')


router.get('/:id/edit', function (req, res) {


    eventCatDAO.getAll(function (err, listEventCat) {
        if (err) console.log('error')
        else {

            var slug = req.params.slug
            var conf = confHelper.modelToHtmlFormFormat(req.confModel)

            eventDAO.getBySlug(req.confModel.acronym, slug, function (err, event) {

                if (err) {
                    return res.render('message/page-not-found', {'message': 'Event category not existed.'})
                } else {

                    event.start_time = new Date(event.date_start).getHours() + ":" + new Date(event.date_start).getMinutes()
                    event.end_time = new Date(event.date_end).getHours() + ":" + new Date(event.date_end).getMinutes()

                    return res.render('conference/event/edit', {
                            'event': event,
                            'conf': conf,
                            'isOwner': req.isOwner,
                            'activeCode': 'event',
                            'listEventCat': listEventCat
                        }
                    )
                }

            }) // end getEventByAcronym

        }

    })
})
// end get /:acronym/edit


router.post('/:slug/save', function (req, res) {

    var event = req.body.formData
    var eventSlug = req.params.slug
    eventDAO.save(eventSlug, event, req.body.confAcronym, function (err, updatedEvent) {

        if (err) {
            console.log(err)
            switch (err.code) {
                case 11000:
                    res.send({
                        'state': 'error',
                        'message': 'This URI has already been used. Please take another one.',
                        'redirect': '/'
                    })
                    break
                default:
                    throw err
            }
        } else {
            // return persist success signal
            return res.send({
                'state': 'success',
                'message': 'Event saved!',
                'redirect': '/conference/' + req.body.confAcronym + '/event/' + event.slug,
                'event': updatedEvent
            })
        }
    }) // end update event
})


router.get('/:acronym/edit', function (req, res) {

    var conf = confHelper.modelToHtmlFormFormat(req.confModel)

    eventDAO.getEventByAcronym(req.params.acronym, function (err, event) {

        if (event._user_id != req.session.user._id) {
            return res.render('message/access-denied', {'message': 'This is not your event, you don\'t have right to edit it.'})
        } else {

            return res.render('conference/event/edit', {
                    'event': event,
                    'conf': conf,
                    'isOwner': req.isOwner,
                    'activeCode': 'event'
                }
            )
        }

    }) // end getEventByAcronym

}) // end get /:acronym/edit


router.post('/:acronym/save', function (req, res) {

    var sess = req.session

    // update event
    var newEventModel = eventHelper.htmlFormToModelFormat(req.body, sess.user._id)
    eventDAO.save(newEventModel, function (err) {

        if (err) {
            console.log(err)
            switch (err.code) {
                case 11000:
                    res.send({
                        'state': 'error',
                        'message': 'This URI has already been used. Please take another one.',
                        'redirect': '/'
                    })
                    break
                default:
                    throw err
            }
        } else {
            // return persist success signal
            return res.send({
                'state': 'success',
                'message': 'Event saved!',
                'redirect': '/conference/' + req.body.confAcronym + '/event/' + newEventModel.acronym
            })
        }
    }) // end update event


}) // end /save


/**********************************/
module.exports = router