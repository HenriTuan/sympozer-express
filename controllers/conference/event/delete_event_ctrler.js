const express = require('express');
const router  = express.Router();

import EventMetier from '../../../metiers/event';

router.post('/delete/:idEvent', function(req, res){

	const idEvent = req.params.idEvent;
	const idConf = req.id_conference;

	new EventMetier()
		.remove(idConf, idEvent)
		.then(() => {
      return res.send({'state'	: 'success',
        'message'	: 'Event deleted!',
        'redirect'	: '/conference/'+req.id_conference+'/event'})
		})
		.catch(() => {
		return res.send({'state'	: 'error',
      'message' : 'This event is not found in database.',
      'redirect': '/'});
		});

	/*eventDAO.deleteBySlug( req.body.eventSlug, req.body.confAcronym, function(err){

		if(err){
			switch(err){
				case -1: 
					res.send({'state'	: 'error',
							  'message' : 'This event is not found in database.',
							  'redirect': '/'})
					break
				default:
					throw err
			}
		}else{
			// return persist success signal
			return res.send({'state'	: 'success',
							 'message'	: 'Event deleted!',
							 'redirect'	: '/conference/'+req.body.confAcronym+'/event'})
		}

	}) // end delete*/
});// end /persist


/**********************************/
module.exports = router;