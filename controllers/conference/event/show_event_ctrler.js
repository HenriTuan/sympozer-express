var express = require('express')
var router = express.Router()

var eventDAO = require('../../../DAO/event_DAO')
var eventCatDAO = require('../../../DAO/eventCategory_DAO')
var eventModel = require('../../../models/event_type/event')
var confHelper = require('../../../helpers/conference_helper')

import EventMetier from '../../../metiers/event';
import TrackMetier from '../../../metiers/track';

// show all events
router.get('/', function (req, res) {

  //On récup les events
  new EventMetier()
    .find(
      req.id_conference,
      null
    )
    .then((events) => {
      //On récup les tracks
      return res.render('conference/event/showAll', {
          'listEvent': events,
          'author': req.author,
          'isOwner': req.isOwner,
          'activeCode': 'event',
          'conf': req.conf
        }
      );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
    });
  /*var conf = confHelper.modelToHtmlFormFormat(req.confModel)

  eventDAO.getAllByConfAcronym(null, req.confModel, function (err, events) {

    if (typeof(events) === 'undefined') {
      return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
    } else {

      return res.render('conference/event/showAll', {
          'listEvent': events,
          'author': req.author,
          'isOwner': req.isOwner,
          'activeCode': 'event',
          'conf': conf
        }
      )
    }


  })*/

  /*
   eventModel
   .find({conference: conf})
   .populate('_super_event')
   .populate('_track')
   .exec(function (err, listEvents) {
   if (err) console.log('erreur');
   if (typeof(listEvents) === 'undefined') {
   return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
   } else {
   return res.render('conference/event/show-all', {
   'listEvent': listEvents,
   'author': req.author,
   'isOwner': req.isOwner,
   'activeCode': 'event',
   'conf': conf,
   'listEventCat': listEventCat
   }
   )
   }
   });*/


}) // end get /all


/*
// show all events in calendar
router.get('/calendar', function (req, res) {
    var conf = confHelper.modelToHtmlFormFormat(req.confModel)
    eventCatDAO.getAll(function (err, listEventCat) {
        if (err) console.log('error')
        else {


            eventDAO.getAllByConfAcronym(null, req.confModel, function (err, events) {


                if (typeof(req.confModel.list_event) === 'undefined') {
                    return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
                } else {
                    console.log(conf)
                    return res.render('conference/event/calendar', {
                            'listEvent': events,
                            'author': req.author,
                            'isOwner': req.isOwner,
                            'activeCode': 'event',
                            'conf': conf,
                            'listEventCat': listEventCat
                        }
                    )
                }


            })

        }
    })

}) // end for calendar

*/


// show all events in calendar
router.get('/calendar', function (req, res) {
  var conf = confHelper.modelToHtmlFormFormat(req.confModel)
  eventCatDAO.getAll(function (err, listEventCat) {
    if (err) console.log('error')
    else {


      eventDAO.getAllByConfAcronym(null, req.confModel, function (err, events) {


        if (typeof(req.confModel.list_event) === 'undefined') {
          return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
        } else {
          return res.render('conference/event/calendar', {
              'listEvent': events,
              'author': req.author,
              'isOwner': req.isOwner,
              'activeCode': 'event',
              'conf': conf,
              'listEventCat': listEventCat
            }
          )
        }


      })

    }
  })

}) // end for calendar


// show all events in calendar
router.get('/:slug/calendar', function (req, res) {

  var conf = confHelper.modelToHtmlFormFormat(req.confModel)
  var superEventSlug = req.params.slug
  eventCatDAO.getAll(function (err, listEventCat) {
    if (err) console.log('error')
    else {


      eventDAO.getAllByConfAcronym(superEventSlug, req.confModel, function (err, events, superEvent) {


        if (typeof(req.confModel.list_event) === 'undefined') {
          return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
        } else {
          if (superEvent === null) {
            return res.render('conference/event/calendar', {
              'listEvent': events,
              'author': req.author,
              'isOwner': req.isOwner,
              'activeCode': 'event',
              'conf': conf,
              'listEventCat': listEventCat
            })
          }


          return res.render('conference/event/calendar', {
              'listEvent': events,
              'author': req.author,
              'isOwner': req.isOwner,
              'activeCode': 'event',
              'conf': conf,
              'listEventCat': listEventCat,
              'SuperEvent': superEvent
            }
          )
        }


      })

    }
  })

}) // end for calendar


// show 1 event
router.get('/:id', function (req, res) {
  const idEvent = req.params.id;
  const idConference = req.id_conference;

  new EventMetier()
    .get(idConference, idEvent)
    .then((event) => {
      return res.render('conference/event/show', {
          'event': event,
          'author': req.author,
          'isOwner': req.isOwner,
          'conf': req.conf,
          'activeCode': 'event',
        }
      );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Event not existed.'})
    });


  /*eventCatDAO.getAll(function (err, listEventCat) {
    if (err) console.log('error')
    else {


      eventDAO.getBySlug(req.confModel.acronym, slug, function (err, event) {

        if (err) {
          return res.render('message/page-not-found', {'message': 'Event not existed.'})
        } else {
          return res.render('conference/event/show', {
              'event': event,
              'author': req.author,
              'isOwner': req.isOwner,
              'conf': conf,
              'activeCode': 'event',
              'listEventCat': listEventCat
            }
          )
        }

      }) // end getBySlug
    }
  })*/

});// end get /:slug

/**********************************/
module.exports = router;