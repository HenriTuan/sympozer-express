const express = require('express');
const countryLanguage = require('country-language');
const router = express.Router();
import ConferenceMetier from '../../metiers/conference';

// create new conference
router.get('/:id/edit', function (req, res) {

    let listLang = [];
    countryLanguage.getLanguages().forEach(function (el) {
        listLang.push({'name': el.name[0], 'val': el.iso639_2en});
    });

    listLang.sort(function (a, b) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
        if (a.name > b.name) {
            return 1;
        }
        if (a.name < b.name) {
            return -1;
        }
        // a must be equal to b
        return 0;
    });


    new ConferenceMetier()
        .get(req.params.id)
        .then((conference) => {
            if (conference._user_id != req.session.user._id) {
                return res.render('message/access-denied', {'message': 'This is not your conference, you don\'t have right to edit it'})
            } else {
                return res.render('conference/edit', {
                        'conf': req.conf,
                        'listLang': listLang,
                        'activeCode': 'edit',
                        'isOwner': true
                    }
                )
            }
        })
        .catch((err) => {
            return res.render('message/page-not-found', {'message': 'Can\'t find conference.'})
        });
});// end get /:acronym/create


router.post('/:id/save', function (req, res) {

    const sess = req.session;
    const formData = req.body.formData;
    const logoUrl = req.body.logoUrl;

    new ConferenceMetier()
        .update(
            req.params.id,
            formData.title,
            formData.uri,
            formData.acronym,
            formData.description,
            formData.homepage,
            formData.date_begin,
            formData.date_end,
            formData.country,
            formData.region,
            logoUrl,
            formData.lang,
            sess.user._id
        )
        .then((conference) => {
            return res.send({
                'state': 'success',
                'message': 'Conference saved!',
                'redirect': '/conference/' + conference._id
            })
        })
        .catch((err) => {
            return res.send({
                'state': 'error',
                'message': 'This URI has already been used. Please take another one.',
                'redirect': '/'
            });
        });
}); // end /save


/**********************************/
module.exports = router;