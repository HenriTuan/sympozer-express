const express = require('express');
const router = express.Router();

import PublicationMetier from '../../../metiers/publication';

// create new publication
router.get('/create', function (req, res) {
    //var conf = confHelper.modelToHtmlFormFormat(req.confModel)
    return res.render('conference/publication/create', {
        'isOwner': req.isOwner,
        'conf': req.conf,
        'author': req.author,
        'activeCode': 'publication'
    });

});// end get /create


router.post('/persist', function (req, res) {

    const data = req.body.formData;

    new PublicationMetier()
        .add(
            req.id_conference,
            data.title,
            data.abstract,
            data.keywords,
            data.idAuthors,
            data.idEvent,
            data.idsEvent,
        )
        .then((publication) => {
            return res.send({
                'state': 'success',
                'message': 'Publication saved!',
                'redirect': '/conference/' + req.id_conference + '/publication',
            });
        })
        .catch((err) => {
            return res.send({
                'state': 'error',
                'message': 'This name has already been used. Please take another one.',
                'redirect': '/'
            });
        });
});// end /persist


/**********************************/
module.exports = router;