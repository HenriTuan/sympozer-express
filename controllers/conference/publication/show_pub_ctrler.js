const express = require('express');
const router = express.Router();

import PublicationMetier from '../../../metiers/publication';
import PersonConferenceMetier from '../../../metiers/person';
import EventMetier from '../../../metiers/event';

// show all publications
router.get('/', function (req, res) {

  return res.render('conference/publication/show-all', {
      'author': req.author,
      'isOwner': req.isOwner,
      'activeCode': 'publication',
      'conf': req.conf
    }
  );
  /*
   new PublicationMetier()
   .find(req.id_conference)
   .then((publications) => {
   return res.render('conference/publication/show-all', {
   'author': req.author,
   'isOwner': req.isOwner,
   'activeCode': 'publication',
   'conf': req.conf
   }
   );
   })
   .catch((err) => {
   return res.render('message/page-not-found', {'message': 'Can\'t find data. Please try again later, thankyou for your comprehension.'});
   });*/
});// end get /all


router.get('/page', function (req, res) {
  let id_min = req.query.id_min;
  let pageSize = parseInt(req.query.page_size);
  if (id_min != '-1') {
    new PublicationMetier()
      .findPage(req.id_conference, req.query.id_min, req.query.term, pageSize + 1)
      .then((publications) => {
        res.render('conference/publication/publication-partial',
          {
            'listPub': publications.slice(0, pageSize),
            'author': req.author,
            'isOwner': req.isOwner,
            'activeCode': 'publication',
            'conf': req.conf,
            'firstPage': false
          }, (err, html) => {
            let resObj = {
              html: html,
              empty: publications.length === 0,
              next: publications.length > pageSize
            };
            res.send(JSON.stringify(resObj));
          });
      })
      .catch((err) => {
        res.json({error: err})
      });
  }
  else {
    new PublicationMetier()
      .findWithTerm(req.id_conference, req.query.term, pageSize + 1)
      .then((publications) => {
        res.render('conference/publication/publication-partial',
          {
            'listPub': publications.slice(0, pageSize),
            'author': req.author,
            'isOwner': req.isOwner,
            'activeCode': 'publication',
            'conf': req.conf,
            'firstPage': true
          }, (err, html) => {
            let resObj = {
              html: html,
              empty: publications.length === 0,
              next: publications.length > pageSize
            };
            res.send(JSON.stringify(resObj));
          });
      })
      .catch((err) => {
        res.json({error: err})
      });
  }

});

// show 1 publication
router.get('/:id', function (req, res) {

  new PublicationMetier()
    .get(
      req.id_conference,
      req.params.id,
    )
    .then((publication) => {
      new PersonConferenceMetier()
        .getMultiple(req.id_conference, publication.idAuthors)
        .then((authors) => {
          authors.sort((a,b) => {
            return publication.idAuthors.indexOf(a._id) > publication.idAuthors.indexOf(b._id) ? 1 : -1;
          });
          return res.render('conference/publication/show', {
              'pub': publication,
              'listAuthor': authors,
              'author': req.author,
              'isOwner': req.isOwner,
              'conf': req.conf,
              'activeCode': 'publication'
            }
          );
        })
        .catch((err) => {
          return res.render('message/page-not-found', {'message': 'Couldn\'t find authors.'})
        });

    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Publication doesn\'t exist.'})
    });

});// end get /:slug

router.delete('/:id/removeauthor/:authid', function (req, res) {
  new PublicationMetier()
    .removeAuthor(
      req.id_conference,
      req.params.id,
      req.params.authid)
    .then((pub) => {
      res.json(pub);
    })
    .catch((err) => {
      res.json({error: err});
    })
});

router.post('/:id/addauthor/:authid', function (req, res) {
  new PublicationMetier()
    .addAuthors(
      req.id_conference,
      req.params.id,
      req.params.authid)
    .then((pub) => {
      new PersonConferenceMetier().get(req.id_conference, req.params.authid)
        .then((person) => {
          return res.render('conference/publication/person-pub-partial', {
              'person': person,
              'pubid': req.params.id,
              'author': req.author,
              'isOwner': req.isOwner,
              'conf': req.conf,
              'activeCode': 'publication'
            }
          );
        })
        .catch((err) => {
          res.json({error: err});
        })

    })
    .catch((err) => {
      res.json({error: err});
    })
});

router.get("/find", (req, res) => {
    new PublicationMetier()
        .findWithTerm(req.id_conference, req.query.term, parseInt(req.query.page_size))
        .then((publications) => {
            return res.json({
                publications: publications,
            });
        })
        .catch((err) => {
            return res.json({error: err});
        });
});

/**
 * Permet de récupérer les events associés à la publication
 */
router.get("/:id/events", (req, res) => {
    new EventMetier()
        .findByIdPublication(
            req.id_conference,
            req.params.id,
        )
        .then((events) => {
            return res.json({
                events: events,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
});

/**
 * Permet de supprimer un event associé à la publication
 */
router.delete("/:id/events/:idEvent", (req, res) => {
    new EventMetier()
        .removeRelatedPublication(
            req.id_conference,
            req.params.idEvent,
            req.params.id,
        )
        .then(() => {
            return res.json({});
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
});

/**********************************/
module.exports = router;