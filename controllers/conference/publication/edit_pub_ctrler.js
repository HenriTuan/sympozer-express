const express = require('express');
const router = express.Router();

import PublicationMetier from '../../../metiers/publication';

router.get('/:id/edit', function (req, res) {

  new PublicationMetier()
    .get(
      req.id_conference,
      req.params.id,
    )
    .then((publication) => {
      return res.render('conference/publication/edit', {
          'pub': publication,
          'author': req.author,
          'isOwner': req.isOwner,
          'conf': req.conf,
          'activeCode': 'publication'
        }
      );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Publication not existed.'})
    });
});// end get /:slug/edit


router.post('/:id/save', function (req, res) {

  const data = req.body.formData;

  new PublicationMetier()
    .update(
      req.id_conference,
      req.params.id,
      data
    )
    .then((publication) => {
      return res.send({
        'state': 'success',
        'message': 'Role saved!',
        'redirect': '/conference/' + req.id_conference + '/publication/' + publication._id
      })
    })
    .catch((err) => {
      console.log(err);
      return res.send({
        'state': 'error',
        'message': 'This email has already been used. Please take another one.',
        'redirect': '/'
      });
    });
});// end /save
/**********************************/
module.exports = router;