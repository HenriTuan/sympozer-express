const express = require('express');
const router = express.Router();

import PublicationMetier from '../../../metiers/publication';

router.post('/delete/:id', function (req, res) {

  new PublicationMetier()
    .remove(
      req.id_conference,
      req.params.id,
    )
    .then(() => {
      return res.send({
        'state': 'success',
        'message': 'Publication deleted!',
        'redirect': '/conference/' + req.id_conference + '/publication',
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This publication is not found in database.',
        'redirect': '/'
      });
    });
});// end /persist


/**********************************/
module.exports = router;