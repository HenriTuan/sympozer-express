const express = require('express');
const router  = express.Router();
const pubDAO = require('../../../DAO/publication_DAO');
const confDAO = require('../../../DAO/conference_DAO');

// post get-list-avaiable-maker
router.post('/makers/get-list-avaiable-maker', function(req, res){

	confDAO.getConfModelByAcronym(req.body.confAcronym, function(err, conf){

		if( err || conf == null || typeof(conf.list_person) === 'undefined' ){
			return res.render('message/page-not-found', {'message': 'Can\'t find people. Pleas try again later, thankyou for your comprehension.'})
		}else{
			return res.render('component/list-user.jade', {'listAvailablePerson': conf.list_person, 'confAcronym': req.body.confAcronym})
		}

	})

});// end post /get-list-maker


// post add maker to pub
router.post('/makers/add-maker-to-pub', function(req, res){

	const listPersonUri = req.body.listPersonUri;
	pubDAO.addListMaker(req.body.pubUri, listPersonUri, req.body.confAcronym, function(err){

		if(err){
			return res.send({'state': 'error', 'message': 'We can\'t add author. Please try again later.'})
		}else{
			return res.send({'state': 'success'})
		}
	})// end addlistMemberByAcronym	

}) ;// end post /get-list-maker



// post remove maker from pub
router.post('/makers/remove-maker-from-pub', function(req, res){

	pubDAO.removeMaker(req.body.pubUri, req.body.personUri, req.body.confAcronym, function(err){

		if(err){
			return res.send({'state': 'error', 'message': 'We can\'t remove this author. Please try again later.'})
		}else{
			return res.send({'state': 'success', 'message': 'author removed.', 'redirect': 'reload'})
		}
	})// end removePersonById

}); // end post remove maker from pub




/**********************************/
module.exports = router;