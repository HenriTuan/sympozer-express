const express = require('express');
const countryLanguage = require('country-language');
const router = express.Router();
import ConferenceMetier from '../../metiers/conference';


// create new conference
router.get('/create', function (req, res) {

    let listLang = [];
    countryLanguage.getLanguages().forEach(function (el) {
        listLang.push({'name': el.name[0], 'val': el.iso639_2en});
    });

    listLang.sort(function (a, b) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
        if (a.name > b.name) {
            return 1;
        }
        if (a.name < b.name) {
            return -1;
        }
        // a must be equal to b
        return 0;
    });

    return res.render('conference/create', {'listLang': listLang})
});// end get /create


router.post('/persist', function (req, res) {

    const sess = req.session;

    const formData = req.body.formData;
    const logoUrl = req.body.logoUrl;

    new ConferenceMetier()
        .add(
            formData.title,
            formData.uri,
            formData.acronym,
            formData.description,
            formData.homepage,
            formData.date_begin,
            formData.date_end,
            formData.country,
            formData.region,
            logoUrl,
            formData.lang,
            sess.user._id,
        )
        .then((conference) => {
            return res.send({
                'state': 'success',
                'message': 'Conference saved!',
                'redirect': '/conference/' + conference._id
            })
        })
        .catch((err) => {
            return res.send({
                'state': 'error',
                'message': 'URI has already been used. Please take another one.',
                'redirect': '/'
            });
        });
});// end /persist


/**********************************/
module.exports = router;