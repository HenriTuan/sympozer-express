/*
    Create by Pierre Marsot on 20/12/2017
*/
const express = require('express');
const router = express.Router();

import EventCategorieMetier from '../../../metiers/event_category';

router.get("", (req, res) => {
    new EventCategorieMetier()
        .find()
        .then((eventCategories) => {
            return res.json({
                event_categories: eventCategories,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
});

module.exports = router;