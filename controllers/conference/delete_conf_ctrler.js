const express = require('express');
const router = express.Router();
import ConferenceMetier from '../../metiers/conference';

router.post('/delete/persist', function (req, res) {

  new ConferenceMetier()
    .remove(req.body.id)
    .then(() => {
      return res.send({
        'state': 'success',
        'message': 'Conference deleted!',
        'redirect': '/conference/myconferences'
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This conference is not found in database.',
        'redirect': '/'
      });
    });
});// end /persist


/**********************************/
module.exports = router;