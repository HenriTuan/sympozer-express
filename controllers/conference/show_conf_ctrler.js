const express = require('express');
const router = express.Router();
import ConferenceMetier from '../../metiers/conference';

const confHelper = require('../../helpers/conference_helper');
const confDAO = require('../../DAO/conference_DAO');
const userModel = require('../../models/user');

// show list conferences of current user
router.get('/myconferences', function (req, res) {

  const sess = req.session;

  if(!sess.user){
    return res.render('message/access-denied', {'message': 'Please login to gain access to this page'})
  }

  new ConferenceMetier()
    .findByAuthor(sess.user._id)
    .then((conferences) => {
      return res.render('conference/show-all', {'listConf': conferences, 'isOwner': true});

    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Server is temporary down, please try again later.'})
    });
}); // end get /myconference


// show list conferences grouped by authors
router.get('/user/all', function (req, res) {

  /*new ConferenceMetier()
    .findAndGroupByAuthor()
    .then((conferences) => {
      var listConfOfUser = []
      var keys = []
      var nConf = Object.keys(conferences).length

      var i = 0 // store key to call in userModel.findOne, because loop and userModel.findOne callback not async
      for (key in conferences) {
        keys[i] = key
        i++
      }

      var j = 0
      for (key in conferences) {
        userModel.findOne({'_id': key}, function (err, user) {
          if (err) {
            console.log(err)
            return res.render('message/page-not-found', {'message': 'Server is temporary down, please try again later.'})
          } else {
            listConfOfUser.push({'author': user, 'listConf': listConf[keys[j]]})
            j++
            if (j == nConf) { // last loop
              return res.render('conference/show-all-group-by-user.jade', {'listConfOfUser': listConfOfUser})
            }
          }
        }) // end userModel.findOne

      }
    })
    .catch((err) => {
      return res.render('conference/show-all-group-by-user.jade', {'errMessage': 'There is no registered conference.'});
    });*/
  confDAO.getAllConfGroupByUser(function (err, listConf) {
    if (err) {
      return res.render('conference/show-all-group-by-user.jade', {'errMessage': 'There is no registered conference.'})
    } else {

      var listConfOfUser = []
      var keys = []
      var nConf = Object.keys(listConf).length

      var i = 0 // store key to call in userModel.findOne, because loop and userModel.findOne callback not async
      for (key in listConf) {
        keys[i] = key
        i++
      }

      var j = 0
      for (key in listConf) {
        userModel.findOne({'_id': key}, function (err, user) {
          if (err) {
            console.log(err)
            return res.render('message/page-not-found', {'message': 'Server is temporary down, please try again later.'})
          } else {
            listConfOfUser.push({'author': user, 'listConf': listConf[keys[j]]})
            j++
            if (j == nConf) { // last loop
              return res.render('conference/show-all-group-by-user.jade', {'listConfOfUser': listConfOfUser})
            }
          }
        }) // end userModel.findOne

      } // end listConf.each

    }
  }) // end getAllConfGroupByUser

}) // end /user/all


// show list conferences of one user
router.get('/user/:acronym', function (req, res) {

  var userAnc = req.params.acronym
  var isOwner = false
  var sess = req.session

  if (typeof(sess.user) != 'undefined' && sess.user !== null) {
    if (userAnc == sess.user.acronym)
      isOwner = true
  }

  confDAO.getAllConfByUserAcronym(userAnc, function (err, listConf) {
    if (err == -1)
      return res.render('message/page-not-found', {'message': 'User does not exist.'})
    else
      return res.render('conference/show-all', {'listConf': listConf, 'isOwner': isOwner})
  }) // end getAllConfByUserAcronym

}) // end get /myconference


// show 1 conference
router.get('/:id', function (req, res, next) {

  
  new ConferenceMetier()
    .get(req.params.id)
    .then((conference) => {
      return res.render('conference/overview', {
        'conf': {
          '_id'		  : conference._id,
          'title'		  : conference.title,
          'acronym'	  : conference.acronym,
          'uri'		  : conference.uri,
          'homepage'	  : conference.homepage,
          'description' : conference.description,
          'GMT'		  : conference.timezone,
          'language'	  : conference.language.name,
          'logo_path'	  : conference.logo_path,
        },
        'author': req.author,
        'isOwner': req.isOwner,
        'activeCode': 'overview'
      });
    })
    .catch((err) => {
      res.render('message/page-not-found', {'message': 'Something has gone wrong in our server. Pleas try again later'})
    });
});// end get /:acronym


// show 1 conference's rdfStore
router.get('/:acronym/rdf-store', function (req, res, next) {

  confDAO.getConfRdfStoreByAcronym(req.params.acronym, function (err, allTriple) {

    if (err)
      res.render('message/page-not-found', {'message': 'Something has gone wrong in our server. Pleas try again later'})
    else {
      // splice allTriple to arrays of 20 triples
      var tripleTable = []

      while (allTriple.length) {
        tripleTable.push(allTriple.splice(0, 20))
      }
      var conf = confHelper.modelToHtmlFormFormat(req.confModel)
      return res.render('conference/rdf-store', {
          'conf': conf,
          'tripleTable': tripleTable,
          'nTripleTable': tripleTable.length,
          'author': req.author,
          'isOwner': req.isOwner,
          'activeCode': 'rdfStore'
        }
      )
    }

  }) // end getConfRdfStoreByAcronym


}) // end get /:acronym/rdf-store


// post tripleTable
router.post('/:acronym/get-triple', function (req, res) {
  var page = req.body.page
  var acronym = req.body.acronym

  confDAO.getConfRdfStoreByAcronym(acronym, function (err, allTriple) {
    if (err) {
      console.log(err)
      return res.render('message/page-not-found', {'message': 'We can\'t find this page of rdf store. Please try again later.'})
    } else {
      var tripleTable = allTriple.splice((page - 1) * 20, 20)
      return res.render('component/triple-table.jade', {'tripleTable': tripleTable})
    }

  })// end getConfRdfStoreByAcronym

}) // end post /get-triple


/**********************************/
module.exports = router;