const express = require('express');
const router = express.Router();
import RoleMetier from '../../../metiers/role';

// load role
router.get('/:id/edit', function (req, res) {

  new RoleMetier()
    .get(
      req.id_conference,
      req.params.id,
    )
    .then((role) => {
      return res.render('conference/role/edit', {
          'role': role,
          'author': req.author,
          'isOwner': req.isOwner,
          'conf': req.conf,
          'activeCode': 'role'
        }
      );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Role not existed.'})
    });
});// end get /:slug/edit


router.post('/:id/save', function (req, res) {

  const data = req.body.formData;
  new RoleMetier()
    .update(
      req.id_conference,
      req.params.id,
      data.name,
    )
    .then((role) => {
      return res.send({
        'state': 'success',
        'message': 'Role saved!',
        'redirect': '/conference/' + req.id_conference + '/role/' + role._id
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This email has already been used. Please take another one.',
        'redirect': '/'
      });
    });
});// end /save


/**********************************/
module.exports = router;