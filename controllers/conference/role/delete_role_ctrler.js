const express = require('express');
const router = express.Router();
import RoleMetier from '../../../metiers/role';

router.post('/delete/:id', function (req, res) {

  console.log('delete role', req.id_conference, req.params.id);
  new RoleMetier()
    .remove(
      req.id_conference,
      req.params.id,
    )
    .then(() => {
      return res.send({
        'state': 'success',
        'message': 'Role deleted!',
        'redirect': '/conference/' + req.id_conference + '/role'
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This role is not found in database.',
        'redirect': 'reload'
      });
    });
}); // end /persist


/**********************************/
module.exports = router;