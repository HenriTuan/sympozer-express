const express = require('express');
const router = express.Router();
import RoleMetier from '../../../metiers/role';
import PersonConferenceMetier from '../../../metiers/person';

router.get('/', function (req, res) {

    return res.render('conference/role/show-all', {
            'author': req.author,
            'isOwner': req.isOwner,
            'activeCode': 'role',
            'conf': req.conf
        }
    );
    /*
    new RoleMetier()
      .find(req.id_conference)
      .then((roles) => {
        return res.render('conference/role/show-all', {
            'author': req.author,
            'isOwner': req.isOwner,
            'activeCode': 'role',
            'conf': req.conf
          }
        );
      })
      .catch((err) => {
        return res.render('message/page-not-found', {'message': 'Can\'t find data. Please try again later, thankyou for your comprehension.'})
      });*/
});// end get


router.get('/page', function (req, res) {
    let id_min = req.query.id_min;
    let pageSize = parseInt(req.query.page_size);
    if (id_min != '-1') {
        new RoleMetier()
            .findPage(req.id_conference, req.query.id_min, req.query.term, pageSize + 1)
            .then((roles) => {
                res.render('conference/role/role-partial',
                    {
                        'listRole': roles.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'role',
                        'conf': req.conf,
                        'firstPage': false
                    }, (err, html) => {
                        let resObj = {
                            html: html,
                            empty: roles.length === 0,
                            next: roles.length > pageSize
                        };
                        res.send(JSON.stringify(resObj));
                    });
            })
            .catch((err) => {
                res.json({error: err})
            });
    }
    else {
        new RoleMetier()
            .findWithTerm(req.id_conference, req.query.term, pageSize + 1)
            .then((roles) => {
                res.render('conference/role/role-partial',
                    {
                        'listRole': roles.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'role',
                        'conf': req.conf,
                        'firstPage': true
                    }, (err, html) => {
                        let resObj = {html: html, empty: roles.length === 0, next: roles.length > pageSize};
                        res.send(JSON.stringify(resObj));
                    });
            })
            .catch((err) => {
                res.json({error: err})
            });
    }

});

/*router.get('/list',function (req, res) {
    new RoleMetier()
        .find(req.id_conference)
        .then((roles) =>{
            res.send(JSON.stringify(roles));
        })
        .catch((err) => {
            console.log(err);
            return
        });
});*/

// show 1 role of conference
router.get('/:id', function (req, res) {

    new RoleMetier()
        .get(
            req.id_conference,
            req.params.id,
        )
        .then(async (role) => {
            try {
                console.log("get persons");
                //On récupère les personnes liées au role
                const persons = await new PersonConferenceMetier()
                    .getAllWithRole(req.id_conference, role._id);
                console.log(persons);

                //On crée la réponse par défaut
                const response = {
                    'role': role,
                    'listPersons': [],
                    'author': req.author,
                    'isOwner': req.isOwner,
                    'conf': req.conf,
                    'activeCode': 'role'
                };

                //Si on a pas de personnes, on les ajoute à la réponse
                if (persons && persons.length > 0) {
                    response.listPersons = persons;
                }

                return res.render('conference/role/show', response);
            }
            catch (e) {
                return res.render('message/page-not-found', {'message': 'Role not existed.'})
            }
        })
        .catch((err) => {
            return res.render('message/page-not-found', {'message': 'Role not existed.'})
        });
});// end get /:slug


/**********************************/
module.exports = router;