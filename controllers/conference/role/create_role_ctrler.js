const express = require('express');
const router = express.Router();
import RoleMetier from '../../../metiers/role';

// create new role
router.get('/create', function (req, res) {
  return res.render('conference/role/create', {
    'isOwner': req.isOwner,
    'conf': req.conf,
    'author': req.author,
    'activeCode': 'role'
  });
});// end get /create


router.post('/persist', function (req, res) {

  const data = req.body.formData;

  new RoleMetier()
    .add(
      req.id_conference,
      data.name,
    )
    .then((role) => {
      return res.send({
        'state': 'success',
        'message': 'Role saved!',
        'redirect': '/conference/' + req.id_conference + '/role'
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This email has already been used. Please take another one.',
        'redirect': '/'
      });
    });
});// end /persist


/**********************************/
module.exports = router;