/**
 * Created by pierremarsot on 20/11/2017.
 */

const express = require('express');
const router = express.Router();
import TrackMetier from '../../../metiers/track';

/**
 * Permet d'afficher la page pour gérer les tracks
 */
router.get("", function (req, res) {
    return res.render('conference/track/index', {
            'author': req.author,
            'isOwner': req.isOwner,
            'conf': req.conf,
            'activeCode': 'tracks'
        }
    );
});

/**
 * Permet de récupérer les chairs d'une track
 */
router.get("/:id/chairs", function (req, res) {
    new TrackMetier()
        .getChairs(req.id_conference, req.params.id)
        .then((chairs) => {
            return res.json({
                chairs: chairs,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
});

/**
 * Permet de récupérer toutes les tracks
 */
router.get("/find", function (req, res) {
    new TrackMetier()
        .find(req.id_conference)
        .then((tracks) => {
            return res.json({tracks: tracks});
        })
        .catch((err) => {
            return res.json({error: err});
        });
});

/**
 * Permet de récupérer une rack via son id
 */
router.get("/:id", function (req, res) {
    new TrackMetier()
        .get(req.params.id, req.id_conference)
        .then((track) => {
            return res.json({track: track});
        })
        .catch((err) => {
            return res.json({error: err});
        });
});

/**
 * Permet d'ajouter une track
 */
router.post("", function (req, res) {
    const data = req.body;

    new TrackMetier()
        .add(req.id_conference, data.name, data.idsPerson, data.trackVotable)
        .then((track) => {
            return res.json({
                track: track,
            });
        })
        .catch((err) => {
            return res.json({error: err});
        });
});

/**
 * Permet de supprimer une track
 */
router.delete("/:id", function (req, res) {
    new TrackMetier()
        .remove(req.params.id, req.id_conference)
        .then(() => {
            return res.json({id_track: req.params.id});
        })
        .catch((err) => {
            return res.json({error: err});
        });
});

/**
 * Permet de modifier un track
 */
router.post("/:id", function (req, res) {
    new TrackMetier()
        .update(req.id_conference, req.params.id, req.body.name, req.body.idsPerson, req.body.trackVotable)
        .then((track) => {
            return res.json({track: track});
        })
        .catch((err) => {
            return res.json({error: err});
        });
});

module.exports = router;