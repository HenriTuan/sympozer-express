import OrganizationMetier from "../../../metiers/organization";
import PersonConferenceMetier from "../../../metiers/person";
const express = require('express');
const router = express.Router();

// show all organizations
router.get('/', function (req, res) {

    return res.render('conference/organization/show-all',
        {
            'author': req.author,
            'isOwner': req.isOwner,
            'activeCode': 'organization',
            'conf': req.conf,
        }
    );
    /*
    new OrganizationMetier()
        .find(req.id_conference)
        .then((organizations) => {
            return res.render('conference/organization/show-all',
                {
                    'author': req.author,
                    'isOwner': req.isOwner,
                    'activeCode': 'organization',
                    'conf': req.conf,
                }
            );
        })
        .catch((err) => {
            return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
        });*/
}); // end get /all

router.get('/page', function (req, res) {
    let id_min = req.query.id_min;
    let pageSize = parseInt(req.query.page_size);
    if (id_min != '-1') {
        new OrganizationMetier()
            .findPage(req.id_conference, req.query.id_min, req.query.term, pageSize + 1)
            .then((organizations) => {
                res.render('conference/organization/orga-partial',
                    {
                        'listOrga': organizations.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'organization',
                        'conf': req.conf,
                        'firstPage': false
                    }, (err, html) => {
                        let resObj = {
                          html:html,
                          empty:organizations.length === 0,
                          next: organizations.length > pageSize
                        };
                        res.send(JSON.stringify(resObj));
                    });
            })
            .catch((err) => {
              res.json({error:err})
            });
    }
    else {
        new OrganizationMetier()
            .findWithTerm(req.id_conference, req.query.term, pageSize + 1)
            .then((organizations) => {
                res.render('conference/organization/orga-partial',
                    {
                        'listOrga': organizations.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'organization',
                        'conf': req.conf,
                        'firstPage': true
                    }, (err, html) => {
                        let resObj = {
                          html:html,
                          empty:organizations.length === 0,
                          next: organizations.length > pageSize
                        };
                        res.send(JSON.stringify(resObj));
                    });
                /*return res.render('conference/organization/orga-partial',
                    {
                        'listOrga': organizations,
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'organization',
                        'conf': req.conf,
                        'firstPage': true
                    });*/
            })
            .catch((err) => {
              res.json({error:err})
            });
    }

});


// show 1 organization of conference
router.get('/:id', function (req, res) {

    new OrganizationMetier()
        .get(req.id_conference, req.params.id)
        .then((organization) => {
            new PersonConferenceMetier().getMultiple(req.id_conference, organization.idPersons)
                .then(listMember => {
                    return res.render('conference/organization/show',
                        {
                            'orga': organization,
                            'listMember': listMember,
                            'author': req.author,
                            'isOwner': req.isOwner,
                            'conf': req.conf,
                            'activeCode': 'organization'
                        }
                    );
                })
                .catch(err => {
                    return res.render('message/page-not-found', { 'message': 'Some person in organization not existed.' })
                });
        })
        .catch((err) => {
            return res.render('message/page-not-found', { 'message': 'Organization not existed.' })
        });
}); // end get /:slug

/**********************************/
module.exports = router;