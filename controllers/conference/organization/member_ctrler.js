var express = require('express')
var router = express.Router()

var orgaHelper = require('../../../helpers/organization_helper')
var orgaDAO = require('../../../DAO/organization_DAO')

var confDAO = require('../../../DAO/conference_DAO')
var confHelper = require('../../../helpers/conference_helper')

import PersonConferenceMetier from '../../../metiers/person';
import OrganisationMetier from '../../../metiers/organization';

// post get-list-avaiable-member
router.post('/members/get-list-avaiable-member', function (req, res) {

	new OrganisationMetier().get(req.id_conference, req.body.orgaId)
		.then((organisation) => {
			new PersonConferenceMetier().find(req.id_conference)
				.then((list_persons) => {
					list_persons.slice().forEach(person => {
						if (organisation.idPersons != null && (organisation.idPersons.indexOf(person._id)) != -1) {
							list_persons.splice(list_persons.indexOf(person), 1);
						}
					})
					return res.render('component/list-user.jade', { 'listAvailablePerson': list_persons, 'confId': req.id_conference, 'orgaId': organisation._id })
				})
				.catch((err) => {
					console.log(err);
					return res.render('component/list-user.jade', { 'err': 'An error has occurred when getting list persons.' });
				})
		})
		.catch((err) => {
			console.log(err);
			return res.render('component/list-user.jade', { 'err': 'An error has occurred when getting organization.' });
		})

}) // end post /get-list-member


// post add member to orga
router.post('/members/add-member-to-orga', function (req, res) {

	let organisationMetier = new OrganisationMetier();
	var listPersonsIds = req.body.listPersonsIds

	organisationMetier.get(req.id_conference, req.body.orgaId)
		.then(organisation => {
			if (listPersonsIds != null && listPersonsIds.length > 0) {
				var listPersonsUpdated = organisation.idPersons.concat(listPersonsIds)
				organisationMetier.updateListPersons(organisation._id, req.id_conference, listPersonsUpdated)
					.then(() => {
						return res.send({ 'state': 'success', 'message': 'Person added.', 'redirect': 'reload' })
					})
					.catch(err => {
						console.log(err)
						return res.send({ 'state': 'error', 'message': 'We can\'t add member. Please try again later.' })
					})
			} else {
				return res.send({ 'state': 'error', 'message': 'Please choose the person to add to this organization.' })
			}
		})
		.catch(err => {
			console.log(err)
			return res.send({ 'state': 'error', 'message': 'We can\'t add member. Please try again later.' })
		});

}) // end post /add-member-to-orga



// post remove member from orga
router.post('/members/remove-member-from-orga', function (req, res) {

	let orgaId = req.body.orgaId;
	let personId = req.body.personId;
	let organisationMetier = new OrganisationMetier();

	organisationMetier.get(req.id_conference, orgaId)
		.then(organisation => {
			if (organisation.idPersons != null && organisation.idPersons.indexOf(personId) != -1) {
				organisation.idPersons.splice(organisation.idPersons.indexOf(personId), 1);
				organisationMetier.updateListPersons(orgaId, req.id_conference, organisation.idPersons)
					.then(() => {
						return res.send({ 'state': 'success', 'message': 'Person removed.', 'redirect': 'reload' })
					})
					.catch(err => {
						return res.send({ 'state': 'error', 'message': 'We can\'t remove this member. Please try again later.' });
					})
			}
		})
		.catch(err => {
			console.log(err);
			return res.send({ 'state': 'error', 'message': 'We can\'t remove this member. Please try again later.' });
		});

}) // end post remove member from orga




/**********************************/
module.exports = router