const express = require('express');
const router = express.Router();
import OrganizationMetier from "../../../metiers/organization";


router.get('/:id/edit', function (req, res) {

  new OrganizationMetier()
    .get(
      req.id_conference,
      req.params.id,
    )
    .then((organization) => {
      return res.render('conference/organization/edit', {
          'orga': organization,
          'author': req.author,
          'isOwner': req.isOwner,
          'conf': req.conf,
          'activeCode': 'orga'
        }
      );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Location not existed.'})
    });
});// end get /:id/edit


router.post('/:id/save', function (req, res) {

  const orga = req.body.formData;

  new OrganizationMetier()
    .update(
      req.params.id,
      req.id_conference,
      orga.name,
      orga.logo_path,
      orga.homepage,
      orga.description,
    )
    .then((organization) => {
      return res.send({
        'state': 'success',
        'message': 'Organization saved!',
        'redirect': '/conference/' + req.id_conference + '/organization/' + organization._id
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This URI has already been used. Please take another one.',
        'redirect': '/'
      });
    });
});// end /save


/**********************************/
module.exports = router;