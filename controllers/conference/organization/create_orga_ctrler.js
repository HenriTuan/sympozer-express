const express = require('express');
const router = express.Router();
import OrganizationMetier from '../../../metiers/organization';

// create new organization
router.get('/create', function (req, res) {

  return res.render('conference/organization/create', {
    'isOwner': req.isOwner,
    'conf': req.conf,
    'author': req.author,
    'activeCode': 'organization'
  });
});// end get /create


router.post('/persist', function (req, res) {

  const formData = req.body.formData;

  new OrganizationMetier()
    .add(
      formData.name,
      formData.logo_path,
      formData.homepage,
      formData.description,
      req.id_conference,
    )
    .then((organization) => {
      return res.send({
        'state': 'success',
        'message': 'Organization saved!',
        'redirect': '/conference/' + req.id_conference + '/organization'
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This name has already been used. Please take another one.',
        'redirect': '/'
      });
    });
});// end /persist


/**********************************/
module.exports = router;