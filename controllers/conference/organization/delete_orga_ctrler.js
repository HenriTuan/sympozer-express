const express = require('express');
const router = express.Router();
import OrganizationMetier from '../../../metiers/organization';

router.post('/delete/:id', function (req, res) {

  new OrganizationMetier()
    .remove(
      req.id_conference,
      req.params.id
    )
    .then(() => {
      return res.send({
        'state': 'success',
        'message': 'Organization deleted!',
        'redirect': '/conference/' + req.id_conference + '/organization'
      })
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This organization is not found in database.',
        'redirect': '/'
      });
    });
});// end /persist


/**********************************/
module.exports = router;