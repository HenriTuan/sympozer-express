const express = require('express');
const router = express.Router();
import PersonConferenceMetier from '../../../metiers/person';

router.post('/delete/:id', function (req, res) {

  new PersonConferenceMetier()
    .remove(
      req.id_conference,
      req.params.id,
    )
    .then(() => {
      return res.send({
        'state': 'success',
        'message': 'Person deleted!',
        'redirect': '/conference/' + req.id_conference + '/person'
      });
    })
    .catch((err) => {
      return res.send({
        'state': 'error',
        'message': 'This person is not found in database.',
        'redirect': 'reload'
      });
    });
});// end /persist


/**********************************/
module.exports = router;