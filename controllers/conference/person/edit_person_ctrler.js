const express = require('express');
const router = express.Router();
import PersonConferenceMetier from '../../../metiers/person';
import RoleMetier from '../../../metiers/role';

// load person
router.get('/:id/edit', function (req, res) {
  let listRoles = [];
  new RoleMetier()
      .find(req.id_conference)
      .then((roles) => listRoles = roles)
      .catch((err) => {
          return res.render('message/page-not-found', {'message': 'roles don\'t existed.'})
      });

  new PersonConferenceMetier()
    .get(
      req.id_conference,
      req.params.id,
    )
    .then((personConference) => {
      return res.render('conference/person/edit', {
          'person': personConference,
          'listRoles': listRoles,
          'author': req.author,
          'isOwner': req.isOwner,
          'conf': req.conf,
          'activeCode': 'person'
        }
      );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Person not existed.'})
    });
});// end get /:acronym/create


router.post('/:id/save', function (req, res) {

  const data = req.body.formData;

  new PersonConferenceMetier()
    .update(
      req.id_conference,
      req.params.id,
      data.firstname,
      data.lastname,
      data.ava_path,
      data.email,
      data.homepage,
      data.country,
      data.social_account,
      data.idRoles,
    )
    .then((personConference) => {
      return res.send({
        'state': 'success',
        'message': 'Person saved!',
        'redirect': '/conference/' + req.id_conference + '/person/' + personConference._id
      });
    })
    .catch((err) => {
      console.log(err);
      return res.send({
        'state': 'error',
        'message': 'This email has already been used. Please take another one.',
        'redirect': '/'
      });
    });
});// end /save

/**********************************/
module.exports = router;