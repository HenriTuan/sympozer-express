const express = require('express');
const router = express.Router();

import PersonConferenceMetier from '../../../metiers/person';
import RoleMetier from "../../../metiers/role";
import OrganizationMetier from "../../../metiers/organization";

import PublicationMetier from "../../../metiers/publication";
// show all persons
router.get('/', function (req, res) {

    return res.render('conference/person/show-all', {
            'author': req.author,
            'isOwner': req.isOwner,
            'activeCode': 'person',
            'conf': req.conf
        }
    );
    /*
  new PersonConferenceMetier()
    .find(req.id_conference)
    .then((personsConference) => {
        return res.render('conference/person/show-all', {
                'author': req.author,
                'isOwner': req.isOwner,
                'activeCode': 'person',
                'conf': req.conf
            }
        );
    })
    .catch((err) => {
      return res.render('message/page-not-found', {'message': 'Can\'t find data. Pleas try again later, thankyou for your comprehension.'})
    });*/
}); // end get /all


router.get('/page', function (req, res) {
    let id_min = req.query.id_min;
    let pageSize = parseInt(req.query.page_size);
    if (id_min != '-1') {
        new PersonConferenceMetier()
            .findPage(req.id_conference, req.query.id_min, req.query.term, pageSize + 1)
            .then((persons) => {
                res.render('conference/person/person-partial',
                    {
                        'listPerson': persons.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'person',
                        'conf': req.conf,
                        'firstPage': false
                    },(err, html) => {
                        let resObj = {
                            html:html,
                            empty:persons.length === 0,
                            next:persons.length > pageSize
                        };
                        res.send(JSON.stringify(resObj));
                    });
            })
            .catch((err) => {
                res.json({error: err})
            });
    }
    else {
        new PersonConferenceMetier()
            .findWithTerm(req.id_conference, req.query.term, pageSize + 1)
            .then((persons) => {
                res.render('conference/person/person-partial',
                    {
                        'listPerson': persons.slice(0, pageSize),
                        'author': req.author,
                        'isOwner': req.isOwner,
                        'activeCode': 'person',
                        'conf': req.conf,
                        'firstPage': true
                    },(err, html) => {
                        let resObj = {
                            html:html,
                            empty:persons.length === 0,
                            next:persons.length > pageSize
                        };
                        res.send(JSON.stringify(resObj));
                    });
            })
            .catch((err) => {
                res.json({error: err})
            });
    }

});

router.get('/rest', function (req, res) {
    new PersonConferenceMetier()
        .findWithTerm(req.id_conference, req.query.term, parseInt(req.query.page_size))
        .then((persons) => {
            res.json(persons);
        })
        .catch((err) => {
            res.json({error: err})
        });
});


// show 1 person of conference
router.get('/:id', function (req, res) {

    new PersonConferenceMetier()
        .get(
            req.id_conference,
            req.params.id,
        )
        .then(async (personConference) => {
            //Init
            let organizations;
            let roles;
            let publications;

            //On récupère les roles
            if (personConference.idRoles && personConference.idRoles.length > 0) {
                try {
                    roles = await new RoleMetier()
                        .getMultiple(req.id_conference, personConference.idRoles);
                }
                catch (e) {
                    return res.render('message/page-not-found', {'message': 'Couldn\'t find roles.'})
                }
            }

            //On récupère les organisations de la personne
            try {
                organizations = await new OrganizationMetier().getByPerson(req.id_conference, personConference._id);
            }
            catch(e) {
                return res.render('message/page-not-found', {'message': 'Couldn\'t find roles.'})
            }

            //On récupère les publications de la personne
            try {
                publications = await new PublicationMetier()
                    .getAllFromAuthor(req.id_conference, req.params.id);
            }
            catch(e) {
                return res.render('message/page-not-found', {'message': 'Couldn\'t find roles.'})
            }

            return res.render('conference/person/show', {
                'person': personConference,
                'listRoles': roles,
                'listPubs': publications,
                'listOrganizations': organizations,
                'author': req.author,
                'isOwner': req.isOwner,
                'conf': req.conf,
                'activeCode': 'person'
            });
        })
        .catch((err) => {
            return res.render('message/page-not-found', {'message': 'Person not existed.'})
        });
});// end get /:slug


/**********************************/
module.exports = router;