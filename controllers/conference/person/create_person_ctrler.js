const express = require('express');
const router = express.Router();

import PersonConferenceMetier from '../../../metiers/person';
import RoleMetier from "../../../metiers/role";

// create new person
router.get('/create', function (req, res) {

    let listRoles = [];
    new RoleMetier()
        .find(req.id_conference)
        .then((roles) => {
          listRoles = roles;
            return res.render('conference/person/create', {
                'listRoles': listRoles,
                'isOwner': req.isOwner,
                'conf': req.conf,
                'author': req.author,
                'activeCode': 'person'
            });
        })
        .catch((err) => {
            console.log(err);
            return res.render('message/page-not-found', {'message': 'roles don\'t existed.'})
        });



});// end get /create


router.post('/persist', function (req, res) {

  const data = req.body.formData;

  new PersonConferenceMetier()
    .add(
      req.id_conference,
      data.firstname,
      data.lastname,
      data.ava_path,
      data.email,
      data.homepage,
      data.country,
      data.social_account,
      data.idRoles,
    )
    .then((personConference) => {
      return res.send({
        'state': 'success',
        'message': 'Person saved!',
        'redirect': '/conference/' + req.id_conference + '/person'
      });
    })
    .catch((err) => {
    console.log(err);
      return res.send({
        'state': 'error',
        'message': 'This email has already been used. Please take another one.',
        'redirect': '/'
      });
    });
});// end /persistavec un bottom 5


/**********************************/
module.exports = router;