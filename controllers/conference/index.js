/*
    Create by Pierre Marsot on 28/11/2017
*/

/**
 * Conference controller Rest
 */

const express = require('express');
const router = express.Router();
import ConferenceMetier from '../../metiers/conference';

router.get('/informations', function (req, res) {

    new ConferenceMetier()
        .get(req.id_conference)
        .then((conference) => {
            return res.json({
                conference: conference,
            });
        })
        .catch((err) => {
            return res.json({
                error: err,
            });
        });
}); //

module.exports = router;