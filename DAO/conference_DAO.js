var underscore = require('underscore')
var async = require('async')
var util = require('../helpers/util')

var userModel = require('../models/user')

var confModel = require('../models/conference')
var confHelper = require('../helpers/conference_helper')

var personDAO = require('../DAO/person_DAO')
var personHelper = require('../helpers/person_helper')

var rdfStoreModel = require('../models/dataset/rdf_store')
var rdfStoreHelper = require('../helpers/rdf_store_helper')




var confDAO = {}

confDAO.persist = function (newConf, creator, cb){ // creator == session.user

	// save in MongoDB conference table
	confModel.create(newConf, function(err, conf){
		if(err) {
			cb(err)
		}else{
			// create triples
			var allTriple = [
				confHelper.createTriple(newConf.title, 'label', newConf.uri),
				confHelper.createTriple(newConf.display_acronym, 'hasAcronym', newConf.uri),
				confHelper.createTriple(newConf.uri+'complete', 'completeGraph', newConf.uri),
				confHelper.createTriple(newConf.type, 'type', newConf.uri),
				confHelper.createTriple(newConf.date_begin,   'dtstart', 	 newConf.uri),
				confHelper.createTriple(newConf.date_end,     'dtend', 	 newConf.uri),
				confHelper.createTriple(newConf.uri+'person/'+creator.displayname, 'creator', newConf.uri),
				confHelper.createTriple(newConf.logo_path,    'hasLogo',	 newConf.uri),
				confHelper.createTriple(newConf.homepage,    'homepage',	 newConf.uri),
			]
			
			var newRdfStore = new rdfStoreModel({
											'file_path': null,
											'file_name': conf.acronym,
											'acronym'  : conf.acronym,
											'uri'	   : conf.uri,
											'source'   : allTriple
											})			
			
			newRdfStore.save(function(err){
    			if(err) {
    				console.log(err)
    				cb(err)
    			}
    			else cb(null, conf) // persist success, callback return err=null
		    })

		} // end if(err)

	}) // end save in MongoDB conference table		

} // end persist


confDAO.save = function(newConf, creator, cb){

	confModel.findOne({'acronym': newConf.acronym}, function(err, oldConf){

		newConf.location.formatted_address = newConf.location.country+', '+newConf.location.region

		oldConf.description = newConf.description
		oldConf.title 		 = newConf.title
		oldConf.date_begin  = newConf.date_begin
		oldConf.date_end    = newConf.date_end
		oldConf.location 	 = newConf.location
		oldConf.language 	 = newConf.language
		oldConf.homepage     = newConf.homepage

		if(newConf.logo_path !== ''){
			oldConf.logo_path = newConf.logo_path
		}

		oldConf.save(function(err){
			if(err) {
				console.log(err)
				cb(err)
			}else{

				rdfStoreModel.findOne({'acronym': newConf.acronym}, function(err, rdfStore){
					if(err) cb(err)

					var newSource = confHelper.updateRdfStore(rdfStore.source, newConf)
					rdfStore.source = newSource

					rdfStore.save(function(err){
		    			if(err) return cb(err)
		    			else return cb(null) // save success, callback return err=null
			    	}) // end rdfStore.save
				
				}) // end rdfStoreModel.findOne
				
			} // end if(err)
		}) // end conf.save

	}) // end confModel.findOne
	
} // end save



/*** get all ***/
var addUserToListConf = function(listConf, userId, listConfOfUser, cb){

	userModel.findOne({'_id': userId}, function(err, user){
		if(err){
			console.log(err)
			cb(err)
		}else{
			listConfOfUser.push({'author': user, 'conf': listConf[key]})
		}
	}) // end userModel.findOne

} // end addUserToListConf

confDAO.getAllConfGroupByUser = function(cb){
	confModel.find(function(err, docs){

		if(err){
			console.log(err)
			cb(err, null)
		}else{
			if(docs == null || docs.length == 0){
				cb({'message': 'No conference existed.'})
			}else{
				var listConf = underscore.groupBy(docs, '_user_id')
				cb(null, listConf)
			}
		}
	}) // end .toArray

}
/** end get all ***/


/*** get by Acronym ***/
confDAO.getConfModelByAcronym = function (acronym, cb){

	confModel.findOne({'acronym': acronym}, function(err, conf){
		if(err){
			console.log(err)
			return cb(err)
		}else{
			return cb(null, conf)
		}
	}) // end findOne

} // end getConfModelByAcronym


confDAO.getConfRdfStoreByAcronym = function(acronym, cb){

	rdfStoreModel.findOne({'acronym': acronym}, function(err, rdfStoreModel){
		if(err){
			console.log(err)
			cb(err)
		}else{
			cb(null, rdfStoreModel.source)
		}
	}) // end findOne

} // end getConfRdfStoreByAcronym


confDAO.getConfAllTypeByAcronym = function(acronym, cb){

	confModel.findOne({'acronym': acronym}, function(err, confModel){
	
		if(confModel == null) cb(err, null, null)
		else{
			rdfStoreModel.findOne({'acronym': acronym}, function(err, rdfStoreModel){
				if(rdfStoreModel == null) cb(err, null, null)
				else cb(null, confModel, rdfStoreModel.source) // getAll success, return err=null
			}) // end findOne
		}// end else

	}) // end findOne	

} // end getConfAllTypeByShortUri

confDAO.getAuthorByAcronym = function(acronym, cb){

	confModel.findOne({'acronym': acronym}, function(err, confModel){
		if(err) {
			console.log(err)
			cb(err)
		}
		userModel.findOne({'_id': confModel._user_id}, function(err, user){
			if(err) {
				console.log(err)
				cb(err)
			}
			cb(null, user)
		}) // end user findOne
	}) // end conference findOne

} // end getAUthorNameByAcronym


/*** end get by Acronym ***/



/*** get by User ***/
confDAO.getAllConfByUserId = function(userId, cb){

	confModel.find({'_user_id': userId}, function(err, listConf){
		cb(err, listConf)
	}) // end find

} // end getAllConfByUserId


confDAO.getAllConfByUserAcronym = function(acronym, cb){

	userModel.findOne({'acronym': acronym}, function(err, user){
		if(err) {
			console.log(err)
			cb(err, null)
		}
		if(user==null) cb(-1, null)
		else{
			confModel.find({'_user_id': user._id}, function(err, listConf){
				if(err){
					console.log(err)
					cb(err)
				}
				cb(null, listConf)
			}) // end find
		}
	})// end findOne

} // end getAllConfByUserAcronym

/*** end get by User ***/


confDAO.deleteConfByAcronym = function (acronym, cb){

	confModel.findOne({'acronym': acronym}, function(err, conf){

		if(conf==null) cb(-1) // fail
		else{
			conf.remove(function(err){
				if(err) {
					console.log(err)
					cb(err)
				}
				else{
					rdfStoreModel.findOne({'acronym': acronym}, function(err, rdfStore){
						if(err) cb(err)
						else
							rdfStore.remove(function(err){
								if(err) {
									cb(err)
									console.log(err)
								}else {
									cb(null)
								} //delete success
							}) // end remove rdfStore
							
					}) // end findOne rdfStore
				}

			}) // end remove conf
		}

	}) // end findOne confModel

} // end deleteConfByUri



confDAO.getListPersonByAcronym = function(confAcronym, cb){



	

} // end getListPersonByAcronym



confDAO.hasPerson = function(confAcronym, email, cb){

	confModel.findOne({'acronym': confAcronym}, function(err, conf){

			if( typeof(conf.list_person) === 'undefined' || conf.list_person === null || conf.listPerson === [] )
				return cb(false)

			for(var i in conf.list_person){
				if( conf.list_person[i].person_email === email ){
					return cb(true)
				}
			}
			return cb(false)

	}) // end comfModel.fineOne

} // end hasPerson




confDAO.getListAvaiablePerson = function(confId, cb){

	personDAO.getAll(null, function(err, listPerson){

		if(err) cb(err)

		var listAvaiablePerson = []
		
		for( var i in listPerson){
			if(!personDAO.hasConference(listPerson[i], confId)) {
				// person not belong to confernce -> add to list
				listAvaiablePerson.push(listPerson[i])
			}
		}
		return cb(null, listAvaiablePerson)
			

	}) // end personDAO.getAll


} // end getListAvaiablePerson



confDAO.addPersonByAcronym = function(confAcronym, person, cb){

	confDAO.getConfModelByAcronym(confAcronym, function(err, conf){

		if(err) return cb(err)
		else{

			conf.list_person.push( {'_person_id': person._id, '_person_acronym': person.acronym, 'person_email': person.email,
									 'person_sha': person.sha, 'list_role': [], 'list_publication': []} )

			conf.save(function(err){
				if(err){
					console.log(err)
					return cb(err)
				}else{

					// get person triple and push to array
					personTriple = personHelper.createAllTriple(person)
					rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){
						if(err){
							console.log(err)
							cb(err)
						}else{
							var newSource = rdfStore.source.concat(personTriple)
							rdfStore.source = newSource

							rdfStore.save(function(err){
								if(err){
									console.log(err)
									cb(err)
								}else{
									cb(null)
								}
							}) // end rdfSTore.save	

						} // end else											
					}) // end rdfStoreModel.findOne

				} // end else

			}) // end conf.save
		}

	}) // end getConfModelByAcronym


} // end addPersonIdByAcronym



confDAO.addlistPersonByAcronym = function(confAcronym, listPerson, cb){

	confDAO.getConfModelByAcronym(confAcronym, function(err, conf){

		if(err) return cb(err)
		else{

			var newListPerson = []

			for( var i in listPerson ){
				var person = listPerson[i]
				newListPerson.push({ '_person_id': person._id, 'person_acronym': person.acronym, 'person_sha': person.sha })
			}

			if( typeof(conf.list_person) == 'undefined' || conf.list_person == null || conf.list_person.length <= 0){
				conf.list_person = newListPerson
			}else{
				conf.list_person = conf.list_person.concat(newListPerson)
			}

			conf.save(function(err){
				if(err){
					console.log(err)
					return cb(err)
				}else{
					// https://github.com/caolan/async#each
					// add confId to each person
					var allPersonTriple = []
					async.each(listPersonId, 
					
						function(personId, cb){
							personDAO.getById(personId, function(err, person){
								if(err){
									console.log(err)
									cb(err)
								}else{
									person._list_id_conference.push(conf.id)
									person.save(function(err){
										if(err){
											console.log(err)
											cb(err)
										}else{
											// get person triple and push to array
											allPersonTriple = allPersonTriple.concat(personHelper.createAllTriple(person))
											cb(null)
										}
									}) // end person.save
								}
							}) // end getById
						}, 

						function(err){
							if(err){
								console.log(err)
								return cb(err)
							}else{
								// merge allPersonTriple to rdfStore.source
								rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){
									if(err){
										console.log(err)
										cb(err)
									}else{
										var newSource = rdfStore.source.concat(allPersonTriple)
										rdfStore.source = newSource

										rdfStore.save(function(err){
											if(err){
												console.log(err)
												cb(err)
											}else{
												cb(null)
											}
										}) // end rdfSTore.save													
									}												
								}) // end rdfStoreModel.findONe

							}
						}

					) // end async.each


				}
			}) // end conf.save

		}

	}) // end getConfModelByAcronym


} // end addListPersonId



confDAO.removePerson = function(confAcronym, person, cb){

	confModel.findOne({'acronym': confAcronym}, function(err, conf){

		if(err){
			console.log(err)
			return cb(err)
		}else{
			var newListPerson = util.removeEleAcronymOfArray(conf.list_person, '_person_acronym', person.acronym) 

			conf.list_person = newListPerson
			conf.save(function(err){
				if(err){
					console.log(err)
					cb(err)
				}else{

					// update rdfStore
					rdfStoreModel.findOne({'acronym': conf.acronym}, function(err, rdfStore){
						rdfStore.source = rdfStoreHelper.removePersonTriple(rdfStore.source, person)

						rdfStore.save(function(err){
							if(err){
								console.log(err)
								cb(err)
							}else{		
								cb(null)	
							}
							
						}) // end rdfStore.save

					}) // end rdfStoreModel.findOne

				}
			}) // end conf.save

		}


	}) // end confModel.findOne


} // end removePerson



confDAO.savePersonInConf = function(confAcronym, person, listRole, listPublication, cb){

	confModel.findOne({'acronym': confAcronym}, function(err, conf){

		for( var i in conf.list_person ){
			if( '"'+conf.list_person[i]._person_id.toString()+'"' == person.id ){
				conf.list_person[i].list_role = listRole
				conf.list_person[i].list_publication = listPublication
			}
		}

		conf.save(function(err){

			if(err){
				console.log(err)
				return cb(err)
			}else{

				rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){
					
					rdfStore.source = rdfStoreHelper.addRoleTriple(rdfStore, person, listRole)
					// rdfStore.source = rdfStoreHelper.addPublicationTriple(rdfStore, person, listPublication)

					rdfStore.save(function(err){
						
						if(err){
							console.log(err)
							return cb(err)
						}else{
							return cb(null)
						}

					}) // end rdfStore.save

				}) // end rdfStoreModel.findOne

			}

		}) // end conf.save

	}) // end confModel.findOne


} // end savePersonInConf



/** organization **/
confDAO.addOrgaByAcronym = function(confAcronym, orga, cb){

	async.waterfall([

	    function(cb) {

	    	confModel.findOne({'acronym': confAcronym}, function(err, conf){
	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			cb(null, conf)
	    		}
	    	}) // end confModel.findOne

	    }, // end function1

	    function(conf, cb) {

	    	var newListOrga = [{'_organization_id': orga._id}]

			if( typeof(conf.list_organization) == 'undefined' || conf.list_organization == null || conf.list_organization == []){
				conf.list_organization = newListOrga
			}else{
				conf.list_organization = conf.list_organization.concat(newListOrga)
			}

			conf.save(function(err){
				if(err){
					console.log(err)
					cb(err)
				}else{
					cb(null, conf)
				}
			}) // end conf.save

	    }, // end function2

	    function(conf, cb) {

	    	orga._conference_id = conf._id
			orga.save(function(err){
				if(err){
					console.log(err)
					return cb(err)
				}else{
					return cb(null, conf)
				}
			}) // end orga.save

	    }, // end function3
	
	    function updateRdfStore(conf, cb){

	    	rdfStoreModel.findOne({'acronym': conf.acronym}, function(err, rdfStore){

	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			
	    			var newSource = rdfStoreHelper.addOrganizationTriple(rdfStore, orga)
	    			rdfStore.source = newSource

	    			rdfStore.save(function(err){

	    				if(err){
	    					console.log(err)
	    					cb(err)
	    				}else{
	    					cb(null)
	    				}

	    			}) // end rdfStore.save
	    		}

	    	}) // end rdfStoreModel

	    } // end updateRdfStore

		], function(err) {
    		cb(err)
		} // end updateRdfStore

	) // end async.waterfall



} // end addOrgaByAcronym


/*** end organization ***/



/**** publication *****/

confDAO.addPubByAcronym = function(confAcronym, pub, cb){

	async.waterfall([

	    function(cb) {

	    	confModel.findOne({'acronym': confAcronym}, function(err, conf){
	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			cb(null, conf)
	    		}
	    	}) // end confModel.findOne

	    }, // end function1

	    function(conf, cb) {

	    	var newListPub = [{'_publication_id': pub._id}]

			if( typeof(conf.list_publication) == 'undefined' || conf.list_publication == null || conf.list_publication == []){
				conf.list_publication = newListOrga
			}else{
				conf.list_publication = conf.list_publication.concat(newListPub)
			}

			conf.save(function(err){
				if(err){
					console.log(err)
					cb(err)
				}else{
					cb(null, conf)
				}
			}) // end conf.save

	    }, // end function2

	    function(conf, cb) {

	    	pub._conference_acronym = conf.acronym
			pub.save(function(err){
				if(err){
					console.log(err)
					return cb(err)
				}else{
					return cb(null, conf)
				}
			}) // end pub.save

	    }, // end function3
	
	    function updateRdfStore(conf, cb){

	    	rdfStoreModel.findOne({'acronym': conf.acronym}, function(err, rdfStore){

	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			
	    			var newSource = rdfStoreHelper.addPublicationTriple(rdfStore, pub)
	    			rdfStore.source = newSource

	    			rdfStore.save(function(err){

	    				if(err){
	    					console.log(err)
	    					cb(err)
	    				}else{
	    					cb(null)
	    				}

	    			}) // end rdfStore.save
	    		}

	    	}) // end rdfStoreModel

	    } // end updateRdfStore

		], function(err) {
    		cb(err)
		} // end updateRdfStore

	) // end async.waterfall



} // end addPubByAcronym


/******** end publication *************/




/**** event *****/

confDAO.addEventByAcronym = function(confAcronym, event, cb){

	async.waterfall([

	    function(cb) {

	    	confModel.findOne({'acronym': confAcronym}, function(err, conf){
	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			cb(null, conf)
	    		}
	    	}) // end confModel.findOne

	    }, // end function1

	    function(conf, cb) {

	    	var newListEvent = [{'_event_id': event._id}]

			if( typeof(conf.list_event) == 'undefined' || conf.list_event == null || conf.list_event == []){
				conf.list_event = newListEvent
			}else{
				conf.list_event = conf.list_event.concat(newListEvent)
			}

			conf.save(function(err){
				if(err){
					console.log(err)
					cb(err)
				}else{
					cb(null, conf)
				}
			}) // end conf.save

	    }, // end function2

	    function(conf, cb) {

	    	event._conference_acronym = conf.acronym
			event.save(function(err){
				if(err){
					console.log(err)
					return cb(err)
				}else{
					return cb(null, conf)
				}
			}) // end event.save

	    }, // end function3
	
	    function updateRdfStore(conf, cb){

	    	rdfStoreModel.findOne({'acronym': conf.acronym}, function(err, rdfStore){

	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			
	    			var newSource = rdfStoreHelper.addEventTriple(rdfStore, event)
	    			rdfStore.source = newSource

	    			rdfStore.save(function(err){

	    				if(err){
	    					console.log(err)
	    					cb(err)
	    				}else{
	    					cb(null)
	    				}

	    			}) // end rdfStore.save
	    		}

	    	}) // end rdfStoreModel

	    } // end updateRdfStore

		], function(err) {
    		cb(err)
		} // end updateRdfStore

	) // end async.waterfall



} // end addEventByAcronym


/******** end event *************/




/********* location **************/

confDAO.addLocationByAcronym = function(confAcronym, location, cb){

	async.waterfall([

	    function(cb) {

	    	confModel.findOne({'acronym': confAcronym}, function(err, conf){
	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			cb(null, conf)
	    		}
	    	}) // end confModel.findOne

	    }, // end function1

	    function(conf, cb) {

	    	var newListLocation = [{'_location_id': location._id}]

			if( typeof(conf.list_event) == 'undefined' || conf.list_event == null || conf.list_event == []){
				conf.list_event = newListLocation
			}else{
				conf.list_event = conf.list_event.concat(newListLocation)
			}

			conf.save(function(err){
				if(err){
					console.log(err)
					cb(err)
				}else{
					cb(null, conf)
				}
			}) // end conf.save

	    }, // end function2

	    function(conf, cb) {

	    	location._conference_acronym = conf.acronym
			location.save(function(err){
				if(err){
					console.log(err)
					return cb(err)
				}else{
					return cb(null, conf)
				}
			}) // end location.save

	    }, // end function3
	
	    function updateRdfStore(conf, cb){

	    	rdfStoreModel.findOne({'acronym': conf.acronym}, function(err, rdfStore){

	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			
	    			var newSource = rdfStoreHelper.addLocationTriple(rdfStore, location)
	    			rdfStore.source = newSource

	    			rdfStore.save(function(err){

	    				if(err){
	    					console.log(err)
	    					cb(err)
	    				}else{
	    					cb(null)
	    				}

	    			}) // end rdfStore.save
	    		}

	    	}) // end rdfStoreModel

	    } // end updateRdfStore

		], function(err) {
    		cb(err)
		} // end updateRdfStore

	) // end async.waterfall



} // end addLocationByAcronym




/********* end location *************/






/***********************/
confDAO.getMatchedPerson = function(confAcronym, user, cb){

	confModel.findOne({'acronym': confAcronym}, function(err, conf){

		if(err){
			console.log(err)
			cb(err)
		}else{

			personDAO.getAllByConference(confAcronym, function(err, listPerson){

				if(err || listPerson == null || listPerson == []){
					return cb(err)
				}else{
					for(var i in listPerson){
					
						if(listPerson[i].sha == user.sha){
							return cb(null, listPerson[i])
						}
					}
					return cb(null, null)
				}

			}) // end personDAO.getAllByConference

		}

	}) // end confModel.findOne

} // end getMatchePerson



confDAO.connectUser = function(confAcronym, person, cb){

	confModel.findOne({'acronym': confAcronym}, function(err, conf){

		if(err){
			console.log(err)
			cb(err)
		}else{

			conf.user_person._person_acronym = person.acronym
			conf.user_person.person_displayname = person.displayname
			conf.user_person.person_sha = person.sha

			conf.save(function(err){
				
				if(err){
					console.log(err)
					cb(err)
				}else{
					cb(null)
				}

			}) // end conf.save

		}

	}) // end confModel.findOne

} // end connectUser




/**************************/
module.exports = confDAO