var util = require('../helpers/util')
var async = require('async')
var underscore = require('underscore')

var pubHelper = require('../helpers/publication_helper')

var personDAO = require('../DAO/person_DAO')

var confDAO = require('../DAO/conference_DAO')
var confModel = require('../models/conference')

var rdfStoreModel = require('../models/dataset/rdf_store')
var rdfStoreHelper = require('../helpers/rdf_store_helper')

var pubDAO = {}



pubDAO.persistToConf = function (newPub, confAcronym, cb){ // creator == session.user

	newPub.uri = 'http://data.semanticweb.org/publication/'+newPub.title.toLowerCase().replace(/ /g, '-')
	newPub.slug = util.encodeUri(newPub.uri)

	async.waterfall([

		function loadConf(cb){
			
			// check if this email already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				return cb(null, conf)

			}) // end confModel.findOne
		
		}, // end checkPub

		function saveConf(conf, cb){

			conf.list_publication.push({
				'slug': newPub.slug,
		        'uri': newPub.uri,
		        'title': newPub.title,
		        'abstract': newPub.abstract
			})

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
				var pubTriples = rdfStoreHelper.publicationModelToTriples(newPub)

				rdfStore.source = allTriple.concat(pubTriples)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null, newPub.slug)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end savePub			

	) // end async.waterfall

	
} // end persistToConf




pubDAO.save = function(pub, confAcronym, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){
		
		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

		var index = 0
		// delete alltriple
		for( var i in allTriple ){
			if(allTriple[i].subject.nominalValue == pub.uri){
				allTriple[i] = null
				index = i
			} // end if
			
		} // end for

		var newPublicationTriples = rdfStoreHelper.publicationModelToTriples(pub)

		//http://stackoverflow.com/questions/7032550/javascript-insert-an-array-inside-another-array
		allTriple.splice.apply(allTriple, [index, 0].concat(newPublicationTriples))
		allTriple = underscore.compact(allTriple)

		rdfStore.source = allTriple
		rdfStore.save(function(err){
			
			if(err) return cb(err)
			
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err) return cb(err)

				for(var i in conf.list_publication){
					var pubConf = conf.list_publication[i]
					if(pubConf.slug == pub.slug){
						pubConf.name = pub.name
					}
				}

				conf.save(function(err){
					if(err) return cb(err)
					return cb(null)
				}) // end confModel.save

			}) // end confModel.findOne

		}) // end rdfStore.save

	}) // end rdfStore.finOne
	
} // end save




pubDAO.deleteByAcronym = function (pubAcronym, confAcronym, cb){
	
	async.waterfall([
	    
		function findPub(cb){

			

		}, // end findPub

	    function removePubInConf(pub, cb) {
	    
	       
	    
	    }, // end removePubInConf
	    
	    function saveCon(conf, pub, cb){

	    	conf.save(function(err){
	    		if(err){
	    			console.log(err)
	    			cb(err)
	    		}else{
	    			cb(null, pub, conf.acronym)
	    			// cb(null, pub)
	    		}
	    	}) // end conf.save

	    }, // end saveConf

	    function updateRdfStore(pub, confAcronym, cb){

	    	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

	    		rdfStore.source = rdfStoreHelper.removePublicationTriple(rdfStore.source, pub)
	    		rdfStore.save(function(err){
	    			if(err){
	    				console.log(err)
	    				cb(err)
	    			}else{
	    				cb(null, pub)
	    			}
	    		}) // end rdfStore.save

	    	}) // end rdfStoreModel.findOne

	    }, // end updateRdfStore

	    function removePub(pub, cb){

	    

	    } // end function4
		
		], function (err) {
		   	cb(err)
		}

	)


} // end deleteByAcronym


/*** get by slug ***/
pubDAO.getBySlug = function(confAcronym, slug, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err)
		}

		var pub = {}
		pub.slug = slug

		var existed = false

		for(var i in rdfStore.source){

			var triple = rdfStore.source[i]
			if(triple.subject.nominalValue != null){
				if( slug == util.encodeUri(triple.subject.nominalValue) ){
					existed = true
					if(typeof(pub.uri) == 'undefined'){
						pub.uri = triple.subject.nominalValue
					}

					rdfStoreHelper.tripleToPublicationModel(pub, triple)
				}
			}

		} // end for

		if(!existed){
			return cb({'code': -1})
		}

		// get list author Uri
		var listAuthorUri = []
		for(var i in rdfStore.source){

			var triple = rdfStore.source[i]
			if(triple.subject.nominalValue == pub.uri && triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/maker'){
				listAuthorUri.push(triple.object.nominalValue)
			}

		}

		// get list author
		var listAuthor = []
		for(var i in listAuthorUri){
			
			var author = {
				'slug': util.encodeUri(listAuthorUri[i]),
				'uri' : listAuthorUri[i]
			}

			for(var j in rdfStore.source){
				
				var triple = rdfStore.source[j]
				if(triple.subject.nominalValue == listAuthorUri[i]){
					rdfStoreHelper.tripleToSimplePersonModel(author, triple)
				}

			}
			if( typeof(author.ava_path) === 'undefined' ){
				author.ava_path = '/img/sympozer_logo.png'
			}
			listAuthor.push(author)

		}

		return cb(null, pub, listAuthor)

	}) // end findOne

} // end getBySlug



pubDAO.deleteBySlug = function (pubSlug, confAcronym, cb){

	async.waterfall([

		function checkPub(cb){
			
			// check if this slug already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				for(var i in conf.list_publication){
					if( conf.list_publication[i].slug == pubSlug){
						var pubUri = conf.list_publication[i].uri
						conf.list_publication[i] = null
						return cb(null, conf, pubUri)
					}
				}
				// no pub matche -> return not found
				return cb({'code': -1})

			}) // end confModel.findOne
		
		}, // end checkPub

		function saveConf(conf, pubUri, cb){

			conf.list_publication = underscore.compact(conf.list_publication)

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null, pubUri)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err, pubUri){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

				// delete alltriple
				for( var i in allTriple ){
					if(allTriple[i].subject.nominalValue == pubUri){
						allTriple[i] = null
					} // end if
				} // end for

				rdfStore.source = underscore.compact(allTriple)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end savePub			

	) // end async.waterfall

} // end deleteBySlug



pubDAO.addListMaker = function(pubUri, listPersonUri, confAcronym, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err)
		}

		// check if user is maker or not
		for( var i in rdfStore.source){

			var triple = rdfStore.source[i]
			if(triple.subject.nominalValue == pubUri && triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/maker'){
				for(var j in listPersonUri){
					if(listPersonUri[j] == triple.object.nominalValue){
						// this person is already a maker -> remove from list
						listPersonUri.splice(j, 1);
					}
				}
			}

		}
		// end check

		// add new maker triple to rdfStore
		var newTriples = []
		for(var i in listPersonUri){
			newTriples.push( rdfStoreHelper.createMakerTriple(pubUri, listPersonUri[i]) )
		}
		// end add new maker

		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
		rdfStore.source = allTriple.concat(newTriples)
	
		rdfStore.save(function(err){
			if(err){
				return cb(err)
			}
			return cb(null)
		})

	}) // end findOne
	
} // end addListMaker



pubDAO.removeMaker = function(pubUri, personUri, confAcronym, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err)
		}

		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

		// check if user is member or not
		for( var i in allTriple){

			var triple = allTriple[i]
			if(	 triple.subject.nominalValue == pubUri 
				&& triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/maker'
				&& triple.object.nominalValue == personUri ){
					// remove triple
					allTriple.splice(i, 1);	
			}

		}
		// end check

		
		rdfStore.source = allTriple
	
		rdfStore.save(function(err){
			if(err){
				return cb(err)
			}
			return cb(null)
		})

	}) // end findOne
	
} // end removeMaker



/**************************/
module.exports = pubDAO