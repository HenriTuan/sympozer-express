var util = require('../helpers/util')
var async = require('async')
var underscore = require('underscore')

var confDAO = require('../DAO/conference_DAO')
var confModel = require('../models/conference')

var rdfStoreModel = require('../models/dataset/rdf_store')
var rdfStoreHelper = require('../helpers/rdf_store_helper')
var eventModel = require('../models/event_type/event')
var eventCatModel = require('../models/category/event_category')


var eventDAO = {}


eventDAO.persistToConf = function (newEvent, confAcronym, cb) { // creator == session.user
    newEvent.uri = 'http://data.semanticweb.org/event/' + newEvent.name.toLowerCase().replace(/ /g, '-')
    newEvent.slug = util.encodeUri(newEvent.uri)


    confModel.findOne({'acronym': confAcronym}, function (err, conf) {

        if (err) {
            return cb(err)
        }
        else {

            eventCatModel.findOne({'name': newEvent.category}).exec(function (err, eventCat) {


                event = {
                    'slug': newEvent.slug,
                    'uri': newEvent.uri,
                    '_track': eventCat,
                    'conference': conf,
                    'name': newEvent.name,
                    'summary': newEvent.summary
                }
                //set Time to date
                //start
                event.date_start = new Date(newEvent.date_start)
                if (newEvent.time_start) {
                    var time_start = newEvent.time_start.split(":");

                    event.date_start.setHours(time_start[0])
                    event.date_start.setMinutes(time_start[1])
                }

                //end
                event.date_end = new Date(newEvent.date_end)
                if(newEvent.time_end) {
                    var time_end = newEvent.time_end.split(":");

                    event.date_end.setHours(time_end[0])
                    event.date_end.setMinutes(time_end[1])
                }
                console.log(newEvent.super_event_slug)
                if (newEvent.super_event_slug ) {

                    eventModel.findOne({
                        'slug': newEvent.super_event_slug,
                        'conference': conf
                    }, function (err, superEvent) {
                        if(superEvent!= null) {
                        event._super_event = superEvent
                        }

                        eventModel.create(event, function (err, addedEvent) {
                            if (err) {
                                return cb(err)
                            }

                            if(superEvent != null) {
                            superEvent._list_sub_event.push(addedEvent)
                            superEvent.save(function (err) {
                                if (err) return cb(err)

                                eventModel.populate(addedEvent, {path: "_track"}, function (err, addedEvent) {
                                    if (err) return cb(err)
                                    else return cb(null, addedEvent)
                                })
                            })}
                            else {
                            if (err) return cb(err)
                            else return cb(null, addedEvent)
                            }


                        })

                    })

                }
                else {

                    eventModel.create(event, function (err, addedEvent) {
                        if (err) return cb(err)
                        eventModel.populate(addedEvent, {path: "_track"}, function (err, addedEvent) {
                            if (err) return cb(err)
                            else return cb(null, addedEvent)
                        })

                    })

                }


                /*
                 conf.list_event.push({
                 'slug': newEvent.slug,
                 'uri': newEvent.uri,
                 'type': eventCat,
                 'name': newEvent.name,
                 'date_end': newEvent.date_end,
                 'date_start': newEvent.date_start
                 })

                 conf.save(function (err) {

                 if (err) {
                 return cb(err)
                 }

                 return cb(null)

                 }) // end conf.save
                 */
            })

        }

    })


    /*

     async.waterfall([],

     function saveRdfStore(err) {

     if (err) {
     return cb(err)
     }

     rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

     if (err) {
     return cb(err)
     }

     var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
     var eventTriples = rdfStoreHelper.eventModelToTriples(newEvent)

     rdfStore.source = allTriple.concat(eventTriples)
     rdfStore.save(function (err) {

     if (err) {
     return cb(err)
     }

     return cb(null, newEvent.slug)

     }) // end rdfStore.save


     }) // end eventModel.create

     } // end persist



     eventDAO.getAllByConference = function(confAcronym, cb){

     eventModel.find(function(err, listEvent){

     if(err){
     console.log(err)
     return cb(err)
     }else{
     var rslt = []
     for( var i in listEvent ){
     if(listEvent[i]._conference_acronym === confAcronym)
     rslt.push(listEvent[i])
     } // end for

     return cb(null, rslt)
     }

     }) // end eventModel.find

     } // end getAllByConference




     eventDAO.deleteByAcronym = function (eventAcronym, confAcronym, cb){

     async.waterfall([

     function findEvent(cb){

     eventModel.findOne({'acronym': eventAcronym}, function(err, event){
     if(err){
     console.log(err)
     cb(err)
     }else{
     cb(null, event)
     }
     }) // end eventModel.findOne

     }, // end findEvent

     function removeEventInConf(event, cb) {

     confModel.findOne({'acronym': confAcronym}, function(err, conf){
     if(err){
     console.log(err)
     cb(err)
     }else{
     for(var i in conf.list_event){
     if(conf.list_event[i]._event_id === event._id )
     conf.list_event[i] = null
     }
     conf.list_event = underscore.compact(conf.list_event)
     cb(null, conf, event)
     }
     }) // end confModel.findOne

     }, // end removeEventInConf

     function saveCon(conf, event, cb){

     conf.save(function(err){
     if(err){
     console.log(err)
     cb(err)
     }else{
     cb(null, event, conf.acronym)
     // cb(null, event)
     }
     }) // end conf.save

     }, // end saveConf

     function updateRdfStore(event, confAcronym, cb){

     rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

     rdfStore.source = rdfStoreHelper.removeEventTriple(rdfStore.source, event)
     rdfStore.save(function(err){
     if(err){
     console.log(err)
     cb(err)
     }else{
     cb(null, event)
     }
     }) // end rdfStore.save

     }) // end rdfStoreModel.findOne

     }, // end updateRdfStore

     function removeEvent(event, cb){

     }) // end rdfStoreModel.findOne


     } // end saveEvent

     ) // end async.waterfall
     */


} // end persistToConf

eventDAO.getAllByConfAcronym = function (supEventSlug, conf, cb) {


    if (supEventSlug) {
        eventModel.findOne({slug: supEventSlug, conference: conf}, function (err, superEvent) {

            if (err) return cb(err)
            else {

                eventModel.find({_super_event: superEvent, conference: conf}).populate('_track')
                    .exec(function (err, events) {
                        if (err) return cb(err)
                        else {

                            return cb(null, events, superEvent)
                        }

                    })

            }

        })
    }

    else {
        eventModel.find({_super_event: null, conference: conf}).populate('_track')
            .exec(function (err, events) {
                if (err) return cb(err)
                else return cb(null, events, null)

            })
    }

}

eventDAO.deleteByAcronym = function (eventAcronym, confAcronym, cb) {

    async.waterfall([

            function findEvent(cb) {


            }, // end findEvent

            function removeEventInConf(event, cb) {

                confModel.findOne({'acronym': confAcronym}, function (err, conf) {
                    if (err) {
                        cb(err)
                    } else {
                        for (var i in conf.list_event) {
                            if (conf.list_event[i]._event_id.equals(event._id))
                                conf.list_event[i] = null
                        }
                        conf.list_event = underscore.compact(conf.list_event)
                        cb(null, conf, event)
                    }
                }) // end confModel.findOne

            }, // end removeEventInConf

            function saveCon(conf, event, cb) {

                conf.save(function (err) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null, event, conf.acronym)
                        // cb(null, event)
                    }
                }) // end conf.save

            }, // end saveConf

            function updateRdfStore(event, confAcronym, cb) {

                rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

                    rdfStore.source = rdfStoreHelper.removeEventTriple(rdfStore.source, event)
                    rdfStore.save(function (err) {
                        if (err) {
                            cb(err)
                        } else {
                            cb(null, event)
                        }
                    }) // end rdfStore.save

                }) // end rdfStoreModel.findOne

            }, // end updateRdfStore

            function removeEvent(event, cb) {

                event.remove(function (err) {
                    if (err) {
                        cb(err)
                    } else {
                        cb(null)
                    }
                }) // end event.remove

            } // end function4

        ], function (err) {
            cb(err)
        }
    )


} // end deleteByAcronym


eventDAO.save = function (oldEvent, newEvent, confAcronym, cb) {

    newEvent.uri = 'http://data.semanticweb.org/event/' + newEvent.name.toLowerCase().replace(/ /g, '-')
    newEvent.slug = util.encodeUri(newEvent.uri)

    eventCatModel.findOne({name: newEvent.category}, function (err, eventCat) {

        if (err) return cb(err)

        confModel.findOne({acronym: confAcronym}, function (err, conf) {


            oldEvent.name = newEvent.name
            oldEvent.summary = newEvent.summary
            oldEvent.date_start = newEvent.date_start
            oldEvent.date_end = newEvent.date_end


            if (err) return cb(err)

            eventModel.findOne({
                'slug': oldEvent,
                'conference': conf
            }, function (err, event) {
                event.slug = newEvent.slug
                event.name = newEvent.name
                event.uri = newEvent.uri
                event.date_start = newEvent.date_start
                event.date_end = newEvent.date_end
                event.summary = newEvent.summary
                if (eventCat) {
                    event._track = eventCat
                }


                event.save(function (err, updatedEvent) {
                    if (err) return cb(err)
                    return cb(null, updatedEvent)
                })


            })


        })


    })


    /*
     rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

     var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

     var index = 0
     // delete alltriple
     for (var i in allTriple) {
     if (allTriple[i].subject.nominalValue == event.uri) {
     allTriple[i] = null
     index = i
     } // end if

     } // end for

     var newEventTriples = rdfStoreHelper.eventModelToTriples(event)

     //http://stackoverflow.com/questions/7032550/javascript-insert-an-array-inside-another-array
     allTriple.splice.apply(allTriple, [index, 0].concat(newEventTriples))
     allTriple = underscore.compact(allTriple)

     rdfStore.source = allTriple
     rdfStore.save(function (err) {

     if (err) return cb(err)

     confModel.findOne({'acronym': confAcronym}, function (err, conf) {

     if (err) return cb(err)

     for (var i in conf.list_event) {
     var eventConf = conf.list_event[i]
     if (eventConf.slug == event.slug) {
     eventConf.name = event.name
     eventConf.summary = event.summary
     eventConf.date_start = event.date_start
     eventConf.date_end = event.date_end
     }
     }

     conf.save(function (err) {
     if (err) return cb(err)
     return cb(null)
     }) // end confModel.save

     }) // end confModel.findOne

     }) // end rdfStore.save

     }) // end rdfStore.finOne */

} // end save


/*** get by Slug ***/
eventDAO.getBySlug = function (confAcronym, eventSlug, cb) {

    confModel.findOne({'acronym': confAcronym}, function (err, conf) {

        eventModel.findOne({
            'conference': conf,
            'slug': eventSlug
        }).populate('_track').populate('_super_event').populate('_list_sub_event')
            .exec(function (err, event) {
                if (event == null) cb(-1) // fail
                else {

                    cb(null, event)

                }
            })

    })

    /*
     rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

     if (err) {
     return cb(err)
     }

     var event = {}
     event.slug = slug
     event.list_sub_event = []
     event.list_super_event = []

     var existed = false

     for (var i in rdfStore.source) {
     var triple = rdfStore.source[i]
     if (triple.subject.nominalValue != null) {
     if (slug == util.encodeUri(triple.subject.nominalValue)) {
     existed = true
     if (typeof(event.uri) == 'undefined') {
     event.uri = triple.subject.nominalValue
     }

     rdfStoreHelper.tripleToEventModel(event, triple)
     }
     }
     } // end for


     if (!existed) {
     return cb({'code': -1})
     }

     return cb(null, event)

     }) // end findOne
     */


} // end getBySlug


/***** delete event **********/

eventDAO.deleteBySlug = function (eventSlug, confAcronym, cb) {

    async.waterfall([

            function checkEvent(cb) {





                // check if this slug already exist in conference
                confModel.findOne({'acronym': confAcronym}, function (err, conf) {

                    if (err) {
                        return cb(err)
                    }


                    eventModel.findOne({'conference': conf, 'slug': eventSlug}, function (err, event) {

                        if (event == null) cb(-1) // fail
                        else {

                            event.remove(function (err) {
                                if (err) {
                                    return cb(err)
                                }
                                else {
                                    return cb(null)
                                }

                            }) // end remove event category
                        }

                    })


                    // no event matche -> return not found
                    //return cb({'code': -1})

                }) // end confModel.findOne

            }/*, // end checkEvent

             function saveConf(conf, eventUri, cb) {

             conf.list_event = underscore.compact(conf.list_event)

             conf.save(function (err) {

             if (err) {
             return cb(err)
             }

             return cb(null, eventUri)

             }) // end conf.save

             }, // end save Conf

             ]*/],

        function saveRdfStore(err, eventUri) {

            if (err) {
                return cb(err)
            }

            rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

                if (err) {
                    return cb(err)
                }

                var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

                // delete alltriple
                for (var i in allTriple) {
                    if (allTriple[i].subject.nominalValue == eventUri) {
                        allTriple[i] = null
                    } // end if
                } // end for

                rdfStore.source = underscore.compact(allTriple)
                rdfStore.save(function (err) {

                    if (err) {
                        return cb(err)
                    }

                    return cb(null)

                }) // end rdfStore.save


            }) // end rdfStoreModel.findOne

        } // end saveEvent

    ) // end async.waterfall

} // end deleteBySlug


eventDAO.persistAsSubEvent = function (newEvent, confAcronym, superEventSlug, cb) {

    newEvent.uri = 'http://data.semanticweb.org/event/' + newEvent.name.toLowerCase().replace(/ /g, '-')
    newEvent.slug = util.encodeUri(newEvent.uri)
    confModel.findOne({'acronym': confAcronym}, function (err, conf) {

        if (err) cb(err)
        else {
            eventModel.findOne({'conference': conf, 'slug': superEventSlug}, function (err, event) {
                if (err) db(err)

                else {


                    event = {
                        'slug': newEvent.slug,
                        'uri': newEvent.uri,
                        '_track': eventCat,
                        'conference': conf,
                        'name': newEvent.name,
                        'date_end': newEvent.date_end,
                        'date_start': newEvent.date_start,
                        'summary': newEvent.summary
                    }


                    eventModel.create(event, function (err, addedEvent) {
                        if (err) {
                            return cb(err)
                        }

                        return cb(null)
                    })


                }
            })


        }


    })


}


/*** sub event ****/
eventDAO.getListAvailableSubEvent = function (confAcronym, eventAcronym, cb) {


} // end getListAvailableSubEvent


eventDAO.addListSubEventByAcronym = function (eventAcronym, listSubEvent, cb) {


} // end addlistSubEventByAcronym


/**** end sub event *********/


/***super event ****/
eventDAO.getListAvailableSuperEvent = function (confAcronym, eventAcronym, cb) {


} // end getListAvailableSuperEvent


eventDAO.addListSuperEventByAcronym = function (eventAcronym, listSuperEvent, cb) {


} // end addlistSuperEventByAcronym


/**************************/
module.exports = eventDAO