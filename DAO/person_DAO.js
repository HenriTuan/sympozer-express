var confDAO = require('./conference_DAO')
var confHelper = require('../helpers/conference_helper')
var confModel = require('../models/conference')

var personHelper = require('../helpers/person_helper')

var rdfStoreHelper = require('../helpers/rdf_store_helper')
var rdfStoreModel = require('../models/dataset/rdf_store')

var util = require('../helpers/util')
var async = require('async')
var underscore = require('underscore')
var mongoose = require('mongoose')
var sha1 = require('sha1')


var personDAO = {}



personDAO.persist = function (newPerson, creator, cb){ // creator == session.user

	// save in MongoDB person table

} // end persist



personDAO.persistToConf = function (newPerson, confAcronym, cb){ // creator == session.user

	async.waterfall([

		function checkPerson(cb){
			
			// check if this email already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				for(var i in conf.list_person){
					if( conf.list_person[i].sha == newPerson.sha){
						return cb({'code': 11000})	
					}
				}

				return cb(null, conf)

			}) // end confModel.findOne
		
		}, // end checkPerson

		function saveConf(conf, cb){

			conf.list_person.push({
				'slug': newPerson.slug,
		        'uri': newPerson.uri,
		        'displayname': newPerson.displayname,
		        'firstname': newPerson.firstname,
		        'lastname': newPerson.lastname,
		        'ava_path': newPerson.ava_path,
		        'sha' : newPerson.sha
			})

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
				var personTriples = rdfStoreHelper.personModelToTriples(newPerson)

				rdfStore.source = allTriple.concat(personTriples)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null, newPerson.slug)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end savePerson			

	) // end async.waterfall

	
} // end persistToConf





personDAO.getBySlug = function(confAcronym, slug, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err);
		}

		var person = {};
		person.slug = slug;
		person.social_account = [];
		person.list_role = [];

		var existed = false;

		for(var i in rdfStore.source){
			var triple = rdfStore.source[i]
			if(triple.subject.nominalValue != null){
				if( slug == util.encodeUri(triple.subject.nominalValue) ){
					existed = true;
					if(typeof(person.uri) == 'undefined'){
						person.uri = triple.subject.nominalValue;
					}

					rdfStoreHelper.tripleToPersonModel(person, triple);
				}
			}
		} // end for

		if(!existed){
			return cb({'code': -1});
		}

		if( typeof(person.ava_path) == 'undefined' ){
			person.ava_path = '/img/sympozer_logo.png'; // default avatar
		}

		var listRoleTriple = rdfStore.source.filter(function(triple){
			return triple.subject.nominalValue === person.uri &&
				triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#holdsRole';
		});

		for(var i in listRoleTriple){
			
			var uri = listRoleTriple[i].object.nominalValue;
			var roleTriple = rdfStore.source.filter(function(triple) {
				return triple.subject.nominalValue === uri && 
					triple.predicate.nominalValue === 'http://www.w3.org/2000/01/rdf-schema#label';
			});

			person.list_role.push(	
									{	'slug': util.encodeUri(uri),
										'name': roleTriple[0].object.nominalValue,
								   		'uri': uri
									}
								 );
		}

		return cb(null, person);

	}); // end findOne

}; // end getBySlug



personDAO.save = function(person, confAcronym, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){
		
		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

		person.displayname = person.firstname + ' ' + person.lastname

		var index = 0
		// delete alltriple
		for( var i in allTriple ){
			if(allTriple[i].subject.nominalValue == person.uri){
				allTriple[i] = null
				index = i
			} // end if
			
		} // end for

		var newPersonTriples = rdfStoreHelper.personModelToTriples(person)

		//http://stackoverflow.com/questions/7032550/javascript-insert-an-array-inside-another-array
		allTriple.splice.apply(allTriple, [index, 0].concat(newPersonTriples))
		allTriple = underscore.compact(allTriple)

		rdfStore.source = allTriple
		rdfStore.save(function(err){
			
			if(err) return cb(err)
			
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err) return cb(err)

				for(var i in conf.list_person){
					var personConf = conf.list_person[i]
					if(personConf.slug == person.slug){
						personConf.displayname = person.displayname
						personConf.firstname = person.firstname
						personConf.lastname = person.lastname
						personConf.ava_path = person.ava_path
					}
				}

				conf.save(function(err){
					if(err) return cb(err)
					return cb(null)
				}) // end confModel.save

			}) // end confModel.findOne

		}) // end rdfStore.save

	}) // end rdfStore.finOne

	
	// 		function checkPerson(cb){
				
	// 			if(emailChange == false){
	// 				// check if this email already exist in conference
	// 				var confDAO = require('./conference_DAO') // don't know why require at first line not work, have to require here
					
	// 				confDAO.hasPerson(oldPerson._conference_acronym, oldPerson.email, function(rslt){

	// 					if(rslt == true){
	// 						cb({'code': 11000})
	// 					}else{
	// 						cb(null)
	// 					}
						
	// 				}) // end confDAO.hasPerson
				
	// 			}else{
	// 				cb(null)
	// 			}
				
	// 		}, // end checkPerson

	// 	], 

	
	
} // end save



personDAO.deleteBySlug = function (personSlug, confAcronym, cb){

	async.waterfall([

		function checkPerson(cb){
			
			// check if this slug already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				for(var i in conf.list_person){
					if( conf.list_person[i].slug == personSlug){
						var personUri = conf.list_person[i].uri
						conf.list_person[i] = null
						return cb(null, conf, personUri)
					}
				}
				// no person matche -> return not found
				return cb({'code': -1})

			}) // end confModel.findOne
		
		}, // end checkPerson

		function saveConf(conf, personUri, cb){

			conf.list_person = underscore.compact(conf.list_person)

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null, personUri)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err, personUri){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

				// delete alltriple
				for( var i in allTriple ){
					if(allTriple[i].subject.nominalValue == personUri){
						allTriple[i] = null
					} // end if
				} // end for

				rdfStore.source = underscore.compact(allTriple)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end savePerson			

	) // end async.waterfall

} // end deleteByAcronym



personDAO.hasConference = function(person, confId){

	var listConferenceId = person._list_id_conference
	
	if( typeof(listConferenceId) === 'undefined' || listConferenceId.length == 0) {
			return false
	}else{
		for(var i in listConferenceId){
			// listConferenceId is object -> convert to string
			if('"'+listConferenceId[i].toString()+'"' == confId){
				return true
			}
		}
		return false
	}

} // end hasConference


personDAO.hasOrganization = function(person, orgaId){

	var listOrgaId = person._list_id_organization
	//console.log(person)
	if( typeof(listOrgaId) === 'undefined' || listOrgaId.length == 0) {
			return false
	}else{
		for(var i in listOrgaId){
			// listOrgaId is object -> convert to string=
			if('"'+listOrgaId[i].toString()+'"' == orgaId){
				return true
			}
		}
		return false
	}

} // end hasOrganzation


personDAO.hasPublication = function(person, pubAcronym){

	var listPub = person.list_publication

	if( typeof(listPub) === 'undefined' || listPub.length == 0) {
			return false
	}else{
		for(var i in listPub){
			if(listPub[i]._publication_acronym == pubAcronym){
				return true
			}
		}
		return false
	}

} // end hasPublication




/**************************/
module.exports = personDAO