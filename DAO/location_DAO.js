var util = require('../helpers/util')
var async = require('async')
var underscore = require('underscore')


var locationDAO = require('../DAO/location_DAO')

var confDAO = require('../DAO/conference_DAO')
var confModel = require('../models/conference')

var rdfStoreModel = require('../models/dataset/rdf_store')
var rdfStoreHelper = require('../helpers/rdf_store_helper')

var locationDAO = {}


locationDAO.persistToConf = function (newLocation, confAcronym, cb) { // creator == session.user

  newLocation.uri = 'http://data.semanticweb.org/location/' + newLocation.name.toLowerCase().replace(/ /g, '-')
  newLocation.slug = util.encodeUri(newLocation.uri)

  async.waterfall([

      function loadConf(cb) {

        // check if this email already exist in conference
        confModel.findOne({'acronym': confAcronym}, function (err, conf) {

          if (err) {
            return cb(err)
          }

          return cb(null, conf)

        }) // end confModel.findOne

      }, // end checkLocation

      function saveConf(conf, cb) {

        conf.list_location.push({
          'slug': newLocation.slug,
          'uri': newLocation.uri,
          'name': newLocation.name
        })

        conf.save(function (err) {

          if (err) {
            return cb(err)
          }

          return cb(null)

        }) // end conf.save

      }, // end save Conf

    ],

    function saveRdfStore(err) {

      if (err) {
        return cb(err)
      }

      rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

        if (err) {
          return cb(err)
        }

        var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
        var locationTriples = rdfStoreHelper.locationModelToTriples(newLocation)

        rdfStore.source = allTriple.concat(locationTriples)
        rdfStore.save(function (err) {

          if (err) {
            return cb(err)
          }

          return cb(null, newLocation.slug)

        }) // end rdfStore.save


      }) // end rdfStoreModel.findOne

    } // end saveLocation

  ) // end async.waterfall


} // end persistToConf


/*** get by Slug  ***/
locationDAO.getBySlug = function (confAcronym, slug, cb) {

  rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

    if (err) {
      return cb(err)
    }

    var location = {}
    location.slug = slug
    location.list_comment = []

    var existed = false

    for (var i in rdfStore.source) {

      var triple = rdfStore.source[i]

      if (triple.subject.nominalValue != null) {
        if (slug == util.encodeUri(triple.subject.nominalValue)) {
          existed = true
          if (typeof(location.uri) == 'undefined') {
            location.uri = triple.subject.nominalValue
          }

          rdfStoreHelper.tripleToLocationModel(location, triple)
        }
      }

    } // end for

    if (!existed) {
      return cb({'code': -1})
    }

    return cb(null, location)

  }) // end findOne

} // end getBySlug


locationDAO.save = function (location, confAcronym, cb) {

  rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

    var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

    var index = 0
    // delete alltriple
    for (var i in allTriple) {
      if (allTriple[i].subject.nominalValue == location.uri) {
        allTriple[i] = null
        index = i
      } // end if

    } // end for

    var newLocationTriples = rdfStoreHelper.locationModelToTriples(location)

    //http://stackoverflow.com/questions/7032550/javascript-insert-an-array-inside-another-array
    allTriple.splice.apply(allTriple, [index, 0].concat(newLocationTriples))
    allTriple = underscore.compact(allTriple)

    rdfStore.source = allTriple
    rdfStore.save(function (err) {

      if (err) return cb(err)

      confModel.findOne({'acronym': confAcronym}, function (err, conf) {

        if (err) return cb(err)

        for (var i in conf.list_location) {
          var locationConf = conf.list_location[i]
          if (locationConf.slug == location.slug) {
            locationConf.name = location.name
          }
        }

        conf.save(function (err) {
          if (err) return cb(err)
          return cb(null)
        }) // end confModel.save

      }) // end confModel.findOne

    }) // end rdfStore.save

  }) // end rdfStore.finOne

} // end save


locationDAO.deleteBySlug = function (locationSlug, confAcronym, cb) {

  async.waterfall([

      function checkLocation(cb) {

        // check if this slug already exist in conference
        confModel.findOne({'acronym': confAcronym}, function (err, conf) {

          if (err) {
            return cb(err)
          }

          for (var i in conf.list_location) {
            if (conf.list_location[i].slug == locationSlug) {
              var locationUri = conf.list_location[i].uri
              conf.list_location[i] = null
              return cb(null, conf, locationUri)
            }
          }
          // no location matche -> return not found
          return cb({'code': -1})

        }) // end confModel.findOne

      }, // end checkLocation

      function saveConf(conf, locationUri, cb) {

        conf.list_location = underscore.compact(conf.list_location)

        conf.save(function (err) {

          if (err) {
            return cb(err)
          }

          return cb(null, locationUri)

        }) // end conf.save

      }, // end save Conf

    ],

    function saveRdfStore(err, locationUri) {

      if (err) {
        return cb(err)
      }

      rdfStoreModel.findOne({'acronym': confAcronym}, function (err, rdfStore) {

        if (err) {
          return cb(err)
        }

        var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

        // delete alltriple
        for (var i in allTriple) {
          if (allTriple[i].subject.nominalValue == locationUri) {
            allTriple[i] = null
          } // end if
        } // end for

        rdfStore.source = underscore.compact(allTriple)
        rdfStore.save(function (err) {

          if (err) {
            return cb(err)
          }

          return cb(null)

        }) // end rdfStore.save


      }) // end rdfStoreModel.findOne

    } // end saveLocation

  ) // end async.waterfall

} // end deleteBySlug


/**************************/
module.exports = locationDAO