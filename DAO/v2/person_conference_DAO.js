const personConferenceModel = require('../../models/v2/personConference');

export default class PersonConferenceDao {
    constructor() {

    }

    /**
     * Permet de créer une personne dans une conférence
     * @param personConference - la personne à créer
     * @returns {Promise}
     */
    add(personConference) {
        return new Promise((resolve, reject) => {
            personConferenceModel.create(personConference, (err, personConference) => {
                if (err) {
                    return reject(err);
                }

                if (personConference === null) {
                    return reject('Error when saving person conference');
                }

                return resolve(personConference);
            });
        });
    }

    /**
     * Permet de récupérer une personne dans une conférence via son id
     * @param idPersonConference - id de la personne
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idPersonConference, idConference) {
        return new Promise((resolve, reject) => {
            personConferenceModel.findOne({
                _id: idPersonConference,
                idConference: idConference
            }, (err, personConference) => {
                if (err) {
                    return reject(err);
                }

                if (personConference === null) {
                    return reject('Error when retreiving person conference');
                }

                return resolve(personConference);
            });
        });
    }

    /**
     * Permet de supprimer une personne d'une conférence
     * @param idPersonConference - Id de la person
     * @param idConference - Id de la conférence
     * @returns {Promise}
     */
    remove(idPersonConference, idConference) {
        return new Promise((resolve, reject) => {
            this.get(idPersonConference, idConference)
                .then((personConference) => {
                    personConference.remove((err) => {
                        if (err) {
                            return reject(err);
                        }

                        return resolve();
                    });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de suppriemr toutes les personnes via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            personConferenceModel.remove({idConference: idConference}, function (err) {
                if (err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer toutes les personnes d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            personConferenceModel.find({idConference: idConference}, (err, personsConference) => {
                if (err) {
                    return reject(err);
                }

                return resolve(personsConference);
            });
        });
    }

    /**
     * Permet de récupérer toutes les personnes d'une conférence via un role
     * @param idConference - id de la conférence
     * @param idRole - id du role
     * @returns {Promise}
     */
    findByRole(idConference, idRole) {
        return new Promise((resolve, reject) => {
            personConferenceModel.find({
                idConference: idConference,
                idRoles: {$in: [idRole]}
            }, (err, personsConference) => {
                if (err) {
                    return reject(err);
                }

                return resolve(personsConference);
            });
        });
    }

    /**
     * Permet de récupérer une page de personne a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de personne a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de personnes a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            personConferenceModel.find({
                idConference: idConference,
                _id: {$gt: idMin},
                displayname: {$regex: searchTerm + '.*'}
            }, {}, {limit: pageSize}, (err, persons) => {
                if (err) {
                    return reject(err);
                }
                return resolve(persons);
            })
        })
    }

    /**
     * Permet de récupérer une page de personne a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de personnes a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        return new Promise((resolve, reject) => {
            personConferenceModel.find({
                idConference: idConference,
                displayname: {$regex: searchTerm + '.*'}
            }, {}, {limit: pageSize}, (err, persons) => {
                if (err) {
                    return reject(err);
                }
                return resolve(persons);
            })
        })
    }

    /**
     * Permet de modifier une personne d'une conférence
     * @param idPersonConference - id de la personne de la conférence
     * @param idConference - id de la conférence
     * @param newPersonConference - nouvel objet de la personne de la conférence
     * @returns {Promise}
     */
    update(idPersonConference, idConference, newPersonConference) {
        return new Promise((resolve, reject) => {
            personConferenceModel.findOneAndUpdate({
                _id: idPersonConference,
                idConference: idConference
            }, newPersonConference, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                if (nbModified < 1) {
                    return reject('Error when updating person conference');
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer plusieurs personnes
     * @param idConference - id de la conférence
     * @param idsPerson - ids des person
     * @returns {Promise}
     */
    mget(idConference, idsPerson) {
        return new Promise((resolve, reject) => {
            personConferenceModel.find({
                _id: {
                    $in: idsPerson
                },
                idConference: idConference,
            }, (err, persons) => {
                if (err) {
                    return reject(err);
                }

                return resolve(persons);
            });
        });
    }

    /**
     * Permet de récupérer plusieurs personnes
     * @param idConference - id de la conférence
     * @param idsPerson - ids des person
     * @returns {Promise}
     */
    mgetLabel(idConference, labels) {
        return new Promise((resolve, reject) => {
            personConferenceModel.find({
                displayname: {
                    $in: labels
                },
                idConference: idConference,
            }, (err, persons) => {
                if (err) {
                    return reject(err);
                }

                return resolve(persons);
            });
        });
    }
}