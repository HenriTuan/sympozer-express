/**
 * Created by pierremarsot on 20/11/2017.
 */
const trackModel = require('../../models/v2/track');

export default class TrackDao {
    constructor() {

    }

    /**
     * Permet de créer une track dans une conférence
     * @param track - la track à créer
     * @returns {Promise}
     */
    add(track) {
        return new Promise((resolve, reject) => {
            trackModel.create(track, (err, track) => {
                if (err) {
                    return reject('Error when saving track');
                }

                if (track === null) {
                    return reject('Error when saving track');
                }

                return resolve(track);
            });
        });
    }

    /**
     * Permet de récupérer une track d'une conférence
     * @param idTrack - id de la track
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idTrack, idConference) {
        return new Promise((resolve, reject) => {
            trackModel.findOne({_id: idTrack, idConference: idConference}, (err, track) => {
                if (err) {
                    return reject(err);
                }

                if (track === null) {
                    return reject('Error when retrieving track');
                }

                return resolve(track);
            });
        });
    }

    /**
     * Permet de récupérer une track via son id
     * @param idTrack - id de la track
     * @returns {Promise<any>}
     */
    getById(idTrack) {
        return new Promise((resolve, reject) => {
            trackModel.findOne({_id: idTrack}, (err, track) => {
                if (err) {
                    return reject(err);
                }

                if (track === null) {
                    return reject('Error when retrieving track');
                }

                return resolve(track);
            });
        });
    }

    /**
     * Permet de supprimer une track d'une conférence
     * @param idTrack - Id de la track
     * @param idConference - Id de la conférence
     * @returns {Promise}
     */
    remove(idTrack, idConference) {
        return new Promise((resolve, reject) => {
            this.get(idTrack, idConference)
                .then((track) => {
                    track.remove((err) => {
                        if (err) {
                            return reject(err);
                        }

                        return resolve();
                    });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de suppriemr toutes les tracks via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            trackModel.remove({idConference: idConference}, function(err){
                if(err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer toutes les tracks d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            trackModel.find({idConference: idConference}, (err, tracks) => {
                if (err) {
                    return reject(err);
                }

                return resolve(tracks);
            });
        });
    }

    /**
     * Permet de récupérer toutes les tracks d'une conférence avec le nom de ses sous-events
     * @param idTrack - id de la track
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    findWithSubEvents(idConference) {
        return new Promise((resolve, reject) => {
            trackModel.find({idConference: idConference})
                .populate('idsSubEvent', ['name'])
                .then((tracks) => {
                    resolve(tracks);
                })
                .catch((err) => {
                    reject(err);
                })
        });
    }

    /**
     * Permet de récupérer toutes les tracks qui sont attachées à un role
     * @param idConference - id de la conférence
     * @param idRole - id du role
     * @returns {Promise}
     */
    getByRole(idConference, idRole) {
        return new Promise((resolve, reject) => {
            trackModel.findOne({idConference: idConference, idRole: idRole}, (err, track) => {
                if (err) {
                    return reject(err);
                }

                return resolve(track);
            });
        });
    }

    /**
     * Permet de modifier une track
     * @param idTrack - id de la tracj
     * @param idConference - id de la conférence
     * @param newTrack - nouvelle données de la track
     * @returns {Promise}
     */
    update(idTrack, idConference, newTrack) {
        return new Promise((resolve, reject) => {
            trackModel.findOneAndUpdate({
                _id: idTrack,
                idConference: idConference
            }, newTrack, (err, nbModified) => {
                if (err) {
                    return reject('Error when updating track');
                }

                if (nbModified < 1) {
                    return reject('Error when updating track');
                }

                return resolve();
            });
        });
    }
}