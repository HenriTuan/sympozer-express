const eventModel = require('../../models/v2/event');
const util = require ('../../helpers/util');

export default class EventDao {
    constructor() {

    }

    /**
     * Permet de créer un event
     * @param event - l'objet de l'event à créer
     * @returns {Promise}
     */
    add(event) {
        return new Promise((resolve, reject) => {
            eventModel.create(event, (err, event) => {
                if (err) {
                    return reject(err);
                }

                return resolve(event);
            });
        });
    }

    /**
     * Permet de suppriemr tous les event via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            eventModel.remove({idConference: idConference}, function(err){
                console.log(err);
                if(err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de supprimer un event via son id
     * @param idEvent - id de l'event à supprimer
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    remove(idEvent, idConference) {
        return new Promise((resolve, reject) => {
            this.get(idEvent, idConference)
                .then((event) => {
                    event.remove((err) => {
                        if (err) {
                            return reject(err);
                        }

                        return resolve();
                    });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de modifier un event
     * @param newEvent - Le nouvel objet de l'event
     * @param idEvent - L'id de l'event à modifier
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    update(newEvent, idEvent, idConference) {
        return new Promise((resolve, reject) => {
            eventModel.findOneAndUpdate({_id: idEvent, idConference: idConference}, newEvent, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                if (nbModified < 1) {
                    return reject('Error when updating event');
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer un event via son id
     * @param idEvent - id de l'event
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idEvent, idConference) {
        return new Promise((resolve, reject) => {
            eventModel.findOne({_id: idEvent, idConference: idConference})
                .populate('track')
                .populate('idsSubEvent')
                .exec()
                .then((event) => {
                    return resolve(event);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer un event via son id
     * @param idEvent - id de l'event
     * @returns {Promise<any>}
     */
    getById(idEvent) {
        return new Promise((resolve, reject) => {
            eventModel.findOne({_id: idEvent}, (err, event) => {
                if (err) {
                    return reject(err);
                }

                if (event === null) {
                    return reject('Error when retreiving event');
                }

                return resolve(event);
            });
        });
    }

    /**
     * Permet de récupérer un event via l'id d'une publication
     * @param idConference - id de la conférence
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    findByIdPublication(idConference, idPublication) {
        return new Promise((resolve, reject) => {
            eventModel.find({idConference: idConference, isEventRelatedTo: {$in: [idPublication]}})
                .populate('track')
                .populate('idsSubEvent')
                .exec()
                .then((event) => {
                    return resolve(event);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer des events via un id de publication
     * @param idPublication - id d'une publication
     * @returns {Promise<any>}
     */
    findByIdPublicationWithoutConference(idPublication) {
        return new Promise((resolve, reject) => {
            eventModel.find({isEventRelatedTo: {$in: [idPublication]}}, (err, events) => {
                if (err) {
                    return reject(err);
                }

                return resolve(events);
            });
        });
    }

  /**
   * Permet de récupérer tous les events d'une conférence
   * @param idConference - id de la conférence
   * @param idSuperEvent - id du super event
   * @returns {Promise}
   */
  find(idConference, idSuperEvent) {
    return new Promise((resolve, reject) => {
      eventModel.find({idConference: idConference, idSuperEvent: idSuperEvent})
        .populate('idTrack')
        .exec()
        .then((events) => {
          return resolve(events);
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

    /**
     * Permet de récupérer une page d'event a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum d'event a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre d'events a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
      const foldedString = util.makeComp(searchTerm);
      return new Promise((resolve,reject) => {
            eventModel.find({
                idConference: idConference,
                _id: {$gt: idMin},
                name: {$regex: foldedString+'.*', $options: 'i'}
            }, {}, {limit:pageSize},(err, events) => {
                if (err) {
                    return reject(err);
                }
                return resolve(events);
            })
        })
    }

    /**
     * Permet de récupérer une page d'event a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre d'events a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
      const foldedString = util.makeComp(searchTerm);
      return new Promise((resolve,reject) => {
            eventModel.find({
                idConference: idConference,
                name: {$regex: foldedString+'.*', $options: 'i'}
            }, {}, {limit:pageSize},(err, events) => {
                if (err) {
                    return reject(err);
                }
                return resolve(events);
            })
        })
    }

    /**
     * Permet d'ajouter un sub event à une conférence
     * @param idConference - id de l'event
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    addSubEvent(idConference, idEvent, idSubEvent) {
        return new Promise((resolve, reject) => {
            eventModel.update({
                _id: idEvent,
                idConference: idConference
            }, {$push: {idsSubEvent: idSubEvent}}, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                return resolve(nbModified === 1);
            })
        });
    }

    /**
     * Permet de supprimer un sub event à un event
     * @param idConference - id de l'event
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    removeSubEvent(idConference, idEvent, idSubEvent) {
        return new Promise((resolve, reject) => {
            eventModel.update({
                _id: idEvent,
                idConference: idConference
            }, {$pull: {idsSubEvent: idSubEvent}}, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                return resolve(nbModified === 1);
            });
        });
    }

    /**
     * Permet d'ajouter une publication dans un event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    addRelatedPublication(idConference, idEvent, idPublication) {
        return new Promise((resolve, reject) => {
            eventModel.update({
                _id: idEvent,
                idConference: idConference
            }, {$push: {isEventRelatedTo: idPublication}}, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

        return resolve(nbModified === 1);
      })
    });
  }
    /**
     * Permet de récupérer tous les events d'une conférence, avec leur sur et sous events
     * @param idConference - id de l'event
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
  findAll(idConference){
      return new Promise((resolve, reject)=> {
          eventModel.find({idConference: idConference})
              .populate({ path: 'idsSubEvent',
                  select: ['name', 'idEventCategory'],
              populate:{path: 'idEventCategory', select: 'name'}}
              )
              .populate('idSuperEvent', ['name'])
              .populate('idTrack', ['name'])
              .populate('idLocation', ['name'])
              .populate('idEventCategory', ['name'])
              .populate('isEventRelatedTo', ['title'])
              .exec()
              .then((events) => {
                  return resolve(events);
              })
              .catch((err) => {
                  return reject(err);
              });
      });
  }



    /**
     * Permet de supprimer une publication d'un event
     * @param idConference - id de la conférence
     * @param idEvent - id de l'event
     * @param idPublication - id de la publication
     * @returns {Promise}
     */
    removeRelatedPublication(idConference, idEvent, idPublication) {
        return new Promise((resolve, reject) => {
            eventModel.update({
                _id: idEvent,
                idConference: idConference
            }, {$pull: {isEventRelatedTo: idPublication}}, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                if(nbModified > 0){
                    return resolve();
                } else {
                    return reject();
                }
            });
        });
    }

    /**
     * Permet de récupérer plusieurs events
     * @param idConference - id de la conférence
     * @param idsEvent - ids des events
     * @returns {Promise}
     */
    mget(idConference, idsEvent) {
        return new Promise((resolve, reject) => {
            eventModel.find({
                _id: {
                    $in: idsEvent
                },
                idConference: idConference,
            }, (err, events) => {
                if (err) {
                    return reject(err);
                }

                return resolve(events);
            });
        });
    }
}