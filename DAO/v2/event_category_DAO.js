const eventCategoryModel = require('../../models/v2/event_categorie');

export default class EventCategoryDao {
  constructor() {

  }

  /**
   * Permet de récupérer tous les event category
   * @returns {Promise}
   */
  find() {
    return new Promise((resolve, reject) => {
      eventCategoryModel.find({}, (err, eventsCategory) => {
        if (err) {
          return reject(err);
        }

        return resolve(eventsCategory);
      });
    });
  }

  /**
   * Permet de récupérer un event category
   * @param idEventCategory - id de l'event category
   * @returns {Promise}
   */
  get(idEventCategory) {
    return new Promise((resolve, reject) => {
      eventCategoryModel.findOne({_id: idEventCategory}, (err, eventCategory) => {
        if (err) {
          return reject(err);
        }

        return resolve(eventCategory);
      });
    });
  }

  /**
   * Permet d'ajouter un event category
   * @param eventCategory - l'objet event category à créer
   * @returns {Promise}
   */
  add(eventCategory) {
    return new Promise((resolve, reject) => {
      eventCategoryModel.create(eventCategory, (err, eventCategory) => {
        if (err) {
          return reject(err);
        }

        return resolve(eventCategory);
      });
    });
  }

  /**
   * Permet de supprimer un event category
   * @param idEventCategory - id de l'event category à supprimer
   * @returns {Promise}
   */
  remove(idEventCategory) {
    return new Promise((resolve, reject) => {
      this.get(idEventCategory)
        .then((eventCategory) => {
          if (!eventCategory) {
            return reject('Error when retrieving event category');
          }

          eventCategory.remove((err) => {
            if (err) {
              return reject(err);
            }

            return resolve();
          });
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

  /**
   * Permet de modifier un event category
   * @param idEventCategory - id de l'event category à modifier
   * @param newEventCategory - nouveau nom de l'event category
   * @returns {Promise}
   */
  update(idEventCategory,
         newEventCategory,) {
    return new Promise((resolve, reject) => {
      eventCategoryModel.findOneAndUpdate({_id: idEventCategory}, newEventCategory, (err, nbModified) => {
        if (err) {
          return reject(err);
        }

        if (nbModified < 1) {
          return reject('Error when updating event category');
        }

        return resolve();
      });
    });
  }
}