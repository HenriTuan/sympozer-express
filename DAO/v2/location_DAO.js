const locationModel = require('../../models/v2/location');
const util = require ('../../helpers/util');

export default class LocationDao {
  constructor() {

  }

  /**
   * Permet de créer une location
   * @param location - la location à créer
   * @returns {Promise}
   */
  add(location) {
    return new Promise((resolve, reject) => {
      locationModel.create(location, (err, location) => {
        if (err) {
          return reject(err);
        }

        if (location === null) {
          return reject('Error when saving location');
        }

        return resolve(location);
      });
    });
  }

  /**
   * Permet de récupérer une location via son id
   * @param idLocation - id de la location
   * @param idConference - id de la conférence
   * @returns {Promise}
   */
  get(idLocation, idConference) {
    return new Promise((resolve, reject) => {
      locationModel.findOne({_id: idLocation, idConference: idConference}, (err, location) => {
        if (err) {
          return reject(err);
        }

        if (location === null) {
          return reject('Error when retreiving location');
        }

        return resolve(location);
      });
    });
  }

    /**
     * Permet de récupérer une location via son nom
     * @param idConference - id de la conférence
     * @param nameLocation - nom de la location
     * @returns {Promise<any>}
     */
  getByName(idConference, nameLocation) {
      return new Promise((resolve, reject) => {
          locationModel.findOne({name: nameLocation, idConference: idConference}, (err, location) => {
              if (err) {
                  return reject(err);
              }

              if (location === null) {
                  return reject('Error when retreiving location');
              }

              return resolve(location);
          });
      });
  }

  /**
   * Permet de supprimer une location via son id
   * @param idLocation - Id de la location
   * @param idConference - id de la conférence
   * @returns {Promise}
   */
  remove(idLocation, idConference) {
    return new Promise((resolve, reject) => {
      this.get(idLocation, idConference)
        .then((location) => {
          location.remove((err) => {
            if (err) {
              return reject(err);
            }

            return resolve();
          });
        })
        .catch((err) => {
          return reject(err);
        });
    });
  }

    /**
     * Permet de suppriemr toutes les locations via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            locationModel.remove({idConference: idConference}, function(err){
                if(err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

  /**
   * Permet de récupérer tous les locations via l'id de la conférence
   * @param idConference - id de la conférence
   * @returns {Promise}
   */
  find(idConference) {
    return new Promise((resolve, reject) => {
      locationModel.find({idConference: idConference}, (err, locations) => {
        if (err) {
          return reject("Eror when get locations");
        }

        return resolve(locations);
      });
    });
  }

    /**
     * Permet de récupérer une page de location a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de location a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de locations a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
      const foldedString = util.makeComp(searchTerm);
      return new Promise((resolve,reject) => {
            locationModel.find({
                idConference: idConference,
                _id: {$gt: idMin},
                name: {$regex: foldedString+'.*', $options: 'i'}
            }, {}, {limit:pageSize},(err, locations) => {
                if (err) {
                    return reject(err);
                }
                return resolve(locations);
            })
        })
    }

    /**
     * Permet de récupérer une page de location a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de locations a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
      const foldedString = util.makeComp(searchTerm);
      return new Promise((resolve,reject) => {
            locationModel.find({
                idConference: idConference,
                name: {$regex: foldedString+'.*', $options: 'i'}
            }, {}, {limit:pageSize},(err, locations) => {
                if (err) {
                    return reject(err);
                }
                return resolve(locations);
            })
        })
    }

  /**
   * Permet de modifier un location
   * @param idLocation - id du location
   * @param idConference - id de la conférence
   * @param newLocation - l'objet location qui contient les nouvelles valeurs
   * @returns {Promise}
   */
  update(idLocation, idConference, newLocation) {
    return new Promise((resolve, reject) => {
      locationModel.findOneAndUpdate({_id: idLocation, idConference: idConference}, newLocation, (err, nbModified) => {
        if(err){
          return reject(err);
        }

        if(nbModified < 1){
          return reject('Error when updating location');
        }

        return resolve();
      });
    });
  }
}