const organizationModel = require('../../models/v2/organisation');
const util = require('../../helpers/util');

export default class OrganizationDao {
    constructor() {

    }

    /**
     * Permet de créer une organisation
     * @param organization - l'organisation à créer
     * @returns {Promise}
     */
    add(organization) {
        return new Promise((resolve, reject) => {
            organizationModel.create(organization, (err, organization) => {
                if (err) {
                    return reject(err);
                }

                if (organization === null) {
                    return reject('Error when saving organization');
                }

                return resolve(organization);
            });
        });
    }

    /**
     * Permet de récupérer une organisation via son id
     * @param idOrganization - id de l'organisation
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idOrganization, idConference) {
        return new Promise((resolve, reject) => {
            organizationModel.findOne({_id: idOrganization, idConference: idConference}, (err, organization) => {
                if (err) {
                    return reject(err);
                }

                if (organization === null) {
                    return reject('Error when retrieving organization');
                }

                return resolve(organization);
            });
        });
    }

    /**
     * Permet de supprimer une organisation via son id
     * @param idOrganization - Id de l'organisation
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    remove(idOrganization, idConference) {
        return new Promise((resolve, reject) => {
            this.get(idOrganization, idConference)
                .then((organization) => {
                    organization.remove((err) => {
                        if (err) {
                            return reject(err);
                        }

                        return resolve();
                    });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de suppriemr toutes les organisations via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            organizationModel.remove({idConference: idConference}, function (err) {
                if (err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer toutes les organisations via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            organizationModel.find({idConference: idConference}, (err, organizations) => {
                if (err) {
                    return reject(err);
                }

                return resolve(organizations);
            });
        });
    }

    /**
     * Permet de récupérer une page d'organisation a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum d'organisation a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre d'organisations a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        const foldedString = util.makeComp(searchTerm);
        return new Promise((resolve, reject) => {
            organizationModel.find({
                idConference: idConference,
                _id: {$gt: idMin},
                name: {$regex: foldedString + '.*', $options: 'i'}
            }, {}, {limit: pageSize}, (err, organizations) => {
                if (err) {
                    return reject(err);
                }
                return resolve(organizations);
            })
        })
    }

    /**
     * Permet de récupérer une page d'organisation a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre d'organisations a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        const foldedString = util.makeComp(searchTerm);
        return new Promise((resolve, reject) => {
            organizationModel.find({
                idConference: idConference,
                name: {$regex: foldedString + '.*', $options: 'i'}
            }, {}, {limit: pageSize}, (err, organizations) => {
                if (err) {
                    return reject(err);
                }
                return resolve(organizations);
            })
        })
    }


    /**
     * Permet de modifier une organisation
     * @param idOrganization - id de l'organisation
     * @param idConference - id de la conférence
     * @param newOrganization - l'objet organisation qui contient les nouvelles valeurs
     * @returns {Promise}
     */
    update(idOrganization, idConference, newOrganization) {
        return new Promise((resolve, reject) => {
            organizationModel.findOneAndUpdate({
                _id: idOrganization,
                idConference: idConference
            }, newOrganization, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                if (nbModified < 1) {
                    return reject('Error when updating organization');
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer les organisations d'une personne
     * @param idConference - l'id de la conférence
     * @param idPerson - l'id de la personne
     * @returns {Promise}
     */
    findByPerson(idConference, idPerson) {
        return new Promise((resolve, reject) => {
            organizationModel.find({
                idConference: idConference,
                idPersons: idPerson
            }, (err, organizations) => {
                if (err)
                    return reject(err);
                return resolve(organizations);
            });
        });
    }
}