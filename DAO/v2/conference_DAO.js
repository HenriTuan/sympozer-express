const confModel = require('../../models/v2/conference');

export default class ConferenceDao {
    constructor() {

    }

    /**
     * Permet de créer une conférence
     * @param conference - l'objet de la conférence à créer
     * @returns {Promise}
     */
    add(conference) {
        return new Promise((resolve, reject) => {
            confModel.create(conference, (err, conference) => {
                if (err) {
                    return reject(err);
                }

                return resolve(conference);
            });
        });
    }

    /**
     * Permet de supprimer une conférence via son id
     * @param idConference - id de la conférence à supprimer
     * @returns {Promise}
     */
    removeById(idConference) {
        return new Promise((resolve, reject) => {
            confModel.findOne({_id: idConference}, (err, conference) => {
                if (err) {
                    return reject(err);
                }

                if (conference === null) {
                    return reject('Error when retrieving conference');
                }

                conference.remove((err) => {
                    if (err) {
                        return reject(err);
                    }

                    return resolve();
                });
            });
        });
    }

    /**
     * Permet de modifier une conférence
     * @param newConference - Le nouvel objet de la conférence
     * @param idConference - L'id de la conférence à modifier
     * @param idUser - id de l'utilisateur qui a créé la conférence
     * @returns {Promise}
     */
    update(newConference, idConference, idUser) {
        return new Promise((resolve, reject) => {

            confModel.findOneAndUpdate({_id: idConference, _user_id: idUser}, newConference, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                if (nbModified < 1) {
                    return reject('Error when updating conference');
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer une conférence via son id
     * @param idConference - l'id de la conférence
     * @returns {Promise}
     */
    get(idConference) {
        return new Promise((resolve, reject) => {
            confModel.findOne({_id: idConference}, (err, conference) => {
                if (err) {
                    return reject(err);
                }

                return resolve(conference);
            });
        });
    }

    /**
     * Permet de récupérer toutes les conférences d'un user
     * @param idUser - id de l'user
     * @returns {Promise}
     */
    getAllByUserAuthor(idUser) {
        return new Promise((resolve, reject) => {
            confModel.find({_user_id: idUser}, (err, conferences) => {
                if (err) {
                    return reject(err);
                }

                return resolve(conferences);
            });
        })
    }

    /**
     * Permet de récupérer toutes les conférences
     * @returns {Promise}
     */
    find() {
        return new Promise((resolve, reject) => {
            confModel.find({}, (err, conferences) => {
                if (err) {
                    return reject(err);
                }

                return resolve(conferences);
            })
        });
    }

    /**
     * Permet d'ajouter un sub event à une conférence
     * @param idConference - id de l'event
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    addSubEvent(idConference, idSubEvent) {
        return new Promise((resolve, reject) => {
            confModel.update({_id: idConference}, {$push: {idsSubEvent: idSubEvent}}, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                return resolve(nbModified === 1);
            })
        });
    }

    /**
     * Permet de supprimer un sub event à une conférence
     * @param idConference - id de l'event
     * @param idSubEvent - id du sub event
     * @returns {Promise}
     */
    removeSubEvent(idConference, idSubEvent) {
        return new Promise((resolve, reject) => {
            confModel.update({_id: idConference}, {$pull: {idsSubEvent: idSubEvent}}, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                return resolve(nbModified === 1);
            })
        });
    }

    /**
     * Permet de trouver une conférence par son acronym
     * @param idUser - Identifiant de l'utilisateur courant
     * @param acronym - acronym de la conférence
     * @returns {Promise<any>}
     */
    getByAcronym(idUser, acronym) {
        return new Promise((resolve, reject) => {
            confModel.findOne({_user_id: idUser, acronym: acronym}, (err, conference) => {
                if (err) {
                    return reject(err);
                }

                return resolve(conference);
            });
        })
    }
}