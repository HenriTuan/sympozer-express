const roleModel = require('../../models/v2/role');
const util = require('../../helpers/util');

export default class RoleDao {
    constructor() {

    }

    /**
     * Permet de créer un rôle dans une conférence
     * @param role - le rôle à créer
     * @returns {Promise}
     */
    add(role) {
        return new Promise((resolve, reject) => {
            roleModel.create(role, (err, role) => {
                if (err) {
                    return reject(err);
                }

                if (role === null) {
                    return reject('Error when saving publication');
                }

                return resolve(role);
            });
        });
    }

    /**
     * Permet de récupérer un rôle d'une conférence
     * @param idRole - id du rôle
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idRole, idConference) {
        return new Promise((resolve, reject) => {
            roleModel.findOne({_id: idRole, idConference: idConference}, (err, role) => {
                if (err) {
                    return reject(err);
                }

                if (role === null) {
                    return reject('Error when retrieving role');
                }

                return resolve(role);
            });
        });
    }

    /**
     * Permet de récupérer plusieurs rôles
     * @param idConference - id de la conférence
     * @param idsRole - ids des rôles
     * @returns {Promise<any>}
     */
    mget(idConference, idsRole) {
        return new Promise((resolve, reject) => {
            roleModel.find({
                _id: {
                    $in: idsRole
                },
                idConference: idConference,
            }, (err, roles) => {
                if (err) {
                    return reject(err);
                }

                return resolve(roles);
            });
        });
    }

    /**
     * Permet de récupérer un role via son name
     * @param idConference - id de la conférence
     * @param roleName - nom du role
     * @returns {Promise<any>}
     */
    getByName(idConference, roleName) {
        return new Promise((resolve, reject) => {
            roleModel.findOne({idConference: idConference, name: roleName}, (err, role) => {
                if (err) {
                    return reject(err);
                }

                return resolve(role);
            });
        });
    }

    /**
     * Permet de supprimer un rôle d'une conférence
     * @param idRole - Id du rôle
     * @param idConference - Id de la conférence
     * @returns {Promise}
     */
    remove(idRole, idConference) {
        return new Promise((resolve, reject) => {
            this.get(idRole, idConference)
                .then((role) => {
                    role.remove((err) => {
                        if (err) {
                            return reject(err);
                        }

                        return resolve();
                    });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de suppriemr tous les roles via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            roleModel.remove({idConference: idConference}, function(err){
                if(err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer tous les rôles d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            roleModel.find({idConference: idConference}, (err, roles) => {
                if (err) {
                    return reject(err);
                }

                return resolve(roles);
            });
        });
    }

    /**
     * Permet de récupérer une page de role a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de role a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de roles a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        const foldedString = util.makeComp(searchTerm);
        return new Promise((resolve, reject) => {
            roleModel.find({
                idConference: idConference,
                _id: {$gt: idMin},
                name: {$regex: foldedString + '.*', $options: 'i'}
            }, {}, {limit: pageSize}, (err, roles) => {
                if (err) {
                    return reject(err);
                }
                return resolve(roles);
            })
        })
    }

    /**
     * Permet de récupérer une page de role a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de roles a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        const foldedString = util.makeComp(searchTerm);
        return new Promise((resolve, reject) => {
            roleModel.find({
                idConference: idConference,
                name: {$regex: foldedString + '.*', $options: 'i'}
            }, {}, {limit: pageSize}, (err, roles) => {
                if (err) {
                    return reject(err);
                }
                return resolve(roles);
            })
        })
    }

    /**
     * Permet de modifier un rôle d'une conférence
     * @param idRole - id du rôle
     * @param idConference - id de la conférence
     * @param newRole - nouvel objet du rôle de la conférence
     * @returns {Promise}
     */
    update(idRole, idConference, newRole) {
        return new Promise((resolve, reject) => {
            roleModel.findOneAndUpdate({
                _id: idRole,
                idConference: idConference
            }, newRole, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                if (nbModified < 1) {
                    return reject('Error when updating role');
                }

                return resolve();
            });
        });
    }
}