const publicationModel = require('../../models/v2/publication');
const util = require ('../../helpers/util');

export default class PublicationDao {
    constructor() {

    }

    /**
     * Permet de créer une publication dans une conférence
     * @param publication - la publication à créer
     * @returns {Promise}
     */
    add(publication) {
        return new Promise((resolve, reject) => {
            publicationModel.create(publication, (err, publication) => {
                if (err) {
                    return reject(err);
                }

                if (publication === null) {
                    return reject('Error when saving publication');
                }

                return resolve(publication);
            });
        });
    }

    /**
     * Permet de récupérer une publication d'une conférence
     * @param idPublication - id de la publication
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    get(idPublication, idConference) {
        return new Promise((resolve, reject) => {
            publicationModel.findOne({_id: idPublication, idConference: idConference}, (err, publication) => {
                if (err) {
                    return reject(err);
                }

                if (publication === null) {
                    return reject('Error when retreiving publication');
                }

                return resolve(publication);
            });
        });
    }

    /**
     * Permet de retrouver une publication via son URI
     * @param uriPublication - URI de la publication
     * @returns {Promise<any>}
     */
    getByUri(uriPublication) {
        return new Promise((resolve, reject) => {
            publicationModel.findOne({uri: uriPublication}, (err, publication) => {
                if (err) {
                    return reject(err);
                }

                if (publication === null) {
                    return reject('Error when retreiving publication');
                }

                return resolve(publication);
            });
        });
    }

    /**
     * Permet de supprimer une publication d'une conférence
     * @param idPublication - Id de la publication
     * @param idConference - Id de la conférence
     * @returns {Promise}
     */
    remove(idPublication, idConference) {
        return new Promise((resolve, reject) => {
            this.get(idPublication, idConference)
                .then((publication) => {
                    publication.remove((err) => {
                        if (err) {
                            return reject(err);
                        }

                        return resolve();
                    });
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de suppriemr toutes les publications via l'id de la conférence
     * @param idConference - id de la conférence
     * @returns {Promise<any>}
     */
    removeByConference(idConference) {
        return new Promise((resolve, reject) => {
            publicationModel.remove({idConference: idConference}, function(err){
                if(err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    }

    /**
     * Permet de récupérer toutes les publications d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    find(idConference) {
        return new Promise((resolve, reject) => {
            publicationModel.find({idConference: idConference}, (err, publications) => {
                if (err) {
                    return reject(err);
                }

                return resolve(publications);
            });
        });
    }

    /**
     * Permet de récupérer toutes les publications d'une conférence
     * @param idConference - id de la conférence
     * @returns {Promise}
     */
    findWithEvent(idConference) {
        return new Promise((resolve, reject) => {
            publicationModel.find({idConference: idConference})
                .populate({path : 'idEvent', select: ['name', 'idEventCategory'],
                    populate:{path: 'idEventCategory', select: 'name'}})
                .exec()
                .then((publications) => {
                    return resolve(publications);
                })
                .catch((err) => {
                    return reject(err);
                });
        });
    }

    /**
     * Permet de récupérer une page de publication a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param idMin - l'id minimum de publication a partir duquel on fait la recherche
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de publications a retourner
     * @returns {Promise}
     */
    findPage(idConference, idMin, searchTerm, pageSize) {
        const foldedString = util.makeComp(searchTerm);
        return new Promise((resolve,reject) => {
            publicationModel.find({
                idConference: idConference,
                _id: {$gt: idMin},
                title: {$regex: foldedString+'.*', $options: 'i'}
            }, {}, {limit:pageSize},(err, publications) => {
                if (err) {
                    return reject(err);
                }
                return resolve(publications);
            })
        })
    }

    /**
     * Permet de récupérer une page de publication a partir d'un id de conférence, d'un id min et d'une taille de page
     * @param idConference - id de la conférence
     * @param searchTerm - la string que le nom doit contenir
     * @param pageSize - le nombre de publications a retourner
     * @returns {Promise}
     */
    findWithTerm(idConference, searchTerm, pageSize) {
        const foldedString = util.makeComp(searchTerm);
        return new Promise((resolve,reject) => {
            publicationModel.find({
                idConference: idConference,
                title: {$regex: foldedString+'.*', $options: 'i'}
            }, {}, {limit:pageSize},(err, publications) => {
                if (err) {
                    return reject(err);
                }
                return resolve(publications);
            })
        })
    }

    /**
     * Permet de modifier une publication d'une conférence
     * @param idPublication - id de la publication de la conférence
     * @param idConference - id de la conférence
     * @param newPublication - nouvel objet de la publication de la conférence
     * @returns {Promise}
     */
    update(idPublication, idConference, newPublication) {
        return new Promise((resolve, reject) => {
            publicationModel.findOneAndUpdate({
                _id: idPublication,
                idConference: idConference
            }, newPublication, (err, nbModified) => {
                if (err) {
                    return reject(err);
                }

                if (nbModified < 1) {
                    return reject('Error when updating publication');
                }

                return resolve();
            });
        });
    }

    getAllFromAuthor(idConference, idAuthor) {
        return new Promise((resolve, reject) => {
            publicationModel.find({
                idConference: idConference,
                idAuthors: idAuthor
            }, (err, publications) => {
                if (err) {
                    return reject(err);
                }

                return resolve(publications);
            })
        });
    }
}