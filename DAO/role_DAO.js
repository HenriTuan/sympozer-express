var rdfStoreHelper = require('../helpers/rdf_store_helper')
var rdfStoreModel = require('../models/dataset/rdf_store')

var confDAO = require('./conference_DAO')
var confHelper = require('../helpers/conference_helper')
var confModel = require('../models/conference')

var util = require('../helpers/util')
var async = require('async')
var underscore = require('underscore')


var roleDAO = {}



roleDAO.persistToConf = function (newRole, confAcronym, cb){ // creator == session.user

	newRole.uri = 'http://data.semanticweb.org/role/'+newRole.name.toLowerCase().replace(/ /g, '-')
	newRole.slug = util.encodeUri(newRole.uri)

	async.waterfall([

		function loadConf(cb){
			
			// check if this email already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				return cb(null, conf)

			}) // end confModel.findOne
		
		}, // end checkRole

		function saveConf(conf, cb){

			conf.list_role.push({
				'slug': newRole.slug,
		        'uri': newRole.uri,
		        'name': newRole.name
			})

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
				var roleTriples = rdfStoreHelper.roleModelToTriples(newRole)

				rdfStore.source = allTriple.concat(roleTriples)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null, newRole.slug)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end saveRole			

	) // end async.waterfall

	
} // end persistToConf




roleDAO.getAllByConference = function(confAcronym, cb){

	roleModel.find(function(err, listRole){

		if(err){
			console.log(err)
			return cb(err)
		}else{
			var rslt = []
			for( var i in listRole ){
				if(listRole[i]._conference_acronym === confAcronym)
					rslt.push(listRole[i])
			} // end for
		
			return cb(null, rslt)
		}

	}) // end roleModel.find

} // end getAllByConference





roleDAO.getBySlug = function(confAcronym, slug, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err);
		}

		var role = {};
		role.slug = slug;
		role.listHolder = [];

		var existed = false;

		for(var i in rdfStore.source){

			var triple = rdfStore.source[i]
			if( slug == util.encodeUri(triple.subject.nominalValue) ){
				existed = true;
				if(typeof(role.uri) == 'undefined'){
					role.uri = triple.subject.nominalValue;
				}

				rdfStoreHelper.tripleToRoleModel(role, triple);
			}

		} // end for

		if(!existed){
			return cb({'code': -1});
		}

		var listHolderTriple = rdfStore.source.filter(function(triple){
			return triple.subject.nominalValue === role.uri &&
				triple.predicate.nominalValue === 'http://data.semanticweb.org/ns/swc/ontology#heldBy';
		});

		for(var i in listHolderTriple){
				
			var holderTriples = rdfStore.source.filter(function(triple){
				return triple.subject.nominalValue === listHolderTriple[i].object.nominalValue
			});

			var uri = listHolderTriple[i].object.nominalValue;
			var roleHolderTriple = listHolderTriple[i];
			var roleHolder = {
				'slug': util.encodeUri(uri),
				'uri' : uri
			}

			for(var j in holderTriples){
				rdfStoreHelper.tripleToSimplePersonModel(roleHolder, holderTriples[j]);				
			}
			
			if( typeof(roleHolder.ava_path) === 'undefined' ){
				roleHolder.ava_path = '/img/sympozer_logo.png'
			}

			role.listHolder.push(roleHolder);
		}
		return cb(null, role);

	}); // end findOne

}; // end getBySlug





roleDAO.save = function(role, confAcronym, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){
		
		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

		var index = 0
		// delete alltriple
		for( var i in allTriple ){
			if(allTriple[i].subject.nominalValue == role.uri){
				allTriple[i] = null
				index = i
			} // end if
			
		} // end for

		var newRoleTriples = rdfStoreHelper.roleModelToTriples(role)

		//http://stackoverflow.com/questions/7032550/javascript-insert-an-array-inside-another-array
		allTriple.splice.apply(allTriple, [index, 0].concat(newRoleTriples))
		allTriple = underscore.compact(allTriple)

		rdfStore.source = allTriple
		rdfStore.save(function(err){
			
			if(err) return cb(err)
			
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err) return cb(err)

				for(var i in conf.list_role){
					var roleConf = conf.list_role[i]
					if(roleConf.slug == role.slug){
						roleConf.name = role.name
					}
				}

				conf.save(function(err){
					if(err) return cb(err)
					return cb(null)
				}) // end confModel.save

			}) // end confModel.findOne

		}) // end rdfStore.save

	}) // end rdfStore.finOne
	
} // end save




roleDAO.deleteBySlug = function (roleSlug, confAcronym, cb){

	async.waterfall([

		function checkRole(cb){
			
			// check if this slug already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				for(var i in conf.list_role){
					if( conf.list_role[i].slug == roleSlug){
						var roleUri = conf.list_role[i].uri
						conf.list_role[i] = null
						return cb(null, conf, roleUri)
					}
				}
				// no role matche -> return not found
				return cb({'code': -1})

			}) // end confModel.findOne
		
		}, // end checkRole

		function saveConf(conf, roleUri, cb){

			conf.list_role = underscore.compact(conf.list_role)

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null, roleUri)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err, roleUri){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

				// delete alltriple
				for( var i in allTriple ){
					if(allTriple[i].subject.nominalValue == roleUri){
						allTriple[i] = null
					} // end if
				} // end for

				rdfStore.source = underscore.compact(allTriple)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end saveRole			

	) // end async.waterfall

} // end deleteBySlug




/**************************/
module.exports = roleDAO