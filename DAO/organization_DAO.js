var util = require('../helpers/util')
var async = require('async')
var underscore = require('underscore')

var orgaHelper = require('../helpers/organization_helper')

var personDAO = require('../DAO/person_DAO')

var confDAO = require('../DAO/conference_DAO')
var confModel = require('../models/conference')

var rdfStoreModel = require('../models/dataset/rdf_store')
var rdfStoreHelper = require('../helpers/rdf_store_helper')

var orgaDAO = {}



orgaDAO.persistToConf = function (newOrga, confAcronym, cb){ // creator == session.user

	newOrga.uri = 'http://data.semanticweb.org/orga/'+newOrga.name.toLowerCase().replace(/ /g, '-')
	newOrga.slug = util.encodeUri(newOrga.uri)

	async.waterfall([

		function loadConf(cb){
			
			// check if this email already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				return cb(null, conf)

			}) // end confModel.findOne

		}, // end checkOrga

		function saveConf(conf, cb){

			conf.list_organization.push({
				'slug': newOrga.slug,
		        'uri': newOrga.uri,
		        'name': newOrga.name,
		        'logo_path': newOrga.logo_path
			})

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
				var orgaTriples = rdfStoreHelper.organizationModelToTriples(newOrga)

				rdfStore.source = allTriple.concat(orgaTriples)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null, newOrga.slug)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end saveOrga			

	) // end async.waterfall

	
} // end persistToConf


orgaDAO.getAll = function(confAcronym, listOrga, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err)
		}

		var result = [];

		for(var k in listOrga){

			var orga = {};
			orga.slug = listOrga[k].slug;
			orga.list_member = [];

			var existed = false

			for(var i in rdfStore.source){

				var triple = rdfStore.source[i]
				if(triple.subject.nominalValue != null){
					if( orga.slug == util.encodeUri(triple.subject.nominalValue) ){
						existed = true
						if(typeof(orga.uri) == 'undefined'){
							orga.uri = triple.subject.nominalValue
						}

						rdfStoreHelper.tripleToOrganizationModel(orga, triple)
					}
				}

			} // end for

			if(!existed){
				return cb({'code': -1})
			}

			if( typeof(orga.logo_path) == 'undefined' ){
				orga.logo_path = '/img/sympozer_logo.png' // default avatar
			}

			// get list member Uri
			var listMemberUri = []
			for(var i in rdfStore.source){

				var triple = rdfStore.source[i]
				if(triple.subject.nominalValue == orga.uri && triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/member'){
					listMemberUri.push(triple.object.nominalValue)
				}

			}
			orga.listMemberUri = listMemberUri;
			result.push(orga);
		}

		return cb(null, result)

	}) // end findOne

}; // emd getAllOrga


orgaDAO.getBySlug = function(confAcronym, slug, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err)
		}

		var orga = {}
		orga.slug = slug
		orga.list_member = []

		var existed = false

		for(var i in rdfStore.source){

			var triple = rdfStore.source[i]
			if(triple.subject.nominalValue != null){
				if( slug == util.encodeUri(triple.subject.nominalValue) ){
					existed = true
					if(typeof(orga.uri) == 'undefined'){
						orga.uri = triple.subject.nominalValue
					}

					rdfStoreHelper.tripleToOrganizationModel(orga, triple)
				}
			}

		} // end for

		if(!existed){
			return cb({'code': -1})
		}

		if( typeof(orga.logo_path) == 'undefined' ){
			orga.logo_path = '/img/sympozer_logo.png' // default avatar
		}

		// get list member Uri
		var listMemberUri = []
		for(var i in rdfStore.source){

			var triple = rdfStore.source[i]
			if(triple.subject.nominalValue == orga.uri && triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/member'){
				listMemberUri.push(triple.object.nominalValue)
			}

		}

		// get list member
		var listMember = []
		for(var i in listMemberUri){
			
			var member = {
				'slug': util.encodeUri(listMemberUri[i]),
				'uri' : listMemberUri[i]
			}

			for(var j in rdfStore.source){
				
				var triple = rdfStore.source[j]
				if(triple.subject.nominalValue == listMemberUri[i]){
					rdfStoreHelper.tripleToSimplePersonModel(member, triple)
				}

			}
			if( typeof(member.ava_path) === 'undefined' ){
				member.ava_path = '/img/sympozer_logo.png'
			}
			listMember.push(member)

		}

		return cb(null, orga, listMember)

	}) // end findOne

} // end getBySlug



orgaDAO.save = function(orga, confAcronym, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){
		
		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

		var index = 0
		// delete alltriple
		for( var i in allTriple ){
			if(allTriple[i].subject.nominalValue == orga.uri){
				allTriple[i] = null
				index = i
			} // end if
			
		} // end for

		var newOrgaTriples = rdfStoreHelper.organizationModelToTriples(orga)

		//http://stackoverflow.com/questions/7032550/javascript-insert-an-array-inside-another-array
		allTriple.splice.apply(allTriple, [index, 0].concat(newOrgaTriples))
		allTriple = underscore.compact(allTriple)

		rdfStore.source = allTriple
		rdfStore.save(function(err){
			
			if(err) return cb(err)
			
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err) return cb(err)

				for(var i in conf.list_organization){
					var orgaConf = conf.list_organization[i]
					if(orgaConf.slug == orga.slug){
						orgaConf.name = orga.name
						orgaConf.logo_path = orga.logo_path
						orgaConf.homepage = orga.homepage
						orgaConf.description = orga.description
					}
				}

				conf.save(function(err){
					if(err) return cb(err)
					return cb(null)
				}) // end confModel.save

			}) // end confModel.findOne

		}) // end rdfStore.save

	}) // end rdfStore.finOne
	
} // end save



orgaDAO.deleteBySlug = function (orgaSlug, confAcronym, cb){

	async.waterfall([

		function checkOrga(cb){
			
			// check if this slug already exist in conference
			confModel.findOne({'acronym': confAcronym}, function(err, conf){

				if(err){
					return cb(err)
				}

				for(var i in conf.list_organization){
					if( conf.list_organization[i].slug == orgaSlug){
						var orgaUri = conf.list_organization[i].uri
						conf.list_organization[i] = null
						return cb(null, conf, orgaUri)
					}
				}
				// no orga matche -> return not found
				return cb({'code': -1})

			}) // end confModel.findOne
		
		}, // end checkOrga

		function saveConf(conf, orgaUri, cb){

			conf.list_organization = underscore.compact(conf.list_organization)

			conf.save(function(err){

				if(err){
					return cb(err)
				}

				return cb(null, orgaUri)

			}) // end conf.save

		}, // end save Conf

	], 

		function saveRdfStore(err, orgaUri){

			if(err){
				return cb(err)
			}

			rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

				if(err){
					return cb(err)
				}

				var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

				// delete alltriple
				for( var i in allTriple ){
					
					if(	allTriple[i].subject.nominalValue == orgaUri
					    ||(allTriple[i].object.nominalValue == orgaUri 
					  		&& allTriple[i].predicate.nominalValue == 'http://swrc.ontoware.org/ontology#affiliation' )
					   ){
						allTriple[i] = null
					} // end if

				} // end for

				rdfStore.source = underscore.compact(allTriple)
				rdfStore.save(function(err){

					if(err){
						return cb(err)
					}

					return cb(null)

				}) // end rdfStore.save


			}) // end rdfStoreModel.findOne

		} // end saveOrga			

	) // end async.waterfall

} // end deleteBySlug


orgaDAO.addListMember = function(orgaUri, confAcronym, listPersonUri, cb){

	
	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err)
		}

		// check if user is member or not
		for( var i in rdfStore.source){

			var triple = rdfStore.source[i]
			if(triple.subject.nominalValue == orgaUri && triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/member'){
				for(var j in listPersonUri){
					if(listPersonUri[j] == triple.object.nominalValue){
						// this person is already a member -> remove from list
						listPersonUri.splice(j, 1);
					}
				}
			}

		}
		// end check

		// add new member triple to rdfStore
		var newTriples = []
		for(var i in listPersonUri){
			newTriples.push( rdfStoreHelper.createMemberTriple(orgaUri, listPersonUri[i]) )
			newTriples.push( rdfStoreHelper.createAffiliationTriple(listPersonUri[i], orgaUri) )
		}
		// end add new member

		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))
		rdfStore.source = allTriple.concat(newTriples)
	
		rdfStore.save(function(err){
			if(err){
				return cb(err)
			}
			return cb(null)
		})

	}) // end findOne
	
} // end addListMember



orgaDAO.removeMember = function(orgaUri, personUri, confAcronym, cb){

	rdfStoreModel.findOne({'acronym': confAcronym}, function(err, rdfStore){

		if(err){
			return cb(err)
		}

		var allTriple = JSON.parse(JSON.stringify(rdfStore.source))

		// check if user is member or not
		for( var i in allTriple){

			var triple = allTriple[i]
			if(	  ( triple.subject.nominalValue == orgaUri 
					&& triple.predicate.nominalValue == 'http://xmlns.com/foaf/0.1/member'
					&& triple.object.nominalValue == personUri )
			  	|| (
			  		triple.subject.nominalValue == personUri 
					&& triple.predicate.nominalValue == 'http://swrc.ontoware.org/ontology#affiliation' 
					&& triple.object.nominalValue == orgaUri
			  		)
			
			   	){
					// remove triple
					allTriple.splice(i, 1);	
			}

		}
		// end check

		
		rdfStore.source = allTriple
	
		rdfStore.save(function(err){
			if(err){
				return cb(err)
			}
			return cb(null)
		})

	}) // end findOne
	
} // end removeMember




/**************************/
module.exports = orgaDAO