/**
 * Created by Mahmoud on 01/04/2017.
 */
var util = require('../helpers/util')
var async = require('async')
var underscore = require('underscore')

var confDAO = require('../DAO/conference_DAO')
var confModel = require('../models/conference')
var eventCatModel = require('../models/category/event_category')

var rdfStoreModel = require('../models/dataset/rdf_store')
var rdfStoreHelper = require('../helpers/rdf_store_helper')


var eventCategoryDAO = {}

eventCategoryDAO.persist = function (newEventCat, cb) {

    eventCatModel.create(newEventCat, function (err, eventCat) {
        if (err) cb(err)
        else {
            cb(null, eventCat)
        }
    })

}

eventCategoryDAO.save = function (oldEventCatName,newEventCatName , cb) {

    eventCatModel.findOne({'name': oldEventCatName}, function (err, eventCat) {

        if (eventCat == null) cb(-1) // fail
        else {
            console.log('old name : ' + eventCat.name)
            eventCat.name = newEventCatName
            console.log('new name : ' + eventCat.name)
            eventCat.save(function(err,newEventCat){
                if(err)  cb(err)
                else cb(null, newEventCat)
            })



        }

    }) //end eventCat findOne
}

eventCategoryDAO.delete = function () {


}

eventCategoryDAO.getByName = function (eventCatName, cb) {
    eventCatModel.findOne({'name': eventCatName}, function (err, eventCat) {

        if (eventCat == null) cb(-1) // fail
        else {

            cb(null, eventCat)

        }

    }) //end eventCat findOne
} // end getByName

eventCategoryDAO.deleteByName = function (eventCatName, cb) {
    eventCatModel.findOne({'name': eventCatName}, function (err, eventCat) {

        if (eventCat == null) cb(-1) // fail
        else {
            eventCat.remove(function (err) {
                if (err) {
                    console.log(err)
                    cb(err)
                }
                else {
                    cb(null)
                }

            }) // end remove event category
        }

    }) // end findOne eventCatModel
}

eventCategoryDAO.getAll = function (cb) {
    eventCatModel.find({}, function (err, listEventCat) {
        cb(err, listEventCat)
    }) // end find
}

module.exports = eventCategoryDAO