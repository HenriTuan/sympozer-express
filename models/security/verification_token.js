var db = require('../../config/db')
var uuid = require('node-uuid')
var mongoose = require('mongoose')
var userModel = require('../user')

// ---- predefine entity  ----
var Schema = mongoose.Schema
var verificationToken = new Schema({

    _userId: {
    	type 	: Schema.Types.ObjectId, 
    	required: true, 
    	ref 	: 'user'
    },

    token: {
    	type 	: String, 
    	required: true
    },

    createdAt: {
    	type 	: Date, 
    	required: true, 
    	default : Date.now, 
    	expires : '4h'
    }

})

var verificationTokenModel = mongoose.model('VerificationToken', verificationToken)

verificationToken.methods.createVerificationToken = function (done) {
    var verificationToken = this
    var token = uuid.v4()
    verificationToken.set('token', token)
    verificationToken.save( function (err) {
        if (err) return done(err)
        return done(null, token)
    })
}
 
verificationToken.methods.verifyUser = function(token, done) {
    var verificationToken = this
    
    verificationTokenModel.findOne({token: token}, function (err, doc){
        if (err) return done(err)
        userModel.findOne({_id: doc._userId}, function (err, user) {
            if (err) return done(err)
            user["is_verified"] = true
            user.save(function(err) {
                done(err)
            })
        })
    }) // end .findOne
} // end methods verifyUser

module.exports = mongoose.model('verificationToken', verificationToken)