var db = require('../config/db')

var mongoose = require('mongoose')
var Schema = mongoose.Schema
var passportLocalMongoose = require('passport-local-mongoose')

// ---- predefine entity  ----
var User = new Schema({
  
    /*
    username:   {
      type: String,
      unique: true
    },*/
    username: String,
    /*
    email: {
      type: String,
      unique: true
    },*/
    email: String,

    sha: {
        require: true,
        type: String
    },

    acronym: String,

    urlName:{
      type: String,
      unquie: true,
      require: false
    },

    password:   String,
    displayname:String,
    firstname:  String,
    lastname:   String,
    homepage:   String,
    more_info:  String,

    _list_id_role:   [{
              type: Schema.Types.ObjectId,
              ref: 'role'
    }],

    _photo_id:  {
              type: Schema.Types.ObjectId,
              ref: 'photo'
    },

    _list_id_organisation:[{
              type: Schema.Types.ObjectId,
              ref: 'Organisation'
    }],

    facebook: {
        _id:         String,
        displayname: String,
        token:       String,
        email:       String
    },

    google: {
        _id:         String,
        displayname: String,
        token:       String,
        email:       String
    },

    twitter: {
        _id:        String,
        displayname:String,
        token:      String,
        email:      String 
    },

    linkedin: {
        _id:        String,
        displayname:String,
        token:      String,
        email:      String
    },

    is_verified: {
        type: Boolean, 
        default: false
    }

});
// --- end predfine entity

// config passport for authentication
User.plugin(passportLocalMongoose)
//User.plugin(passportEmail)
var userModel = mongoose.model('User', User)


// -------- methods

// get all user
User.methods.getAll = function(cb){
    var listUser = []
    userModel.find({}, function(err, docs){
        if(err) throw err
        docs.forEach(function(elem, index, array){
          listUser.push({'displayname' : elem.displayname,
                         'email' : elem.email,
                         'homepage': elem.homepage
                        })
        }) // end docs.forEach   
    cb(listUser)
    }) // end userModel.find
} // end getAll


// get user by username
User.getByName = function(username, cb) {
  
  var user = db.conn.model.findOne({username:username}, function(err, doc){
     console.log(doc)
     cb(err, doc)
  })


}

// --------- end methods

module.exports = mongoose.model('User', User)


