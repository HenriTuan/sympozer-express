var db = require('../../config/db')
var eventSchema = require('../event')

var mongoose = require('mongoose')
var extend = require('mongoose-schema-extend')


// ---- predefine entity  ----
var Session = eventSchema.extend({

	_id_publication_category: {
		type: Schema.Types.ObjectId,
		ref: 'publication_category'
	},
	/*
	_id_location: {
		type: Schema.Types.ObjectId,
		ref: 'place'
	}*/

	time_begin: {
        type     : Date, 
        required : true, 
        default  : Date.now
    },

    time_end:{
    	type 	: Date,
    	required : true
    }
	
})
// end predefine entity


module.exports = mongoose.model('Session', Session)