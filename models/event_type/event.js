var db = require('../../config/db')

var mongoose = require('mongoose')
var extend = require('mongoose-schema-extend')

var Schema = mongoose.Schema

// ---- predefine entity  ----
var Event = new Schema({

    name: String,

    slug: String,
    /*
    acronym: {// https://www.npmjs.com/package/mongoose-slug-generator
        type: String,
        slug: "default_name",
        unique: true
    },*/

    _track: {
        type: Schema.Types.ObjectId,
        ref: 'EventCategory'
    },
    summary: String,

    uri: { // https://www.example.org
        type: String,
        unique: false,
    },

    conference: {
        type: Schema.Types.ObjectId,
        ref: 'Conference'
    },

    location: {
        location_uri: String,
        _location_id: {
            type: Schema.Types.ObjectId,
            ref: 'location'
        }
    },

    date_end: String,
    date_start: String,

    _list_sub_event: [{
        type: Schema.Types.ObjectId,
        ref: 'Event'
    }],

    _super_event:{
        type: Schema.Types.ObjectId,
        ref: 'Event'
    },

    // user who owns the event
    _user_id: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },

})
// end predefine entity


module.exports = mongoose.model('Event2', Event)