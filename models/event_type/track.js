var db = require('../../config/db')
var eventSchema = require('../event')

var mongoose = require('mongoose')
var extend = require('mongoose-schema-extend')


// ---- predefine entity  ----
var Track = eventSchema.extend({

	_id_publication_category: {
		type: Schema.Types.ObjectId,
		ref: 'publication_category'
	},
	
	_list_id_session: [{
		type: Schema.Types.ObjectId,
		ref: 'session'
	}]
	
})
// end predefine entity


module.exports = mongoose.model('Track', Track)