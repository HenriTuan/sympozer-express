var db = require('../config/db')
var slug = require('mongoose-slug-generator')

var mongoose = require('mongoose')
var extend = require('mongoose-schema-extend')


mongoose.plugin(slug)
var Schema = mongoose.Schema


// ---- predefine entity  ----
var Conference = new Schema({

    title:         String,
    
    display_acronym: {
            type    : String,
            unique  : false,
            required: true,
    },

    acronym:{ // https://www.npmjs.com/package/mongoose-slug-generator
            type    : String,
            slug    : "display_acronym",
            unique  : true
    },

    uri:{ // https://www.example.org
            type    : String,
            unique  : false,
    }, 

    description:   String,
    homepage: String,

    logo_path:{
        type: String,
        default: '/img/sympozer_logo.png',
        required: false
    },

    date_begin: {
    		type     : String, 
            required: false, 
            // default : Date.now 
    },

    date_end: {
            type     : String, 
            required: false,
    },

    timezone: Number,
    language: {
            code: String,
            name: String,
    },

    location:{
            formatted_address: {
                type: String,
                default: '',
            },
            country: {
                type: String,
                default: null,
            },
            region: {
                type: String,
                default: null,
            },
            city: {
                type: String,
                default: null,
            },
            street_name: {
                type: String,
                default: null,
            },
            street_number: {
                type: Number,
                default: null,
            },

            long: Number,
            lat: Number,
            zipcode: Number,
    },


    list_location:[{
        slug: String,
        uri: String,
        name: String,
    }],

    list_person:[{
        slug: String,
        uri: String,
        displayname: String,
        firstname: String,
        lastname: String,
        ava_path: String,
        sha: String
    }],

    list_organization: [{
        slug: String,
        uri: String,
        name: String,
        logo_path: String,
        homepage: String,
        description: String
    }],

    list_publication: [{
        slug: String,
        uri: String,
        title: String,
        abstract: String
    }],

    list_role: [{
        slug: String,
        uri: String,
        name: String
    }],

    list_event: [{
        slug: String,
        type: {
            type : Schema.Types.ObjectId,
            ref: 'event_category'
        },
        uri: String,
        name: String,
        date_end: String,
        date_start: String,
        summary: String
    }],


    type: String,       

    // user who created the modle
    _user_id: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },

    user_person:{
        _person_acronym: {
            type: String,
            default: null
        },
        person_sha: {
            type: String,
            default: null
        },
        person_displayname: {
            type: String,
            default: null
        }
    }


},
{ versionKey: false }

)
// --- end predfine entity


module.exports = mongoose.model('Conferenceold', Conference)