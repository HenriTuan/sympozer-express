/**
 * Created by pierremarsot on 20/11/2017.
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// ---- predefine entity  ----
const Track = new Schema({
        name: {
            type: String,
            required: true,
        },
        idsSubEvent: [{
            type: Schema.Types.ObjectId,
            ref: 'Event',
            required: false
        }],
        idConference: {
            type: Schema.Types.ObjectId,
            ref: 'Conference',
            required: true
        },
        idRole: {
            type: Schema.Types.ObjectId,
            ref: 'Role',
            required: true
        },
        idsPerson: [{
            type: Schema.Types.ObjectId,
            ref: 'PersonConference',
            required: false
        }],
        uri: String,
        is_votable: {
            type: Boolean,
            default: false,
        }
    },
    {versionKey: false}
);

module.exports = mongoose.model('Track', Track);