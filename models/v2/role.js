const slug = require('mongoose-slug-generator');
const mongoose = require('mongoose');

mongoose.plugin(slug);
const Schema = mongoose.Schema;

// ---- predefine entity  ----
const Role = new Schema({
  idConference: {
    type: Schema.Types.ObjectId,
    ref: 'conference'
  },
  slug: String,
  uri: String,
  name: String
});
// --- end predfine entity

module.exports = mongoose.model('Role', Role);
