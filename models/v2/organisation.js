const slug = require('mongoose-slug-generator');
const mongoose = require('mongoose');

mongoose.plugin(slug);
const Schema = mongoose.Schema;

// ---- predefine entity  ----
const Organisation = new Schema({
    idConference: {
        type: Schema.Types.ObjectId,
        ref: 'conference'
    },
    uri: String,
    name: String,
    logo_path: String,
    homepage: String,
    description: String,
    idPersons: [{
        type: Schema.Types.ObjectId,
        ref: 'personConference',
        required: false
    }]
});
// --- end predfine entity


module.exports = mongoose.model('Organisation', Organisation);