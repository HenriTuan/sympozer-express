const slug = require('mongoose-slug-generator');
const mongoose = require('mongoose');

mongoose.plugin(slug);
const Schema = mongoose.Schema;


// ---- predefine entity  ----
const Conference = new Schema({

        title: String,

        display_acronym: {
            type: String,
            unique: false,
            required: true,
        },

        acronym: { // https://www.npmjs.com/package/mongoose-slug-generator
            type: String,
            slug: "display_acronym",
            unique: true
        },

        uri: { // https://www.example.org
            type: String,
            unique: false,
        },

        description: String,
        homepage: String,

        logo_path: {
            type: String,
            default: '/img/sympozer_logo.png',
            required: false
        },

        date_begin: {
            type: String,
            required: false,
            // default : Date.now
        },

        date_end: {
            type: String,
            required: false,
        },

        timezone: Number,
        language: {
            code: String,
            name: String,
        },

        location: {
            formatted_address: {
                type: String,
                default: '',
            },
            country: {
                type: String,
                default: null,
            },
            region: {
                type: String,
                default: null,
            },
            city: {
                type: String,
                default: null,
            },
            street_name: {
                type: String,
                default: null,
            },
            street_number: {
                type: Number,
                default: null,
            },
            long: Number,
            lat: Number,
            zipcode: Number,
        },
        idsSubEvent: [{
            type: Schema.Types.ObjectId,
            ref: 'Event'
        }],
        type: String,

        // user who created the modle
        _user_id: {
            type: Schema.Types.ObjectId,
            ref: 'user'
        }
    },
    {versionKey: false}
);
// --- end predfine entity


module.exports = mongoose.model('Conference', Conference);