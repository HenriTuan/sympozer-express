const slug = require('mongoose-slug-generator');
const mongoose = require('mongoose');


mongoose.plugin(slug);
const Schema = mongoose.Schema;


// ---- predefine entity  ----
const Event = new Schema({
    idConference: {
        type: Schema.Types.ObjectId,
        ref: 'Conference'
    },
    idLocation: {
        type: Schema.Types.ObjectId,
        ref: 'Location'
    },
    idsSubEvent: [{
        type: Schema.Types.ObjectId,
        ref: 'Event'
    }],
    isEventRelatedTo: [{
        type: Schema.Types.ObjectId,
        ref: 'Publication'
    }],
    idSuperEvent: {
        type: Schema.Types.ObjectId,
        ref: 'Event'
    },
    idTrack: {
        type: Schema.Types.ObjectId,
        ref: 'Track'
    },
    idEventCategory: {
        type: Schema.Types.ObjectId,
        ref: 'EventCategory'
    },
    uri: String,
    name: String,
    date_end: String,
    date_start: String,
    time_end: String,
    time_start: String,
    summary: String
});
// --- end predfine entity


module.exports = mongoose.model('Event', Event);