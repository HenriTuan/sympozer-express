const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// ---- predefine entity  ----
const EventCategory = new Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true
        },
        linkWithEvent: {
            type: Boolean,
            required: false,
        }
    },
    {versionKey: false}
);

module.exports = mongoose.model('EventCategory', EventCategory);