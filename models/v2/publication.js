const slug = require('mongoose-slug-generator');
const mongoose = require('mongoose');

mongoose.plugin(slug);
const Schema = mongoose.Schema;

// ---- predefine entity  ----
const Publication = new Schema({
    idConference: {
        type: Schema.Types.ObjectId,
        ref: 'conference'
    },
    title: String,
    abstract: String,
    uri: String,
    keywords: [String],
    idAuthors: [{
        type: Schema.Types.ObjectId,
        ref: 'PersonConference',
        required: false
    }],
    idEvent: {
        type: Schema.Types.ObjectId,
        ref: 'Event',
        required: false
    },
});
// --- end predfine entity


module.exports = mongoose.model('Publication', Publication);
