const slug = require('mongoose-slug-generator');
const mongoose = require('mongoose');
mongoose.plugin(slug);
const Schema = mongoose.Schema;


// ---- predefine entity  ----
const Location = new Schema({
    idConference: {
        type: Schema.Types.ObjectId,
        ref: 'conference'
    },
    name: String,
    nb_place: Number,
    equipements: [String],
    gps: {
        type: [Number],
        index: '2d'
    },
    description: String,
    address: String,
});
// --- end predfine entity


module.exports = mongoose.model('Location', Location);