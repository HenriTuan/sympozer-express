const slug = require('mongoose-slug-generator');
const mongoose = require('mongoose');


mongoose.plugin(slug);
const Schema = mongoose.Schema;

// ---- predefine entity  ----
const PersonConference = new Schema({
    idConference: {
        type: Schema.Types.ObjectId,
        ref: 'Conference'
    },
    idRole: {
        type: Schema.Types.ObjectId,
        ref: 'Conference',
        required: false,
    },
    idRoles: [{
        type: Schema.Types.ObjectId,
        ref: 'role',
        required: false
    }],
    displayname: String,
    firstname: String,
    lastname: String,
    ava_path: String,
    email: String,
    sha: String,
    homepage: String,
    country: String,
    mboxSha1sum: String,
    social_account: [String],
    uri: String,
});
// --- end predfine entity


module.exports = mongoose.model('PersonConference', PersonConference);