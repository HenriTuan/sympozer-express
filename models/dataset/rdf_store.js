var db = require('../../config/db')

var mongoose = require('mongoose')
var Schema = mongoose.Schema

// ---- predefine entity  ----
var rdfStore = new Schema({

	file_path:{
		type: 	  String,
	},

	file_name:{
		type: 	String,
		required: true
	},

	acronym:{
		type: String,
		unique: true,
		required: true
	},

	uri: String,

	// user who created the modle
    _user_id: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },

	source:{
		type: Schema.Types.Mixed
	}
	
},
{ versionKey: false }

)
// end predefine entity


/**********************/
module.exports = mongoose.model('rdfStore', rdfStore)