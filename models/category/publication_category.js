var db = require('../../config/db')

var mongoose = require('mongoose')
var Schema = mongoose.Schema

// ---- predefine entity  ----
var PublicationCategory = new Schema({

	name:{
		type: 	  String,
		required: true
	}
	
})
// end predefine entity


module.exports = mongoose.model('PublicationCategory', PublicationCategory)