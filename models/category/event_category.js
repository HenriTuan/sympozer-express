var db = require('../../config/db');

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// ---- predefine entity  ----
var EventCategoryOld = new Schema({
    name: {
      type: String,
      required: true,
      unique: true
    }
  },
  {versionKey: false}
);
// end predefine entity


module.exports = mongoose.model('EventCategoryOld', EventCategoryOld);